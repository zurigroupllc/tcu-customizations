<%@ Assembly Name="ZuriGroup.TCU.FormBuilder.BBIS.Web" %>
<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FormBuilderDisplay.ascx.vb" Inherits="ZuriGroup.TCU.FormBuilder.BBIS.Web.FormBuilderDisplay" %>
<%@ Import Namespace="ZuriGroup.TCU.FormBuilder.BBIS.Web" %>
<style>
  .FormBuilderWrapper table {
    border-collapse: collapse;
    border-spacing: 0px;
  }

  .HiddenRow {
    display: none;
  }

  .HiddenLabel * {
    display: none;
  }
  .FormBuilderHelpText {
    display:block;
    font-size:.8em;
  }
</style>
<asp:Label ID="lblError" runat="server" Font-Bold="true" ForeColor="red" />
<asp:HiddenField ID="hfPartTitle" runat="server" />
<asp:HiddenField ID="hfSiteId" runat="server" />
<asp:HiddenField ID="hfSiteName" runat="server" />
<asp:HiddenField ID="hfFormName" runat="server" />
<asp:HiddenField ID="hfEmailTemplateId" runat="server" />
<asp:Panel ID="FormBuilderWrapper" runat="server" CssClass="FormBuilderWrapper">
  <asp:MultiView ID="FormBuilderMultiView" runat="server" ActiveViewIndex="0">
    <asp:View ID="FormBuilderFormView" runat="server">
      <asp:ValidationSummary ID="FormBuilderValidationSummary" runat="server" ValidationGroup="FormBuilder" HeaderText="The following issues must be corrected before you can continue" CssClass="BBFormValidatorSummary FormBuilderValidatorSummary" />
      <table class="FormBuilderTable">
        <asp:ListView ID="ListFormBuilder" runat="server" ItemPlaceholderID="ItemPlaceholder" OnItemDataBound="ListFormBuilder_DataBound">
          <LayoutTemplate>
            <tr id="ItemPlaceholder" runat="server"></tr>
          </LayoutTemplate>
          <ItemTemplate>
            <asp:PlaceHolder ID="Placeholder" runat="server"></asp:PlaceHolder>
            <asp:HiddenField ID="Settings" runat="server" Value='<%# Eval("Settings")%>' Visible="false" />
            <asp:HiddenField ID="FieldType" runat="server" Value='<%# Convert.ToInt32(Eval("Type"))%>' Visible="false" />
          </ItemTemplate>
        </asp:ListView>
        <tr id="trReCaptcha" runat="server" visible="false">
          <td></td>
          <td class="BBFieldControlCell FormBuilderReCAPTCHAControlCell">
            <asp:Panel ID="panelReCaptchaV2" runat="server" Enabled="false" Visible="false" CssClass="g-recaptcha"></asp:Panel>
            <div style="clear: both;"></div>
            <asp:CustomValidator ID="validatorReCaptcha" runat="server" ErrorMessage="You must complete the reCAPTCHA challenge"
              ClientValidationFunction="getReCaptchaResponse" OnServerValidate="validatorReCaptcha_ServerValidate"
              ValidationGroup="FormBuilder" Display="None" Enabled="false"></asp:CustomValidator>
          </td>
        </tr>
        <tr id="trSubmitButton" runat="server">
          <td class="BBFormButtonCell FormBuilderButtonCell" colspan="2">
            <asp:Button ID="SubmitButton" runat="server"
              Text="Submit" ValidationGroup="FormBuilder" OnClick="SubmitButton_Click"></asp:Button>
          </td>
        </tr>
      </table>
    </asp:View>
    <asp:View ID="FormBuilderSuccessView" runat="server">
      <div>
        <asp:PlaceHolder ID="phSuccessMessage" runat="server"></asp:PlaceHolder>
      </div>
    </asp:View>
    <asp:View ID="FormBuilderErrorView" runat="server">
      <div>
        <asp:Label ID="labelFatalError" runat="server"></asp:Label>
      </div>
    </asp:View>
  </asp:MultiView>
</asp:Panel>
<script>
  $(document).ready(function () {
    BindControlEvents();
  });
  //Re-bind for callbacks
  var prm = Sys.WebForms.PageRequestManager.getInstance();

  prm.add_endRequest(function () {
    BindControlEvents();
  });
  function BindControlEvents() {

    $('span.FormBuilderShowOtherOption').each(function (index) {

      if ($(this).find('input').is(":checked")) {
        $(this).closest('tr.FormBuilderMultipleChoiceList').next('tr.FormBuilderMultipleChoiceOther').show()
      } else {
        $(this).closest('tr.FormBuilderMultipleChoiceList').next('tr.FormBuilderMultipleChoiceOther').hide()
      }
    });
  };
  function getReCaptchaResponse(source, args) {

    var response = grecaptcha.getResponse();

    if (response.length == 0)
      args.IsValid = false
    else
      args.IsValid = true
  };
  function ValidateCheckBoxList(sender, args) {
    args.IsValid = false;

    $("#" + sender.id).parent().find("table[id*=" + sender.ControlId + "_]").find(":checkbox").each(function () {
      if ($(this).attr("checked")) {
        args.IsValid = true;
        return;
      }
    });
  };
  function ValidateCheckboxListOtherOption(sender, args) {
    args.IsValid = true;
    var parent = $("#" + sender.id).closest('.FormBuilderMultipleChoiceOther');

    if (parent.is(":visible")) {
      if (parent.find("input").val() == "") {
        args.IsValid = false
      }
    }
  };
  function ValidateCheckbox(sender, args) {
    args.IsValid = false;
    var parent = $("#" + sender.id).closest('td');
    if (parent.find("input").checked) {
      args.IsValid = true;
    }
  };
  function ValidateFileUploadSize(sender, args) {
    args.IsValid = true;
    var maxsize = $("#" + sender.id).attr('maxsize')
    var fileUploadId = $("#" + sender.id).closest('td').find("input").attr('id');
    var input = document.getElementById(fileUploadId);
    if (input) {
      if (input.files) {
        if (input.files[0]) {
          var file = input.files[0]
          if (file.size > maxsize) {
            args.IsValid = false;
          }
        }
      }
    }
  }
  function ShowMultipleChoiceOtherOption(id, show, origin) {
    if (show) {
      $('#' + id).closest('tr.FormBuilderMultipleChoiceList').next('tr').show()
    } else {
      $('#' + id).closest('tr.FormBuilderMultipleChoiceList').next('tr').hide()
    }
  }
</script>
