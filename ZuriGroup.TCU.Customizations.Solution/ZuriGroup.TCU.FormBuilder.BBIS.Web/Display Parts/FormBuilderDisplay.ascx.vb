Imports BBNCExtensions
Imports System.Net
Imports System.IO
Imports System.Web.Script.Serialization
Imports Blackbaud.Web.Content.Core
Imports Blackbaud.Web.Content.Common

Partial Public Class FormBuilderDisplay
  Inherits BBNCExtensions.Parts.CustomPartDisplayBase

  Private mContent As FormBuilderProperties
  Private language As Language
  Private constituent As Constituent

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim success = Request("s")
    If success = "true" Then
      RenderSuccessMessage()
    Else
      If Not Page.IsPostBack Then
        FormBuilderMultiView.ActiveViewIndex = 0

        SetContentPartDetails()
        CheckForFatalErrors()


      End If
      PopulateUserDetails()
      SetLanguageProperties()

      If MyContent.Fields IsNot Nothing AndAlso MyContent.Fields.Count > 0 Then
        Me.ListFormBuilder.DataSource = MyContent.Fields.OrderBy(Function(x) x.Position).ToList()
        Me.ListFormBuilder.DataBind()
      End If

      SetReCaptchaProperties()

      Page.Header.Controls.Add(New LiteralControl("<script src='https://www.google.com/recaptcha/api.js'></script>"))
    End If
  End Sub

  Private Sub LoadSelectedFields()
    If MyContent.Fields.Count > 0 Then
      Me.ListFormBuilder.DataSource = MyContent.Fields.OrderBy(Function(x) x.Position).ToList()
      Me.ListFormBuilder.DataBind()
    End If

  End Sub
  Private Sub SetContentPartDetails()
    Dim view = New ViewForms.FormBuilderEmailProperties.BBISFormBuilderContentPartDetailsViewDataFormData
    Try
      view = ViewForms.FormBuilderEmailProperties.BBISFormBuilderContentPartDetailsViewDataForm.LoadData(Me.API.AppFxWebServiceProvider, Me.Content.ContentID)
      hfSiteId.Value = view.SITEID
      hfSiteName.Value = view.SITENAME
      hfPartTitle.Value = view.TITLE
      hfEmailTemplateId.Value = view.EMAILTEMPLATEID
      hfFormName.Value = MyContent.FormName
    Catch ex As Exception
      Throw ex
    End Try
  End Sub
  Private Sub CheckForFatalErrors()
    Dim fatalError As Boolean = False

    If MyContent.Fields Is Nothing Then
      fatalError = True
    Else
      If MyContent.Fields.Count = 0 Then
        fatalError = True
      End If
    End If

    If MyContent.EmailRecipients IsNot Nothing Then
      If MyContent.EmailRecipients.Count = 0 Then
        fatalError = True
      End If
    End If

    If MyContent.UseReCaptcha AndAlso MyContent.ReCaptchaV2SiteKey = "" Then
      fatalError = True
    End If
    If MyContent.DisplayAlternativeSuccessPage AndAlso MyContent.ConfirmationPageId = Nothing Then
      fatalError = True
    End If
    Dim emailTemplate = New EmailTemplate(Integer.Parse(hfEmailTemplateId.Value))
    If emailTemplate Is Nothing Then
      fatalError = True
    Else
      If emailTemplate.FromAddress = String.Empty Or emailTemplate.FromDisplayName = String.Empty Then
        fatalError = True
      End If
    End If
    If fatalError Then
      FormBuilderMultiView.ActiveViewIndex = 2
      labelFatalError.Text += "This form is not configured correctly. Please check that all required setup fields are filled in."
    End If
  End Sub
  Public Overrides Sub RegisterLanguageFields()
    ''''Field labels''''
    ''Address
    Me.RegisterLanguageField("47C34287-C756-48FA-8A2B-138D0F1B50C4", Nothing, "Address field: Address", "Address:", "Field labels", False)
    Me.RegisterLanguageField("39F57C13-AE5D-4966-87B9-4C28F87A1332", Nothing, "Address field: Country", "Country:", "Field labels", False)
    Me.RegisterLanguageField("A8677AD5-173B-4C99-8BBC-CC94DB84F4A7", Nothing, "Address field: City", "City:", "Field labels", False)
    Me.RegisterLanguageField("E04F5731-12CE-408A-98DC-49A2D22D9266", Nothing, "Address field: State", "State:", "Field labels", False)
    Me.RegisterLanguageField("8B4B508F-B42E-46A1-93BB-591EDD400329", Nothing, "Address field: ZIP code", "ZIP code:", "Field labels", False)
    Me.RegisterLanguageField("9149F8C6-AAD1-4904-BC29-4A5C7DF88F5E", Nothing, "Address field: Address type", "Address type:", "Field labels", False)
    ''Email
    Me.RegisterLanguageField("9D2385E5-E3FD-4090-BED1-D9151BBAE2EC", Nothing, "Email field: Email address", "Email address:", "Field labels", False)
    Me.RegisterLanguageField("78B71CCC-5DDA-4617-A6BA-679EBF0A6B9A", Nothing, "Email field: Verify email address", "Verify email address:", "Field labels", False)
    Me.RegisterLanguageField("0A8408AC-CD90-4F92-A4E6-3141DDAF99A7", Nothing, "Email field: Email type", "Email type:", "Field labels", False)
    ''Phone
    Me.RegisterLanguageField("2A8EF767-53AC-422E-9DC1-CFBCDF9519C6", Nothing, "Phone field: Phone number", "Phone number:", "Field labels", False)
    Me.RegisterLanguageField("C8882604-73C5-4B96-A4CE-9140DC911852", Nothing, "Phone field: Phone type", "Phone type:", "Field labels", False)
    ''Other
    Me.RegisterLanguageField("E8CA1853-E0D5-4587-BD7B-A187F61D94B4", Nothing, "Submit button", "Submit", "Field labels", False)
    ''''Validation messages''''
    Me.RegisterLanguageField("7596FC9C-0C30-46AE-B38C-EF05C3E608BF", Nothing, "Validation summary", "The following issues must be corrected before you can continue:", "Validation messages", False)

    ''''Validation messages -- Required''''
    ''Address
    Me.RegisterLanguageField("5CE5D8A4-3D7A-4904-A98F-928570007B4F", Nothing, "Address field: Address required (dynamic)", "[Address] is required.", "Validation messages", False)
    Me.RegisterLanguageField("C909D9A0-F0C6-4600-86FE-D3F8BC7A070E", Nothing, "Address field: Country required (dynamic)", "[Country] is required.", "Validation messages", False)
    Me.RegisterLanguageField("22084467-E26D-4E2B-BCB1-1E2D4846F9C8", Nothing, "Address field: City required (dynamic)", "[City] is required.", "Validation messages", False)
    Me.RegisterLanguageField("7F35A655-F06F-48B8-80B5-F2E3D6E25B27", Nothing, "Address field: State required (dynamic)", "[State] is required.", "Validation messages", False)
    Me.RegisterLanguageField("3536AF04-E48A-4F11-8B07-3F4C2F11DE9A", Nothing, "Address field: ZIP code required (dynamic)", "[ZIP code] is required.", "Validation messages", False)
    Me.RegisterLanguageField("4CE19EC6-0B0F-4429-AF9C-18175397B983", Nothing, "Address field: Address type required (dynamic)", "[Address type] is required.", "Validation messages", False)
    ''Email
    Me.RegisterLanguageField("22ACE170-8806-4F6E-B674-037D065318D5", Nothing, "Email field: Email address required (dynamic)", "[Email address] is required.", "Validation messages", False)
    Me.RegisterLanguageField("854171A3-AA60-4444-B893-A62217180DC7", Nothing, "Email field: Email type required (dynamic)", "[Email type] is required.", "Validation messages", False)
    ''Phone
    Me.RegisterLanguageField("AF94E34D-F58E-44CA-929B-52F66314CA5E", Nothing, "Phone field: Phone number required (dynamic)", "[Phone number] is required.", "Validation messages", False)
    Me.RegisterLanguageField("8762D131-3582-4A80-BC22-0DEF5FE1FC60", Nothing, "Phone field: Phone type required (dynamic)", "[Phone type] is required.", "Validation messages", False)
    ''Other fields
    Me.RegisterLanguageField("E1BBCB08-5E9C-49D9-A233-41150416A3C2", Nothing, "Text/number/date/drop-down field: Field label required (dynamic)", "[Field label] is required.", "Validation messages", False)
    Me.RegisterLanguageField("28F00608-61C8-449C-9CE9-684118A8EF37", Nothing, "Checkbox field: Checkbox required (dynamic)", "[Field label] is required.", "Validation messages", False)
    Me.RegisterLanguageField("5DA166DE-678A-4292-AA6F-428962BAAAFC", Nothing, "Multiple choice field: Group label required (dynamic)", "[Group label] is required.", "Validation messages", False)
    Me.RegisterLanguageField("7AAA4341-0CA1-40EC-B6D9-4E5CC11064B6", Nothing, "Multiple choice field: Other text required (dynamic)", "[Group label] '[Other label]' is required.", "Validation messages", False)
    Me.RegisterLanguageField("D98EB598-4FF2-4C73-95CC-F76380E497CC", Nothing, "File upload: Field label required (dynamic)", "[Field label] is required. Please select a file.", "Validation messages", False)

    ''''Validation messages -- Invalid''''
    Me.RegisterLanguageField("85A19B45-BF7D-4CEF-95D6-F67611DE26DA", Nothing, "Address field: ZIP code invalid (dynamic)", "[ZIP code] is invalid. Please use only numbers and hyphens.", "Validation messages", False)
    Me.RegisterLanguageField("D4911017-C95C-4CFB-A3A7-431BE09CD143", Nothing, "Email field: Email address invalid (dynamic)", "[Email address] is invalid.", "Validation messages", False)
    Me.RegisterLanguageField("6317671D-23C9-4EBD-B94A-DB2BAFC8C4AA", Nothing, "Email field: Email addresses do not match (dynamic)", "[Email address] and [Verify email address] do not match.", "Validation messages", False)
    Me.RegisterLanguageField("21192992-D2C2-4348-9773-FD009044221E", Nothing, "Phone field: Phone number invalid (dynamic)", "[Phone number] is invalid. Please use only permitted characters.", "Validation messages", False)
    Me.RegisterLanguageField("D59A2DAB-FC55-4599-8005-AEBC11D1F431", Nothing, "reCAPTCHA required", "The reCAPTCHA challenge must be completed.", "Validation messages", False)
    ''Other fields
    Me.RegisterLanguageField("EF4C7F01-5CDC-4A7D-A143-ED58AA3E3356", Nothing, "Text/number/date field: Invalid characters (dynamic)", "[Field label] is invalid. Please use only permitted characters.", "Validation messages", False)
    Me.RegisterLanguageField("14C8ECC7-1D30-4826-BABD-66331489B3B1", Nothing, "Number field: Number outside of acceptable range (dynamic)", "[Field label] is outside of the acceptable range ([Min] - [Max])", "Validation messages", False)
    Me.RegisterLanguageField("D0BC4CE9-45A0-4F85-8706-B20FE7F47EF4", Nothing, "File upload: File too large (dynamic)", "[Field label] is too large. File cannot exceed [Max file size] megabytes.", "Validation messages", False)
    Me.RegisterLanguageField("81533AD1-0A50-4A36-BF03-1C9F98F2A53B", Nothing, "File upload: File type invalid (dynamic)", "[Field label] file type is invalid. Please select a permitted file type.", "Validation messages", False)
    Me.RegisterLanguageField("1979D73A-053D-45C2-B4F2-83A691B6DA04", Nothing, "Date field: Month is invalid (dynamic)", "[Field label] month is invalid. Please enter a value between 1 and 12.", "Validation messages", False)
    Me.RegisterLanguageField("C4843124-AB0D-4C99-AE2C-42BC52EF3625", Nothing, "Date field: Day is invalid (dynamic)", "[Field label] day is invalid. Please enter a value between 1 and 31.", "Validation messages", False)
    Me.RegisterLanguageField("9ADB50C2-5F1A-4EBF-B5D8-A91B65C41B49", Nothing, "Date field: Year is invalid (dynamic)", "[Field label] year is invalid. Please enter a valid year.", "Validation messages", False)
    Me.RegisterLanguageField("8234D95D-0ADB-419F-9A23-0744E6415755", Nothing, "Date field: Date is invalid (dynamic)", "[Field label] is invalid. Please enter a valid date in the format MM/DD/YYYY.", "Validation messages", False)
    Me.RegisterLanguageField("10F66C12-33B2-45AA-81B5-A38A58C67C2D", Nothing, "Date field: Time is invalid (dynamic)", "[Field label] is invalid. Please enter a valid time in the format [Time format].", "Validation messages", False)

    MyBase.RegisterLanguageFields()
  End Sub
  Private Sub SetLanguageProperties()
    FormBuilderValidationSummary.HeaderText = Me.GetLanguageString("{7596FC9C-0C30-46AE-B38C-EF05C3E608BF}")
    SubmitButton.Text = Me.GetLanguageString("{E8CA1853-E0D5-4587-BD7B-A187F61D94B4}")
    validatorReCaptcha.ErrorMessage = Me.GetLanguageString("{D59A2DAB-FC55-4599-8005-AEBC11D1F431}")
    SetLanguageVariable()
  End Sub
  Private Sub SetLanguageVariable()
    language = New Language()
    language.AddressAddressLabel = Me.GetLanguageString("{47C34287-C756-48FA-8A2B-138D0F1B50C4}")
    language.AddressCountryLabel = Me.GetLanguageString("{39F57C13-AE5D-4966-87B9-4C28F87A1332}")
    language.AddressCityLabel = Me.GetLanguageString("{A8677AD5-173B-4C99-8BBC-CC94DB84F4A7}")
    language.AddressStateLabel = Me.GetLanguageString("{E04F5731-12CE-408A-98DC-49A2D22D9266}")
    language.AddressZIPLabel = Me.GetLanguageString("{8B4B508F-B42E-46A1-93BB-591EDD400329}")
    language.AddressTypeLabel = Me.GetLanguageString("{9149F8C6-AAD1-4904-BC29-4A5C7DF88F5E}")

    language.EmailAddressLabel = Me.GetLanguageString("{9D2385E5-E3FD-4090-BED1-D9151BBAE2EC}")
    language.EmailVerifyLabel = Me.GetLanguageString("{78B71CCC-5DDA-4617-A6BA-679EBF0A6B9A}")
    language.EmailTypeLabel = Me.GetLanguageString("{0A8408AC-CD90-4F92-A4E6-3141DDAF99A7}")

    language.PhoneNumberLabel = Me.GetLanguageString("{2A8EF767-53AC-422E-9DC1-CFBCDF9519C6}")
    language.PhoneTypeLabel = Me.GetLanguageString("{C8882604-73C5-4B96-A4CE-9140DC911852}")

    language.AddressAddressRequired = Me.GetLanguageString("{5CE5D8A4-3D7A-4904-A98F-928570007B4F}")
    language.AddressCountryRequired = Me.GetLanguageString("{C909D9A0-F0C6-4600-86FE-D3F8BC7A070E}")
    language.AddressCityRequired = Me.GetLanguageString("{22084467-E26D-4E2B-BCB1-1E2D4846F9C8}")
    language.AddressStateRequired = Me.GetLanguageString("{7F35A655-F06F-48B8-80B5-F2E3D6E25B27}")
    language.AddressZIPRequired = Me.GetLanguageString("{3536AF04-E48A-4F11-8B07-3F4C2F11DE9A}")
    language.AddressTypeRequired = Me.GetLanguageString("{4CE19EC6-0B0F-4429-AF9C-18175397B983}")

    language.EmailAddressRequired = Me.GetLanguageString("{22ACE170-8806-4F6E-B674-037D065318D5}")
    language.EmailTypeRequired = Me.GetLanguageString("{854171A3-AA60-4444-B893-A62217180DC7}")

    language.PhoneNumberRequired = Me.GetLanguageString("{AF94E34D-F58E-44CA-929B-52F66314CA5E}")
    language.PhoneTypeRequired = Me.GetLanguageString("{8762D131-3582-4A80-BC22-0DEF5FE1FC60}")

    language.TextNumberDateDropdownRequired = Me.GetLanguageString("{E1BBCB08-5E9C-49D9-A233-41150416A3C2}")
    language.CheckboxRequired = Me.GetLanguageString("{28F00608-61C8-449C-9CE9-684118A8EF37}")
    language.MultipleChoiceRequired = Me.GetLanguageString("{5DA166DE-678A-4292-AA6F-428962BAAAFC}")
    language.MultipleChoiceOtherRequired = Me.GetLanguageString("{7AAA4341-0CA1-40EC-B6D9-4E5CC11064B6}")
    language.FileUploadRequired = Me.GetLanguageString("{D98EB598-4FF2-4C73-95CC-F76380E497CC}")

    language.AddressZIPInvalid = Me.GetLanguageString("{85A19B45-BF7D-4CEF-95D6-F67611DE26DA}")
    language.EmailAddressInvalid = Me.GetLanguageString("{D4911017-C95C-4CFB-A3A7-431BE09CD143}")
    language.EmailAddressDoNotMatch = Me.GetLanguageString("{6317671D-23C9-4EBD-B94A-DB2BAFC8C4AA}")
    language.PhoneNumberInvalid = Me.GetLanguageString("{21192992-D2C2-4348-9773-FD009044221E}")

    language.TextNumberDateInvalid = Me.GetLanguageString("{EF4C7F01-5CDC-4A7D-A143-ED58AA3E3356}")
    language.NumberOutsideRange = Me.GetLanguageString("{14C8ECC7-1D30-4826-BABD-66331489B3B1}")
    language.FileUploadTooLarge = Me.GetLanguageString("{D0BC4CE9-45A0-4F85-8706-B20FE7F47EF4}")
    language.FileUploadInvalid = Me.GetLanguageString("{81533AD1-0A50-4A36-BF03-1C9F98F2A53B}")
    language.DateMonthInvalid = Me.GetLanguageString("{1979D73A-053D-45C2-B4F2-83A691B6DA04}")
    language.DateDayInvalid = Me.GetLanguageString("{C4843124-AB0D-4C99-AE2C-42BC52EF3625}")
    language.DateYearInvalid = Me.GetLanguageString("{9ADB50C2-5F1A-4EBF-B5D8-A91B65C41B49}")
    language.DateDateInvalid = Me.GetLanguageString("{8234D95D-0ADB-419F-9A23-0744E6415755}")
    language.DateTimeInvalid = Me.GetLanguageString("{10F66C12-33B2-45AA-81B5-A38A58C67C2D}")
  End Sub

  Private ReadOnly Property MyContent() As FormBuilderProperties
    Get
      If mContent Is Nothing Then
        mContent = Me.Content.GetContent(GetType(FormBuilderProperties))
        If mContent Is Nothing Then
          mContent = New FormBuilderProperties
        End If
      End If
      Return mContent
    End Get
  End Property

  Protected Sub ListFormBuilder_DataBound(sender As Object, e As ListViewItemEventArgs)
    Dim item = DirectCast(e.Item, ListViewDataItem)
    Dim field = DirectCast(item.DataItem, Field)

    Dim ph = DirectCast(item.FindControl("Placeholder"), PlaceHolder)
    ph.Controls.Clear()

    ph.Controls.Add(field.Display(constituent, language, Me.API.AppFxWebServiceProvider))

  End Sub

#Region "reCAPTCHA"
  Private Sub SetReCaptchaProperties()
    If MyContent.UseReCaptcha Then
      Dim user = BBNCExtensions.API.NetCommunity.Current.Users.CurrentUser
      If MyContent.RequireRegisteredReCaptcha Or user.BackOfficeGuid = Guid.Empty Then
        trReCaptcha.Visible = True
        panelReCaptchaV2.Visible = True
        panelReCaptchaV2.Enabled = True
        validatorReCaptcha.Enabled = True

        If MyContent.ReCaptchaV2SiteKey <> "" Then
          panelReCaptchaV2.Attributes.Add("data-sitekey", MyContent.ReCaptchaV2SiteKey)
        Else
          lblError.Text += vbCrLf + "reCAPTCHA has been enabled but no site key has been provided. Please update the form settings."
        End If
      End If
    End If
  End Sub
  Protected Sub validatorReCaptcha_ServerValidate(source As Object, args As ServerValidateEventArgs)
    args.IsValid = reCaptchaValidate()
  End Sub
  Public Function reCaptchaValidate() As Boolean
    Dim response As String = Request("g-recaptcha-response")
    Dim valid As Boolean = False
    Dim req As HttpWebRequest = DirectCast(WebRequest.Create(Convert.ToString("https://www.google.com/recaptcha/api/siteverify?secret=" + MyContent.ReCaptchaV2SecretKey + "&response=") & response), HttpWebRequest)

    Try
      Using wResponse As WebResponse = req.GetResponse()

        Using readStream As New StreamReader(wResponse.GetResponseStream())

          Dim jsonResponse As String = readStream.ReadToEnd()
          Dim js As New JavaScriptSerializer()
          Dim data As ReCaptchaSuccess = js.Deserialize(Of ReCaptchaSuccess)(jsonResponse)

          valid = Convert.ToBoolean(data.success)
          Return valid
        End Using
      End Using
    Catch ex As Exception
      Return False
    End Try
  End Function
#End Region
#Region "User details"

  Private Sub PopulateUserDetails()
    constituent = New Constituent()

    Dim user = BBNCExtensions.API.NetCommunity.Current.Users.CurrentUser

    If user.BackOfficeGuid <> Guid.Empty Then
      PopulateLinkedUserDetails(user.BackOfficeGuid)
    ElseIf user.UserID <> 0 Then
      constituent.FirstName = user.FirstName
      constituent.LastName = user.LastName
    End If
  End Sub
  Private Sub PopulateLinkedUserDetails(UserId As Guid)
    Dim view = New ViewForms.Constituent.BBISFormBuilderConstituentDetailsViewDataFormData
    Try
      view = ViewForms.Constituent.BBISFormBuilderConstituentDetailsViewDataForm.LoadData(Me.API.AppFxWebServiceProvider, UserId.ToString())
      constituent.FirstName = view.FIRSTNAME
      constituent.MiddleInitial = view.MIDDLEINITIAL
      constituent.MiddleName = view.MIDDLENAME
      constituent.LastName = view.LASTNAME
      constituent.MaidenName = view.MAIDENNAME
      constituent.Nickname = view.NICKNAME
      constituent.ConstituentId = view.CONSTITUENTID

      constituent.PrimaryAddressBlock = view.PRIMARYADDRESSBLOCK
      constituent.PrimaryAddressCity = view.PRIMARYADDRESSCITY
      'constituent.PrimaryAddressCountry = view.PRIMARYADDRESSCOUNTRY
      constituent.PrimaryAddressCountryId = view.PRIMARYADDRESSCOUNTRYID.ToString
      'constituent.PrimaryAddressState = view.PRIMARYADDRESSSTATE
      constituent.PrimaryAddressStateId = view.PRIMARYADDRESSSTATEID.ToString
      constituent.PrimaryAddressPostCode = view.PRIMARYADDRESSPOSTCODE

      constituent.PrimaryPhoneNumber = view.PRIMARYPHONENUMBER

      constituent.PrimaryEmailAddress = view.PRIMARYEMAILADDRESS

      constituent.ClassYear = view.CLASSYEAR
    Catch ex As Exception
    End Try
  End Sub

#End Region

  Protected Sub SubmitButton_Click(sender As Object, e As EventArgs)
    Try
      If Page.IsValid Then
        Dim submission As XElement = New XElement("Submission")
        If SaveFormBuilderSubmission(submission) AndAlso SendEmailNotification(FillMergeData(submission), submission) Then
          If MyContent.DisplayAlternativeSuccessPage Then
            Response.Redirect(URLBuilder.BuildSitePageLink(MyContent.ConfirmationPageId))
          Else
            Response.Redirect(Request.Path + "?s=true")
          End If
        Else
          lblError.Text = "An error occured while trying to submit the form. Please try again."
        End If
      End If
    Catch ex As Exception
      lblError.Text = ex.Message
    End Try

  End Sub

  Private Function SaveFormBuilderSubmission(ByRef submission As XElement) As Boolean
    Dim add = New AddForms.FormBuilderSubmission.BBISFormBuilderSubmissionAddDataFormData
    Try
      Dim user = BBNCExtensions.API.NetCommunity.Current.Users.CurrentUser

      If user.BackOfficeGuid <> Guid.Empty Then
        add.CONSTITUENTID = user.BackOfficeGuid
      End If

      If user.UserID <> 0 Then
        add.CLIENTUSERSID = user.UserID
      End If

      add.BBISPAGEID = BBNCExtensions.API.NetCommunity.Current.Pages.CurrentPage.Id
      add.SITECONTENTID = Me.Content.ContentID
      add.FORMNAME = hfFormName.Value

      Dim uri = New Uri(Request.Url.AbsoluteUri)
      add.URL = uri.GetLeftPart(UriPartial.Path)

      Dim recipients = New XElement("Recipients")
      For Each item In MyContent.EmailRecipients
        Dim recipient = New XElement("Recipient")
        recipient.Add(New XElement("Name") With {.Value = item.DisplayName})
        recipient.Add(New XElement("EmailAddress") With {.Value = item.EmailAddress})

        recipients.Add(recipient)
      Next
      submission.Add(recipients)

      Dim fields = New XElement("Fields")
      For Each item In ListFormBuilder.Items
        Dim type As eFieldType.FieldType = DirectCast(item.FindControl("FieldType"), HiddenField).Value
        Dim typeLabel As String = eFieldType.FieldTypeLabel(type)
        Dim settings As XElement = XElement.Parse(DirectCast(item.FindControl("Settings"), HiddenField).Value)

        Dim f As XElement = New XElement("Field")
        f.Add(New XAttribute("FieldType", type))
        f.Add(New XAttribute("FieldTypeLabel", typeLabel))
        f.Add(New XAttribute("FieldTypeId", Integer.Parse(type)))

        Select Case type
          Case eFieldType.FieldType.SingleLineText
            f.Add(New XElement("Label") With {.Value = settings.Element("Label").Value})
            f.Add(New XElement("Value") With {.Value = DirectCast(item.FindControl("FieldText"), TextBox).Text})
          Case eFieldType.FieldType.MultiLineText
            f.Add(New XElement("Label") With {.Value = settings.Element("Label").Value})

            Dim lines = DirectCast(item.FindControl("FieldText"), TextBox).Text.Split(ControlChars.Lf)
            Dim text As String = ""

            For i = 0 To lines.Length - 1 Step 1
              text += lines(i) + IIf(i = lines.Length - 1, "", vbCrLf)
            Next

            f.Add(New XElement("Value") With {.Value = text})
          Case eFieldType.FieldType.NumberField
            f.Add(New XElement("Label") With {.Value = settings.Element("Label").Value})
            Dim text As String = DirectCast(item.FindControl("FieldText"), TextBox).Text
            If settings.Element("BlankValue").Value.Length > 0 And text.Length = 0 Then
              f.Add(New XElement("Value") With {.Value = settings.Element("BlankValue").Value})
            Else
              f.Add(New XElement("Value") With {.Value = text})
            End If
          Case eFieldType.FieldType.Checkbox
            Dim text As String = ""
            text = IIf(DirectCast(item.FindControl("FieldCheck"), CheckBox).Checked, "Yes", "No")

            f.Add(New XElement("Caption") With {.Value = settings.Element("Caption").Value})

            If settings.Element("Label").Value.Length > 0 Then
              f.Add(New XElement("Label") With {.Value = settings.Element("Label").Value})
              text += " (" + settings.Element("Caption").Value + ")"
            Else
              f.Add(New XElement("Label") With {.Value = settings.Element("Caption").Value})
            End If

            f.Add(New XElement("Value") With {.Value = text})
          Case eFieldType.FieldType.MultipleChoice
            f.Add(New XElement("Label") With {.Value = settings.Element("Label").Value})
            Dim mcCheck As CheckBoxList
            Dim mcRadio As RadioButtonList
            Dim values = New XElement("Values")

            If settings.Element("RenderStyle").Value = 0 Then
              mcCheck = item.FindControl("CheckboxList")

              For Each check As ListItem In mcCheck.Items
                If check.Selected Then
                  If settings.Element("ShowOtherOption").Value Then
                    If Not (check.Text = settings.Element("OtherLabel").Value) Then
                      values.Add(New XElement("Value") With {.Value = check.Text})
                    End If
                  Else
                    values.Add(New XElement("Value") With {.Value = check.Text})
                  End If
                End If
              Next
            Else
              mcRadio = item.FindControl("RadioList")

              For Each radio As ListItem In mcRadio.Items
                If radio.Selected Then
                  If settings.Element("ShowOtherOption").Value Then
                    If Not (radio.Text = settings.Element("OtherLabel").Value) Then
                      values.Add(New XElement("Value") With {.Value = radio.Text})
                    End If
                  Else
                    values.Add(New XElement("Value") With {.Value = radio.Text})
                  End If
                End If
              Next
            End If

            If settings.Element("ShowOtherOption").Value Then
              Dim other = New XElement("Value")
              Dim otherTextbox As TextBox = item.FindControl("FieldOtherText")

              other.Value = otherTextbox.Text
              values.Add(other)
            End If
            f.Add(values)
            Dim text As String = ""
            Dim subvalues = f.Element("Values").Elements("Value")
            For x = 0 To subvalues.Count - 1 Step 1
              Dim value As String = subvalues(x).Value
              If value.Length > 0 Then
                text += value
                If x < subvalues.Count - 1 AndAlso subvalues(x + 1).Value.Length > 0 Then
                  text += ", "
                End If
              End If
            Next
            f.Add(New XElement("Value") With {.Value = text})
          Case eFieldType.FieldType.DropDownMenu
            f.Add(New XElement("Label") With {.Value = settings.Element("Label").Value})
            f.Add(New XElement("Value") With {.Value = DirectCast(item.FindControl("FieldDropdownList"), DropDownList).SelectedItem.Text})
          Case eFieldType.FieldType.EmailField
            f.Add(New XElement("Label") With {.Value = Me.GetLanguageString("{9D2385E5-E3FD-4090-BED1-D9151BBAE2EC}")})
            Dim email As String = DirectCast(item.FindControl("FieldText"), TextBox).Text
            Dim emailType As String = DirectCast(item.FindControl("FieldEmailType"), DropDownList).SelectedItem.Text
            f.Add(New XElement("EmailAddress") With {.Value = email})
            f.Add(New XElement("Type") With {.Value = type})
            f.Add(New XElement("Value") With {.Value = email + IIf(email.Length > 0 And emailType.Length > 0, " (" + emailType + ")", "")})
          Case eFieldType.FieldType.AddressField
            f.Add(New XElement("Label") With {.Value = Me.GetLanguageString("{47C34287-C756-48FA-8A2B-138D0F1B50C4}")})
            f.Add(New XElement("AddressBlock") With {.Value = DirectCast(item.FindControl("FieldTextAddress"), TextBox).Text})
            f.Add(New XElement("Country") With {.Value = DirectCast(item.FindControl("FieldListCountry"), DropDownList).SelectedItem.Text})
            f.Add(New XElement("City") With {.Value = DirectCast(item.FindControl("FieldTextCity"), TextBox).Text})
            f.Add(New XElement("State") With {.Value = DirectCast(item.FindControl("FieldListState"), DropDownList).SelectedItem.Text})
            f.Add(New XElement("ZIPCode") With {.Value = DirectCast(item.FindControl("FieldTextZIPCode"), TextBox).Text})
            f.Add(New XElement("Type") With {.Value = DirectCast(item.FindControl("FieldAddressType"), DropDownList).SelectedItem.Text})

            Dim text As String = ""
            text += IIf(f.Element("AddressBlock").Value.Length > 0, f.Element("AddressBlock").Value + vbCrLf, "")
            text += IIf(f.Element("City").Value.Length > 0, f.Element("City").Value + ", ", "")
            text += IIf(f.Element("State").Value.Length > 0, f.Element("State").Value + " ", "")
            text += IIf(f.Element("ZIPCode").Value.Length > 0, f.Element("ZIPCode").Value + vbCrLf, "")
            text += IIf(text.Length > 0, f.Element("Country").Value, "")
            text += IIf(f.Element("Type").Value.Length > 0 And text.Length > 0, vbCrLf + "Type: " + f.Element("Type").Value, "")

            f.Add(New XElement("Value") With {.Value = text})
          Case eFieldType.FieldType.PhoneField
            f.Add(New XElement("Label") With {.Value = Me.GetLanguageString("{2A8EF767-53AC-422E-9DC1-CFBCDF9519C6}")})
            Dim phone As String = DirectCast(item.FindControl("FieldText"), TextBox).Text
            Dim phoneType As String = DirectCast(item.FindControl("FieldPhoneType"), DropDownList).SelectedItem.Text
            f.Add(New XElement("PhoneNumber") With {.Value = phone})
            f.Add(New XElement("Type") With {.Value = phoneType})
            f.Add(New XElement("Value") With {.Value = phone + IIf(phone.Length > 0 And phoneType.Length > 0, " (" + phoneType + ")", "")})
          Case eFieldType.FieldType.DateTimeField
            f.Add(New XElement("Label") With {.Value = settings.Element("Label").Value})
            Dim value As String
            If settings.Element("DateType").Value = 0 Then
              If settings.Element("SplitMonthDayYear") Then
                value = DirectCast(item.FindControl("FieldTextMonth"), TextBox).Text + "/" + _
                    DirectCast(item.FindControl("FieldTextDay"), TextBox).Text + "/" + _
                    DirectCast(item.FindControl("FieldTextYear"), TextBox).Text
              Else
                value = DirectCast(item.FindControl("FieldTextDate"), TextBox).Text
              End If
            Else
              value = DirectCast(item.FindControl("FieldTextTime"), TextBox).Text
            End If
            f.Add(New XElement("Value") With {.Value = IIf(value = "//", "", value)})

          Case eFieldType.FieldType.FileUpload
            f.Add(New XElement("Label") With {.Value = settings.Element("Label").Value})
            Dim upload As FileUpload = item.FindControl("FileUpload")
            Dim url As String = ""
            Dim uploadId As Integer
            Dim filename As String = ""
            If upload.FileBytes.Length > 0 Then
              uploadId = UploadFileFromFileUpload(upload, settings.Element("FolderId").Value, filename)
              If uploadId = 0 Then
                Throw New ArgumentException("File upload error")
              Else
                url = URLBuilder.BuildDocumentLink(uploadId, True, "", Nothing)
              End If
            End If

            f.Add(New XElement("Value") With {.Value = uploadId})
            f.Add(New XElement("FileName") With {.Value = filename})
            f.Add(New XElement("Url") With {.Value = url})
          Case Else

        End Select

        If f.Elements.Count > 0 Then
          fields.Add(f)
        End If

      Next
      submission.Add(fields)
      Dim doc As XDocument = New XDocument()
      doc.Add(submission)
      add.SUBMITTEDDATA = doc.ToString


      add.Save(Me.API.AppFxWebServiceProvider)

      Return True
    Catch ex As Exception
      Return False
    End Try
    Return True
  End Function

  Private Function UploadFileFromFileUpload(ByRef upload As FileUpload, folderId As Integer, ByRef filename As String) As Integer
    Try
      Dim username As String = ""
      Dim userId As Integer
      If Me.API.Users.CurrentUser.UserID > 0 Then
        username = Me.API.Users.CurrentUser.Username
        userId = Me.API.Users.CurrentUser.UserID
      Else
        username = "Anonymous"
        userId = 0
      End If

      Dim ticks = (DateTime.UtcNow.Ticks - New DateTime(1970, 1, 1).Ticks).ToString
      Dim hexTicks = Long.Parse(ticks).ToString("X")

      Dim folderPath As String = ""
      folderPath = userId.ToString + "/" + hexTicks + "/" + StripInvalidURLCharacters(upload.FileName)
      Dim url As String = GetBaseURL()
      url += folderPath.ToLower

      Dim uploader = New Data.ShelbyDocUploadFile()
      uploader.Name = upload.FileName + " (" + username + ")"
      filename = uploader.Name
      uploader.URL = folderPath.ToLower
      uploader.Contents = upload.FileBytes
      uploader.Size = upload.FileBytes.Length
      uploader.FolderId = folderId

      uploader.Approved = True
      uploader.UploadDate = Date.Now
      uploader.Description = "File uploaded by " + username + " on " + Date.Now.ToString("MM/dd/yyyy HH:mm")
      uploader.OwnerID = userId
      uploader.ClientSitesId = hfSiteId.Value
      uploader.Save()

      'Return url
      Return uploader.ID
    Catch ex As Exception
      Return 0
    End Try
  End Function

  Private Function GetBaseURL() As String
    Dim text As String = String.Empty
    text = PortalSettings.Current().GetRequestAppPathURL()

    text += "file/"

    Return text.ToLower()
  End Function

  Private Function StripInvalidURLCharacters(value As String) As String
    Dim str As String = RemoveAccentedCharacters(value)
    str = Regex.Replace(str, "'", "")
    str = Regex.Replace(str, "[^A-Za-z0-9_.-]", "_")
    Return str
  End Function

  Private Function RemoveAccentedCharacters(value As String) As String
    Dim bytes As Byte() = Encoding.GetEncoding("Cyrillic").GetBytes(value)
    Return Encoding.ASCII.GetString(bytes)
  End Function

  Private Function SendEmailNotification(mergeData As List(Of MergeData), submission As XElement) As Boolean
    Dim result As Boolean = True
    Dim fields As String = ""
    Dim fieldsMergeField As String = "<img src=""insertField.field?id=1&amp;nmode=0&amp;name=Submitted+fields&amp;type=199621441"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""1"" attribid=""0"" searchable=""0"" fieldname=""Submitted fields"" fieldtype=""199621441"" htmlencode=""True"" isloop=""False"" />"

    Dim template As New EmailTemplate(Integer.Parse(hfEmailTemplateId.Value))
    For Each recipient In MyContent.EmailRecipients
      Dim email As New EMail(template)
      email.Save()

      Dim name = ""
      Dim emailAddress = ""


      name = recipient.DisplayName
      emailAddress = recipient.EmailAddress
      mergeData(0).RecipientDisplayName = name

      If Not String.IsNullOrWhiteSpace(emailAddress) Then
        Dim provider() As MergeFieldsData = {New MergeFieldsData(mergeData.ToArray())}

        If InStr(email.ContentHTML, fieldsMergeField) > 0 Then
          email.ContentHTML = email.ContentHTML.Replace(fieldsMergeField, GenerateFieldsHTML(submission))
        End If

        Try
          email.Send(emailAddress, name, Nothing, Nothing, provider, Me.Page)
        Catch ex As Exception
          result = False
        End Try
      End If
    Next
    Return result
  End Function

  Private Function FillMergeData(ByRef submission As XElement) As List(Of MergeData)
    Dim list As List(Of MergeData) = New List(Of MergeData)

    Dim data As MergeData = New MergeData()

    'data.Fields = GenerateFields(submission)
    data.PageName = API.Pages.CurrentPage.Name
    data.SiteName = hfSiteName.Value
    data.FormName = hfFormName.Value
    data.DateSubmitted = Date.Now()
    data.ConstituentLookupId = API.Users.CurrentUser.LookupID

    list.Add(data)

    Return list
  End Function

  Private Sub RenderSuccessMessage()
    Dim htmlControl As New BBNCExtensions.ServerControls.HtmlDisplay

    htmlControl.StorageHTML = MyContent.SuccessMessageStorageHTML
    If htmlControl.StorageHTML IsNot Nothing Then
      Me.phSuccessMessage.Controls.Add(htmlControl)
    End If
    FormBuilderMultiView.ActiveViewIndex = 1
  End Sub

  Private Function GenerateFieldsHTML(submission As XElement) As String
    Dim html As String = ""
    Dim table As HtmlTable = New HtmlTable()

    For Each f In submission.Element("Fields").Elements("Field")
      Dim row As HtmlTableRow = New HtmlTableRow()
      Dim cell1 As HtmlTableCell = New HtmlTableCell()
      cell1.Attributes.Add("style", "vertical-align:top; width:200px; padding:3px 3px 3px 0px;")
      Dim cell2 As HtmlTableCell = New HtmlTableCell()
      cell2.Attributes.Add("style", "vertical-align:top; width:320px; padding:3px 0px 3px 3px;")
      Dim type As eFieldType.FieldType = f.Attribute("FieldTypeId").Value

      Select Case type
        Case eFieldType.FieldType.SingleLineText
          cell1.InnerText = f.Element("Label").Value
          cell2.InnerText = f.Element("Value").Value
        Case eFieldType.FieldType.MultiLineText
          cell1.InnerText = f.Element("Label").Value
          Dim text = f.Element("Value").Value.Replace(vbCrLf, "<br />")
          cell2.InnerHtml = text
        Case eFieldType.FieldType.NumberField
          cell1.InnerText = f.Element("Label").Value
          cell2.InnerText = f.Element("Value").Value
        Case eFieldType.FieldType.Checkbox
          cell1.InnerText = f.Element("Label").Value
          cell2.InnerText = f.Element("Value").Value
        Case eFieldType.FieldType.MultipleChoice
          cell1.InnerText = f.Element("Label").Value
          cell2.InnerText = f.Element("Value").Value
        Case eFieldType.FieldType.DropDownMenu
          cell1.InnerText = f.Element("Label").Value
          cell2.InnerText = f.Element("Value").Value
        Case eFieldType.FieldType.EmailField
          cell1.InnerText = f.Element("Label").Value
          cell2.InnerText = f.Element("Value").Value
        Case eFieldType.FieldType.AddressField
          cell1.InnerText = f.Element("Label").Value
          cell2.InnerHtml = f.Element("Value").Value.Replace(vbCrLf, "<br />")
        Case eFieldType.FieldType.PhoneField
          cell1.InnerText = f.Element("Label").Value
          cell2.InnerText = f.Element("Value").Value
        Case eFieldType.FieldType.DateTimeField
          cell1.InnerText = f.Element("Label").Value
          cell2.InnerText = f.Element("Value").Value
        Case eFieldType.FieldType.FileUpload
          cell1.InnerText = f.Element("Label").Value
          cell2.InnerText = f.Element("Url").Value
        Case Else

      End Select
      If type <> eFieldType.FieldType.Heading And type <> eFieldType.FieldType.Paragraph Then
        row.Controls.Add(cell1)
        row.Controls.Add(cell2)
        table.Controls.Add(row)
      End If
    Next

    Dim writer As StringWriter = New StringWriter()
    table.RenderControl(New HtmlTextWriter(writer))
    html = writer.ToString

    Return html
  End Function

  Private Function GenerateFields(submission As XElement) As MergeCustomFieldData()
    Dim fields(0) As MergeCustomFieldData
    Array.Resize(fields, submission.Element("Fields").Elements("Field").Count)

    For i = 0 To submission.Element("Fields").Elements("Field").Count - 1 Step 1
      Dim item As XElement = submission.Element("Fields").Elements("Field")(i)

      Dim type As eFieldType.FieldType = item.Attribute("FieldTypeId").Value
      Dim field As MergeCustomFieldData = New MergeCustomFieldData()
      field.Type = item.Attribute("FieldType")

      Select Case type
        Case eFieldType.FieldType.SingleLineText
          field.Label = item.Element("Label").Value
          field.Value = item.Element("Value").Value
        Case eFieldType.FieldType.MultiLineText
          field.Label = item.Element("Label").Value
          field.Value = item.Element("Value").Value
        Case eFieldType.FieldType.NumberField
          field.Label = item.Element("Label").Value
          field.Value = item.Element("Value").Value
        Case eFieldType.FieldType.Checkbox
          field.Label = item.Element("Label").Value
          field.Value = item.Element("Value").Value
        Case eFieldType.FieldType.MultipleChoice
          field.Label = item.Element("Label").Value


          Dim values = item.Element("Values").Elements("Value")

          Dim text As String = ""

          For x = 0 To values.Count - 1 Step 1
            Dim value As String = values(x).Value
            If value.Length > 0 Then
              text += value
              If x < values.Count - 1 AndAlso values(x + 1).Value.Length > 0 Then
                text += ", "
              End If
            End If
          Next

          field.Value = text

        Case eFieldType.FieldType.DropDownMenu
          field.Label = item.Element("Label").Value
          field.Value = item.Element("Value").Value
        Case eFieldType.FieldType.EmailField
          field.Label = "Email address:"

          Dim text As String = ""
          If item.Element("EmailAddress").Value.Length > 0 Then
            text += item.Element("EmailAddress").Value + IIf(item.Element("Type").Value.Length > 0, " (" + item.Element("Type").Value + ")", "")
          End If

          field.Value = text
        Case eFieldType.FieldType.AddressField
          field.Label = "Address:"

          Dim text As String = ""
          text += item.Element("AddressBlock").Value + vbCrLf
          text += item.Element("City").Value + ", " + item.Element("State").Value + " " + item.Element("ZIPCode").Value + vbCrLf
          text += item.Element("Country").Value
          If item.Element("Type").Value.Length > 0 Then
            text += vbCrLf
            text += "Type: " + item.Element("Type").Value
          End If

          field.Value = text

        Case eFieldType.FieldType.PhoneField
          field.Label = "Phone number:"

          Dim text As String = ""
          If item.Element("PhoneNumber").Value.Length > 0 Then
            text += item.Element("PhoneNumber").Value + IIf(item.Element("Type").Value.Length > 0, " (" + item.Element("Type").Value + ")", "")
          End If

          field.Value = text

        Case eFieldType.FieldType.DateTimeField
          field.Label = item.Element("Label").Value
          field.Value = item.Element("Value").Value
        Case eFieldType.FieldType.FileUpload
          field.Label = item.Element("Label").Value
          field.Value = item.Element("Value").Value
        Case Else

      End Select

      fields(i) = field
    Next

    Return fields
  End Function

  'Private Function Expand(html As String, data As Generic.IEnumerable(Of MergeData)) As String
  '  'Check if a loop exists
  '  If InStr(html, HTMLHelper.MERGE_FIELD_ISLOOP_ATTR) = 0 Then
  '    Return html
  '  End If

  '  Dim builder = New StringBuilder
  '  Dim writer = New StringWriter(builder)
  '  Dim textwriter = New HtmlTextWriter(writer)

  '  Dim sections As IEnumerable(Of String) = Nothing

  '  sections = MergeFieldHelper.GetSections(html, MergeFieldsProvider.eMergeField.BeginSubmittedFields, MergeFieldsProvider.eMergeField.EndSubmittedFields, True)

  '  If sections IsNot Nothing Then
  '    For Each htmlSection In sections
  '      If Not String.IsNullOrEmpty(htmlSection) Then
  '        Dim row As Integer = 0

  '        For Each item In data(0).Fields

  '          Dim workingString As String = htmlSection
  '          Dim htmlBreak = "<br />"

  '          Dim htmlControl As Control = Page.ParseControl(String.Concat(HTMLHelper.CORE_CONTROLS_REGISTER_DIRECTIVE, workingString))

  '          AssignRowNumbers(htmlControl.Controls, row)
  '          htmlControl.RenderControl(textwriter)

  '          row += 1
  '        Next
  '        html = html.Replace(htmlSection, builder.ToString)
  '      End If
  '    Next
  '  End If

  '  Return html
  'End Function

  'Public Shared Sub AssignRowNumbers(ByVal ctls As ControlCollection, ByVal itemRowNumber As Integer)
  '  'this method is stolen from this file:
  '  '$\Infinity\DEV\Firebird\ClassicCMS\Web\Content\Core\Components\Html\HTMLHelper.vb

  '  Dim MERGE_FIELD_ID_ATTR As String = "fieldid"
  '  Dim MERGE_FIELD_ROWNUMBER_ATTR As String = "rownumber" 'ViM for Top X merge fields
  '  Dim MERGE_FIELD_ISLOOP_ATTR As String = "isloop"

  '  'loop thru controls and assign rownumber to appropriate fields
  '  For Each ctl As Control In ctls
  '    Dim img As HtmlControls.HtmlImage = TryCast(ctl, HtmlControls.HtmlImage)
  '    If img IsNot Nothing Then
  '      Dim attrs As System.Web.UI.AttributeCollection = img.Attributes
  '      Dim fieldID As Integer = DataObject.safeDBFieldIntValue(attrs.Item(MERGE_FIELD_ID_ATTR))
  '      Dim isLoop As Boolean = DataObject.safeDBFieldBooleanValue(attrs.Item(MERGE_FIELD_ISLOOP_ATTR))

  '      If fieldID > 0 Then
  '        If Not isLoop Then
  '          Dim rowNumAttr As String = attrs.Item(MERGE_FIELD_ROWNUMBER_ATTR)

  '          If Len(rowNumAttr) > 0 Then
  '            rowNumAttr = String.Concat(itemRowNumber, "_", rowNumAttr)
  '          Else
  '            rowNumAttr = itemRowNumber.ToString
  '          End If
  '          attrs.Item(MERGE_FIELD_ROWNUMBER_ATTR) = rowNumAttr
  '        End If
  '        attrs.Item("runat") = "server"
  '      End If
  '    End If

  '    If ctl.Controls.Count > 0 Then
  '      AssignRowNumbers(ctl.Controls, itemRowNumber)
  '    End If
  '  Next
  'End Sub
End Class