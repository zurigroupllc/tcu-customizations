﻿Option Infer On
Option Strict Off

Imports bbAppFxWebAPI = Blackbaud.AppFx.WebAPI



Namespace AddForms

    Namespace [FormBuilderSubmission]
    
		

		

		    ''' <summary>
    ''' Provides WebApi access to the "BBIS Form Builder Submission Add Data Form" catalog feature.  A data form for adding bbis form builder submission records
    ''' </summary>
<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
        Public NotInheritable Class [BBISFormBuilderSubmissionAddDataForm]

            Private Sub New()
            End Sub

            Private Shared ReadOnly _specId As Guid = New Guid("21e22812-dd04-4bc6-8450-71ee9a216e6a")

			''' <summary>
			''' The Spec ID value for the "BBIS Form Builder Submission Add Data Form" AddForm
			''' </summary>
            Public Shared ReadOnly Property SpecId() As Guid
                Get
                    Return _specId
                End Get
            End Property
 
            Public Shared Function CreateLoadRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider) As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest
                Return CreateLoadRequest(provider, Nothing)
            End Function

            Public Shared Function CreateLoadRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal options as bbAppFxWebAPI.LoadRequestOptions) As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest
                Return Blackbaud.AppFx.WebAPI.DataFormServices.CreateDataFormLoadRequest(provider, _specId, options)
            End Function

            ''' <summary>
            ''' Returns an instance of BBISFormBuilderSubmissionAddDataFormData with default data form fields populated.
            ''' </summary>
            ''' <returns>An instance of BBISFormBuilderSubmissionAddDataFormData with the form field properties that have defaults populated with those default values.</returns>
            ''' <remarks>This function will make a web request to the AppFx WebService DataFormLoad method to obtain the default data form field values.</remarks>
            Public Shared Function LoadData(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider) As BBISFormBuilderSubmissionAddDataFormData
                Return LoadDataWithOptions(provider, Nothing)
            End Function

            ''' <summary>
            ''' Returns an instance of BBISFormBuilderSubmissionAddDataFormData with default data form fields populated.
            ''' </summary>
            ''' <returns>An instance of BBISFormBuilderSubmissionAddDataFormData with the form field properties that have defaults populated with those default values.</returns>
            ''' <remarks>This function will make a web request to the AppFx WebService DataFormLoad method to obtain the default data form field values.</remarks>
            Public Shared Function LoadDataWithOptions(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal options As bbAppFxWebAPI.LoadRequestOptions) As BBISFormBuilderSubmissionAddDataFormData

				

                Dim request = CreateLoadRequest(provider, options)

				
				
				

                Return LoadData(provider, request)

            End Function

            ''' <summary>
            ''' Returns an instance of BBISFormBuilderSubmissionAddDataFormData with default data form fields populated.
            ''' </summary>
            ''' <returns>An instance of BBISFormBuilderSubmissionAddDataFormData with the form field properties that have defaults populated with those default values.</returns>
            ''' <remarks>This function will make a web request to the AppFx WebService DataFormLoad method to obtain the default data form field values.</remarks>
            Public Shared Function LoadData(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal request As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest) As BBISFormBuilderSubmissionAddDataFormData
                Return bbAppFxWebAPI.DataFormServices.GetFormData(Of BBISFormBuilderSubmissionAddDataFormData)(provider, request)
            End Function
   
        End Class

#Region "Data Class"

        ''' <summary>
        ''' Represents the data form field values in the "BBIS Form Builder Submission Add Data Form" data form.
        ''' </summary>
		<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
	    Public NotInheritable Class BBISFormBuilderSubmissionAddDataFormData
			Inherits bbAppFxWebAPI.AddFormData
        
#Region "Constructors"
        
			Public Sub New()
				Mybase.New()
			End Sub

			Friend Sub New(ByVal reply as bbAppFxWebAPI.ServiceProxy.DataFormLoadReply,request as bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest)
				Mybase.New()					
				If (reply IsNot Nothing) AndAlso (reply.DataFormItem IsNot Nothing) Then
					Me.SetValues(reply.DataFormItem)
				End If
			End Sub
        
        	Public Sub New(ByVal dfi as Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
				Mybase.New()					
				Me.SetValues(dfi)
			End Sub
			
			Public Sub New(ByVal dataFormItemXml As String)
                MyBase.New()
                Me.SetValuesFromDataFormItem(dataFormItemXml)
            End Sub

#End Region
        
#Region "Form Field Properties"

Private [_CONSTITUENTID] As Nullable(of System.Guid)
''' <summary>
''' Constituent ID
''' </summary>
Public Property [CONSTITUENTID] As Nullable(of System.Guid)
    Get
        Return Me.[_CONSTITUENTID]
    End Get
    Set
        Me.[_CONSTITUENTID] = value 
    End Set
End Property

Private [_CLIENTUSERSID] As Nullable(of Integer)
''' <summary>
''' Client users ID
''' </summary>
Public Property [CLIENTUSERSID] As Nullable(of Integer)
    Get
        Return Me.[_CLIENTUSERSID]
    End Get
    Set
        Me.[_CLIENTUSERSID] = value 
    End Set
End Property

Private [_SUBMITTEDDATA] As String
''' <summary>
''' Submitted data
''' </summary>
Public Property [SUBMITTEDDATA] As String
    Get
        Return Me.[_SUBMITTEDDATA]
    End Get
    Set
        Me.[_SUBMITTEDDATA] = value 
    End Set
End Property

Private [_BBISPAGEID] As Nullable(of Integer)
''' <summary>
''' BBIS Page ID
''' </summary>
Public Property [BBISPAGEID] As Nullable(of Integer)
    Get
        Return Me.[_BBISPAGEID]
    End Get
    Set
        Me.[_BBISPAGEID] = value 
    End Set
End Property

Private [_SITECONTENTID] As Nullable(of Integer)
''' <summary>
''' Site Content ID
''' </summary>
Public Property [SITECONTENTID] As Nullable(of Integer)
    Get
        Return Me.[_SITECONTENTID]
    End Get
    Set
        Me.[_SITECONTENTID] = value 
    End Set
End Property

Private [_FORMNAME] As String
''' <summary>
''' Form name
''' </summary>
Public Property [FORMNAME] As String
    Get
        Return Me.[_FORMNAME]
    End Get
    Set
        Me.[_FORMNAME] = value 
    End Set
End Property

Private [_URL] As String
''' <summary>
''' URL
''' </summary>
Public Property [URL] As String
    Get
        Return Me.[_URL]
    End Get
    Set
        Me.[_URL] = value 
    End Set
End Property

        

#End Region

            Public Overrides ReadOnly Property DataFormInstanceID() As System.Guid
                Get
                    Return AddForms.[FormBuilderSubmission].BBISFormBuilderSubmissionAddDataForm.SpecId
                End Get
            End Property

            Public Overrides Function ToDataFormItem() As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem
                Return Me.BuildDataFormItemForSave()
            End Function
    
			Friend Sub SetValues(ByVal dfi As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
				
				If dfi Is Nothing Then Exit Sub
	            
				
Dim value As Object = Nothing
Dim dfiFieldValue As Blackbaud.AppFx.XmlTypes.DataForms.DataFormFieldValue = Nothing
Dim stringFieldValue As String = Nothing

Dim guidFieldValue As System.Guid
guidFieldValue = System.Guid.Empty
If dfi.TryGetValueForPropertyAssignment("CONSTITUENTID", guidFieldValue) Then
Me.[CONSTITUENTID] = guidFieldValue
End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("CLIENTUSERSID", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
If TypeOf value Is String Then 
Me.[CLIENTUSERSID] = Blackbaud.AppFx.DataListUtility.DataListStringValueToInt(value)

Else
Me.[CLIENTUSERSID] = value
End If
End If

	End If

End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("SUBMITTEDDATA", stringFieldValue) Then
Me.[SUBMITTEDDATA] = stringFieldValue
End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("BBISPAGEID", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
If TypeOf value Is String Then 
Me.[BBISPAGEID] = Blackbaud.AppFx.DataListUtility.DataListStringValueToInt(value)

Else
Me.[BBISPAGEID] = value
End If
End If

	End If

End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("SITECONTENTID", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
If TypeOf value Is String Then 
Me.[SITECONTENTID] = Blackbaud.AppFx.DataListUtility.DataListStringValueToInt(value)

Else
Me.[SITECONTENTID] = value
End If
End If

	End If

End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("FORMNAME", stringFieldValue) Then
Me.[FORMNAME] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("URL", stringFieldValue) Then
Me.[URL] = stringFieldValue
End If


	            
			End Sub

			Private Function BuildDataFormItemForSave() As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem
        
				Dim dfi As New Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem	 
	                    
				Dim value As Object = Nothing
value = Me.[CONSTITUENTID]
	dfi.SetValueIfNotNull("CONSTITUENTID",value)
value = Me.[CLIENTUSERSID]
	dfi.SetValueIfNotNull("CLIENTUSERSID",value)
value = Me.[SUBMITTEDDATA]
	dfi.SetValueIfNotNull("SUBMITTEDDATA",value)
value = Me.[BBISPAGEID]
	dfi.SetValueIfNotNull("BBISPAGEID",value)
value = Me.[SITECONTENTID]
	dfi.SetValueIfNotNull("SITECONTENTID",value)
value = Me.[FORMNAME]
	dfi.SetValueIfNotNull("FORMNAME",value)
value = Me.[URL]
	dfi.SetValueIfNotNull("URL",value)

	    
				Return dfi	    
	        
			End Function
			
			Public Overrides Sub SetValuesFromDataFormItem(ByVal dataFormItem As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
                Me.SetValues(dataFormItem)
            End Sub
            
			
	 
		End Class
    
#End Region
    
    End Namespace

End Namespace

