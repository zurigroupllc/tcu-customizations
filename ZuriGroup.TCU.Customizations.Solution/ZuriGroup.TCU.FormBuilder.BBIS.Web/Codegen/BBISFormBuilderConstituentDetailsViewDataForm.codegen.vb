﻿Option Infer On
Option Strict Off

Imports bbAppFxWebAPI = Blackbaud.AppFx.WebAPI



Namespace ViewForms

    Namespace [Constituent]
    
		

		    ''' <summary>
    ''' Provides WebApi access to the "BBIS Form Builder Constituent Details View Data Form" catalog feature.  A data form for viewing bbis form builder constituent details
    ''' </summary>
<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
        Public NotInheritable Class [BBISFormBuilderConstituentDetailsViewDataForm]

            Private Sub New()
            End Sub

            Private Shared ReadOnly _specId As Guid = New Guid("e050d6c7-a025-4e04-be30-bddb463fe5cf")

			''' <summary>
			''' The Spec ID value for the "BBIS Form Builder Constituent Details View Data Form" ViewForm
			''' </summary>
            Public Shared ReadOnly Property SpecId() As Guid
                Get
                    Return _specId
                End Get
            End Property
 
            Public Shared Function CreateRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider) As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest
                Return CreateRequest(provider, Nothing)
            End Function
            
            Public Shared Function CreateRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal options As bbAppFxWebAPI.LoadRequestOptions) As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest
                Return Blackbaud.AppFx.WebAPI.DataFormServices.CreateDataFormLoadRequest(provider, _specId, options)
            End Function

            Public Shared Function LoadData(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal recordId As String) As BBISFormBuilderConstituentDetailsViewDataFormData
				Return LoadDataWithOptions(provider, recordId, Nothing)
            End Function

            Public Shared Function LoadDataWithOptions(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal recordId As String, ByVal options As bbAppFxWebAPI.LoadRequestOptions) As BBISFormBuilderConstituentDetailsViewDataFormData

				bbAppFxWebAPI.DataFormServices.ValidateRecordId(recordId)

                Dim request = CreateRequest(provider, options)

				
				
				request.RecordID = recordId

                Return LoadData(provider, request)

            End Function

            Public Shared Function LoadData(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal request As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest) As BBISFormBuilderConstituentDetailsViewDataFormData
                Return bbAppFxWebAPI.DataFormServices.GetFormData(Of BBISFormBuilderConstituentDetailsViewDataFormData)(provider, request)
            End Function

        End Class

#Region "Data Class"
	
	    ''' <summary>
        ''' Represents the data form field values in the "BBIS Form Builder Constituent Details View Data Form" data form.
        ''' </summary>
		<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
	    Public NotInheritable Class BBISFormBuilderConstituentDetailsViewDataFormData
			Inherits bbAppFxWebAPI.ViewFormData
		
			Private [_CONSTITUENTID] As String
''' <summary>
''' Constituent ID
''' </summary>
Public Property [CONSTITUENTID] As String
    Get
        Return Me.[_CONSTITUENTID]
    End Get
    Set
        Me.[_CONSTITUENTID] = value 
    End Set
End Property

Private [_FIRSTNAME] As String
''' <summary>
''' First name
''' </summary>
Public Property [FIRSTNAME] As String
    Get
        Return Me.[_FIRSTNAME]
    End Get
    Set
        Me.[_FIRSTNAME] = value 
    End Set
End Property

Private [_MIDDLENAME] As String
''' <summary>
''' Middle name
''' </summary>
Public Property [MIDDLENAME] As String
    Get
        Return Me.[_MIDDLENAME]
    End Get
    Set
        Me.[_MIDDLENAME] = value 
    End Set
End Property

Private [_MIDDLEINITIAL] As String
''' <summary>
''' Middle initial
''' </summary>
Public Property [MIDDLEINITIAL] As String
    Get
        Return Me.[_MIDDLEINITIAL]
    End Get
    Set
        Me.[_MIDDLEINITIAL] = value 
    End Set
End Property

Private [_LASTNAME] As String
''' <summary>
''' Last name
''' </summary>
Public Property [LASTNAME] As String
    Get
        Return Me.[_LASTNAME]
    End Get
    Set
        Me.[_LASTNAME] = value 
    End Set
End Property

Private [_MAIDENNAME] As String
''' <summary>
''' Maiden name
''' </summary>
Public Property [MAIDENNAME] As String
    Get
        Return Me.[_MAIDENNAME]
    End Get
    Set
        Me.[_MAIDENNAME] = value 
    End Set
End Property

Private [_NICKNAME] As String
''' <summary>
''' Nickname
''' </summary>
Public Property [NICKNAME] As String
    Get
        Return Me.[_NICKNAME]
    End Get
    Set
        Me.[_NICKNAME] = value 
    End Set
End Property

Private [_PRIMARYPHONENUMBER] As String
''' <summary>
''' Primary phone number
''' </summary>
Public Property [PRIMARYPHONENUMBER] As String
    Get
        Return Me.[_PRIMARYPHONENUMBER]
    End Get
    Set
        Me.[_PRIMARYPHONENUMBER] = value 
    End Set
End Property

Private [_PRIMARYEMAILADDRESS] As String
''' <summary>
''' Primary email address
''' </summary>
Public Property [PRIMARYEMAILADDRESS] As String
    Get
        Return Me.[_PRIMARYEMAILADDRESS]
    End Get
    Set
        Me.[_PRIMARYEMAILADDRESS] = value 
    End Set
End Property

Private [_PRIMARYADDRESSBLOCK] As String
''' <summary>
''' Primary address block
''' </summary>
Public Property [PRIMARYADDRESSBLOCK] As String
    Get
        Return Me.[_PRIMARYADDRESSBLOCK]
    End Get
    Set
        Me.[_PRIMARYADDRESSBLOCK] = value 
    End Set
End Property

Private [_PRIMARYADDRESSCITY] As String
''' <summary>
''' Primary address city
''' </summary>
Public Property [PRIMARYADDRESSCITY] As String
    Get
        Return Me.[_PRIMARYADDRESSCITY]
    End Get
    Set
        Me.[_PRIMARYADDRESSCITY] = value 
    End Set
End Property

Private [_PRIMARYADDRESSSTATEID] As Nullable(of System.Guid)
''' <summary>
''' Primary address state ID
''' </summary>
Public Property [PRIMARYADDRESSSTATEID] As Nullable(of System.Guid)
    Get
        Return Me.[_PRIMARYADDRESSSTATEID]
    End Get
    Set
        Me.[_PRIMARYADDRESSSTATEID] = value 
    End Set
End Property

Private [_PRIMARYADDRESSCOUNTRYID] As Nullable(of System.Guid)
''' <summary>
''' Primary address country ID
''' </summary>
Public Property [PRIMARYADDRESSCOUNTRYID] As Nullable(of System.Guid)
    Get
        Return Me.[_PRIMARYADDRESSCOUNTRYID]
    End Get
    Set
        Me.[_PRIMARYADDRESSCOUNTRYID] = value 
    End Set
End Property

Private [_PRIMARYADDRESSPOSTCODE] As String
''' <summary>
''' Primary address post code
''' </summary>
Public Property [PRIMARYADDRESSPOSTCODE] As String
    Get
        Return Me.[_PRIMARYADDRESSPOSTCODE]
    End Get
    Set
        Me.[_PRIMARYADDRESSPOSTCODE] = value 
    End Set
End Property

Private [_CLASSYEAR] As String
''' <summary>
''' Class year
''' </summary>
Public Property [CLASSYEAR] As String
    Get
        Return Me.[_CLASSYEAR]
    End Get
    Set
        Me.[_CLASSYEAR] = value 
    End Set
End Property


	        
			Public Sub New()
				MyBase.New()
			End Sub

			Public Sub New(ByVal reply as bbAppFxWebAPI.ServiceProxy.DataFormLoadReply)
				If (reply IsNot Nothing) AndAlso (reply.DataFormItem IsNot Nothing) Then
					Me.SetValues(reply.DataFormItem)
				End If
			End Sub
        
			Public Sub New(ByVal dataFormItemXml As String)
				MyBase.New()
				Me.SetValuesFromDataFormItem(dataFormItemXml)
			End Sub
        
			Public Overrides ReadOnly Property DataFormInstanceID() As System.Guid
				Get
					Return ViewForms.[Constituent].BBISFormBuilderConstituentDetailsViewDataForm.SpecId
				End Get
			End Property
	    
			Friend Sub SetValues(ByVal dfi As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
	            
				
Dim value As Object = Nothing
Dim dfiFieldValue As Blackbaud.AppFx.XmlTypes.DataForms.DataFormFieldValue = Nothing
Dim stringFieldValue As String = Nothing

Dim guidFieldValue As System.Guid
stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("CONSTITUENTID", stringFieldValue) Then
Me.[CONSTITUENTID] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("FIRSTNAME", stringFieldValue) Then
Me.[FIRSTNAME] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("MIDDLENAME", stringFieldValue) Then
Me.[MIDDLENAME] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("MIDDLEINITIAL", stringFieldValue) Then
Me.[MIDDLEINITIAL] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("LASTNAME", stringFieldValue) Then
Me.[LASTNAME] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("MAIDENNAME", stringFieldValue) Then
Me.[MAIDENNAME] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("NICKNAME", stringFieldValue) Then
Me.[NICKNAME] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("PRIMARYPHONENUMBER", stringFieldValue) Then
Me.[PRIMARYPHONENUMBER] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("PRIMARYEMAILADDRESS", stringFieldValue) Then
Me.[PRIMARYEMAILADDRESS] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("PRIMARYADDRESSBLOCK", stringFieldValue) Then
Me.[PRIMARYADDRESSBLOCK] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("PRIMARYADDRESSCITY", stringFieldValue) Then
Me.[PRIMARYADDRESSCITY] = stringFieldValue
End If

guidFieldValue = System.Guid.Empty
If dfi.TryGetValueForPropertyAssignment("PRIMARYADDRESSSTATEID", guidFieldValue) Then
Me.[PRIMARYADDRESSSTATEID] = guidFieldValue
End If

guidFieldValue = System.Guid.Empty
If dfi.TryGetValueForPropertyAssignment("PRIMARYADDRESSCOUNTRYID", guidFieldValue) Then
Me.[PRIMARYADDRESSCOUNTRYID] = guidFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("PRIMARYADDRESSPOSTCODE", stringFieldValue) Then
Me.[PRIMARYADDRESSPOSTCODE] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("CLASSYEAR", stringFieldValue) Then
Me.[CLASSYEAR] = stringFieldValue
End If


	            
			End Sub
				
			Public Overrides Sub SetValuesFromDataFormItem(ByVal dataFormItem As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
				Me.SetValues(dataFormItem)
			End Sub

			
	 
		End Class

#End Region
    
    End Namespace

End Namespace



