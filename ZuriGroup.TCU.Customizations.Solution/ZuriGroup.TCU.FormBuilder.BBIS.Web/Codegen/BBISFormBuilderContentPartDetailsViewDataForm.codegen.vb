﻿Option Infer On
Option Strict Off

Imports bbAppFxWebAPI = Blackbaud.AppFx.WebAPI



Namespace ViewForms

    Namespace [FormBuilderEmailProperties]
    
		

		    ''' <summary>
    ''' Provides WebApi access to the "BBIS Form Builder Content Part Details View Data Form" catalog feature.  A data form for viewing bbis form builder content part details
    ''' </summary>
<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
        Public NotInheritable Class [BBISFormBuilderContentPartDetailsViewDataForm]

            Private Sub New()
            End Sub

            Private Shared ReadOnly _specId As Guid = New Guid("1f3aac21-9395-4466-9c45-05116ef5129e")

			''' <summary>
			''' The Spec ID value for the "BBIS Form Builder Content Part Details View Data Form" ViewForm
			''' </summary>
            Public Shared ReadOnly Property SpecId() As Guid
                Get
                    Return _specId
                End Get
            End Property
 
            Public Shared Function CreateRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider) As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest
                Return CreateRequest(provider, Nothing)
            End Function
            
            Public Shared Function CreateRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal options As bbAppFxWebAPI.LoadRequestOptions) As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest
                Return Blackbaud.AppFx.WebAPI.DataFormServices.CreateDataFormLoadRequest(provider, _specId, options)
            End Function

            Public Shared Function LoadData(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal recordId As String) As BBISFormBuilderContentPartDetailsViewDataFormData
				Return LoadDataWithOptions(provider, recordId, Nothing)
            End Function

            Public Shared Function LoadDataWithOptions(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal recordId As String, ByVal options As bbAppFxWebAPI.LoadRequestOptions) As BBISFormBuilderContentPartDetailsViewDataFormData

				bbAppFxWebAPI.DataFormServices.ValidateRecordId(recordId)

                Dim request = CreateRequest(provider, options)

				
				
				request.RecordID = recordId

                Return LoadData(provider, request)

            End Function

            Public Shared Function LoadData(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal request As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest) As BBISFormBuilderContentPartDetailsViewDataFormData
                Return bbAppFxWebAPI.DataFormServices.GetFormData(Of BBISFormBuilderContentPartDetailsViewDataFormData)(provider, request)
            End Function

        End Class

#Region "Data Class"
	
	    ''' <summary>
        ''' Represents the data form field values in the "BBIS Form Builder Content Part Details View Data Form" data form.
        ''' </summary>
		<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
	    Public NotInheritable Class BBISFormBuilderContentPartDetailsViewDataFormData
			Inherits bbAppFxWebAPI.ViewFormData
		
			Private [_TITLE] As String
''' <summary>
''' Title
''' </summary>
Public Property [TITLE] As String
    Get
        Return Me.[_TITLE]
    End Get
    Set
        Me.[_TITLE] = value 
    End Set
End Property

Private [_SITEID] As Nullable(of Integer)
''' <summary>
''' Site ID
''' </summary>
Public Property [SITEID] As Nullable(of Integer)
    Get
        Return Me.[_SITEID]
    End Get
    Set
        Me.[_SITEID] = value 
    End Set
End Property

Private [_SITENAME] As String
''' <summary>
''' Site name
''' </summary>
Public Property [SITENAME] As String
    Get
        Return Me.[_SITENAME]
    End Get
    Set
        Me.[_SITENAME] = value 
    End Set
End Property

Private [_EMAILTEMPLATEID] As Nullable(of Integer)
''' <summary>
''' Email template ID
''' </summary>
Public Property [EMAILTEMPLATEID] As Nullable(of Integer)
    Get
        Return Me.[_EMAILTEMPLATEID]
    End Get
    Set
        Me.[_EMAILTEMPLATEID] = value 
    End Set
End Property


	        
			Public Sub New()
				MyBase.New()
			End Sub

			Public Sub New(ByVal reply as bbAppFxWebAPI.ServiceProxy.DataFormLoadReply)
				If (reply IsNot Nothing) AndAlso (reply.DataFormItem IsNot Nothing) Then
					Me.SetValues(reply.DataFormItem)
				End If
			End Sub
        
			Public Sub New(ByVal dataFormItemXml As String)
				MyBase.New()
				Me.SetValuesFromDataFormItem(dataFormItemXml)
			End Sub
        
			Public Overrides ReadOnly Property DataFormInstanceID() As System.Guid
				Get
					Return ViewForms.[FormBuilderEmailProperties].BBISFormBuilderContentPartDetailsViewDataForm.SpecId
				End Get
			End Property
	    
			Friend Sub SetValues(ByVal dfi As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
	            
				
Dim value As Object = Nothing
Dim dfiFieldValue As Blackbaud.AppFx.XmlTypes.DataForms.DataFormFieldValue = Nothing
Dim stringFieldValue As String = Nothing

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("TITLE", stringFieldValue) Then
Me.[TITLE] = stringFieldValue
End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("SITEID", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
If TypeOf value Is String Then 
Me.[SITEID] = Blackbaud.AppFx.DataListUtility.DataListStringValueToInt(value)

Else
Me.[SITEID] = value
End If
End If

	End If

End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("SITENAME", stringFieldValue) Then
Me.[SITENAME] = stringFieldValue
End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("EMAILTEMPLATEID", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
If TypeOf value Is String Then 
Me.[EMAILTEMPLATEID] = Blackbaud.AppFx.DataListUtility.DataListStringValueToInt(value)

Else
Me.[EMAILTEMPLATEID] = value
End If
End If

	End If

End If


	            
			End Sub
				
			Public Overrides Sub SetValuesFromDataFormItem(ByVal dataFormItem As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
				Me.SetValues(dataFormItem)
			End Sub

			
	 
		End Class

#End Region
    
    End Namespace

End Namespace



