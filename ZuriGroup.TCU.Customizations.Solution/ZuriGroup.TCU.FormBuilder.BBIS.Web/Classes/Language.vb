﻿Public Class Language
  Public Property AddressAddressLabel As String
  Public Property AddressCountryLabel As String
  Public Property AddressCityLabel As String
  Public Property AddressStateLabel As String
  Public Property AddressZIPLabel As String
  Public Property AddressTypeLabel As String

  Public Property EmailAddressLabel As String
  Public Property EmailVerifyLabel As String
  Public Property EmailTypeLabel As String

  Public Property PhoneNumberLabel As String
  Public Property PhoneTypeLabel As String

  Public Property AddressAddressRequired As String
  Public Property AddressCountryRequired As String
  Public Property AddressCityRequired As String
  Public Property AddressStateRequired As String
  Public Property AddressZIPRequired As String
  Public Property AddressTypeRequired As String

  Public Property EmailAddressRequired As String
  Public Property EmailTypeRequired As String

  Public Property PhoneNumberRequired As String
  Public Property PhoneTypeRequired As String

  Public Property TextNumberDateDropdownRequired As String
  Public Property CheckboxRequired As String
  Public Property MultipleChoiceRequired As String
  Public Property MultipleChoiceOtherRequired As String
  Public Property FileUploadRequired As String

  Public Property AddressZIPInvalid As String
  Public Property EmailAddressInvalid As String
  Public Property EmailAddressDoNotMatch As String
  Public Property PhoneNumberInvalid As String

  Public Property TextNumberDateInvalid As String
  Public Property NumberOutsideRange As String
  Public Property FileUploadTooLarge As String
  Public Property FileUploadInvalid As String
  Public Property DateMonthInvalid As String
  Public Property DateDayInvalid As String
  Public Property DateYearInvalid As String
  Public Property DateDateInvalid As String
  Public Property DateTimeInvalid As String

  Public Sub New()
    AddressAddressLabel = ""
    AddressCountryLabel = ""
    AddressCityLabel = ""
    AddressStateLabel = ""
    AddressZIPLabel = ""
    AddressTypeLabel = ""

    EmailAddressLabel = ""
    EmailVerifyLabel = ""
    EmailTypeLabel = ""

    PhoneNumberLabel = ""
    PhoneTypeLabel = ""

    AddressAddressRequired = ""
    AddressCountryRequired = ""
    AddressCityRequired = ""
    AddressStateRequired = ""
    AddressZIPRequired = ""
    AddressTypeRequired = ""

    EmailAddressRequired = ""
    EmailTypeRequired = ""

    PhoneNumberRequired = ""
    PhoneTypeRequired = ""

    TextNumberDateDropdownRequired = ""
    CheckboxRequired = ""
    MultipleChoiceRequired = ""
    MultipleChoiceOtherRequired = ""
    FileUploadRequired = ""

    AddressZIPInvalid = ""
    EmailAddressInvalid = ""
    EmailAddressDoNotMatch = ""
    PhoneNumberInvalid = ""

    TextNumberDateInvalid = ""
    NumberOutsideRange = ""
    FileUploadTooLarge = ""
    FileUploadInvalid = ""
    DateMonthInvalid = ""
    DateDayInvalid = ""
    DateYearInvalid = ""
    DateDateInvalid = ""
    DateTimeInvalid = ""
  End Sub
End Class
