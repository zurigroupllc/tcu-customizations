﻿Imports Blackbaud.AppFx.WebAPI

Public Class Field
  Private Const ValidationGroup As String = "FormBuilder"
  Public Property Type As eFieldType.FieldType
  Public ReadOnly Property TypeLabel As String
    Get
      Return eFieldType.FieldTypeLabel(Type)
    End Get
  End Property
  Public Property FieldId As Integer
  Public Property Position As Integer
  Private _Settings As XElement
  Dim provider As AppFxWebServiceProvider

  Public Property Settings As XElement
    Get
      If Me._Settings Is Nothing Then
        Me._Settings = EstablishSettings()
      End If
      Return _Settings
    End Get
    Set(value As XElement)
      _Settings = value
    End Set
  End Property
  Public Property Setting(type As String) As String
    Get
      If Me.Settings.Element(type) IsNot Nothing Then
        Return Me.Settings.Element(type)
      Else
        Return ""
      End If
    End Get
    Set(ByVal value As String)
      If Me.Settings.Element(type) IsNot Nothing Then
        Me.Settings.Element(type).Value = value
      Else
        Me.Settings.Add(New XElement(type, value))
      End If
    End Set
  End Property

  Public Function EstablishSettings() As XElement
    Dim x As XElement = New XElement("Field")
    Select Case Type
      Case eFieldType.FieldType.Heading
        x.Add(New XElement("Text", "Heading"))
        x.Add(New XElement("Type", "h1"))
        x.Add(New XElement("CSSClass", ""))
      Case eFieldType.FieldType.Paragraph
        x.Add(New XElement("Text", "Paragraph text"))
        x.Add(New XElement("CSSClass", ""))
      Case eFieldType.FieldType.SingleLineText
        x.Add(New XElement("Label", "Field label:"))
        x.Add(New XElement("HideLabel", "False"))
        x.Add(New XElement("Required", "True"))
        x.Add(New XElement("Hidden", "False"))
        x.Add(New XElement("HelpText", ""))
        x.Add(New XElement("DefaultValue", ""))
        x.Add(New XElement("PrePopulate", "0"))
        x.Add(New XElement("MaxLength", ""))
        x.Add(New XElement("NumbersOnly", "False"))
      Case eFieldType.FieldType.MultiLineText
        x.Add(New XElement("Label", "Field label:"))
        x.Add(New XElement("HideLabel", "False"))
        x.Add(New XElement("Required", "False"))
        x.Add(New XElement("Hidden", "False"))
        x.Add(New XElement("HelpText", ""))
        x.Add(New XElement("Rows", "3"))
        x.Add(New XElement("DefaultValue", ""))
      Case eFieldType.FieldType.NumberField
        x.Add(New XElement("Label", "Field label:"))
        x.Add(New XElement("HideLabel", "False"))
        x.Add(New XElement("Required", "False"))
        x.Add(New XElement("Hidden", "False"))
        x.Add(New XElement("HelpText", ""))
        x.Add(New XElement("NumberType", "0"))
        x.Add(New XElement("AvailableCharacters", "-."))
        x.Add(New XElement("MinValue", ""))
        x.Add(New XElement("MaxValue", ""))
        x.Add(New XElement("BlankValue", "0"))
        x.Add(New XElement("DefaultValue", ""))
        x.Add(New XElement("MaxLength", ""))
      Case eFieldType.FieldType.Checkbox
        x.Add(New XElement("Label", "Checkbox label:"))
        x.Add(New XElement("HideLabel", "True"))
        x.Add(New XElement("Required", "False"))
        x.Add(New XElement("Hidden", "False"))
        x.Add(New XElement("Caption", "Checkbox caption text"))
        x.Add(New XElement("DefaultValue", "False"))
      Case eFieldType.FieldType.MultipleChoice
        x.Add(New XElement("Label", "Group label:"))
        x.Add(New XElement("HideLabel", "False"))
        x.Add(New XElement("Required", "False"))
        x.Add(New XElement("Columns", "1"))
        x.Add(New XElement("RenderStyle", "0"))
        x.Add(New XElement("Values", "Option 1" + ControlChars.Lf + "Option 2" + ControlChars.Lf + "Option 3"))
        x.Add(New XElement("ShowOtherOption", "False"))
        x.Add(New XElement("OtherLabel", "Other (please specify):"))
        x.Add(New XElement("CodeTable", ""))
      Case eFieldType.FieldType.DropDownMenu
        x.Add(New XElement("Label", "Field label:"))
        x.Add(New XElement("HideLabel", "False"))
        x.Add(New XElement("Required", "False"))
        x.Add(New XElement("Hidden", "False"))
        x.Add(New XElement("HelpText", ""))
        x.Add(New XElement("Values", "Option 1" + ControlChars.Lf + "Option 2" + ControlChars.Lf + "Option 3"))
        x.Add(New XElement("CodeTable", ""))
      Case eFieldType.FieldType.EmailField
        x.Add(New XElement("EmailTypes", "Personal" + ControlChars.Lf + "Business"))
        x.Add(New XElement("AddTypeToLabel", "False"))
        x.Add(New XElement("Required", "True"))
        x.Add(New XElement("ShowConfirmation", "True"))
        x.Add(New XElement("TypeRequired", "False"))
        x.Add(New XElement("HelpText", ""))
        x.Add(New XElement("PrePopulate", "71"))
      Case eFieldType.FieldType.AddressField
        x.Add(New XElement("AddressTypes", "Home" + ControlChars.Lf + "Business"))
        x.Add(New XElement("AddTypeToLabel", "False"))
        x.Add(New XElement("AddressRequired", "False"))
        x.Add(New XElement("TypeRequired", "False"))
        x.Add(New XElement("DefaultToUSA", "True"))
        x.Add(New XElement("OnlyAllowUSA", "True"))
        x.Add(New XElement("PrePopulate", "31"))
      Case eFieldType.FieldType.PhoneField
        x.Add(New XElement("Required", "False"))
        x.Add(New XElement("TypeRequired", "False"))
        x.Add(New XElement("HelpText", "(e.g., 555-123-4567)"))
        x.Add(New XElement("PhoneTypes", "Home" + ControlChars.Lf + "Business" + ControlChars.Lf + "Mobile"))
        x.Add(New XElement("AddTypeToLabel", "False"))
        x.Add(New XElement("AvailableCharacters", ""))
        x.Add(New XElement("PrePopulate", "51"))
      Case eFieldType.FieldType.DateTimeField
        x.Add(New XElement("Label", "Field label:"))
        x.Add(New XElement("HideLabel", "False"))
        x.Add(New XElement("Required", "False"))
        x.Add(New XElement("DateType", "0"))
        x.Add(New XElement("SplitMonthDayYear", "True"))
      Case eFieldType.FieldType.FileUpload
        x.Add(New XElement("Label", "Field label:"))
        x.Add(New XElement("HideLabel", "False"))
        x.Add(New XElement("Required", "False"))
        x.Add(New XElement("HelpText", "jpg, jpeg, png or gif"))
        x.Add(New XElement("FileType", "0"))
        x.Add(New XElement("MaxFileSize", "1"))
        x.Add(New XElement("FolderId", "0"))
        x.Add(New XElement("FolderPath", "Files\"))
      Case Else

    End Select
    Return x
  End Function

  Public Function Display(constituent As Constituent, language As Language, provider As AppFxWebServiceProvider, Optional preview As Boolean = False) As HtmlTableRow
    Me.provider = provider
    If constituent Is Nothing Then
      constituent = New Constituent()
    End If
    Dim out = New HtmlTableRow()
    Select Case Type
      Case eFieldType.FieldType.Heading
        out = GenerateHeadingControl(preview)
      Case eFieldType.FieldType.Paragraph
        out = GenerateParagraphControl(preview)
      Case eFieldType.FieldType.SingleLineText
        out = GenerateSingleLineTextControl(preview, constituent, language)
      Case eFieldType.FieldType.MultiLineText
        out = GenerateMultiLineTextControl(preview, constituent, language)
      Case eFieldType.FieldType.NumberField
        out = GenerateNumberFieldControl(preview, language)
      Case eFieldType.FieldType.Checkbox
        out = GenerateCheckboxFieldControl(preview, language)
      Case eFieldType.FieldType.MultipleChoice
        out = GenerateMultipleChoiceControl(preview, language)
      Case eFieldType.FieldType.DropDownMenu
        out = GenerateDropDownMenuControl(preview, language)
      Case eFieldType.FieldType.EmailField
        out = GenerateEmailControl(preview, constituent, language)
      Case eFieldType.FieldType.AddressField
        out = GenerateAddressControl(preview, constituent, language)
      Case eFieldType.FieldType.PhoneField
        out = GeneratePhoneControl(preview, constituent, language)
      Case eFieldType.FieldType.DateTimeField
        out = GenerateDateTimeFieldControl(preview, language)
      Case eFieldType.FieldType.FileUpload
        out = GenerateFileUploadControl(preview, language)
      Case Else

    End Select

    Return out
  End Function

  Private Function GenerateHeadingControl(preview As Boolean) As HtmlTableRow
    Dim row = GenerateFormRow(preview)
    Dim cell = GenerateDetailCell(preview)

    Dim header = New HtmlGenericControl(Setting("Type"))
    header.InnerText = Setting("Text")

    AddClass(header, "FormBuilderHeader")
    AddClass(header, Setting("CSSClass"))

    cell.Controls.Add(header)
    row.Controls.Add(cell)

    Return row
  End Function
  Private Function GenerateParagraphControl(preview As Boolean) As HtmlTableRow
    Dim row = GenerateFormRow(preview)
    Dim cell = GenerateDetailCell(preview)

    Dim lines = Setting("Text").Split(ControlChars.Lf)
    For Each line In lines
      Dim p = New HtmlGenericControl("p")
      p.InnerText = line

      AddClass(p, "FormBuilderParagraph")
      AddClass(p, Setting("CSSClass"))

      cell.Controls.Add(p)
    Next

    row.Controls.Add(cell)

    Return row
  End Function
  Private Function GenerateSingleLineTextControl(preview As Boolean, constituent As Constituent, language As Language) As HtmlTableRow
    Dim row = GenerateFormRow(preview, Setting("Hidden"))

    Dim cell1 = GenerateFormLabelCell(preview, Setting("Label"), Setting("HideLabel"), Setting("Required"))
    Dim cell2 = GenerateFormControlCell(preview)

    Dim textbox As TextBox
    If Setting("PrePopulate") > 0 Then
      If preview Then
        textbox = GenerateTextbox(TextBoxMode.SingleLine, "FieldText", ePopulatedFieldType.Label(Setting("PrePopulate")))
      Else
        textbox = GenerateTextbox(TextBoxMode.SingleLine, "FieldText", constituent.GetPrePopulateValue(Setting("PrePopulate")))
      End If
    Else
      textbox = GenerateTextbox(TextBoxMode.SingleLine, "FieldText", Setting("DefaultValue"))
    End If

    If Setting("MaxLength").Length > 0 Then
      textbox.MaxLength = Setting("MaxLength")
    End If

    cell2.Controls.Add(textbox)

    GenerateHelpText(cell2, Setting("HelpText"))

    If Not preview Then
      If Setting("Required") Then
        Dim requiredMessage = Replace(language.TextNumberDateDropdownRequired, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
        cell2.Controls.Add(RequiredFieldValidator("FieldText", requiredMessage))
      End If
      If Setting("NumbersOnly") Then
        Dim numbersOnlyMessage = Replace(language.TextNumberDateInvalid, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
        cell2.Controls.Add(RegularExpressionValidator("FieldText", numbersOnlyMessage, "^[0-9]*$"))
      End If
    End If

    row.Controls.Add(cell1)
    row.Controls.Add(cell2)

    Return row
  End Function
  Private Function GenerateMultiLineTextControl(preview As Boolean, constituent As Constituent, language As Language) As HtmlTableRow
    Dim row = GenerateFormRow(preview, Setting("Hidden"))

    Dim cell1 = GenerateFormLabelCell(preview, Setting("Label"), Setting("HideLabel"), Setting("Required"))
    Dim cell2 = GenerateFormControlCell(preview)

    Dim max As Integer = 0
    If Setting("MaxCharacters").Length > 0 AndAlso Setting("MaxCharacters") > 0 Then
      max = Setting("MaxCharacters")
    End If

    cell2.Controls.Add(GenerateTextbox(TextBoxMode.MultiLine, "FieldText", Setting("DefaultValue"), Setting("Rows")))
    GenerateHelpText(cell2, Setting("HelpText"))

    If Setting("Required") And Not (preview) Then
      Dim requiredMessage = Replace(language.TextNumberDateDropdownRequired, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
      cell2.Controls.Add(RequiredFieldValidator("FieldText", requiredMessage))
    End If

    row.Controls.Add(cell1)
    row.Controls.Add(cell2)

    Return row
  End Function
  Private Function GenerateNumberFieldControl(preview As Boolean, language As Language) As HtmlTableRow
    Dim row = GenerateFormRow(preview, Setting("Hidden"))

    Dim cell1 = GenerateFormLabelCell(preview, Setting("Label"), Setting("HideLabel"), Setting("Required"))
    Dim cell2 = GenerateFormControlCell(preview)

    Dim textbox As TextBox = GenerateTextbox(TextBoxMode.SingleLine, "FieldText", Setting("DefaultValue"))

    If Setting("MaxLength").Length > 0 Then
      textbox.MaxLength = Setting("MaxLength")
    End If

    cell2.Controls.Add(textbox)

    GenerateHelpText(cell2, Setting("HelpText"))

    If Setting("Required") And Not (preview) Then
      Dim requiredMessage = Replace(language.TextNumberDateDropdownRequired, "[Field label]", Setting("Label").Trim(":"))
      cell2.Controls.Add(RequiredFieldValidator("FieldText", requiredMessage))
    End If

    If Setting("MinValue").Length > 0 And Setting("MaxValue").Length > 0 Then
      Dim invalidRangeMessage = Replace(language.NumberOutsideRange, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
      invalidRangeMessage = invalidRangeMessage.Replace("[Min]", Setting("MinValue")).Replace("[Max]", Setting("MaxValue"))
      cell2.Controls.Add(RangeValidator("FieldText", invalidRangeMessage, Setting("MinValue"), Setting("MaxValue")))
    End If

    If Setting("BlankValue").Length > 0 Then
      cell2.Controls.Add(New HiddenField() With {.ID = "HiddenBlankValue", .Value = Setting("BlankValue")})
    End If

    Dim invalidCharactersMessage = Replace(language.TextNumberDateInvalid, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
    cell2.Controls.Add(RegularExpressionValidator("FieldText", invalidCharactersMessage, "^[0-9" + Setting("AvailableCharacters") + "]*$"))

    row.Controls.Add(cell1)
    row.Controls.Add(cell2)

    Return row
  End Function
  Private Function GenerateCheckboxFieldControl(preview As Boolean, language As Language) As HtmlTableRow
    Dim row = GenerateFormRow(preview, Setting("Hidden"))

    Dim cell1 = GenerateFormLabelCell(preview, Setting("Label"), Setting("HideLabel"), Setting("Required"))
    Dim cell2 = GenerateFormControlCell(preview)

    cell2.Controls.Add(GenerateCheckbox("FieldCheck", Setting("Caption"), Setting("DefaultValue")))

    If Setting("Required") And Not (preview) Then
      Dim requiredMessage = Replace(language.CheckboxRequired, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
      cell2.Controls.Add(CheckboxValidator("FieldCheck", requiredMessage))
    End If

    row.Controls.Add(cell1)
    row.Controls.Add(cell2)

    Return row
  End Function
  Private Function GenerateMultipleChoiceControl(preview As Boolean, language As Language) As HtmlTableRow
    Dim row = GenerateFormRow(preview, False)

    Dim table = New HtmlTable()
    table.Attributes.Add("style", "width:100%")
    Dim row1 = New HtmlTableRow()
    row1.Attributes.Add("class", "FormBuilderMultipleChoiceList")
    Dim row2 = New HtmlTableRow()
    row2.Attributes.Add("class", "FormBuilderMultipleChoiceOther")

    Dim cell1 = GenerateFormLabelCell(preview, Setting("Label"), Setting("HideLabel"), Setting("Required"))
    Dim cell2 = GenerateFormControlCell(preview)

    Dim cList = New CheckBoxList()
    cList.ID = "CheckboxList"
    cList.RepeatColumns = Setting("Columns")
    Dim rList = New RadioButtonList()
    rList.ID = "RadioList"
    rList.RepeatColumns = Setting("Columns")

    Dim otherTextbox = GenerateTextbox(TextBoxMode.SingleLine, "FieldOtherText")

    Dim items As List(Of String) = New List(Of String)
    If Setting("CodeTable").Length > 0 Then
      items = CodeTableValues(Setting("CodeTable"))
    Else
      items = Setting("Values").Split(ControlChars.Lf).ToList
    End If

    If Setting("ShowOtherOption") Then
      items.Add(Setting("OtherLabel"))
    End If

    Dim i As Integer = 0
    While i < items.Count
      If Setting("RenderStyle") = 0 Then
        cList.Items.Add(items(i))
        If Setting("ShowOtherOption") Then
          If (i = items.Count - 1) Then
            cList.Items(i).Attributes.Add("class", "FormBuilderShowOtherOption")
          End If
          cList.Items(i).Attributes.Add("onclick", "ShowMultipleChoiceOtherOption($(this).closest('table').attr('id'), $(this).closest('table').find('.FormBuilderShowOtherOption input').is(':checked'));")
        End If
      Else
        rList.Items.Add(items(i))
        If Setting("ShowOtherOption") Then
          If (i = items.Count - 1) Then
            rList.Items(i).Attributes.Add("class", "FormBuilderShowOtherOption")
          End If
          rList.Items(i).Attributes.Add("onclick", "ShowMultipleChoiceOtherOption($(this).closest('table').attr('id'), $(this).closest('table').find('.FormBuilderShowOtherOption input').is(':checked'));")
        End If
      End If
      i = i + 1
    End While

    If rList.Items.Count > 0 Then
      cell2.Controls.Add(rList)
    End If
    If cList.Items.Count > 0 Then
      cell2.Controls.Add(cList)
    End If

    If Setting("Required") AndAlso Not (preview) Then
      Dim requiredMessage = Replace(language.MultipleChoiceRequired, "[Group label]", StripHTML(Setting("Label")).Trim(":"))
      If cList.Items.Count > 0 Then
        cell2.Controls.Add(CheckboxListValidator("CheckboxList", requiredMessage))
      ElseIf rList.Items.Count > 0 Then
        cell2.Controls.Add(RequiredFieldValidator("RadioList", requiredMessage))
      End If

    End If

    row1.Controls.Add(cell1)
    row1.Controls.Add(cell2)

    Dim cell3 = GenerateFormLabelCell(preview, "", False, False)
    Dim cell4 = GenerateFormControlCell(preview)

    cell4.Controls.Add(otherTextbox)

    GenerateRequiredFieldMarker(cell4, True)
    Dim fieldOtherRequiredMessage = Replace(language.MultipleChoiceOtherRequired, "[Group label]", Setting("Label").Trim(":"))
    fieldOtherRequiredMessage = Replace(fieldOtherRequiredMessage, "[Other label]", Setting("OtherLabel").Trim(":"))
    If cList.Items.Count > 0 Then
      cell4.Controls.Add(CheckboxListOtherOptionValidator("FieldOtherText", "CheckboxList", False, fieldOtherRequiredMessage))
    Else
      cell4.Controls.Add(CheckboxListOtherOptionValidator("FieldOtherText", "RadioList", True, fieldOtherRequiredMessage))
    End If


    row2.Controls.Add(cell3)
    row2.Controls.Add(cell4)

    table.Controls.Add(row1)

    If Setting("ShowOtherOption") Then
      table.Controls.Add(row2)
    End If

    Dim tableCell = New HtmlTableCell() With {.ColSpan = "2"}
    tableCell.Controls.Add(table)
    row.Controls.Add(tableCell)

    Return row
  End Function
  Private Function GenerateDropDownMenuControl(preview As Boolean, language As Language) As HtmlTableRow
    Dim row = GenerateFormRow(preview)

    Dim cell1 = GenerateFormLabelCell(preview, Setting("Label"), Setting("HideLabel"), Setting("Required"))
    Dim cell2 = GenerateFormControlCell(preview)

    Dim items As List(Of String) = New List(Of String)
    If Setting("CodeTable").Length > 0 Then
      items = CodeTableValues(Setting("CodeTable"))
    Else
      items = Setting("Values").Split(ControlChars.Lf).ToList
    End If

    cell2.Controls.Add(GenerateDropdownList("FieldDropdownList", items))

    GenerateHelpText(cell2, Setting("HelpText"))

    If Setting("Required") And Not (preview) Then
      Dim requiredMessage = Replace(language.TextNumberDateDropdownRequired, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
      cell2.Controls.Add(RequiredFieldValidator("FieldDropdownList", requiredMessage))
    End If

    row.Controls.Add(cell1)
    row.Controls.Add(cell2)

    Return row
  End Function
  Private Function GenerateEmailControl(preview As Boolean, constituent As Constituent, language As Language) As HtmlTableRow
    Dim row = GenerateFormRow(preview)

    Dim table = New HtmlTable()
    table.Attributes.Add("style", "width:100%")
    Dim row1 = New HtmlTableRow() 'Email address
    Dim row2 = New HtmlTableRow() 'Verify email address
    Dim row3 = New HtmlTableRow() 'Email address type

    If language.EmailAddressLabel = "" Then
      language.EmailAddressLabel = "Email address:"
      language.EmailVerifyLabel = "Verify email address:"
      language.EmailTypeLabel = "Email type:"
    End If
    Dim types = New List(Of String)
    If Setting("EmailTypes").Length > 0 Then
      types = Setting("EmailTypes").Split(ControlChars.Lf).ToList()
    End If
    If types.Count = 1 AndAlso Setting("AddTypeToLabel") Then
      language.EmailAddressLabel = types(0) + " " + LowercaseFirst(language.EmailAddressLabel)
    End If
    'Email address cells
    Dim cell1 = GenerateFormLabelCell(preview, language.EmailAddressLabel, False, Setting("Required"))
    Dim cell2 = GenerateFormControlCell(preview)
    'Verify email address cells
    Dim cell3 = GenerateFormLabelCell(preview, language.EmailVerifyLabel, False, Setting("Required"))
    Dim cell4 = GenerateFormControlCell(preview)
    'Email address type cells
    Dim cell5 = GenerateFormLabelCell(preview, language.EmailTypeLabel, False, Setting("TypeRequired"))
    Dim cell6 = GenerateFormControlCell(preview)

    If Setting("PrePopulate") > 0 Then
      If preview Then
        cell2.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldText", ePopulatedFieldType.Label(Setting("PrePopulate"))))
        cell4.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextVerify", ePopulatedFieldType.Label(Setting("PrePopulate"))))
      Else
        cell2.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldText", constituent.GetPrePopulateValue(ePopulatedFieldType.FieldType.EmailAddress)))
        cell4.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextVerify", constituent.GetPrePopulateValue(ePopulatedFieldType.FieldType.EmailAddress)))
      End If
    Else
      cell2.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldText"))
      cell4.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextVerify"))
    End If

    GenerateHelpText(cell2, Setting("HelpText"))

    If types.Count <= 1 Then
      cell6.Controls.Add(GenerateDropdownList("FieldEmailType", types, False))
      AddClass(row3, "HiddenRow")
    Else
      cell6.Controls.Add(GenerateDropdownList("FieldEmailType", types))
    End If

    If Not (preview) Then
      If Setting("Required") Then
        Dim requiredEmailMessage = Replace(language.EmailAddressRequired, "[Email address]", language.EmailAddressLabel.Trim(":"))
        cell2.Controls.Add(RequiredFieldValidator("FieldText", requiredEmailMessage))
      End If
      If Setting("ShowConfirmation") Then
        Dim requiredEmailVerifyMessage = Replace(language.EmailAddressDoNotMatch, "[Email address]", language.EmailAddressLabel.Trim(":"))
        requiredEmailVerifyMessage = Replace(requiredEmailVerifyMessage, "[Verify email address]", language.EmailVerifyLabel.Trim(":"))
        cell4.Controls.Add(CompareFieldsValidator("FieldText", "FieldTextVerify", requiredEmailVerifyMessage))
      End If
      If Setting("TypeRequired") AndAlso types.Count > 0 Then
        Dim requiredTypeMessage = Replace(language.EmailTypeRequired, "[Email type]", language.EmailTypeLabel.Trim(":"))
        cell6.Controls.Add(RequiredFieldValidator("FieldEmailType", requiredTypeMessage))
      End If

      Dim invalidEmailMessage = Replace(language.EmailAddressInvalid, "[Email address]", language.EmailAddressLabel.Trim(":"))
      cell2.Controls.Add(RegularExpressionValidator("FieldText", invalidEmailMessage, "^[ ]*[A-Za-z0-9\-_.'`!#$%&*+/=?^{|}~]+@[A-Za-z0-9][A-Za-z0-9\-]*(\.[A-Za-z0-9][A-Za-z0-9\-]*)*\.[A-Za-z]{2,6}[ ]*$"))
    End If

    row1.Controls.Add(cell1)
    row1.Controls.Add(cell2)
    row2.Controls.Add(cell3)
    row2.Controls.Add(cell4)
    row3.Controls.Add(cell5)
    row3.Controls.Add(cell6)

    If Setting("ShowConfirmation") = False Then
      row2.Visible = False
    End If

    table.Controls.Add(row1)
    table.Controls.Add(row2)
    table.Controls.Add(row3)

    Dim tableCell = New HtmlTableCell() With {.ColSpan = "2"}
    tableCell.Controls.Add(table)
    row.Controls.Add(tableCell)

    Return row
  End Function
  Private Function GenerateAddressControl(preview As Boolean, constituent As Constituent, language As Language) As HtmlTableRow
    Dim row = GenerateFormRow(preview)

    Dim table = New HtmlTable()
    table.Attributes.Add("style", "width:100%")
    Dim row1 = New HtmlTableRow() 'Address block
    Dim row2 = New HtmlTableRow() 'Country
    Dim row3 = New HtmlTableRow() 'City
    Dim row4 = New HtmlTableRow() 'State
    Dim row5 = New HtmlTableRow() 'ZIP code
    Dim row6 = New HtmlTableRow() 'Address type

    If language.AddressAddressLabel = "" Then
      language.AddressAddressLabel = "Address:"
      language.AddressCountryLabel = "Country:"
      language.AddressCityLabel = "City:"
      language.AddressStateLabel = "State:"
      language.AddressZIPLabel = "ZIP code:"
      language.AddressTypeLabel = "Address type:"
    End If
    Dim types = New List(Of String)
    If Setting("AddressTypes").Length > 0 Then
      types = Setting("AddressTypes").Split(ControlChars.Lf).ToList()
    End If
    If types.Count = 1 AndAlso Setting("AddTypeToLabel") Then
      language.AddressAddressLabel = types(0) + " " + LowercaseFirst(language.AddressAddressLabel)
    End If
    'Address block cells
    Dim cell1 = GenerateFormLabelCell(preview, language.AddressAddressLabel, False, Setting("AddressRequired"))
    Dim cell2 = GenerateFormControlCell(preview)
    'Country cells
    Dim cell3 = GenerateFormLabelCell(preview, language.AddressCountryLabel, False, Setting("AddressRequired"))
    Dim cell4 = GenerateFormControlCell(preview)
    'City cells
    Dim cell5 = GenerateFormLabelCell(preview, language.AddressCityLabel, False, Setting("AddressRequired"))
    Dim cell6 = GenerateFormControlCell(preview)
    'State cells
    '' See below
    Dim cell8 = GenerateFormControlCell(preview)
    'ZIP code cells
    Dim cell9 = GenerateFormLabelCell(preview, language.AddressZIPLabel, False, Setting("AddressRequired"))
    Dim cell10 = GenerateFormControlCell(preview)
    'Address type cells
    Dim cell11 = GenerateFormLabelCell(preview, language.AddressTypeLabel, False, Setting("TypeRequired"))
    Dim cell12 = GenerateFormControlCell(preview)

    Dim countryDropdownList As DropDownList
    Dim stateDropdownList As DropDownList
    If Setting("PrePopulate") > 0 Then
      If preview Then
        countryDropdownList = GenerateDropdownList("FieldListCountry", CountryValues(Setting("OnlyAllowUSA")), False, "")
      Else
        countryDropdownList = GenerateDropdownList("FieldListCountry", CountryValues(Setting("OnlyAllowUSA")), False, constituent.GetPrePopulateValue(ePopulatedFieldType.FieldType.AddressCountry))
      End If
    Else
      countryDropdownList = GenerateDropdownList("FieldListCountry", CountryValues(Setting("OnlyAllowUSA")), False)
    End If

    If countryDropdownList.Items.Count <= 1 Then
      AddClass(row2, "HiddenRow")
    Else
      countryDropdownList.AutoPostBack = True
      AddHandler countryDropdownList.SelectedIndexChanged, AddressOf countryDropdownList_SelectedIndexChanged
    End If

    If Setting("DefaultToUSA") AndAlso countryDropdownList.Items.Count > 1 AndAlso countryDropdownList.SelectedValue = Guid.Empty.ToString Then
      countryDropdownList.SelectedIndex = 1
    End If

    If Setting("PrePopulate") > 0 Then
      If preview Then
        cell2.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextAddress", ePopulatedFieldType.Label(ePopulatedFieldType.FieldType.AddressBlock)))
        cell6.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextCity", ePopulatedFieldType.Label(ePopulatedFieldType.FieldType.AddressCity)))
        stateDropdownList = GenerateDropdownList("FieldListState", StateValues(countryDropdownList.SelectedValue), False, "")
        cell10.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextZIPCode", ePopulatedFieldType.Label(ePopulatedFieldType.FieldType.AddressPostCode)))
      Else
        cell2.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextAddress", constituent.GetPrePopulateValue(ePopulatedFieldType.FieldType.AddressBlock)))
        cell6.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextCity", constituent.GetPrePopulateValue(ePopulatedFieldType.FieldType.AddressCity)))
        stateDropdownList = GenerateDropdownList("FieldListState", StateValues(countryDropdownList.SelectedValue), False, constituent.GetPrePopulateValue(ePopulatedFieldType.FieldType.AddressState))
        cell10.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextZIPCode", constituent.GetPrePopulateValue(ePopulatedFieldType.FieldType.AddressPostCode)))
      End If
    Else
      cell2.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextAddress"))
      cell6.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextCity"))
      stateDropdownList = GenerateDropdownList("FieldListState", StateValues(countryDropdownList.SelectedValue), False)
      cell10.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextZIPCode"))
    End If

    cell4.Controls.Add(countryDropdownList)

    cell8.Controls.Add(stateDropdownList)

    'State label cell (wait to define requirement until state dropdown is populated)
    Dim cell7 = GenerateFormLabelCell(preview, language.AddressStateLabel, False, (Setting("AddressRequired") AndAlso stateDropdownList.Items.Count > 1))

    If types.Count <= 1 Then
      cell12.Controls.Add(GenerateDropdownList("FieldAddressType", types, False))
      AddClass(row6, "HiddenRow")
    Else
      cell12.Controls.Add(GenerateDropdownList("FieldAddressType", types))
    End If

    If Not (preview) Then
      If Setting("AddressRequired") Then
        'Address
        Dim requiredAddressMessage = Replace(language.AddressAddressRequired, "[Address]", language.AddressAddressLabel.Trim(":"))
        cell2.Controls.Add(RequiredFieldValidator("FieldTextAddress", requiredAddressMessage))
        'Country
        Dim requiredCountryMessage = Replace(language.AddressCountryRequired, "[Country]", language.AddressCountryLabel.Trim(":"))
        cell4.Controls.Add(RequiredFieldValidator("FieldListCountry", requiredCountryMessage, Guid.Empty.ToString))
        'City
        Dim requiredCityMessage = Replace(language.AddressCityRequired, "[City]", language.AddressCityLabel.Trim(":"))
        cell6.Controls.Add(RequiredFieldValidator("FieldTextCity", requiredCityMessage))
        'State
        If stateDropdownList.Items.Count > 1 Then
          Dim requiredStateMessage = Replace(language.AddressStateRequired, "[State]", language.AddressStateLabel.Trim(":"))
          cell8.Controls.Add(RequiredFieldValidator("FieldListState", requiredStateMessage, Guid.Empty.ToString))
        End If
        'ZIP code
        Dim requiredZIPCodeMessage = Replace(language.AddressZIPRequired, "[ZIP code]", language.AddressZIPLabel.Trim(":"))
        cell10.Controls.Add(RequiredFieldValidator("FieldTextZIPCode", requiredZIPCodeMessage))
      End If
      If Setting("TypeRequired") AndAlso types.Count > 0 Then
        Dim requiredTypeMessage = Replace(language.AddressTypeRequired, "[Address type]", language.AddressTypeLabel.Trim(":"))
        cell12.Controls.Add(RequiredFieldValidator("FieldAddressType", requiredTypeMessage))
      End If

      Dim invalidZIPCodeMessage = Replace(language.AddressZIPInvalid, "[ZIP code]", language.AddressZIPLabel.Trim(":"))
      cell2.Controls.Add(RegularExpressionValidator("FieldTextZIPCode", invalidZIPCodeMessage, "^[0-9-]*$"))
    End If

    row1.Controls.Add(cell1)
    row1.Controls.Add(cell2)
    row2.Controls.Add(cell3)
    row2.Controls.Add(cell4)
    row3.Controls.Add(cell5)
    row3.Controls.Add(cell6)
    row4.Controls.Add(cell7)
    row4.Controls.Add(cell8)
    row5.Controls.Add(cell9)
    row5.Controls.Add(cell10)
    row6.Controls.Add(cell11)
    row6.Controls.Add(cell12)

    table.Controls.Add(row1)
    table.Controls.Add(row2)
    table.Controls.Add(row3)
    table.Controls.Add(row4)
    table.Controls.Add(row5)
    table.Controls.Add(row6)

    Dim tableCell = New HtmlTableCell() With {.ColSpan = "2"}
    If (preview) Then
      tableCell.Controls.Add(table)
    Else
      Dim update = New UpdatePanel()
      update.ID = "UpdatePanel"
      update.ContentTemplateContainer.Controls.Add(table)
      update.ChildrenAsTriggers = True
      update.RenderMode = UpdatePanelRenderMode.Block
      update.UpdateMode = UpdatePanelUpdateMode.Conditional
      tableCell.Controls.Add(update)
    End If

    row.Controls.Add(tableCell)

    Return row
  End Function
  Private Function GeneratePhoneControl(preview As Boolean, constituent As Constituent, language As Language) As HtmlTableRow
    Dim row = GenerateFormRow(preview)

    Dim regex As String = "^[0-9-" + Setting("AvailableCharacters") + "]*$"
    Dim regex2 As String = "[^0-9-" + Setting("AvailableCharacters") + "]"

    Dim table = New HtmlTable()
    table.Attributes.Add("style", "width:100%")
    Dim row1 = New HtmlTableRow() 'Phone number
    Dim row2 = New HtmlTableRow() 'Phone type

    If language.PhoneNumberLabel = "" Then
      language.PhoneNumberLabel = "Phone number:"
      language.PhoneTypeLabel = "Phone type:"
    End If
    Dim types = New List(Of String)
    If Setting("PhoneTypes").Length > 0 Then
      types = Setting("PhoneTypes").Split(ControlChars.Lf).ToList()
    End If
    If types.Count = 1 AndAlso Setting("AddTypeToLabel") Then
      language.PhoneNumberLabel = types(0) + " " + LowercaseFirst(language.PhoneNumberLabel)
    End If
    'Phone number cells
    Dim cell1 = GenerateFormLabelCell(preview, language.PhoneNumberLabel, False, Setting("Required"))
    Dim cell2 = GenerateFormControlCell(preview)
    'Phone type cells
    Dim cell3 = GenerateFormLabelCell(preview, language.PhoneTypeLabel, False, Setting("TypeRequired"))
    Dim cell4 = GenerateFormControlCell(preview)

    If Setting("PrePopulate") > 0 Then
      If preview Then
        cell2.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldText", ePopulatedFieldType.Label(Setting("PrePopulate"))))
      Else
        Dim phoneNumber = StripNonPermittedCharactersFromPhone(constituent.GetPrePopulateValue(ePopulatedFieldType.FieldType.PhoneNumber), regex2)
        cell2.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldText", phoneNumber))
      End If
    Else
      cell2.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldText"))
    End If

    GenerateHelpText(cell2, Setting("HelpText"))

    If types.Count <= 1 Then
      cell4.Controls.Add(GenerateDropdownList("FieldPhoneType", types, False))
      AddClass(row2, "HiddenRow")
    Else
      cell4.Controls.Add(GenerateDropdownList("FieldPhoneType", types))
    End If

    If Not (preview) Then
      If Setting("Required") Then
        Dim requiredPhoneMessage = Replace(language.PhoneNumberRequired, "[Phone number]", language.PhoneNumberRequired.Trim(":"))
        cell2.Controls.Add(RequiredFieldValidator("FieldText", requiredPhoneMessage))
      End If

      Dim invalidPhoneMessage = Replace(language.PhoneNumberInvalid, "[Phone number]", language.PhoneNumberLabel.Trim(":"))
      cell2.Controls.Add(RegularExpressionValidator("FieldText", invalidPhoneMessage, regex))

      If Setting("TypeRequired") AndAlso types.Count > 0 Then
        Dim requiredTypeMessage = Replace(language.PhoneTypeRequired, "[Email type]", language.PhoneTypeLabel.Trim(":"))
        cell4.Controls.Add(RequiredFieldValidator("FieldPhoneType", requiredTypeMessage))
      End If

    End If

    row1.Controls.Add(cell1)
    row1.Controls.Add(cell2)
    row2.Controls.Add(cell3)
    row2.Controls.Add(cell4)

    table.Controls.Add(row1)
    table.Controls.Add(row2)

    Dim tableCell = New HtmlTableCell() With {.ColSpan = "2"}
    tableCell.Controls.Add(table)
    row.Controls.Add(tableCell)

    Return row
  End Function
  Private Function GenerateDateTimeFieldControl(preview As Boolean, language As Language) As HtmlTableRow
    Dim row = GenerateFormRow(preview)

    Dim cell1 = GenerateFormLabelCell(preview, Setting("Label"), Setting("HideLabel"), Setting("Required"))
    Dim cell2 = GenerateFormControlCell(preview)

    If Setting("DateType") = 0 Then 'Date
      If Setting("SplitMonthDayYear") Then
        Dim table = New HtmlTable()
        Dim row1 = New HtmlTableRow()

        Dim monthFieldCell = New HtmlTableCell()
        monthFieldCell.Attributes.Add("class", "DateFieldCell MonthFieldCell")
        Dim dayFieldCell = New HtmlTableCell()
        dayFieldCell.Attributes.Add("class", "DateFieldCell DayFieldCell")
        Dim yearFieldCell = New HtmlTableCell()
        yearFieldCell.Attributes.Add("class", "DateFieldCell YearFieldCell")

        Dim monthField = GenerateTextbox(TextBoxMode.SingleLine, "FieldTextMonth")
        Dim dayField = GenerateTextbox(TextBoxMode.SingleLine, "FieldTextDay")
        Dim yearField = GenerateTextbox(TextBoxMode.SingleLine, "FieldTextYear")

        monthFieldCell.Controls.Add(monthField)
        GenerateHelpText(monthFieldCell, "MM")
        dayFieldCell.Controls.Add(dayField)
        GenerateHelpText(dayFieldCell, "DD")
        yearFieldCell.Controls.Add(yearField)
        GenerateHelpText(yearFieldCell, "YYYY")

        Dim invalidMonthMessage = Replace(language.DateMonthInvalid, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
        monthFieldCell.Controls.Add(RangeValidator("FieldTextMonth", invalidMonthMessage, 1, 12))

        Dim invalidDayMessage = Replace(language.DateDayInvalid, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
        dayFieldCell.Controls.Add(RangeValidator("FieldTextDay", invalidDayMessage, 1, 31))

        Dim invalidYearMessage = Replace(language.DateYearInvalid, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
        yearFieldCell.Controls.Add(RangeValidator("FieldTextYear", invalidYearMessage, 1901, 2999))

        row1.Controls.Add(monthFieldCell)
        row1.Controls.Add(dayFieldCell)
        row1.Controls.Add(yearFieldCell)

        table.Controls.Add(row1)

        cell2.Controls.Add(table)
      Else
        cell2.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextDate"))
        GenerateHelpText(cell2, "MM/DD/YYYY")
        Dim invalidDateMessage = Replace(language.DateDateInvalid, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
        cell2.Controls.Add(RegularExpressionValidator("FieldTextDate", invalidDateMessage, "^([0-9]{1,2})[./-]+([0-9]{1,2})[./-]+([0-9]{2}|[0-9]{4})$"))
      End If
    Else
      Dim invalidTimeMessage = Replace(language.DateTimeInvalid, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
      If Setting("DateType") = 1 Then
        cell2.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextTime"))
        GenerateHelpText(cell2, "HH:MM AM/PM")
        invalidTimeMessage = Replace(invalidTimeMessage, "[Time format]", "HH:MM AM/PM")
        cell2.Controls.Add(RegularExpressionValidator("FieldTextTime", invalidTimeMessage, "^([0-9]{1,2})[:]+([0-9]{1,2})[ ]+([AaPp][Mm])$"))
      Else
        cell2.Controls.Add(GenerateTextbox(TextBoxMode.SingleLine, "FieldTextTime"))
        GenerateHelpText(cell2, "HH:MM")
        invalidTimeMessage = Replace(invalidTimeMessage, "[Time format]", "HH:MM")
        cell2.Controls.Add(RegularExpressionValidator("FieldTextTime", invalidTimeMessage, "^([0-9]{1,2})[:]+([0-9]{1,2})$"))
      End If
    End If

    row.Controls.Add(cell1)
    row.Controls.Add(cell2)

    Return row
  End Function
  Private Function GenerateFileUploadControl(preview As Boolean, language As Language) As HtmlTableRow
    Dim row = GenerateFormRow(preview)

    Dim cell1 = GenerateFormLabelCell(preview, Setting("Label"), Setting("HideLabel"), Setting("Required"))
    Dim cell2 = GenerateFormControlCell(preview)

    If Setting("FileType") = 0 Then
      cell2.Controls.Add(GenerateFileUpload("FileUpload", "image/jpeg,image/png,image/gif"))
    ElseIf Setting("FileType") = 1 Then
      cell2.Controls.Add(GenerateFileUpload("FileUpload", "application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,text/csv,text/plain,application/rtf"))
    End If

    GenerateHelpText(cell2, Setting("HelpText"))

    If Setting("Required") And Not (preview) Then
      Dim requiredMessage = Replace(language.FileUploadRequired, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
      cell2.Controls.Add(RequiredFieldValidator("FileUpload", requiredMessage))
    End If

    Dim size As Integer = Setting("MaxFileSize") * 1024 * 1024
    Dim fileTooLargeMessage = Replace(language.FileUploadTooLarge, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
    fileTooLargeMessage = Replace(fileTooLargeMessage, "[Max file size]", Setting("MaxFileSize"))
    cell2.Controls.Add(FileUploadSizeValidator("FileUpload", fileTooLargeMessage, size))

    Dim types As String = ""
    If Setting("FileType") = 0 Then
      types = "(.+\.([Jj][Pp][Gg])|.+\.([Jj][Pp][Ee][Gg])|.+\.([Pp][Nn][Gg])|.+\.([Gg][Ii][Ff]))"
    ElseIf Setting("FileType") = 1 Then
      types = "(.+\.([Pp][Dd][Ff])|.+\.([Dd][Oo][Cc][Xx])|.+\.([Dd][Oo][Cc])|.+\.([Xx][Ll][Ss][Xx])|.+\.([Xx][Ll][Ss])|.+\.([Cc][Ss][Vv])|.+\.([Tt][Xx][Tt])|.+\.([Rr][Tt][Ff]))"
    End If
    Dim fileTypeInvalidMessage = Replace(language.FileUploadInvalid, "[Field label]", StripHTML(Setting("Label")).Trim(":"))
    cell2.Controls.Add(RegularExpressionValidator("FileUpload", fileTypeInvalidMessage, types))

    row.Controls.Add(cell1)
    row.Controls.Add(cell2)

    Return row
  End Function

  Private Function GenerateTextbox(mode As TextBoxMode, id As String, Optional value As String = "", Optional rows As Integer = 0) As TextBox
    Dim textbox = New TextBox()
    textbox.ID = id
    textbox.Text = value

    If rows > 0 Then
      textbox.TextMode = TextBoxMode.MultiLine
      textbox.Rows = rows
    Else
      textbox.TextMode = mode
    End If

    Return textbox
  End Function
  Private Function GenerateDropdownList(id As String, values As List(Of ListItem), Optional firstIsBlank As Boolean = True, Optional value As String = "") As DropDownList
    Dim dropdownlist = GenerateDropdownList(id, firstIsBlank)

    For Each item In values
      dropdownlist.Items.Add(item)
    Next

    If value.Length > 0 And dropdownlist.Items.FindByValue(value) IsNot Nothing Then
      dropdownlist.SelectedValue = value
    End If

    Return dropdownlist
  End Function
  Private Function GenerateDropdownList(id As String, values As List(Of String), Optional firstIsBlank As Boolean = True, Optional value As String = "") As DropDownList
    Dim list = New List(Of ListItem)
    For Each item In values
      list.Add(New ListItem(item))
    Next

    Return GenerateDropdownList(id, list, firstIsBlank, value)
  End Function
  Private Function GenerateDropdownList(id As String, Optional firstIsBlank As Boolean = True) As DropDownList
    Dim dropdownlist = New DropDownList()
    dropdownlist.ID = id

    If firstIsBlank Then
      dropdownlist.Items.Add("")
    End If

    Return dropdownlist
  End Function
  Private Function GenerateCheckbox(id As String, caption As String, checked As Boolean) As CheckBox
    Dim checkbox = New CheckBox()
    checkbox.ID = id
    checkbox.Text = caption
    checkbox.Checked = checked
    Return checkbox
  End Function
  Private Function GenerateFileUpload(id As String, accept As String) As FileUpload
    Dim fileUpload = New FileUpload()
    fileUpload.ID = id
    fileUpload.Attributes.Add("accept", accept)

    Return fileUpload
  End Function

  Private Function GenerateFormRow(preview As Boolean, Optional hidden As Boolean = False) As HtmlTableRow
    Dim row = New HtmlTableRow()
    If preview Then
      AddClass(row, "PreviewRow")
    Else
      AddClass(row, "FormBuilderRow")
    End If

    If hidden Then
      AddClass(row, "HiddenRow")
    End If

    Return row
  End Function
  Private Function GenerateFormLabelCell(preview As Boolean, text As String, hidden As Boolean, required As Boolean, Optional id As String = "FieldLabel") As HtmlTableCell
    Dim cell = New HtmlTableCell()

    If preview Then
      AddClass(cell, "PreviewCaptionCell")
    Else
      AddClass(cell, "BBFieldCaption FormBuilderFieldCaption")
    End If

    If hidden Then
      AddClass(cell, "HiddenLabel")
    End If

    cell.Controls.Add(FieldLabel(text))

    GenerateRequiredFieldMarker(cell, required)

    Return cell
  End Function
  Private Function GenerateFormControlCell(preview As Boolean, Optional css As String = "") As HtmlTableCell
    Dim cell = New HtmlTableCell()

    If preview Then
      AddClass(cell, "PreviewControlCell")
    Else
      AddClass(cell, "BBFieldControlCell FormBuilderFieldControlCell")
    End If

    AddClass(cell, css)

    Return cell
  End Function
  Private Function GenerateDetailCell(preview As Boolean, Optional css As String = "") As HtmlTableCell
    Dim cell = New HtmlTableCell()
    cell.Attributes.Add("colspan", "2")

    If preview Then
      AddClass(cell, "PreviewDetailCell")
    Else
      AddClass(cell, "FormBuilderDetailsCell")
    End If

    AddClass(cell, css)

    Return cell
  End Function
  Private Sub AddClass(ByRef cell As HtmlTableCell, css As String)
    If css.Length > 0 Then
      If cell.Attributes("class") <> "" Then
        cell.Attributes.Add("class", cell.Attributes("class") + " " + css)
      Else
        cell.Attributes.Add("class", css)
      End If
    End If
  End Sub
  Private Sub AddClass(ByRef row As HtmlTableRow, css As String)
    If css.Length > 0 Then
      If row.Attributes("class") <> "" Then
        row.Attributes.Add("class", row.Attributes("class") + " " + css)
      Else
        row.Attributes.Add("class", css)
      End If
    End If
  End Sub
  Private Sub AddClass(ByRef control As HtmlGenericControl, css As String)
    If css.Length > 0 Then
      If control.Attributes("class") <> "" Then
        control.Attributes.Add("class", control.Attributes("class") + " " + css)
      Else
        control.Attributes.Add("class", css)
      End If
    End If
  End Sub
  Private Sub GenerateRequiredFieldMarker(ByRef cell As HtmlTableCell, required As Boolean)
    If required Then
      cell.Controls.Add(RequiredFieldMarker)
    End If
  End Sub
  Private Sub GenerateHelpText(ByRef control As Control, text As String)
    If text.Length > 0 Then
      Dim label = New Label()
      label.Text = text
      label.CssClass = "FormBuilderHelpText"
      control.Controls.Add(label)
    End If
  End Sub
  Private Function RequiredFieldValidator(id As String, message As String, Optional initial As String = "") As RequiredFieldValidator
    Dim validator = New RequiredFieldValidator()
    validator.ControlToValidate = id
    validator.ErrorMessage = message
    validator.InitialValue = initial
    validator.ValidationGroup = ValidationGroup
    validator.Display = ValidatorDisplay.None

    Return validator
  End Function
  Private Function CompareFieldsValidator(id As String, id2 As String, message As String) As CompareValidator
    Dim validator = New CompareValidator()
    validator.ControlToValidate = id
    validator.ControlToCompare = id2
    validator.ErrorMessage = message
    validator.ValidationGroup = ValidationGroup
    validator.Display = ValidatorDisplay.None

    Return validator
  End Function
  Private Function RangeValidator(id As String, message As String, min As Integer, max As Integer) As RangeValidator
    Dim validator = New RangeValidator()
    validator.ControlToValidate = id
    validator.ErrorMessage = message
    validator.ValidationGroup = ValidationGroup
    validator.Display = ValidatorDisplay.None

    validator.MinimumValue = min
    validator.MaximumValue = max

    Return validator
  End Function
  Private Function RegularExpressionValidator(id As String, message As String, expression As String) As RegularExpressionValidator
    Dim validator = New RegularExpressionValidator()
    validator.ControlToValidate = id
    validator.ErrorMessage = message
    validator.ValidationGroup = ValidationGroup
    validator.Display = ValidatorDisplay.None
    '^[0-9-]*$ (0123456789-)
    validator.ValidationExpression = expression

    Return validator
  End Function
  Private Function CheckboxValidator(id As String, message As String) As CustomValidator
    Dim validator = New CustomValidator()
    validator.ControlId = id
    validator.ErrorMessage = message
    validator.ValidationGroup = ValidationGroup
    validator.Display = ValidatorDisplay.None
    validator.ClientValidationFunction = "ValidateCheckBox"
    AddHandler validator.ServerValidate, AddressOf ValidateCheckbox

    Return validator
  End Function
  Private Function CheckboxListValidator(id As String, message As String) As CustomValidator
    Dim validator = New CustomValidator()
    validator.ControlId = id
    validator.ErrorMessage = message
    validator.ValidationGroup = ValidationGroup
    validator.Display = ValidatorDisplay.None
    validator.ClientValidationFunction = "ValidateCheckBoxList"
    AddHandler validator.ServerValidate, AddressOf ValidateCheckboxList

    Return validator
  End Function
  Private Function CheckboxListOtherOptionValidator(id As String, listId As String, radio As Boolean, message As String) As CustomValidator
    Dim validator = New CustomValidator()
    validator.ControlId = id
    validator.Attributes.Add("list-id", listId)
    validator.Attributes.Add("is-radio", radio.ToString)
    validator.ErrorMessage = message
    validator.ValidationGroup = ValidationGroup
    validator.Display = ValidatorDisplay.None
    validator.ClientValidationFunction = "ValidateCheckboxListOtherOption"
    AddHandler validator.ServerValidate, AddressOf ValidateCheckboxListOtherOption
    Return validator
  End Function
  Private Function FileUploadSizeValidator(id As String, message As String, size As Integer) As CustomValidator
    Dim validator = New CustomValidator()
    validator.ControlId = size.ToString
    validator.Attributes.Add("maxsize", size.ToString)
    validator.ControlToValidate = id
    validator.ErrorMessage = message
    validator.ValidationGroup = ValidationGroup
    validator.Display = ValidatorDisplay.None
    validator.ClientValidationFunction = "ValidateFileUploadSize"
    AddHandler validator.ServerValidate, AddressOf ValidateFileUploadSize
    Return validator
  End Function
  Private Function FieldLabel(text As String) As Label
    Dim label = New Label()

    label.Text = text

    Return label
  End Function
  Private ReadOnly Property RequiredFieldMarker() As Label
    Get
      Dim label As Label = New Label()
      label.CssClass = "RequiredFieldMarker"
      label.Text = "*"

      Return label
    End Get
  End Property
  Private Function LowercaseFirst(value As String) As String
    If String.IsNullOrEmpty(value) Then
      Return String.Empty
    End If
    Return Char.ToLower(value(0)) + value.Substring(1)
  End Function
  Private Function StripNonPermittedCharactersFromPhone(value As String, exp As String) As String
    Dim number = String.Empty
    Dim exp2 = New Regex(exp)
    Dim exp3 = New Regex("[^0-9]")

    Dim number1 = exp2.Replace(value, String.Empty) 'Strip all non-permitted characters
    Dim number2 = exp3.Replace(value, String.Empty) 'Strip to just numbers

    If number2.Length = 10 Then
      number = number2.Substring(0, 3) + "-" + number2.Substring(2, 3) + "-" + number2.Substring(6, 4)
    Else
      number = number1
    End If

    Return number
  End Function
  Private Function StripHTML(value As String) As String
    Return Regex.Replace(value, "<.*?>", String.Empty)
  End Function
  Protected Sub ValidateCheckbox(sender As Object, e As ServerValidateEventArgs)
    Dim validator = DirectCast(sender, CustomValidator)
    Dim cb = DirectCast(validator.Parent.FindControl(validator.ControlId), CheckBox)

    Dim isValueSelected As Boolean = cb.Checked

    e.IsValid = isValueSelected
  End Sub
  Protected Sub ValidateCheckboxList(sender As Object, e As ServerValidateEventArgs)
    Dim validator = DirectCast(sender, CustomValidator)
    Dim cblist = DirectCast(validator.Parent.FindControl(validator.ControlId), CheckBoxList)

    Dim isValueSelected As Boolean = False
    For Each item As ListItem In cblist.Items
      If item.Selected Then
        isValueSelected = True
        Exit For
      End If
    Next
    e.IsValid = isValueSelected
  End Sub
  Protected Sub ValidateCheckboxListOtherOption(sender As Object, e As ServerValidateEventArgs)
    e.IsValid = True

    Dim validator = DirectCast(sender, CustomValidator)
    Dim textbox = DirectCast(validator.Parent.FindControl(validator.ControlId), TextBox)
    Dim list = validator.Parent.Parent.Parent.FindControl(validator.Attributes("list-id"))

    If validator.Attributes("is-radio") Then
      Dim rList = DirectCast(list, RadioButtonList).Items()
      Dim rListLast = rList.Item(rList.Count - 1)
      If rListLast.Attributes("class") = "FormBuilderShowOtherOption" Then
        If rListLast.Selected AndAlso textbox.Text.Length = 0 Then
          e.IsValid = False
        End If
      End If
    Else
      Dim cList = DirectCast(list, CheckBoxList).Items()
      Dim cListLast = cList.Item(cList.Count - 1)
      If cListLast.Attributes("class") = "FormBuilderShowOtherOption" Then
        If cListLast.Selected AndAlso textbox.Text.Length = 0 Then
          e.IsValid = False
        End If
      End If
    End If
  End Sub
  Protected Sub ValidateFileUploadSize(sender As Object, e As ServerValidateEventArgs)
    e.IsValid = True

    Dim validator As CustomValidator = sender
    Dim fileUpload As FileUpload = validator.Parent.FindControl(validator.ControlToValidate)

    If fileUpload.FileBytes.Length > Integer.Parse(validator.ControlId) Then
      e.IsValid = False
    End If
  End Sub
  Private Function CountryValues(onlyUSA As Boolean) As List(Of ListItem)
    Dim list = New List(Of ListItem)

    If onlyUSA = False Then
      list.Add(New ListItem("", Guid.Empty.ToString))
    End If

    Dim Countries() As Blackbaud.AppFx.WebAPI.SimpleDataListsData
    Dim parameter As New Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem()
    parameter.SetValue("ONLYDEFAULT", onlyUSA)
    Countries = Blackbaud.AppFx.WebAPI.SimpleDataLists.LoadSimpleDataList(provider, Guid.Parse("dd585637-29df-42fe-9863-85e4c02d83c6"), parameter)

    For Each item In Countries
      list.Add(New ListItem(item.Label, item.Value))
    Next

    Return list
  End Function
  Private Function StateValues(country As String) As List(Of ListItem)
    Dim list = New List(Of ListItem)

    If country <> Guid.Empty.ToString Then
      list.Add(New ListItem("", Guid.Empty.ToString))
    End If

    Dim States() As Blackbaud.AppFx.WebAPI.SimpleDataListsData
    Dim parameter As New Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem()
    parameter.SetValue("COUNTRYID", country)
    States = Blackbaud.AppFx.WebAPI.SimpleDataLists.LoadSimpleDataList(provider, Guid.Parse("eabb9215-c596-4761-a6de-564931cb78c9"), parameter)

    For Each item In States
      list.Add(New ListItem(item.Label, item.Value))
    Next

    Return list
  End Function
  Private Function CodeTableValues(table As String) As List(Of String)
    Dim list = New List(Of String)

    Try
      Dim Entries() As CodeTableEntryItem
      Entries = Blackbaud.AppFx.WebAPI.CodeTableServices.GetList(provider, table, CodeTableEntryIncludeOption.IncludeActiveOnly)

      For Each item In Entries
        list.Add(item.Description)
      Next

    Catch ex As Exception
    End Try

    Return list
  End Function
  Protected Sub countryDropdownList_SelectedIndexChanged(sender As Object, e As EventArgs)
    Dim country = DirectCast(sender, DropDownList)
    Dim state = DirectCast(country.Parent.Parent.Parent.FindControl("FieldListState"), DropDownList)

    state.Items.Clear()

    For Each item In StateValues(country.SelectedValue)
      state.Items.Add(New ListItem(item.Text, item.Value))
    Next

  End Sub
End Class
