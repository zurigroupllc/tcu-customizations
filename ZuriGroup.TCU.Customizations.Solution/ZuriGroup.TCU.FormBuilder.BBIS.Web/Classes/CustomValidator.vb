﻿Public Class CustomValidator
  Inherits System.Web.UI.WebControls.CustomValidator
  Public Property ControlId() As String
    Get
      Return m_ControlId
    End Get
    Set(value As String)
      m_ControlId = value
    End Set
  End Property
  Private m_ControlId As String

  Protected Overrides Sub OnLoad(e As EventArgs)
    If Enabled Then
      Page.ClientScript.RegisterExpandoAttribute(ClientID, "ControlId", ControlId)
    End If

    MyBase.OnLoad(e)
  End Sub
End Class