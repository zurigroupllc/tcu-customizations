﻿Public Class Constituent
  Public Property ConstituentId As String
  Public Property FirstName As String
  Public Property MiddleName As String
  Public Property MiddleInitial As String
  Public Property LastName As String
  Public Property MaidenName As String
  Public Property Nickname As String
  Public Property PrimaryPhoneNumber As String
  Public Property PrimaryEmailAddress As String
  Public Property PrimaryAddressBlock As String
  Public Property PrimaryAddressCity As String
  Public Property PrimaryAddressStateId As String
  Public Property PrimaryAddressState As String
  Public Property PrimaryAddressCountryId As String
  Public Property PrimaryAddressCountry As String
  Public Property PrimaryAddressPostCode As String
  Public Property ClassYear As String

  Public Sub New()
    ConstituentId = ""
    FirstName = ""
    MiddleName = ""
    MiddleInitial = ""
    LastName = ""
    MaidenName = ""
    Nickname = ""
    PrimaryPhoneNumber = ""
    PrimaryEmailAddress = ""
    PrimaryAddressBlock = ""
    PrimaryAddressCity = ""
    PrimaryAddressStateId = Guid.Empty.ToString
    PrimaryAddressState = ""
    PrimaryAddressCountryId = Guid.Empty.ToString
    PrimaryAddressCountry = ""
    PrimaryAddressPostCode = ""
    ClassYear = ""
  End Sub

  Public Function GetPrePopulateValue(type As ePopulatedFieldType.FieldType) As String
    Select Case type
      Case ePopulatedFieldType.FieldType.FirstName
        Return FirstName
      Case ePopulatedFieldType.FieldType.MiddleName
        Return MiddleName
      Case ePopulatedFieldType.FieldType.MiddleInitial
        Return MiddleInitial
      Case ePopulatedFieldType.FieldType.LastName
        Return LastName
      Case ePopulatedFieldType.FieldType.MaidenName
        Return MaidenName
      Case ePopulatedFieldType.FieldType.Nickname
        Return Nickname
      Case ePopulatedFieldType.FieldType.ClassYear
        Return ClassYear
      Case ePopulatedFieldType.FieldType.AddressBlock
        Return PrimaryAddressBlock
      Case ePopulatedFieldType.FieldType.AddressCountry
        Return PrimaryAddressCountryId
      Case ePopulatedFieldType.FieldType.AddressCity
        Return PrimaryAddressCity
      Case ePopulatedFieldType.FieldType.AddressState
        Return PrimaryAddressStateId
      Case ePopulatedFieldType.FieldType.AddressPostCode
        Return PrimaryAddressPostCode
      Case ePopulatedFieldType.FieldType.PhoneNumber
        Return PrimaryPhoneNumber
      Case ePopulatedFieldType.FieldType.EmailAddress
        Return PrimaryEmailAddress
      Case Else
        Return ""
    End Select

  End Function
End Class
