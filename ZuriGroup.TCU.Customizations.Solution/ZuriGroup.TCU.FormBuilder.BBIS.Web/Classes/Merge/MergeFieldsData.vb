﻿Public Class MergeFieldsData
  Implements Blackbaud.Web.Content.Core.Data.IDataProvider2

  Private Property Data As MergeData()

  Public Property GetEFieldType As Integer Implements Blackbaud.Web.Content.Core.Data.IDataProvider.GetEFieldType
    Get
      Return MergeFieldsProvider.FIELD_TYPE
    End Get
    Set(value As Integer)
    End Set
  End Property

  Public Sub New(ByVal data As MergeData())
    Me.Data = data
  End Sub

  Public Function GetFieldById(fieldId As Integer, attributeId As Integer) As String Implements Blackbaud.Web.Content.Core.Data.IDataProvider.GetFieldById
    Return GetFieldById(fieldId, attributeId, 0)
  End Function

  Public Function GetFieldById(fieldId As Integer, attributeId As Integer, rowNumber As Integer) As String Implements Blackbaud.Web.Content.Core.Data.IDataProvider.GetFieldById
    Return GetFieldById(fieldId, attributeId, CStr(rowNumber))
  End Function
  Public Function GetFieldById(fieldId As Integer, attributeId As Integer, rowNumber As String) As String Implements Blackbaud.Web.Content.Core.Data.IDataProvider2.GetFieldById

    If 0 = rowNumber.Trim.Length Then
      rowNumber = "0"
    End If

    Dim rowNum() As Integer = Array.ConvertAll(rowNumber.Split(CChar("_")), New Converter(Of String, Integer)(AddressOf Convert.ToInt32))

    Try
      Dim max As Integer = rowNum.Length - 1

      Select Case CType(fieldId, MergeFieldsProvider.eMergeField)
        Case MergeFieldsProvider.eMergeField.SubmittedFields
          Return ""
          'Case MergeFieldsProvider.eMergeField.FieldLabel
          'Return Me.Data(0).Fields(rowNum(max)).Label
          'Case MergeFieldsProvider.eMergeField.FieldValue
          'Return Me.Data(0).Fields(rowNum(max)).Value
          'Case MergeFieldsProvider.eMergeField.FieldType
          'Return Me.Data(0).Fields(rowNum(max)).Type
        Case MergeFieldsProvider.eMergeField.RecipientDisplayName
          Return Me.Data(0).RecipientDisplayName
        Case MergeFieldsProvider.eMergeField.FormName
          Return Me.Data(0).FormName
        Case MergeFieldsProvider.eMergeField.PageName
          Return Me.Data(0).PageName
        Case MergeFieldsProvider.eMergeField.SiteName
          Return Me.Data(0).SiteName
        Case MergeFieldsProvider.eMergeField.DateSubmitted
          Return Me.Data(0).DateSubmitted
        Case MergeFieldsProvider.eMergeField.ConstituentLookupId
          Return Me.Data(0).ConstituentLookupId
      End Select

    Catch ex As Exception
    End Try
    Return String.Empty
  End Function

#Region "Ignore"
  Public Function GetChildRecords(fieldCategory As Integer, fieldIds As String) As DataTable Implements Blackbaud.Web.Content.Core.Data.IDataProvider.GetChildRecords
    Return Nothing
  End Function


  Public Function GetFieldNameById(fieldId As Integer, attributeId As Integer, Optional fullyQualified As Boolean = False) As String Implements Blackbaud.Web.Content.Core.Data.IDataProvider.GetFieldNameById
    Return Nothing
  End Function

  Public Function GetMultiRowRecordCount(fieldCategory As Integer) As Integer Implements Blackbaud.Web.Content.Core.Data.IDataProvider.GetMultiRowRecordCount

  End Function

  Public Function IsFieldPrivate(fieldId As Integer, attributeId As Integer) As Boolean Implements Blackbaud.Web.Content.Core.Data.IDataProvider.IsFieldPrivate

  End Function

  Public Function NewRecord(fieldCategory As Integer) As Integer Implements Blackbaud.Web.Content.Core.Data.IDataProvider.NewRecord

  End Function

  Public Sub SetFieldById(fieldId As Integer, attributeId As Integer, rowNumber As Integer, value As String) Implements Blackbaud.Web.Content.Core.Data.IDataProvider.SetFieldById

  End Sub

  Public Sub SetFieldById(fieldId As Integer, attributeId As Integer, value As String) Implements Blackbaud.Web.Content.Core.Data.IDataProvider.SetFieldById

  End Sub

  Public Sub SetFieldById(fieldId As Integer, attributeId As Integer, value() As String) Implements Blackbaud.Web.Content.Core.Data.IDataProvider.SetFieldById

  End Sub
#End Region

End Class
