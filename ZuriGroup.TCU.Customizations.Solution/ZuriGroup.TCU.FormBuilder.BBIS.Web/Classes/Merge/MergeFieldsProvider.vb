﻿Partial Public Class MergeFieldsProvider
  Inherits Blackbaud.Web.Content.Core.Data.BaseFieldProvider

  Public Const FIELD_TYPE = 199621441

  Protected Overrides Function GetFieldData() As DataSet

    If m_fieldData Is Nothing Then
      m_fieldData = New DataSet
      CreateFieldsTable(m_fieldData)

      Dim table As System.Data.DataTable = m_fieldData.Tables.Item("Fields")

      AddField(table, FIELD_TYPE, eMergeField.Category_Details)
      AddField(table, FIELD_TYPE, eMergeField.Category_Fields)

      AddField(table, FIELD_TYPE, eMergeField.SubmittedFields, eMergeField.Category_Fields)

      'AddField(table, FIELD_TYPE, eMergeField.BeginSubmittedFields, eMergeField.Category_Fields)
      'AddField(table, FIELD_TYPE, eMergeField.FieldLabel, eMergeField.Category_Fields)
      'AddField(table, FIELD_TYPE, eMergeField.FieldValue, eMergeField.Category_Fields)
      'AddField(table, FIELD_TYPE, eMergeField.FieldType, eMergeField.Category_Fields)
      'AddField(table, FIELD_TYPE, eMergeField.EndSubmittedFields, eMergeField.Category_Fields)

      AddField(table, FIELD_TYPE, eMergeField.RecipientDisplayName, eMergeField.Category_Details)
      AddField(table, FIELD_TYPE, eMergeField.FormName, eMergeField.Category_Details)
      AddField(table, FIELD_TYPE, eMergeField.PageName, eMergeField.Category_Details)
      AddField(table, FIELD_TYPE, eMergeField.SiteName, eMergeField.Category_Details)
      AddField(table, FIELD_TYPE, eMergeField.DateSubmitted, eMergeField.Category_Details)
      AddField(table, FIELD_TYPE, eMergeField.ConstituentLookupId, eMergeField.Category_Details)

      m_fieldData.Relations.Add("FieldsTree", m_fieldData.Tables("Fields").Columns("FieldId"), m_fieldData.Tables("Fields").Columns("ParentFieldId"))
    End If

    Return m_fieldData
  End Function

  Public Overrides Function GetFieldName(fieldId As Integer, attributeId As Integer, Optional fullyQualified As Boolean = False) As String
    Select Case CType(fieldId, eMergeField)
      Case eMergeField.Category_Fields
        Return "Fields"
      Case eMergeField.Category_Details
        Return "Details"
      Case eMergeField.SubmittedFields
        Return "Submitted fields"
        'Case eMergeField.BeginSubmittedFields
        '  Return "Field loop.Begin fields"
        'Case eMergeField.EndSubmittedFields
        '  Return "Field loop.End fields"
        'Case eMergeField.FieldLabel
        '  Return "Field label"
        'Case eMergeField.FieldValue
        '  Return "Field value"
        'Case eMergeField.FieldType
        '  Return "Field type"
      Case eMergeField.RecipientDisplayName
        Return "Recipient display name"
      Case eMergeField.FormName
        Return "Form name"
      Case eMergeField.PageName
        Return "Page name"
      Case eMergeField.SiteName
        Return "Site name"
      Case eMergeField.DateSubmitted
        Return "Date submitted"
      Case eMergeField.ConstituentLookupId
        Return "Constituent lookup ID"
      Case Else
        Return "???"
    End Select
  End Function
End Class
