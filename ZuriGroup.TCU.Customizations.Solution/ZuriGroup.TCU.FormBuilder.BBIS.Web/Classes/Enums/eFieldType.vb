﻿Public Class eFieldType
  Public Enum FieldType
    Heading = 1
    Paragraph = 2

    SingleLineText = 11
    MultiLineText = 12
    NumberField = 13
    Checkbox = 14
    MultipleChoice = 15
    DropDownMenu = 16

    EmailField = 31
    AddressField = 32
    PhoneField = 33
    DateTimeField = 34

    FileUpload = 51
  End Enum

  Public Shared ReadOnly Property FieldTypeLabel(fieldType As eFieldType.FieldType) As String
    Get
      Select Case fieldType
        Case eFieldType.FieldType.Heading
          Return "Heading"
        Case eFieldType.FieldType.Paragraph
          Return "Paragraph"

        Case eFieldType.FieldType.SingleLineText
          Return "Single line text"
        Case eFieldType.FieldType.MultiLineText
          Return "Multi line text"
        Case eFieldType.FieldType.NumberField
          Return "Number field"
        Case eFieldType.FieldType.Checkbox
          Return "Checkbox"
        Case eFieldType.FieldType.MultipleChoice
          Return "Multiple choice"
        Case eFieldType.FieldType.DropDownMenu
          Return "Drop-down menu"

        Case eFieldType.FieldType.EmailField
          Return "Email"
        Case eFieldType.FieldType.AddressField
          Return "Address"
        Case eFieldType.FieldType.PhoneField
          Return "Phone"
        Case eFieldType.FieldType.DateTimeField
          Return "Date/Time"

        Case eFieldType.FieldType.FileUpload
          Return "File upload"

        Case Else
          Return "Unknown field"
      End Select
    End Get
  End Property

  Public Shared Function GetValues() As IEnumerable(Of FieldType)
    Return [Enum].GetValues(GetType(FieldType)).Cast(Of FieldType)()
  End Function
End Class
