﻿Partial Public Class MergeFieldsProvider
  Public Enum eMergeField
    'BeginSubmittedFields = 1
    'FieldLabel = 2
    'FieldValue = 3
    'FieldType = 4
    'EndSubmittedFields = 10
    SubmittedFields = 1

    RecipientDisplayName = 21
    PageName = 22
    SiteName = 23
    FormName = 26
    DateSubmitted = 24
    ConstituentLookupId = 25

    Category_Fields = 101
    Category_Details = 102
  End Enum
End Class