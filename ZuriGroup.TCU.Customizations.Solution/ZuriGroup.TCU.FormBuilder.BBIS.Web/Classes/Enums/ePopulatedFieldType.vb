﻿Public Class ePopulatedFieldType
  Enum FieldType
    'Biographical
    FirstName = 1
    MiddleName = 2
    MiddleInitial = 3
    LastName = 4
    MaidenName = 5
    Nickname = 6

    ClassYear = 11

    'Address
    AddressPrimary = 31

    AddressBlock = 41
    AddressCountry = 42
    AddressCity = 43
    AddressState = 44
    AddressPostCode = 45
    AddressType = 46

    'Phone
    PhonePrimary = 51

    PhoneNumber = 61
    PhoneType = 62

    'Email
    EmailPrimary = 71

    EmailAddress = 81
    EmailType = 82
  End Enum
  Enum Group
    Biographical
    Address
    AddressFields
    Phone
    PhoneFields
    Email
    EmailFields
  End Enum

  Public Shared ReadOnly Property Label(fieldType As FieldType)
    Get
      Select Case fieldType

        Case ePopulatedFieldType.FieldType.FirstName
          Return "First name"
        Case ePopulatedFieldType.FieldType.MiddleName
          Return "Middle name"
        Case ePopulatedFieldType.FieldType.MiddleInitial
          Return "Middle initial"
        Case ePopulatedFieldType.FieldType.LastName
          Return "Last name"
        Case ePopulatedFieldType.FieldType.MaidenName
          Return "Maiden name"
        Case ePopulatedFieldType.FieldType.Nickname
          Return "Nickname"
        Case ePopulatedFieldType.FieldType.ClassYear
          Return "Class year"

        Case ePopulatedFieldType.FieldType.AddressPrimary
          Return "Primary address"
        Case ePopulatedFieldType.FieldType.AddressBlock
          Return "Address"
        Case ePopulatedFieldType.FieldType.AddressCountry
          Return "Country"
        Case ePopulatedFieldType.FieldType.AddressCity
          Return "City"
        Case ePopulatedFieldType.FieldType.AddressState
          Return "State"
        Case ePopulatedFieldType.FieldType.AddressPostCode
          Return "Post code"
        Case ePopulatedFieldType.FieldType.AddressType
          Return "Address type"

        Case ePopulatedFieldType.FieldType.PhonePrimary
          Return "Primary phone number"
        Case ePopulatedFieldType.FieldType.PhoneNumber
          Return "Phone number"
        Case ePopulatedFieldType.FieldType.PhoneType
          Return "Phone type"

        Case ePopulatedFieldType.FieldType.EmailPrimary
          Return "Primary email address"
        Case ePopulatedFieldType.FieldType.EmailAddress
          Return "Email address"
        Case ePopulatedFieldType.FieldType.EmailType
          Return "Email type"

        Case Else
          Return ""
      End Select
    End Get
  End Property

  Public Shared Function GetValues() As IEnumerable(Of FieldType)
    Return [Enum].GetValues(GetType(FieldType)).Cast(Of FieldType)().Where(Function(x) x < 31)
  End Function
  Public Shared Function GetValues(group As Group) As IEnumerable(Of FieldType)
    Dim items = [Enum].GetValues(GetType(FieldType)).Cast(Of FieldType)()
    Select Case group
      Case ePopulatedFieldType.Group.Biographical
        Return items.Where(Function(x) x > 1 And x < 11)
      Case ePopulatedFieldType.Group.Address
        Return items.Where(Function(x) x >= 31 And x <= 40)
      Case ePopulatedFieldType.Group.AddressFields
        Return items.Where(Function(x) x >= 41 And x <= 50)
      Case ePopulatedFieldType.Group.Phone
        Return items.Where(Function(x) x >= 51 And x <= 60)
      Case ePopulatedFieldType.Group.PhoneFields
        Return items.Where(Function(x) x >= 61 And x <= 70)
      Case ePopulatedFieldType.Group.Email
        Return items.Where(Function(x) x >= 71 And x <= 80)
      Case ePopulatedFieldType.Group.EmailFields
        Return items.Where(Function(x) x >= 81 And x <= 90)
      Case Else
        Return items
    End Select
  End Function
End Class
