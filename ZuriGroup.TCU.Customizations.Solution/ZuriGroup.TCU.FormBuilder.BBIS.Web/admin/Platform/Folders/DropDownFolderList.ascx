<%@ Control Language="vb" AutoEventWireup="false" Codebehind="DropDownFolderList.ascx.vb" Inherits="Blackbaud.Web.Content.Portal.Platform.Folders.DropDownFolderList" %>
<%@ Register Src="~/admin/Platform/Folders/FolderTree.ascx" TagName="FolderTree" TagPrefix="uc1" %>

<style type="text/css">
    .TreeDropDown
    {
        position:absolute;
    }
    .TreeDropDownField
    {
        white-space:nowrap;
        overflow:hidden; 
    }
    .TreeDropDownMenu
    {
        position:absolute;
        top:20px;
        left:0px;
        background-color:#FFFFFF;
        width:100%;
        border:solid 1px LightGray;
       z-index:51;
    }
    .TreeDropDownMenu .FolderTreeButtons
    {
        display:none;
    }
    .TreeDropDownMenu .FolderTreeWrapper
    {
        height:299px !important;
    }
    .TreeDropDownMenu .FolderTree
    {
        padding:0px;
        margin:0px;
        height:300px;
    }
    .TreeDropDownDefaultValue
    {
        display:none;
    }
    .TreeDropDownValue
    {
        display:none;
    }
    .FolderTree input
    {
        display:none;
    }
</style>

<div class="TreeDropDown" runat="server" id="DropDownFolderListWrapper">
    <div class="SearchFieldContainer">
        <div class="SearchFieldBox">
            <a class="SearchField TreeDropDownField" href="#" title="Select a Folder">
                <span class="SearchFieldIcon">
                    <img src="<%= page.resolveUrl("~/images/folder16.gif")%>" alt="" />
                </span>
                <span class="SearchFieldText TreeDropDownText" id="spanFolderName" runat="server">All Folders</span>
                <span class="SearchFieldButton"><img src="<%= page.resolveUrl("~/images/Admin/Buttons/search.gif")%>" alt="" /></span>
            </a><%-- END SearchField --%>
        </div>
    </div><%-- END SearchFieldContainer --%>
    
    <div class="TreeDropDownMenu" style="display:none;">
        <uc1:FolderTree ID="treeFolders" runat="server" FolderType="SitePages" ShowAllFoldersNode="true" Width="100%" TreeHeight="299px" />
    </div><%-- END DropDownFolderSelect --%>

</div><%-- END DropDownFolder --%>