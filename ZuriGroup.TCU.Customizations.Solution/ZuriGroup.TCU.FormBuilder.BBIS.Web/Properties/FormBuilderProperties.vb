Public Class FormBuilderProperties
  Public FormName As String

  Public Fields As List(Of Field)
  Public FieldOrder As List(Of String)
  Public FieldOrderText As String

  Public UseReCaptcha As Boolean
  Public RequireRegisteredReCaptcha As Boolean
  Public ReCaptchaV2SiteKey As String
  Public ReCaptchaV2SecretKey As String

  Public DisplayAlternativeSuccessPage As Boolean
  Public ConfirmationPageId As Integer
  Public SuccessMessageStorageHTML As String

  Public EmailRecipients As System.Collections.Generic.List(Of EmailRecipient)

  Public PartIsNotNew As Boolean
End Class