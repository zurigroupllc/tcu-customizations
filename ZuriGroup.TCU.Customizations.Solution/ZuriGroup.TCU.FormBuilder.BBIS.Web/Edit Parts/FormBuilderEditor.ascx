<%@ Assembly Name="ZuriGroup.TCU.FormBuilder.BBIS.Web" %>
<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FormBuilderEditor.ascx.vb" Inherits="ZuriGroup.TCU.FormBuilder.BBIS.Web.FormBuilderEditor" %>
<%@ Import Namespace="ZuriGroup.TCU.FormBuilder.BBIS.Web" %>
<%@ Register TagPrefix="uc" TagName="HTMLEditor" Src="~/admin/HtmlEditorControl2.ascx" %>
<%@ Register TagPrefix="uc2" TagName="pagelink" Src="~/admin/PageLink.ascx" %>
<%@ Register TagPrefix="bbnc" Namespace="BBNCExtensions.ServerControls" Assembly="BBNCExtensions" %>
<%@ Register TagPrefix="bb" Src="~/admin/Platform/Folders/DropDownFolderList.ascx" TagName="DropDownFolderList" %>
<link rel="stylesheet" href="https://use.fontawesome.com/b6da1745f0.css">
<asp:Label ID="lblError" runat="server" Font-Bold="true" ForeColor="red" />
<style>
  .RadioContainer .ribbon_tab_btn {
    background: #f7fbff !important;
  }

  .RadioContainer .SearchFieldContainer {
    width: 350px;
  }

  .ui-tabs .ui-tabs-panel {
    padding: 10px 5px;
  }

  input[type="radio"] + label, input[type="checkbox"] + label {
    padding-left: 4px;
  }

  .PreviewRequiredMarker, .PropertyRequiredMarker {
    font-weight: bold;
    color: red;
    padding-left: 4px;
  }

  .HiddenRow {
    display: none;
  }

  .StepGrouping {
    padding-left: 0px;
  }

  .SingleFieldRow {
    width: 960px !important;
  }

  .FieldSetRow_2uneven .FieldSetColumnFirst {
    width: 500px !important;
  }

  .DataGridItemAlternating td, .DataGridItem td {
    white-space: normal !important;
  }

  .FieldSetRow_2uneven .FieldSetColumnLast {
    width: 460px !important;
  }

  .button {
    display: block;
    text-decoration: none;
    padding: 3px;
    color: #000 !important;
  }

  .ui-state-highlight {
    background: #DCEBFE;
    border: none;
    height: 40px;
  }

  .FieldOptionsTable {
  }

  .FieldOptionsRow {
  }

  .FieldTypeLabel {
    font-size: .8em;
  }

  #tblSelectedFields > table {
    width: 100%;
  }
  #tblSelectedFields table {
    border-collapse: collapse;
  }

  .FieldOptionsNoteCell {
    padding: 5px;
    background: #fcf8e3;
    color: #8a6d3b;
    border-bottom: 1px solid #A6B6C5;
  }

  .FieldOptionsCaptionCell {
    padding: 6px 10px 0px 4px;
    width: 100px;
    vertical-align: top;
  }

  .FieldOptionsControlCell {
    padding: 4px 0px;
  }

  .FieldOptionsSectionCell {
    background: #f6faff;
    font-weight: bold;
    padding: 4px;
    border-bottom: 1px solid #A6B6C5;
    border-top: 1px solid #A6B6C5;
  }

  table .FieldOptionsSectionCell.First {
    border-top: none;
  }

  .FieldOptionsButtonCell {
    text-align: center;
    background: #f6faff;
    padding: 4px;
    border-top: 1px solid #A6B6C5;
  }

  .PreviewControlCell input[type="text"],
  .PreviewControlCell textarea,
  .PreviewControlCell select {
    margin: 3px 0px 4px 0px;
    width:160px;
  }

  .PreviewControlCell input[type="file"] {
    width: 180px;
  }

  .FieldOptionsControlCell input[type="text"],
  .FieldOptionsControlCell textarea,
  .FieldOptionsControlCell select {
    width: 320px;
    padding: 2px;
  }

  .FieldOptionsHelpText, .FormBuilderHelpText {
    display: block;
    font-size: .8em;
  }

  .PreviewCaptionCell {
    width: 160px;
    vertical-align: top;
    padding-top: 6px;
  }

  .PreviewRowHidden {
    font-style: italic;
    color: #d5d5d5;
  }

  .HiddenLabel * {
    display: none;
  }

  .MonthFieldCell input, .DayFieldCell input {
    width: 30px !important;
  }

  .YearFieldCell input {
    width: 60px !important;
  }

  .MonthFieldCell, .DayFieldCell {
    padding-right: 4px;
  }

  .ValidationSummary {
    background: #f2dede;
    color: #a94442;
    font-weight: bold;
    padding: 5px 8px;
    border-bottom: 1px solid #A6B6C5;
  }

    .ValidationSummary ul {
      font-weight: normal;
      padding: 5px;
    }
</style>
<asp:HiddenField ID="hfPartTitle" runat="server" />
<asp:HiddenField ID="hfSiteId" runat="server" />
<asp:HiddenField ID="hfSiteName" runat="server" />
<asp:HiddenField ID="hfEmailTemplateId" runat="server" />
<asp:ValidationSummary ID="ValidationSummary" runat="server" CssClass="YellowBox validationerror" Style="padding: 4px 0px 2px 4px; margin-bottom: 10px;" />
<div id="tabs_Main" style="display: none">
  <ul>
    <li><a href="#tab_FormBuilder">Form Elements</a></li>
    <li><a href="#tab_FormProperties">Form Properties</a></li>
    <li><a href="#tab_AcknowledgementEmail">Acknowledgement Email</a></li>
  </ul>
  <div id="tab_FormBuilder">
    <div class="StepGrouping">
      <h1 class="StepGroupingHeading">Form fields and elements</h1>
      <div class="StepGroupingBody">
        <div class="HelpText">
          Select a field type or element from the drop-down menu then select Add. Each form field or element can be customized below and on the Language tab above. Changes made on the Language tab will not be displayed in the sample form.
        </div>
        <div class="SingleFieldRow FieldSetRow_1">
          <asp:DropDownList ID="ddlAvailableFields" runat="server"></asp:DropDownList>
          <asp:Button ID="btnAddNewField" runat="server" OnClick="btnAddNewField_Click" Text="Add" CausesValidation="false" />
        </div>
        <div class="SingleFieldRow FieldSetRow_2uneven">
          <div class="FieldSetColumn FieldSetColumnFirst">
            <asp:ListView ID="listSelectedFields" runat="server" ItemPlaceholderID="trPlaceholder"
              OnItemCommand="listSelectedFields_ItemCommand"
              OnItemDataBound="listSelectedFields_ItemDataBound">
              <LayoutTemplate>
                <table>
                  <tbody>
                    <tr>
                      <td>
                        <div class="GridContainer" style="margin-bottom: 4px;">
                          <table id="tblSelectedFields" class="DataGrid" style="border-collapse: collapse;">
                            <tbody>
                              <tr class="DataGridHeader">
                                <td style="width: 360px;">Selected form elements</td>
                                <td style="width: 70px"></td>
                                <td style="width: 20px;"></td>
                                <td style="display: none;"></td>
                              </tr>
                              <tr id="trPlaceholder" runat="server"></tr>
                            </tbody>
                          </table>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td style="text-align: right;">
                        <span style="text-align: right;">Drag and drop using the <i class="fa fa-bars" style="font-size: 16px; color: #d5d5d5; margin: 0 5px 0 5px" aria-hidden="true"></i>icon to change the order of the fields.</span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </LayoutTemplate>
              <ItemTemplate>
                <tr class="DataGridItem">
                  <td class="DataGridItemCell" style="width: 360px;">
                    <asp:HiddenField ID="hfFieldType" runat="server" Value='<%# Convert.ToInt32(Eval("Type"))%>' />
                    <asp:TextBox ID="fieldSettingsDisplay" runat="server" Text='<%# Eval("Settings")%>' TextMode="MultiLine" Visible="false"></asp:TextBox>
                    <asp:PlaceHolder ID="Placeholder" runat="server"></asp:PlaceHolder>
                  </td>
                  <td class="DataGridItemCell" style="text-align: right; width: 70px;">
                    <asp:LinkButton ID="Edit" runat="server" Text="Edit" CommandArgument='<%# Eval("FieldId")%>' CommandName="Modify" CausesValidation="false"></asp:LinkButton><br />
                    <asp:LinkButton ID="Delete" runat="server" Text="Remove" CommandArgument='<%# Eval("FieldId")%>' CommandName="Remove" CausesValidation="false" OnClientClick="javascript: return confirm('Are you sure you want to remove this form element?')"></asp:LinkButton>
                  </td>
                  <td class="DataGridItemCell DataGridItemCellRight handle" style="cursor: pointer; text-align: right; font-size: 16px; width: 20px; color: #d5d5d5; padding-right: 10px;"><i class="fa fa-bars" aria-hidden="true"></i></td>
                  <td style="display: none">
                    <asp:Label ID="hfFieldId" runat="server" Text='<%# Eval("FieldId")%>' />
                  </td>
                </tr>
              </ItemTemplate>
              <EmptyDataTemplate>
                <table>
                  <tbody>
                    <tr>
                      <td>
                        <div class="GridContainer">
                          <table id="tblSelectedFields" class="DataGrid" style="border-collapse: collapse;">
                            <tbody>
                              <tr class="DataGridHeader">
                                <td style="width: 400px;">Element settings & options</td>
                                <td style="width: 90px"></td>
                                <td style="width: 20px;"></td>
                                <td style="display: none;"></td>
                              </tr>
                              <tr>
                                <td colspan="4" class="DataGridItemCell DataGridItemCellLeft DataGridItemCellRight"><strong>No fields have been added.</strong> Use the drop-down above to add new fields.</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </EmptyDataTemplate>
            </asp:ListView>
            <div style="display: none;">
              <asp:TextBox ID="OrderOfFields" runat="server" Width="90%" />
            </div>
          </div>
          <div class="FieldSetColumn FieldSetColumnLast">
            <table style="width: 100%">
              <tbody>
                <tr>
                  <td>
                    <div class="GridContainer" style="margin-bottom: 4px;">

                      <table id="tblSelectedFields" class="DataGrid" style="border-collapse: collapse;">
                        <tbody>
                          <tr class="DataGridHeader">
                            <td>Form element properties</td>
                          </tr>
                          <tr>
                            <td class="DataGridItemCell DataGridItemCellLeft DataGridItemCellRight" style="padding: 0px;">
                              <asp:ValidationSummary ID="FieldEditsValidationSummary" runat="server" ValidationGroup="FieldEdits" HeaderText="The following issues must be corrected:" CssClass="ValidationSummary" />
                              <asp:HiddenField ID="hfFieldId" runat="server" Visible="true" />
                              <asp:UpdatePanel ID="FieldPropertiesUpdatePanel" runat="server" ChildrenAsTriggers="true" RenderMode="Block" UpdateMode="Conditional">
                                <ContentTemplate>
                                  <asp:Panel ID="Settings_Form" runat="server">
                                  </asp:Panel>
                                  <!-- Header -->
                                  <asp:Panel ID="Settings_Header" runat="server" Visible="false">
                                    <table>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell First">Header options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Heading:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="Heading_TextHeading" runat="server"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="Heading_TextHeading_Required" runat="server" Display="None" ErrorMessage="Heading is required" ControlToValidate="Heading_TextHeading" ValidationGroup="FieldEdits"></asp:RequiredFieldValidator>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Type:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:DropDownList ID="Heading_ListType" runat="server">
                                            <asp:ListItem Text="h1 (biggest)" Value="h1"></asp:ListItem>
                                            <asp:ListItem Text="h2" Value="h2"></asp:ListItem>
                                            <asp:ListItem Text="h3" Value="h3"></asp:ListItem>
                                            <asp:ListItem Text="h4" Value="h4"></asp:ListItem>
                                            <asp:ListItem Text="h5" Value="h5"></asp:ListItem>
                                            <asp:ListItem Text="h6 (smallest)" Value="h6"></asp:ListItem>
                                          </asp:DropDownList>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell">Advanced options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">CSS class:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="Heading_TextCssClass" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                    </table>
                                  </asp:Panel>
                                  <!-- Paragraph -->
                                  <asp:Panel ID="Settings_Paragraph" runat="server" Visible="false">
                                    <table>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell First">Paragraph options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Text:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="Paragraph_TextText" runat="server" TextMode="MultiLine" Rows="6"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="Paragraph_TextText_Required" runat="server" Display="None" ErrorMessage="Text is required" ControlToValidate="Paragraph_TextText" ValidationGroup="FieldEdits"></asp:RequiredFieldValidator>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell">Advanced options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">CSS class:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="Paragraph_TextCssClass" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                    </table>
                                  </asp:Panel>
                                  <!-- Single line text -->
                                  <asp:Panel ID="Settings_SingleLineText" runat="server" Visible="false">
                                    <table>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell First">Single line text options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Field label:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="SingleLineText_TextLabel" runat="server"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="SingleLineText_TextLabel_Required" runat="server" Display="None" ErrorMessage="Field label is required" ControlToValidate="SingleLineText_TextLabel" ValidationGroup="FieldEdits"></asp:RequiredFieldValidator>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="SingleLineText_CheckRequired" runat="server" Text="Field is required" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Help text:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="SingleLineText_TextHelpText" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Pre-Populate:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:DropDownList ID="SingleLineText_ListPrePopulate" runat="server"></asp:DropDownList>
                                          <span class="FieldOptionsHelpText">When a user is signed in the field will be pre-populated with the value above (if available).</span>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell">Advanced options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="SingleLineText_CheckHideLabel" runat="server" Text="Hide label from user" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="SingleLineText_CheckHidden" runat="server" Text="Hide form element from user" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Default value:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="SingleLineText_TextDefaultValue" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Max length:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="SingleLineText_TextMaxLength" runat="server" Style="width: 40px;"></asp:TextBox>
                                          <asp:RegularExpressionValidator ID="SingleLineText_TextMaxLength_NumbersOnlyValidator" runat="server" ErrorMessage="Max length can only contain numbers."
                                            Display="None" ValidationGroup="FieldEdits" ControlToValidate="SingleLineText_TextMaxLength" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="SingleLineText_CheckNumbersOnly" runat="server" Text="Numbers only" />
                                        </td>
                                      </tr>
                                    </table>
                                  </asp:Panel>
                                  <!-- Multi-line Text -->
                                  <asp:Panel ID="Settings_MultiLineText" runat="server" Visible="false">
                                    <table>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell First">Multi-line text options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Field label:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="MultiLineText_TextLabel" runat="server"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="MultiLineText_TextLabel_Required" runat="server" Display="None" ErrorMessage="Field label is required" ControlToValidate="MultiLineText_TextLabel" ValidationGroup="FieldEdits"></asp:RequiredFieldValidator>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="MultiLineText_CheckRequired" runat="server" Text="Field is required" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Help text:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="MultiLineText_TextHelpText" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Rows:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="MultiLineText_TextRows" runat="server" Style="width: 40px;" TextMode="Number"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="MultiLineText_TextRows_Required" runat="server" Display="None" ErrorMessage="Number of rows is required" ControlToValidate="MultiLineText_TextRows" ValidationGroup="FieldEdits"></asp:RequiredFieldValidator>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell">Advanced options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="MultiLineText_CheckHideLabel" runat="server" Text="Hide label from user" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="MultiLineText_CheckHidden" runat="server" Text="Hide form element from user" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Default value:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="MultiLineText_TextDefaultValue" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                        </td>
                                      </tr>
                                    </table>
                                  </asp:Panel>
                                  <!-- Number field -->
                                  <asp:Panel ID="Settings_NumberField" runat="server" Visible="false">
                                    <table>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell First">Number field options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Field label:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="NumberField_TextLabel" runat="server"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="NumberField_TextLabel_Required" runat="server" Display="None" ErrorMessage="Field label is required" ControlToValidate="NumberField_TextLabel" ValidationGroup="FieldEdits"></asp:RequiredFieldValidator>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="NumberField_CheckRequired" runat="server" Text="Field is required" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Help text:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="NumberField_TextHelpText" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Number type:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:RadioButtonList ID="NumberField_RadioNumberType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="NumberField_RadioNumberType_SelectedIndexChanged">
                                            <asp:ListItem Text="Number" Value="0" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Currency" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Percentage" Value="2"></asp:ListItem>
                                          </asp:RadioButtonList>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Value range:</td>
                                        <td class="FieldOptionsControlCell">Min:
                                      <asp:TextBox ID="NumberField_TextMinValue" runat="server" Style="width: 40px;"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">Max:
                                      <asp:TextBox ID="NumberField_TextMaxValue" runat="server" Style="width: 40px;"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Blank value:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="NumberField_TextBlankValue" runat="server"></asp:TextBox><span class="FieldOptionsHelpText">Value to return if left blank by the user.</span>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell">Advanced options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="NumberField_CheckHideLabel" runat="server" Text="Hide label from user" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="NumberField_CheckHidden" runat="server" Text="Hide form element from user" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Permitted characters:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="NumberField_TextAvailableCharacters" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Default value:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="NumberField_TextDefaultValue" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Max length:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="NumberField_TextMaxLength" runat="server" Style="width: 40px;"></asp:TextBox>
                                          <asp:RegularExpressionValidator ID="NumberField_TextMaxLength_NumbersOnlyValidator" runat="server" ErrorMessage="Max length can only contain numbers."
                                            Display="None" ValidationGroup="FieldEdits" ControlToValidate="NumberField_TextMaxLength" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                        </td>
                                      </tr>
                                    </table>
                                  </asp:Panel>
                                  <!-- Checkbox -->
                                  <asp:Panel ID="Settings_Checkbox" runat="server" Visible="false">
                                    <table>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell First">Checkbox options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Field label:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="Checkbox_TextLabel" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Caption:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="Checkbox_TextCaption" runat="server"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="Checkbox_TextCaption_Required" runat="server" Display="None" ErrorMessage="Caption is required" ControlToValidate="Checkbox_TextCaption" ValidationGroup="FieldEdits"></asp:RequiredFieldValidator>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="Checkbox_CheckRequired" runat="server" Text="Field is required" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="Checkbox_CheckDefaultValue" runat="server" Text="Checked by default" />
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell">Advanced options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="Checkbox_CheckHideLabel" runat="server" Text="Hide label from user" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="Checkbox_CheckHidden" runat="server" Text="Hide form element from user" />
                                        </td>
                                      </tr>
                                    </table>
                                  </asp:Panel>
                                  <!-- Multiple choice -->
                                  <asp:Panel ID="Settings_MultipleChoice" runat="server" Visible="false">
                                    <table>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell First">Multiple choice options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Group label:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="MultipleChoice_TextLabel" runat="server"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="MultipleChoice_TextLabel_Required" runat="server" Display="None" ErrorMessage="Group label is required" ControlToValidate="MultipleChoice_TextLabel" ValidationGroup="FieldEdits"></asp:RequiredFieldValidator>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="MultipleChoice_CheckRequired" runat="server" Text="Selection is required" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Columns:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:DropDownList ID="MultipleChoice_ListColumns" runat="server">
                                            <asp:ListItem Text="1"></asp:ListItem>
                                            <asp:ListItem Text="2"></asp:ListItem>
                                            <asp:ListItem Text="3"></asp:ListItem>
                                          </asp:DropDownList>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Display type:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:RadioButtonList ID="MultipleChoice_RadioRenderStyle" runat="server">
                                            <asp:ListItem Text="Checkboxes (multiple responses)" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Radio options (single response)" Value="1"></asp:ListItem>
                                          </asp:RadioButtonList>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Values:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="MultipleChoice_TextValues" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                          <span class="FieldOptionsHelpText">One option per line</span>
                                          <asp:RequiredFieldValidator ID="MultipleChoice_TextValues_Required" runat="server" Display="None" ErrorMessage="At least one value is required" ControlToValidate="MultipleChoice_TextValues" ValidationGroup="FieldEdits"></asp:RequiredFieldValidator>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Code table:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:DropDownList ID="MultipleChoice_ListCodeTables" runat="server" OnSelectedIndexChanged="MultipleChoice_ListCodeTables_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                          <span class="FieldOptionsHelpText">Instead of the values above, a code table can be used.</span>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="MultipleChoice_CheckShowOtherOption" runat="server" Text="Show 'other' option" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">'Other' label:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="MultipleChoice_TextOtherLabel" runat="server"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="MultipleChoice_TextOtherLabel_Required" runat="server" Display="None" ErrorMessage="'Other' label is required" ControlToValidate="MultipleChoice_TextOtherLabel" ValidationGroup="FieldEdits"></asp:RequiredFieldValidator>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell">Advanced options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="MultipleChoice_CheckHideLabel" runat="server" Text="Hide label from user" />
                                        </td>
                                      </tr>
                                    </table>
                                  </asp:Panel>
                                  <!-- Drop down menu -->
                                  <asp:Panel ID="Settings_DropDownMenu" runat="server" Visible="false">
                                    <table>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell First">Drop-down menu options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Field label:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="DropDownMenu_TextLabel" runat="server"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="DropDownMenu_TextLabel_Required" runat="server" Display="None" ErrorMessage="Field label is required" ControlToValidate="DropDownMenu_TextLabel" ValidationGroup="FieldEdits"></asp:RequiredFieldValidator>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="DropDownMenu_CheckRequired" runat="server" Text="Field is required" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Help text:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="DropDownMenu_TextHelpText" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Values:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="DropDownMenu_TextValues" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                          <span class="FieldOptionsHelpText">One option per line</span>
                                          <asp:RequiredFieldValidator ID="DropDownMenu_TextValues_Required" runat="server" Display="None" ErrorMessage="No values have been added or no code table has been selected." ControlToValidate="DropDownMenu_TextValues" ValidationGroup="FieldEdits"></asp:RequiredFieldValidator>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Code table:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:DropDownList ID="DropDownMenu_ListCodeTables" runat="server" OnSelectedIndexChanged="DropDownMenu_ListCodeTables_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                          <span class="FieldOptionsHelpText">Instead of the values above, a code table can be used.</span>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell">Advanced options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="DropDownMenu_CheckHidden" runat="server" Text="Hide form element from user" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="DropDownMenu_CheckHideLabel" runat="server" Text="Hide label from user" />
                                        </td>
                                      </tr>
                                    </table>
                                  </asp:Panel>
                                  <!-- Email field -->
                                  <asp:Panel ID="Settings_Email" runat="server" Visible="false">
                                    <table>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell First">Email options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td colspan="2" class="FieldOptionsNoteCell">Email field labels can be changed from the 'Language' tab above.</td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="EmailField_CheckRequired" runat="server" Text="Email is required" AutoPostBack="true" OnCheckedChanged="EmailField_CheckRequired_CheckedChanged" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="EmailField_CheckTypeRequired" runat="server" Text="Type is required" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="EmailField_CheckShowConfirmation" runat="server" Text="Require email confirmation" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Help text:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="EmailField_TextHelpText" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Email types:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="EmailField_TextTypes" runat="server" TextMode="MultiLine" Rows="3" OnTextChanged="EmailField_TextTypes_TextChanged" AutoPostBack="true"></asp:TextBox>
                                          <span class="FieldOptionsHelpText">One option per line. If only one option is listed, it will be chosen by default and will be hidden from the user.</span>
                                        </td>
                                      </tr>
                                      <tr id="EmailField_trAddTypeToLabel" runat="server" class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="EmailField_CheckAddTypeToLabel" runat="server" Text="Add email type to beginning of label" />
                                          <span class="FieldOptionsHelpText">If only one type option is present, the email field label will begin with that text. (e.g., "Personal email address:")</span>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Pre-Populate:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:DropDownList ID="EmailField_ListPrePopulate" runat="server"></asp:DropDownList>
                                          <span class="FieldOptionsHelpText">When a user is signed in the field will be pre-populated with the value above (if available).</span>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell">Advanced options
                                        </td>
                                      </tr>
                                    </table>
                                  </asp:Panel>
                                  <!-- Address field -->
                                  <asp:Panel ID="Settings_Address" runat="server" Visible="false">
                                    <table>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell First">Address options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td colspan="2" class="FieldOptionsNoteCell">Address field labels can be changed from the 'Language' tab above.</td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="AddressField_CheckAllowOnlyUSA" runat="server" Text="Hide country field" AutoPostBack="true" OnCheckedChanged="AddressField_CheckAllowOnlyUSA_CheckedChanged" />
                                          <span class="FieldOptionsHelpText">If checked, country will default to United States and be hidden.</span>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="AddressField_CheckDefaultToUSA" runat="server" Text="Default to United States" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="AddressField_CheckAddressRequired" runat="server" Text="Address is required" AutoPostBack="true" OnCheckedChanged="AddressField_CheckAddressRequired_CheckedChanged" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="AddressField_CheckTypeRequired" runat="server" Text="Type is required" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Address types:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="AddressField_TextTypes" runat="server" TextMode="MultiLine" Rows="3" OnTextChanged="AddressField_TextTypes_TextChanged" AutoPostBack="true"></asp:TextBox>
                                          <span class="FieldOptionsHelpText">One option per line. If only one option is listed, it will be chosen by default and will be hidden from the user.</span>
                                        </td>
                                      </tr>
                                      <tr id="AddressField_trAddTypeToLabel" runat="server" class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="AddressField_CheckAddTypeToLabel" runat="server" Text="Add address type to beginning of label" />
                                          <span class="FieldOptionsHelpText">If only one type option is present, the address field label will begin with that text. (e.g., "Personal address:")</span>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Pre-Populate:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:DropDownList ID="AddressField_ListPrePopulate" runat="server"></asp:DropDownList>
                                          <span class="FieldOptionsHelpText">When a user is signed in the field will be pre-populated with the value above (if available).</span>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell">Advanced options
                                        </td>
                                      </tr>
                                    </table>
                                  </asp:Panel>
                                  <!-- Phone field -->
                                  <asp:Panel ID="Settings_Phone" runat="server" Visible="false">
                                    <table>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell First">Phone options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td colspan="2" class="FieldOptionsNoteCell">Phone field labels can be changed from the 'Language' tab above.</td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="PhoneField_CheckRequired" runat="server" Text="Phone is required" AutoPostBack="true" OnCheckedChanged="PhoneField_CheckRequired_CheckedChanged" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="PhoneField_CheckTypeRequired" runat="server" Text="Type is required" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Help text:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="PhoneField_TextHelpText" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Phone types:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="PhoneField_TextTypes" runat="server" TextMode="MultiLine" Rows="4" OnTextChanged="PhoneField_TextTypes_TextChanged" AutoPostBack="true"></asp:TextBox>
                                          <span class="FieldOptionsHelpText">One option per line. If only one option is listed, it will be chosen by default and will be hidden from the user.</span>
                                        </td>
                                      </tr>
                                      <tr id="PhoneField_trAddTypeToLabel" runat="server" class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="PhoneField_CheckAddTypeToLabel" runat="server" Text="Add phone type to beginning of label" />
                                          <span class="FieldOptionsHelpText">If only one type option is present, the phone field label will begin with that text. (e.g., "Personal phone number:")</span>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Pre-Populate:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:DropDownList ID="PhoneField_ListPrePopulate" runat="server"></asp:DropDownList>
                                          <span class="FieldOptionsHelpText">When a user is signed in the field will be pre-populated with the value above (if available).</span>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell">Advanced options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Permitted characters:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="PhoneField_TextAvailableCharacters" runat="server"></asp:TextBox>
                                          <span class="FieldOptionsHelpText">Numbers, hyphens and all characters above will be permitted.</span>
                                          <asp:RegularExpressionValidator ID="PhoneField_TextAvailableCharacters_RegularExpressionValidator" runat="server" ControlToValidate="PhoneField_TextAvailableCharacters"
                                            ErrorMessage="Numbers and hyphens are already permitted characters" Display="None" ValidationExpression="^[^0-9-]*$" ValidationGroup="FieldEdits"></asp:RegularExpressionValidator>
                                        </td>
                                      </tr>
                                    </table>
                                  </asp:Panel>
                                  <!-- Date/time field -->
                                  <asp:Panel ID="Settings_DateTime" runat="server" Visible="false">
                                    <table>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell First">Date/time options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Field label:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="DateTimeField_TextLabel" runat="server"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="DateTimeField_TextLabel_Required" runat="server" ControlToValidate="DateTimeField_TextLabel"
                                            ErrorMessage="Field label is required" Display="None" ValidationGroup="FieldEdits"></asp:RequiredFieldValidator>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="DateTimeField_CheckRequired" runat="server" Text="Field is required" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Type:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:RadioButtonList ID="DateTimeField_RadioDateType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DateTimeField_RadioDateType_SelectedIndexChanged">
                                            <asp:ListItem Text="Date" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Time (HH:MM AM/PM)" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Time (HH:MM only)" Value="2"></asp:ListItem>
                                          </asp:RadioButtonList>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="DateTimeField_CheckSplitMonthDayYear" runat="server" Text="Split month, day and year text fields" />
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell">Advanced options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="DateTimeField_CheckHideLabel" runat="server" Text="Hide label from user" />
                                        </td>
                                      </tr>
                                    </table>
                                  </asp:Panel>
                                  <!-- File upload -->
                                  <asp:Panel ID="Settings_FileUpload" runat="server" Visible="false">
                                    <table>
                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell First">File upload options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Field label:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="FileUpload_TextLabel" runat="server"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="FileUpload_TextLabel_Required" runat="server" Display="None" ErrorMessage="Field label is required" ControlToValidate="FileUpload_TextLabel" ValidationGroup="FieldEdits"></asp:RequiredFieldValidator>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="FileUpload_CheckRequired" runat="server" Text="Field is required" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Help text:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="FileUpload_TextHelpText" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Type:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:RadioButtonList ID="FileUpload_RadioFileType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="FileUpload_RadioFileType_SelectedIndexChanged">
                                            <asp:ListItem Text="Image" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Document" Value="1"></asp:ListItem>
                                          </asp:RadioButtonList>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">File types:</td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:TextBox ID="FileUpload_TextFileTypesPermitted" runat="server" Enabled="false"></asp:TextBox>
                                          <span class="FieldOptionsHelpText">These are the file types permitted based on your selection above.</span>
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Folder:</td>
                                        <td class="FieldOptionsControlCell" style="position: relative; height: 50px;">
                                          <asp:Panel ID="FileUpload_FolderSelectorPanel" runat="server">
                                            <style>
                                              .TreeDropDown {
                                                top: 6px;
                                                left: 0;
                                              }
                                            </style>
                                            <bb:DropDownFolderList ID="FileUpload_DropDownFolderList" runat="server" Width="320px" />
                                            <asp:Button ID="FileUpload_ButtonFolderDisplaySave" runat="server" Text="Save selected folder" OnClick="FileUpload_ButtonFolderDisplaySave_Click" style="position:absolute; top:30px;" CausesValidation="false"></asp:Button>
                                          </asp:Panel>
                                          <asp:Panel ID="FileUpload_FolderDisplayPanel" runat="server">
                                            <asp:TextBox ID="FileUpload_TextFolderDisplay" runat="server" style="position:absolute; top:3px;" Enabled="false"></asp:TextBox>
                                            <asp:Button ID="FileUpload_ButtonFolderDisplayChange" runat="server" Text="Select new folder" OnClick="FileUpload_ButtonFolderDisplayChange_Click" style="position:absolute; top:30px;" CausesValidation="false"></asp:Button>
                                            <asp:HiddenField ID="FileUpload_FolderDisplayIdHidden" runat="server" />
                                          </asp:Panel>
                                          <asp:CustomValidator ID="FileUpload_FolderDisplayCustomValidator" runat="server" ErrorMessage="Please save your selected folder." Display="None" 
                                            ValidationGroup="FieldEdits" OnServerValidate="FileUpload_FolderDisplayCustomValidator_ServerValidate" ClientValidationFunction="CheckFileUploadFolderState"></asp:CustomValidator>
                                        </td>
                                      </tr>

                                      <tr>
                                        <td colspan="2" class="FieldOptionsSectionCell">Advanced options
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell"></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:CheckBox ID="FileUpload_CheckHideLabel" runat="server" Text="Hide label from user" />
                                        </td>
                                      </tr>
                                      <tr class="FieldOptionsRow">
                                        <td class="FieldOptionsCaptionCell">Max file size:<span class="PropertyRequiredMarker">*</span></td>
                                        <td class="FieldOptionsControlCell">
                                          <asp:DropDownList ID="FileUpload_ListMaxFileSize" runat="server">
                                            <asp:ListItem Text="1 MB" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="2 MB" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="3 MB" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="4 MB" Value="4"></asp:ListItem>
                                          </asp:DropDownList>
                                        </td>
                                      </tr>
                                    </table>
                                  </asp:Panel>
                                </ContentTemplate>
                              </asp:UpdatePanel>
                              <asp:Panel ID="Settings_Controls" runat="server" Visible="false">
                                <table style="margin-top: 10px; width:100%;">
                                  <tr class="FieldOptionsRow">
                                    <td class="FieldOptionsButtonCell">
                                      <asp:Button ID="btnSaveSettings" runat="server" Text="Update" OnClick="btnSaveSettings_Click" ValidationGroup="FieldEdits" />
                                      <asp:Button ID="btnCancelSettings" runat="server" Text="Cancel" OnClick="btnCancelSettings_Click" CausesValidation="false" />
                                    </td>
                                  </tr>
                                </table>
                              </asp:Panel>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="tab_FormProperties">
    <div class="StepGrouping">
      <h1 class="StepGroupingHeading">Form properties</h1>
      <div class="StepGroupingBody">
        <div class="VerticalOption">
          <div class="SelectOption">
            <div class="Field SelectOptionField">
              <div class="FieldHeading">
                <asp:Label ID="TextFormNameLabel" runat="server" Text="Form name:" CssClass="FieldLabel"></asp:Label>
                <span class="RequiredFieldMarker">*</span><span class="clear"></span>
              </div>
              <div class="FieldContentText">
                <asp:TextBox ID="TextFormName" runat="server" CssClass="FieldSelect FieldInput"></asp:TextBox>
                <asp:RequiredFieldValidator ID="TextFormName_Required" runat="server" Display="None" ErrorMessage="Form name is required" ControlToValidate="TextFormName"></asp:RequiredFieldValidator>
              </div>
            </div>
            <div class="SelectOptionHelp">
              <div class="SelectOptionHelpInterior">
                <img class="SelectOptionHelpArrow" alt="" src='<%= page.resolveUrl("~/images/FieldHelpArrow.gif")%>'><div class="SelectOptionHelpText">
                  The form name will be used to help distinguish between different forms inside of Blackbaud CRM as well as
                  in email notifications sent out after the form has been submitted.
                </div>
              </div>
            </div>
            <div class="clear">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="StepGrouping">
      <h1 class="StepGroupingHeading">Google reCAPTCHA</h1>
      <div class="StepGroupingBody">
        <div class="HelpText">
          Choose whether the form should display Google reCAPTCHA functionality.
        </div>
        <div class="VerticalOption" id="divReCaptchaSettings" runat="server">
          <table class="CheckboxOption">
            <tbody>
              <tr>
                <td class="CheckboxOptionField">
                  <asp:CheckBox ID="checkIncludeReCaptcha" runat="server" Text="Include Google reCAPTCHA V2" />
                </td>
              </tr>
            </tbody>
          </table>
          <div class="SubOptionContainer" id="trReCaptchaOptions">
            <div class="StepGrouping">
              <div class="StepGroupingBody">
                <div class="HelpText">
                  Enter your Google reCAPTCHA V2 site key and secret key. You can find more information about Google 
                            reCAPTCHA V2 and the required keys by visting <a href="https://www.google.com/recaptcha" target="_blank" title="Google reCAPTCHA">https://www.google.com/recaptcha</a>.
                </div>
                <div class="VerticalOption">
                  <div class="SelectOption">
                    <div class="SelectOptionField" style="width: 60%">
                      <div class="FieldHeading">
                        <asp:Label ID="labelReCaptchaV2SiteKey" runat="server" AssociatedControlID="inputReCaptchaV2SiteKey" Text="Site key: " CssClass="FieldLabel"></asp:Label>
                        <span class="FieldRequired">*</span>
                        <span class="clear"></span>
                      </div>
                      <div class="FieldContent">
                        <asp:TextBox ID="inputReCaptchaV2SiteKey" runat="server" CssClass="FieldSelect FieldInput"></asp:TextBox>
                        <asp:CustomValidator ID="validateReCaptchaV2SiteKey" runat="server" ControlToValidate="inputReCaptchaV2SiteKey"
                          ErrorMessage="Please enter your reCAPTCHA V2 site key." Display="None" ValidateEmptyText="true"
                          OnServerValidate="validateReCaptchaV2SiteKey_ServerValidate"></asp:CustomValidator>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="VerticalOption">
                  <div class="SelectOption">
                    <div class="SelectOptionField" style="width: 60%">
                      <div class="FieldHeading">
                        <asp:Label ID="labelReCaptchaV2SecretKey" runat="server" AssociatedControlID="inputReCaptchaV2SecretKey" Text="Secret key: " CssClass="FieldLabel"></asp:Label>
                        <span class="FieldRequired">*</span>
                        <span class="clear"></span>
                      </div>
                      <div class="FieldContent">
                        <asp:TextBox ID="inputReCaptchaV2SecretKey" runat="server" CssClass="FieldSelect FieldInput"></asp:TextBox>
                        <asp:CustomValidator ID="validateReCaptchaV2SecretKey" runat="server" ControlToValidate="inputReCaptchaV2SecretKey"
                          ErrorMessage="Please enter your reCAPTCHA V2 secret key." Display="None" ValidateEmptyText="true"
                          OnServerValidate="validateReCaptchaV2SecretKey_ServerValidate"></asp:CustomValidator>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="VerticalOption" style="padding-top: 5px;">
                  <table class="CheckboxOption">
                    <tbody>
                      <tr>
                        <td class="CheckboxOptionField">
                          <asp:CheckBox ID="checkRequireRegisteredReCaptcha" runat="server" Text="Require linked users to complete reCAPTCHA test" />
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="StepGrouping">
      <h1 class="StepGroupingHeading">Form submission</h1>
      <div class="StepGroupingBody">
        <div class="HelpText">
          Choose whether the form will display a customizable message or navigate to an alternative page after it has been submitted.
        </div>
        <div class="RadioContainer">
          <div class="RadioGroupingNarrow">
            <p id="ColumnDefaultItem" class="RadioSelected">
              <asp:RadioButton runat="server" ID="ColumnDefaulRadioButton" GroupName="ColumnGroup"
                Text="Success message" Checked="true"></asp:RadioButton>
            </p>
            <p id="ColumnCustomItem">
              <asp:RadioButton runat="server" ID="ColumnCustomRadioButton" GroupName="ColumnGroup"
                Text="Alternative Page"></asp:RadioButton>
            </p>
          </div>
          <div class="SelectedAreaNarrow">
            <div class="SelectedAreaInner">
              <div id="ColumnDefaultPanel">
                <div class="HelpText">
                  Define what the success message will look like on the contact us page after the form has been submitted.
                </div>
                <asp:UpdatePanel ID="SuccessMessageUpdatePanel" runat="server" ChildrenAsTriggers="true" RenderMode="Block" UpdateMode="Conditional">
                  <ContentTemplate>
                    <div>
                      <p style="text-align: right">
                        <asp:LinkButton ID="buttonHtmlSuccessMessageLoadDefault" runat="server" Text="Load default success message" OnClick="buttonHtmlSuccessMessageLoadDefault_Click" CausesValidation="false"></asp:LinkButton>
                      </p>
                      <bbnc:HTMLEditor id="htmlSuccessMessageEditor" runat="server" />
                    </div>
                  </ContentTemplate>
                </asp:UpdatePanel>
              </div>
              <div id="ColumnCustomPanel" style="display: none;">
                <div class="HelpText">
                  Define the alternative page that will be displayed after the form has been submitted.
                </div>
                <asp:UpdatePanel ID="PageLinkUpdatePanel" runat="server" ChildrenAsTriggers="true" RenderMode="Block" UpdateMode="Conditional">
                  <ContentTemplate>
                    <uc2:pagelink ID="pageLinkConfirmationPage" CanDelete="true" runat="server" Tag="Confirmation page" />
                  </ContentTemplate>
                </asp:UpdatePanel>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="tab_AcknowledgementEmail">
    <div class="StepGrouping">
      <h1 class="StepGroupingHeading">Acknowledgement recipients</h1>
      <div class="StepGroupingBody">
        <div class="HelpText">
          Define who will receive the acknowledgement email when the form has been submitted. At least one recipient must be added.
        </div>
        <div class="VerticalOption">
          <asp:UpdatePanel ID="EmailRecipientUpdatePanel" runat="server" ChildrenAsTriggers="true" RenderMode="Block" UpdateMode="Conditional">
            <ContentTemplate>
              <asp:CustomValidator ID="EmailRecipientCustomValidator" runat="server" Display="None" OnServerValidate="EmailRecipientCustomValidator_ServerValidate" />
              <asp:DataGrid ID="EmailRecipientDataGrid" runat="server" AutoGenerateColumns="false" CssClass="DataGrid" OnItemCommand="EmailRecipientDataGrid_ItemCommand"
                CellPadding="0" CellSpacing="0" GridLines="None">
                <ItemStyle CssClass="DataGridItem" />
                <AlternatingItemStyle CssClass="DataGridItemAlternating" />
                <HeaderStyle CssClass="DataGridHeader" />
                <Columns>
                  <asp:TemplateColumn HeaderText="Display name">
                    <HeaderStyle Width="40%" />
                    <ItemStyle Wrap="false" CssClass="DataGridItemCell DataGridItemCellLeft" />
                    <ItemTemplate>
                      <asp:TextBox ID="NameTextBox" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.DisplayName") %>' Width="90%" runat="server" />
                      <asp:Label ID="NameRequiredLabel" CssClass="RequiredFieldMarker" Text="*" runat="server"></asp:Label>
                    </ItemTemplate>
                  </asp:TemplateColumn>
                  <asp:TemplateColumn HeaderText="Email address">
                    <HeaderStyle Width="40%" />
                    <ItemStyle Wrap="false" CssClass="DataGridItemCell" />
                    <ItemTemplate>
                      <asp:TextBox ID="EmailAddressTextBox" MaxLength="80" Text='<%# DataBinder.Eval(Container, "DataItem.EmailAddress")%>' Width="90%" runat="server" />
                      <asp:Label ID="EmailAddressRequiredLabel" CssClass="RequiredFieldMarker" Text="*" runat="server"></asp:Label>
                    </ItemTemplate>
                  </asp:TemplateColumn>
                  <asp:TemplateColumn>
                    <ItemStyle HorizontalAlign="Center" CssClass="DataGridItemCell DataGridItemCellRight" />
                    <ItemTemplate>
                      <asp:Button ID="RemoveRecipientButton" CausesValidation="false" CssClass="CommandButton" Text='Remove'
                        ToolTip="Remove" runat="server" CommandName="Recipient" CommandArgument="Remove" />
                    </ItemTemplate>
                  </asp:TemplateColumn>
                </Columns>
              </asp:DataGrid>
              <div class="ListAdditionButton">
                <asp:Button ID="AddRecipientButton" CausesValidation="false" CssClass="CommandButton"
                  Text="Add Recipient" Width="125px" runat="server" OnClick="AddRecipientButton_Click" />
              </div>
            </ContentTemplate>
          </asp:UpdatePanel>
        </div>
      </div>
    </div>
    <div class="StepGrouping">
      <h1 class="StepGroupingHeading">Acknowledgement email</h1>
      <div class="StepGroupingBody">
        <div class="HelpText">
        </div>
        <asp:UpdatePanel ID="AcknowledgementEmailUpdatePanel" runat="server" ChildrenAsTriggers="true" RenderMode="Block" UpdateMode="Conditional">
          <ContentTemplate>
            <div class="VerticalOption">
              <table>
                <tr>
                  <td class="captionCell">
                    <asp:Label ID="labelAcknowledgementName" runat="server" Text="Name of email:"></asp:Label>
                    <asp:RequiredFieldValidator ID="requiredAcknowledgementName" runat="server" ErrorMessage="Acknowledgement name of email is required" Display="None" ControlToValidate="inputAcknowledgementName"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="validateAcknowledgementName" runat="server" ErrorMessage="Error" Display="None" ControlToValidate="inputAcknowledgementName" OnServerValidate="validateAcknowledgementName_ServerValidate"></asp:CustomValidator>
                  </td>
                  <td class="controlCell" style="width: 297px;">
                    <asp:TextBox ID="inputAcknowledgementName" runat="server" Style="width: 275px;"></asp:TextBox><span class="RequiredFieldMarker">*</span>
                  </td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td class="captionCell">
                    <asp:Label ID="labelAcknowledgementSubject" runat="server" Text="Subject:"></asp:Label>
                    <asp:RequiredFieldValidator ID="requiredAcknowledgementSubject" runat="server" ErrorMessage="Acknowledgement subject is required" Display="None" ControlToValidate="inputAcknowledgementSubject"></asp:RequiredFieldValidator>
                  </td>
                  <td class="controlCell" style="width: 297px;">
                    <asp:TextBox ID="inputAcknowledgementSubject" runat="server" Style="width: 275px;"></asp:TextBox><span class="RequiredFieldMarker">*</span>
                  </td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td class="captionCell">
                    <asp:Label ID="labelAcknowledgementFromAddress" runat="server" Text="From address:"></asp:Label>
                    <asp:RequiredFieldValidator ID="requiredAcknowledgementFromAddress" runat="server" ErrorMessage="Acknowledgement email from address is required" Display="None" ControlToValidate="inputAcknowledgementFromAddress"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="validAcknowledgementFromAddress" runat="server" ControlToValidate="inputAcknowledgementFromAddress"
                      ErrorMessage="Acknowledgement email from address is invalid" Display="None"
                      ValidationExpression="^[ ]*[A-Za-z0-9\-_.'`!#$%&*+/=?^{|}~]+@[A-Za-z0-9][A-Za-z0-9\-]*(\.[A-Za-z0-9][A-Za-z0-9\-]*)*\.[A-Za-z]{2,6}[ ]*$"></asp:RegularExpressionValidator>
                  </td>
                  <td class="controlCell" style="width: 297px;">
                    <asp:TextBox ID="inputAcknowledgementFromAddress" runat="server" Style="width: 275px;"></asp:TextBox><span class="RequiredFieldMarker">*</span>
                  </td>
                  <td class="captionCell">
                    <asp:Label ID="labelAcknowledgementFromName" runat="server" Text="From name:"></asp:Label>
                    <asp:RequiredFieldValidator ID="requiredAcknowledgementFromName" runat="server" ErrorMessage="Acknowledgement email from name is required" Display="None" ControlToValidate="inputAcknowledgementFromName"></asp:RequiredFieldValidator>
                  </td>
                  <td class="controlCell">
                    <asp:TextBox ID="inputAcknowledgementFromName" runat="server"></asp:TextBox><span class="RequiredFieldMarker">*</span>
                  </td>
                </tr>
              </table>
            </div>
            <div class="VerticalOption">
              <p style="text-align: right">
                <asp:LinkButton ID="buttonHtmlAcknowledgementDefault" runat="server" Text="Load default acknowledgement" OnClick="buttonHtmlAcknowledgementDefault_Click" CausesValidation="false"></asp:LinkButton>
              </p>
              <uc:HTMLEditor id="htmlAcknowledgementEditor" runat="server"></uc:HTMLEditor>
            </div>
          </ContentTemplate>
          <Triggers>
            <asp:AsyncPostBackTrigger ControlID="buttonHtmlAcknowledgementDefault" EventName="Click" />
          </Triggers>
        </asp:UpdatePanel>
      </div>
    </div>
  </div>
</div>

<%--used to keep the user on the correct tab after a postback--%>
<asp:HiddenField runat="server" ID="hfLastTab" Value="0" />
<script>
  function CheckFileUploadFolderState(sender, args) {
    args.IsValid = true;

    if ($('div[id$="_FileUpload_FolderSelectorPanel"').is(":visible")) {
      args.IsValid = false;
    }
  }
  var stepCurrentSelection = new Array("PlaceholderForZeroIndex", "ColumnDefault");

  function ChangePanel(stepNumber, newSelection)
  {		
    currentSelection = stepCurrentSelection[stepNumber];
    ToggleSelection(currentSelection, newSelection);
    stepCurrentSelection[stepNumber]=newSelection;
  }

  function ToggleSelection(oldSelection, newSelection)
  {
    //change radio button background
    currentRadioItem = $get(oldSelection+'Item');
    newRadioItem = $get(newSelection+'Item');
		
    currentRadioItem.className="";
    newRadioItem.className="RadioSelected";
				
    //change panel
    currentPanel = $get(oldSelection+'Panel');
    newPanel = $get(newSelection+'Panel');
		
    currentPanel.style.display = 'none';
    newPanel.style.display = '';		
  }

  $(document).ready(function () {

    if ($('#<%# Settings_Header.ClientID %>').is(":visible")) {
      alert("Heading is visible");
    }

    //start tabs
    $("#tabs_Main").tabs({ 
      show: function() { 
        var sel = $("#tabs_Main").tabs("option", "selected"); 
        $("#<%= hfLastTab.ClientID%>").val(sel); 
      }, selected: <%= hfLastTab.Value%>             
      });  

    $("#tabs_Main").show();
    //end tabs

    //start sortable
    $("#tblSelectedFields").sortable({
      placeholder: "ui-state-highlight",
      handle: ".handle",
      items: ".DataGridItem",
      stop: function (event, ui) {
        //"when the drag operation stops, grab the new order in the form of a list of question ids
        var orderedFieldIDs = "";
        $("#tblSelectedFields .DataGridItem").each(function (i, el) {
          //scan the new state of things and build a new list of Question ID GUIDs.
          var currentFieldIDField = $(el)[0].cells.item(3);
          var currentFieldID = "";
          if (currentFieldIDField.innerText != null) {
            currentFieldID = currentFieldIDField.innerText.trim();
            if (currentFieldID != null) {
              orderedFieldIDs += "[" + currentFieldID + '];'
            }
          }
        });
        //drop the final list into a server side control so the code-behind can pay attention to it on save
        document.getElementById('<%=OrderOfFields.ClientID%>').value = orderedFieldIDs;
      },
      'start': function (event, ui) {
        ui.placeholder.html("<td colspan='5'></td>")
      }
    });
    //end sortable
  });
</script>
