Imports BBNCExtensions
Imports System.Linq
Imports System.Xml.Linq
Imports Blackbaud.Web.Content.Portal.Platform
Imports Blackbaud.Web.Content.Core
Imports Blackbaud.Web.Content.SPWrap.fnEmailTemplateNameIsOK
Imports Infragistics.WebUI.UltraWebNavigator
Imports Infragistics.WebUI.Shared

Partial Public Class FormBuilderEditor
  Inherits BBNCExtensions.Parts.CustomPartEditorBase

  Private mContent As FormBuilderProperties
  Private language As Language
  Private constituent As Constituent

  Protected Overrides Sub OnPreRender(e As EventArgs)
    MyBase.OnPreRender(e)
    RenderPanels()
  End Sub
  Private Sub RenderPanels()
    Dim stringBuilder As StringBuilder = New StringBuilder()
    If MyContent.DisplayAlternativeSuccessPage Then
      stringBuilder.Append("ChangePanel(1, 'ColumnCustom');")
    End If
    ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "LoadPanels", stringBuilder.ToString(), True)
  End Sub

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    language = New Language()
    constituent = New Constituent()
    If Not (Page.IsPostBack) Then
      SetContentPartDetails()
      LoadEmailTemplate()
      FillAvailableFieldsMenu()
      LoadSelectedFields()
      LoadEmailRecipients()
      PopulateFormProperties()
      PopulateReCAPTCHASettings()
      PopulateAcknowledgementSettings()
      PopulateSuccessSettings()

    End If
    DirectCast(pageLinkConfirmationPage.FindControl("btnRemove"), ImageButton).CausesValidation = False
    Page.MaintainScrollPositionOnPostBack = True
    ColumnCustomRadioButton.Attributes("onClick") = "ChangePanel(1,'ColumnCustom');"
    ColumnDefaulRadioButton.Attributes("onClick") = "ChangePanel(1, 'ColumnDefault');"

  End Sub

  Public Overrides Function OnSaveContent(Optional ByVal bDialogIsClosing As Boolean = True) As Boolean

    With MyContent
      SaveSelectedFields()
      SaveFormProperties()
      SaveReCAPTCHASettings()
      SaveAcknowledgementSettings()
      SaveSuccessSettings()
      MyContent.PartIsNotNew = True
    End With

    Page.Validate()
    If Page.IsValid Then
      Me.Content.SaveContent(MyContent)
      Return True
    Else
      Return False
    End If

  End Function

  Private ReadOnly Property MyContent() As FormBuilderProperties
    Get
      If mContent Is Nothing Then
        mContent = Me.Content.GetContent(GetType(FormBuilderProperties))
        If mContent Is Nothing Then
          mContent = New FormBuilderProperties
        End If
      End If
      Return mContent
    End Get
  End Property

  Private Sub htmlAcknowledgementEditor_Init(ByVal sender As Object, ByVal e As EventArgs) Handles htmlAcknowledgementEditor.Init
    htmlAcknowledgementEditor.MergeFields = BBNCExtensions.Interfaces.EMergeFields.None

    htmlAcknowledgementEditor.EmailMode = True
    htmlAcknowledgementEditor.LayoutMode = False

    htmlAcknowledgementEditor.FieldListProvider = New Blackbaud.Web.Content.Core.Data.IFieldListProvider() {New MergeFieldsProvider}
  End Sub

  Private Property SelectedFieldSortOrder() As List(Of String)
    Get
      Return Me.MyContent.FieldOrder
    End Get
    Set(value As List(Of String))
      Me.MyContent.FieldOrder = value
    End Set
  End Property
  Private Property SelectedFieldSortOrderText As String
    Get
      Return Me.MyContent.FieldOrderText
    End Get
    Set(value As String)
      Me.MyContent.FieldOrderText = value
    End Set
  End Property
  Private Property SelectedFieldsDataSource() As List(Of Field)
    Get
      Return Me.MyContent.Fields
    End Get
    Set(value As List(Of Field))
      Me.MyContent.Fields = value
    End Set
  End Property
  Private Sub PopulateFormProperties()
    If Not (MyContent.PartIsNotNew) Then
      TextFormName.Text = hfPartTitle.Value
    Else
      TextFormName.Text = MyContent.FormName
    End If
  End Sub
  Private Sub SaveFormProperties()
    MyContent.FormName = TextFormName.Text
  End Sub
  Private Sub PopulateReCAPTCHASettings()
    checkIncludeReCaptcha.Checked = MyContent.UseReCaptcha
    checkRequireRegisteredReCaptcha.Checked = MyContent.RequireRegisteredReCaptcha
    inputReCaptchaV2SiteKey.Text = MyContent.ReCaptchaV2SiteKey
    inputReCaptchaV2SecretKey.Text = MyContent.ReCaptchaV2SecretKey
  End Sub
  Private Sub SaveReCAPTCHASettings()
    MyContent.UseReCaptcha = checkIncludeReCaptcha.Checked
    MyContent.RequireRegisteredReCaptcha = checkRequireRegisteredReCaptcha.Checked
    MyContent.ReCaptchaV2SiteKey = inputReCaptchaV2SiteKey.Text
    MyContent.ReCaptchaV2SecretKey = inputReCaptchaV2SecretKey.Text
  End Sub
  Private Sub FillAvailableFieldsMenu()
    ddlAvailableFields.Items.Add("")
    For Each item In eFieldType.GetValues()
      ddlAvailableFields.Items.Add(New ListItem(eFieldType.FieldTypeLabel(item), item))
    Next
  End Sub

  Private Function RegenerateFields(fields As ListView) As List(Of Field)

    SelectedFieldSortOrderText = OrderOfFields.Text
    Dim sortOrderText As String = OrderOfFields.Text
    Dim sortOrderTextClean = sortOrderText.Replace("[", "").Replace("]", "").Trim(";")
    SelectedFieldSortOrder = sortOrderTextClean.Split(";").ToList()

    Dim list As List(Of Field) = New List(Of Field)()

    For Each item As ListViewItem In fields.Items
      Dim li As Field = New Field()
      li.Type = DirectCast(item.FindControl("hfFieldType"), HiddenField).Value
      li.FieldId = DirectCast(item.FindControl("hfFieldId"), Label).Text
      If SelectedFieldSortOrder IsNot Nothing AndAlso SelectedFieldSortOrder.Count > 1 Then
        li.Position = SelectedFieldSortOrder.FindIndex(Function(x) x.Equals(li.FieldId.ToString()))
      Else
        li.Position = item.DataItemIndex
      End If

      li.Settings = XElement.Parse(DirectCast(item.FindControl("fieldSettingsDisplay"), TextBox).Text)
      list.Add(li)
    Next

    list = list.OrderBy(Function(x) x.Position).ToList()

    Return list
  End Function

  Private Sub GetFieldValues(field As Field, ByRef item As ListViewDataItem)
    Dim ph = DirectCast(item.FindControl("Placeholder"), PlaceHolder)
    Dim table = New HtmlTable()

    table.Controls.Add(field.Display(constituent, language, Me.API.AppFxWebServiceProvider, True))

    ph.Controls.Add(table)
  End Sub

  Private Sub SaveSelectedFields()
    Dim list As List(Of Field) = Me.RegenerateFields(Me.listSelectedFields)
    Me.SelectedFieldsDataSource = list
    Me.listSelectedFields.DataSource = Me.SelectedFieldsDataSource
    Me.listSelectedFields.DataBind()
  End Sub
  Private Sub LoadSelectedFields()
    Me.SelectedFieldSortOrder = MyContent.FieldOrder
    Me.SelectedFieldSortOrderText = MyContent.FieldOrderText
    Me.OrderOfFields.Text = MyContent.FieldOrderText

    If SelectedFieldsDataSource IsNot Nothing AndAlso SelectedFieldsDataSource.Count > 0 Then
      Me.SelectedFieldsDataSource = SelectedFieldsDataSource.OrderBy(Function(x) x.Position).ToList()
    Else
      Me.SelectedFieldsDataSource = MyContent.Fields
    End If

    Me.listSelectedFields.DataSource = Me.SelectedFieldsDataSource
    Me.listSelectedFields.DataBind()
  End Sub

  Protected Sub listSelectedFields_ItemCommand(sender As Object, e As ListViewCommandEventArgs)
    If e.CommandName = "Remove" Then
      Dim list As List(Of Field) = Me.RegenerateFields(Me.listSelectedFields)
      Dim eItem = e.Item
      list.RemoveAll(Function(x) x.FieldId = e.CommandArgument)

      If OrderOfFields.Text.Length <> 0 Then
        OrderOfFields.Text = OrderOfFields.Text.Replace("[" + e.CommandArgument + "];", "")
      Else

      End If

      Me.SelectedFieldsDataSource = list
      Me.listSelectedFields.DataSource = Me.SelectedFieldsDataSource
      Me.listSelectedFields.DataBind()

      If hfFieldId.Value = e.CommandArgument Then
        SetSectionVisibility()
      End If

    End If

    If e.CommandName = "Modify" Then
      SaveSelectedFields()
      SetFieldEditDisplay(e.CommandArgument, e.Item)
    End If

  End Sub

  Protected Sub SetFieldEditDisplay(id As Integer, item As ListViewItem)
    Dim fieldId As Integer = DirectCast(item.FindControl("hfFieldId"), Label).Text
    Dim fieldType As Integer = DirectCast(item.FindControl("hfFieldType"), HiddenField).Value
    Dim settings As XElement = XElement.Parse(DirectCast(item.FindControl("fieldSettingsDisplay"), TextBox).Text)

    hfFieldId.Value = fieldId
    SetSectionVisibility(fieldType)

    PopulateSettingsFields(fieldType, settings)
  End Sub

  Private Sub SetSectionVisibility()
    SetSectionVisibility(99999)
  End Sub
  Private Sub SetSectionVisibility(fieldType As Integer)
    Settings_Header.Visible = (fieldType = eFieldType.FieldType.Heading)
    Settings_Paragraph.Visible = (fieldType = eFieldType.FieldType.Paragraph)
    Settings_SingleLineText.Visible = (fieldType = eFieldType.FieldType.SingleLineText)
    Settings_MultiLineText.Visible = (fieldType = eFieldType.FieldType.MultiLineText)
    Settings_NumberField.Visible = (fieldType = eFieldType.FieldType.NumberField)
    Settings_Checkbox.Visible = (fieldType = eFieldType.FieldType.Checkbox)
    Settings_MultipleChoice.Visible = (fieldType = eFieldType.FieldType.MultipleChoice)
    Settings_DropDownMenu.Visible = (fieldType = eFieldType.FieldType.DropDownMenu)
    Settings_Email.Visible = (fieldType = eFieldType.FieldType.EmailField)
    Settings_Address.Visible = (fieldType = eFieldType.FieldType.AddressField)
    Settings_Phone.Visible = (fieldType = eFieldType.FieldType.PhoneField)
    Settings_DateTime.Visible = (fieldType = eFieldType.FieldType.DateTimeField)
    Settings_FileUpload.Visible = (fieldType = eFieldType.FieldType.FileUpload)
    Settings_Controls.Visible = (fieldType < 99999)
    Settings_Form.Visible = (fieldType = 99999)
  End Sub

  Private Sub PopulateSettingsFields(fieldType As Integer, settings As XElement)
    Select Case fieldType
      Case eFieldType.FieldType.Heading
        Heading_TextHeading.Text = settings.Element("Text")
        Heading_ListType.SelectedValue = settings.Element("Type")
        Heading_TextCssClass.Text = settings.Element("CSSClass").Value
      Case eFieldType.FieldType.Paragraph
        Paragraph_TextText.Text = settings.Element("Text")
        Paragraph_TextCssClass.Text = settings.Element("CSSClass").Value

      Case eFieldType.FieldType.SingleLineText
        PopulatePrePopulateField(SingleLineText_ListPrePopulate)
        SingleLineText_TextLabel.Text = settings.Element("Label").Value
        SingleLineText_CheckHideLabel.Checked = settings.Element("HideLabel").Value
        SingleLineText_CheckRequired.Checked = settings.Element("Required").Value
        SingleLineText_CheckHidden.Checked = settings.Element("Hidden").Value
        SingleLineText_TextHelpText.Text = settings.Element("HelpText").Value
        SingleLineText_TextDefaultValue.Text = settings.Element("DefaultValue").Value
        SingleLineText_ListPrePopulate.SelectedValue = settings.Element("PrePopulate").Value
        SingleLineText_CheckNumbersOnly.Checked = settings.Element("NumbersOnly").Value
        SingleLineText_TextMaxLength.Text = settings.Element("MaxLength").Value
      Case eFieldType.FieldType.MultiLineText
        MultiLineText_TextLabel.Text = settings.Element("Label").Value
        MultiLineText_CheckHideLabel.Checked = settings.Element("HideLabel").Value
        MultiLineText_CheckRequired.Checked = settings.Element("Required").Value
        MultiLineText_CheckHidden.Checked = settings.Element("Hidden").Value
        MultiLineText_TextHelpText.Text = settings.Element("HelpText").Value
        MultiLineText_TextRows.Text = settings.Element("Rows").Value
        MultiLineText_TextDefaultValue.Text = settings.Element("DefaultValue").Value
      Case eFieldType.FieldType.NumberField
        NumberField_TextLabel.Text = settings.Element("Label").Value
        NumberField_CheckHideLabel.Checked = settings.Element("HideLabel").Value
        NumberField_CheckRequired.Checked = settings.Element("Required").Value
        NumberField_CheckHidden.Checked = settings.Element("Hidden").Value
        NumberField_TextHelpText.Text = settings.Element("HelpText").Value
        NumberField_RadioNumberType.SelectedValue = settings.Element("NumberType").Value
        NumberField_TextMinValue.Text = settings.Element("MinValue").Value
        NumberField_TextMaxValue.Text = settings.Element("MaxValue").Value
        NumberField_TextBlankValue.Text = settings.Element("BlankValue").Value
        NumberField_TextDefaultValue.Text = settings.Element("DefaultValue").Value
        NumberField_TextAvailableCharacters.Text = settings.Element("AvailableCharacters").Value
        NumberField_TextMaxLength.Text = settings.Element("MaxLength").Value
      Case eFieldType.FieldType.Checkbox
        Checkbox_TextLabel.Text = settings.Element("Label").Value
        Checkbox_CheckHideLabel.Checked = settings.Element("HideLabel").Value
        Checkbox_CheckRequired.Checked = settings.Element("Required").Value
        Checkbox_CheckHidden.Checked = settings.Element("Hidden").Value
        Checkbox_TextCaption.Text = settings.Element("Caption").Value
        Checkbox_CheckDefaultValue.Checked = settings.Element("DefaultValue").Value
      Case eFieldType.FieldType.MultipleChoice
        MultipleChoice_TextLabel.Text = settings.Element("Label").Value
        MultipleChoice_CheckHideLabel.Checked = settings.Element("HideLabel").Value
        MultipleChoice_CheckRequired.Checked = settings.Element("Required").Value
        MultipleChoice_ListColumns.SelectedValue = settings.Element("Columns").Value
        MultipleChoice_RadioRenderStyle.SelectedValue = settings.Element("RenderStyle").Value
        MultipleChoice_TextValues.Text = settings.Element("Values").Value
        MultipleChoice_CheckShowOtherOption.Checked = settings.Element("ShowOtherOption").Value
        MultipleChoice_TextOtherLabel.Text = settings.Element("OtherLabel").Value

        PopulateCodeTableField(MultipleChoice_ListCodeTables)

        Try
          MultipleChoice_ListCodeTables.SelectedValue = settings.Element("CodeTable").Value
          If settings.Element("CodeTable").Value.Length > 0 Then
            MultipleChoice_TextValues.Enabled = False
            MultipleChoice_TextValues_Required.Enabled = False
          End If
        Catch ex As Exception
        End Try
      Case eFieldType.FieldType.DropDownMenu
        DropDownMenu_TextLabel.Text = settings.Element("Label").Value
        DropDownMenu_CheckHideLabel.Checked = settings.Element("HideLabel").Value
        DropDownMenu_CheckRequired.Checked = settings.Element("Required").Value
        DropDownMenu_CheckHidden.Checked = settings.Element("Hidden").Value
        DropDownMenu_TextHelpText.Text = settings.Element("HelpText").Value
        DropDownMenu_TextValues.Text = settings.Element("Values").Value

        PopulateCodeTableField(DropDownMenu_ListCodeTables)

        Try
          DropDownMenu_ListCodeTables.SelectedValue = settings.Element("CodeTable").Value
          If settings.Element("CodeTable").Value.Length > 0 Then
            DropDownMenu_TextValues.Enabled = False
            DropDownMenu_TextValues_Required.Enabled = False
          End If
        Catch ex As Exception
        End Try
      Case eFieldType.FieldType.EmailField
        PopulatePrePopulateField(EmailField_ListPrePopulate, ePopulatedFieldType.Group.Email)
        EmailField_TextTypes.Text = settings.Element("EmailTypes").Value
        EmailField_CheckAddTypeToLabel.Checked = settings.Element("AddTypeToLabel").Value
        EmailField_CheckRequired.Checked = settings.Element("Required").Value
        EmailField_CheckShowConfirmation.Checked = settings.Element("ShowConfirmation").Value
        EmailField_CheckTypeRequired.Checked = settings.Element("TypeRequired").Value
        EmailField_TextHelpText.Text = settings.Element("HelpText").Value
        EmailField_ListPrePopulate.SelectedValue = settings.Element("PrePopulate").Value

        EmailField_CheckTypeRequired.Enabled = settings.Element("Required").Value

        EmailField_CheckAddTypeToLabel.Enabled = (EmailField_TextTypes.Text.Split(vbLf).ToList.Count = 1 And EmailField_TextTypes.Text.Length > 0)
      Case eFieldType.FieldType.AddressField
        PopulatePrePopulateField(AddressField_ListPrePopulate, ePopulatedFieldType.Group.Address)
        AddressField_TextTypes.Text = settings.Element("AddressTypes").Value
        AddressField_CheckAddTypeToLabel.Checked = settings.Element("AddTypeToLabel").Value
        AddressField_CheckAddressRequired.Checked = settings.Element("AddressRequired").Value
        AddressField_CheckTypeRequired.Checked = settings.Element("TypeRequired").Value
        AddressField_CheckDefaultToUSA.Checked = settings.Element("DefaultToUSA").Value
        AddressField_CheckAllowOnlyUSA.Checked = settings.Element("OnlyAllowUSA").Value
        AddressField_ListPrePopulate.SelectedValue = settings.Element("PrePopulate").Value

        AddressField_CheckTypeRequired.Enabled = settings.Element("AddressRequired").Value
        AddressField_CheckDefaultToUSA.Enabled = (settings.Element("OnlyAllowUSA").Value = False)

        AddressField_CheckAddTypeToLabel.Enabled = (settings.Element("AddressTypes").Value.Split(vbLf).ToList.Count = 1)
      Case eFieldType.FieldType.PhoneField
        PopulatePrePopulateField(PhoneField_ListPrePopulate, ePopulatedFieldType.Group.Phone)
        PhoneField_CheckRequired.Checked = settings.Element("Required").Value
        PhoneField_CheckTypeRequired.Checked = settings.Element("TypeRequired").Value
        PhoneField_TextHelpText.Text = settings.Element("HelpText").Value
        PhoneField_TextTypes.Text = settings.Element("PhoneTypes").Value
        PhoneField_CheckAddTypeToLabel.Checked = settings.Element("AddTypeToLabel").Value
        PhoneField_TextAvailableCharacters.Text = settings.Element("AvailableCharacters").Value
        PhoneField_ListPrePopulate.SelectedValue = settings.Element("PrePopulate").Value

        PhoneField_CheckTypeRequired.Enabled = settings.Element("Required").Value

        PhoneField_CheckAddTypeToLabel.Enabled = (settings.Element("PhoneTypes").Value.Split(vbLf).ToList.Count = 1)
      Case eFieldType.FieldType.DateTimeField
        DateTimeField_TextLabel.Text = settings.Element("Label").Value
        DateTimeField_CheckHideLabel.Checked = settings.Element("HideLabel").Value
        DateTimeField_CheckRequired.Checked = settings.Element("Required").Value
        DateTimeField_RadioDateType.SelectedValue = settings.Element("DateType").Value
        DateTimeField_CheckSplitMonthDayYear.Checked = settings.Element("SplitMonthDayYear").Value

        DateTimeField_CheckSplitMonthDayYear.Enabled = (settings.Element("DateType").Value = 0)
      Case eFieldType.FieldType.FileUpload
        FileUpload_TextLabel.Text = settings.Element("Label").Value
        FileUpload_CheckHideLabel.Checked = settings.Element("HideLabel").Value
        FileUpload_CheckRequired.Checked = settings.Element("Required").Value
        FileUpload_TextHelpText.Text = settings.Element("HelpText").Value
        FileUpload_RadioFileType.SelectedIndex = settings.Element("FileType").Value
        FileUpload_ListMaxFileSize.SelectedValue = settings.Element("MaxFileSize").Value
        FileUpload_TextFolderDisplay.Text = settings.Element("FolderPath").Value
        FileUpload_FolderDisplayIdHidden.Value = settings.Element("FolderId").Value
        PopulateRadioFileType()
        FileUpload_FolderDisplayPanel.Visible = True
        FileUpload_FolderSelectorPanel.Attributes.Add("class", "HiddenRow")
      Case Else

    End Select
  End Sub

  Private Sub SaveSettingsFields(id As Integer)
    Dim item As ListViewItem = New ListViewItem(ListViewItemType.EmptyItem)
    For Each i In listSelectedFields.Items
      If DirectCast(i.FindControl("hfFieldId"), Label).Text = id Then
        item = i
        Exit For
      End If
    Next
    Dim fieldId As Integer = DirectCast(item.FindControl("hfFieldId"), Label).Text
    Dim fieldType As Integer = DirectCast(item.FindControl("hfFieldType"), HiddenField).Value
    Dim settingsField As TextBox = DirectCast(item.FindControl("fieldSettingsDisplay"), TextBox)
    Dim settings As XElement = XElement.Parse(DirectCast(item.FindControl("fieldSettingsDisplay"), TextBox).Text)

    Select Case fieldType
      Case eFieldType.FieldType.Heading
        settings.Element("Text").Value = Heading_TextHeading.Text
        settings.Element("Type").Value = Heading_ListType.SelectedValue
        settings.Element("CSSClass").Value = Heading_TextCssClass.Text
      Case eFieldType.FieldType.Paragraph
        settings.Element("Text").Value = Paragraph_TextText.Text
        settings.Element("CSSClass").Value = Paragraph_TextCssClass.Text

      Case eFieldType.FieldType.SingleLineText
        settings.Element("Label").Value = SingleLineText_TextLabel.Text
        settings.Element("HideLabel").Value = SingleLineText_CheckHideLabel.Checked
        settings.Element("Required").Value = SingleLineText_CheckRequired.Checked
        settings.Element("Hidden").Value = SingleLineText_CheckHidden.Checked
        settings.Element("HelpText").Value = SingleLineText_TextHelpText.Text
        settings.Element("DefaultValue").Value = SingleLineText_TextDefaultValue.Text
        settings.Element("PrePopulate").Value = SingleLineText_ListPrePopulate.SelectedValue
        settings.Element("NumbersOnly").Value = SingleLineText_CheckNumbersOnly.Checked
        settings.Element("MaxLength").Value = SingleLineText_TextMaxLength.Text
      Case eFieldType.FieldType.MultiLineText
        settings.Element("Label").Value = MultiLineText_TextLabel.Text
        settings.Element("HideLabel").Value = MultiLineText_CheckHideLabel.Checked
        settings.Element("Required").Value = MultiLineText_CheckRequired.Checked
        settings.Element("Hidden").Value = MultiLineText_CheckHidden.Checked
        settings.Element("HelpText").Value = MultiLineText_TextHelpText.Text
        settings.Element("Rows").Value = MultiLineText_TextRows.Text
        settings.Element("DefaultValue").Value = MultiLineText_TextDefaultValue.Text
      Case eFieldType.FieldType.NumberField
        settings.Element("Label").Value = NumberField_TextLabel.Text
        settings.Element("HideLabel").Value = NumberField_CheckHideLabel.Checked
        settings.Element("Required").Value = NumberField_CheckRequired.Checked
        settings.Element("Hidden").Value = NumberField_CheckHidden.Checked
        settings.Element("HelpText").Value = NumberField_TextHelpText.Text
        settings.Element("NumberType").Value = NumberField_RadioNumberType.SelectedValue
        settings.Element("MinValue").Value = NumberField_TextMinValue.Text
        settings.Element("MaxValue").Value = NumberField_TextMaxValue.Text
        settings.Element("BlankValue").Value = NumberField_TextBlankValue.Text
        settings.Element("DefaultValue").Value = NumberField_TextDefaultValue.Text
        settings.Element("AvailableCharacters").Value = NumberField_TextAvailableCharacters.Text
        settings.Element("MaxLength").Value = NumberField_TextMaxLength.Text
      Case eFieldType.FieldType.Checkbox
        settings.Element("Label").Value = Checkbox_TextLabel.Text
        settings.Element("HideLabel").Value = Checkbox_CheckHideLabel.Checked
        settings.Element("Required").Value = Checkbox_CheckRequired.Checked
        settings.Element("Hidden").Value = Checkbox_CheckHidden.Checked
        settings.Element("Caption").Value = Checkbox_TextCaption.Text
        settings.Element("DefaultValue").Value = Checkbox_CheckDefaultValue.Checked
      Case eFieldType.FieldType.MultipleChoice
        settings.Element("Label").Value = MultipleChoice_TextLabel.Text
        settings.Element("HideLabel").Value = MultipleChoice_CheckHideLabel.Checked
        settings.Element("Required").Value = MultipleChoice_CheckRequired.Checked
        settings.Element("Columns").Value = MultipleChoice_ListColumns.SelectedValue
        settings.Element("RenderStyle").Value = MultipleChoice_RadioRenderStyle.SelectedValue
        settings.Element("Values").Value = MultipleChoice_TextValues.Text
        settings.Element("ShowOtherOption").Value = MultipleChoice_CheckShowOtherOption.Checked
        settings.Element("OtherLabel").Value = MultipleChoice_TextOtherLabel.Text
        settings.Element("CodeTable").Value = MultipleChoice_ListCodeTables.SelectedValue
      Case eFieldType.FieldType.DropDownMenu
        settings.Element("Label").Value = DropDownMenu_TextLabel.Text
        settings.Element("HideLabel").Value = DropDownMenu_CheckHideLabel.Checked
        settings.Element("Required").Value = DropDownMenu_CheckRequired.Checked
        settings.Element("Hidden").Value = DropDownMenu_CheckHidden.Checked
        settings.Element("HelpText").Value = DropDownMenu_TextHelpText.Text
        settings.Element("Values").Value = DropDownMenu_TextValues.Text
        settings.Element("CodeTable").Value = DropDownMenu_ListCodeTables.SelectedValue
      Case eFieldType.FieldType.EmailField
        settings.Element("EmailTypes").Value = EmailField_TextTypes.Text
        settings.Element("AddTypeToLabel").Value = EmailField_CheckAddTypeToLabel.Checked
        settings.Element("Required").Value = EmailField_CheckRequired.Checked
        settings.Element("ShowConfirmation").Value = EmailField_CheckShowConfirmation.Checked
        settings.Element("TypeRequired").Value = EmailField_CheckTypeRequired.Checked
        settings.Element("HelpText").Value = EmailField_TextHelpText.Text
        settings.Element("PrePopulate").Value = EmailField_ListPrePopulate.SelectedValue
      Case eFieldType.FieldType.AddressField
        settings.Element("AddressTypes").Value = AddressField_TextTypes.Text
        settings.Element("AddTypeToLabel").Value = AddressField_CheckAddTypeToLabel.Checked
        settings.Element("AddressRequired").Value = AddressField_CheckAddressRequired.Checked
        settings.Element("TypeRequired").Value = AddressField_CheckTypeRequired.Checked
        settings.Element("DefaultToUSA").Value = AddressField_CheckDefaultToUSA.Checked
        settings.Element("OnlyAllowUSA").Value = AddressField_CheckAllowOnlyUSA.Checked
        settings.Element("PrePopulate").Value = AddressField_ListPrePopulate.SelectedValue
      Case eFieldType.FieldType.PhoneField
        settings.Element("Required").Value = PhoneField_CheckRequired.Checked
        settings.Element("TypeRequired").Value = PhoneField_CheckTypeRequired.Checked
        settings.Element("HelpText").Value = PhoneField_TextHelpText.Text
        settings.Element("PhoneTypes").Value = PhoneField_TextTypes.Text
        settings.Element("AddTypeToLabel").Value = PhoneField_CheckAddTypeToLabel.Checked
        settings.Element("AvailableCharacters").Value = PhoneField_TextAvailableCharacters.Text
        settings.Element("PrePopulate").Value = PhoneField_ListPrePopulate.SelectedValue
      Case eFieldType.FieldType.DateTimeField
        settings.Element("Label").Value = DateTimeField_TextLabel.Text
        settings.Element("HideLabel").Value = DateTimeField_CheckHideLabel.Checked
        settings.Element("Required").Value = DateTimeField_CheckRequired.Checked
        settings.Element("DateType").Value = DateTimeField_RadioDateType.SelectedValue
        settings.Element("SplitMonthDayYear").Value = DateTimeField_CheckSplitMonthDayYear.Checked
      Case eFieldType.FieldType.FileUpload
        settings.Element("Label").Value = FileUpload_TextLabel.Text
        settings.Element("HideLabel").Value = FileUpload_CheckHideLabel.Checked
        settings.Element("Required").Value = FileUpload_CheckRequired.Checked
        settings.Element("HelpText").Value = FileUpload_TextHelpText.Text
        settings.Element("FileType").Value = FileUpload_RadioFileType.SelectedIndex
        settings.Element("MaxFileSize").Value = FileUpload_ListMaxFileSize.SelectedValue
        settings.Element("FolderPath").Value = FileUpload_TextFolderDisplay.Text
        settings.Element("FolderId").Value = FileUpload_FolderDisplayIdHidden.Value
      Case Else

    End Select
    settingsField.Text = settings.ToString

  End Sub

  Protected Sub PopulatePrePopulateField(ByRef field As DropDownList, group As ePopulatedFieldType.FieldType)
    field.Items.Clear()

    field.Items.Add(New ListItem("", "0"))

    For Each item In ePopulatedFieldType.GetValues(group)
      field.Items.Add(New ListItem(ePopulatedFieldType.Label(item), item))
    Next
  End Sub
  Protected Sub PopulatePrePopulateField(ByRef field As DropDownList)
    field.Items.Clear()

    field.Items.Add(New ListItem("", "0"))

    For Each item In ePopulatedFieldType.GetValues()
      field.Items.Add(New ListItem(ePopulatedFieldType.Label(item), item))
    Next
  End Sub

  Protected Sub listSelectedFields_ItemDataBound(sender As Object, e As ListViewItemEventArgs)
    Dim item = DirectCast(e.Item, ListViewDataItem)
    Dim field = DirectCast(item.DataItem, Field)

    GetFieldValues(field, item)
  End Sub

  Protected Sub FieldTypeList_ItemCommand(sender As Object, e As ListViewCommandEventArgs)
    Dim list As List(Of Field) = Me.RegenerateFields(Me.listSelectedFields)
    Dim NextFieldId As Integer = 0
    Dim NextPosition As Integer = 0
    If list.Count > 0 Then
      NextFieldId = list.Max(Function(t) t.FieldId) + 1
      NextPosition = list.Count + 1
    End If

    If OrderOfFields.Text.Length = 0 Then
      OrderOfFields.Text = "[" + NextFieldId.ToString + "];"
    Else
      OrderOfFields.Text = OrderOfFields.Text + "[" + NextFieldId.ToString + "];"
    End If

    Dim item As Field = New Field() With {.Type = e.CommandArgument, .Position = NextPosition, .FieldId = NextFieldId}
    list.Add(item)

    Me.SelectedFieldsDataSource = list
    Me.listSelectedFields.DataSource = Me.SelectedFieldsDataSource
    Me.listSelectedFields.DataBind()

  End Sub

  Protected Sub btnSaveSettings_Click(sender As Object, e As EventArgs)
    If hfFieldId.Value IsNot Nothing Then
      SaveSettingsFields(hfFieldId.Value)
      SaveSelectedFields()
      SetSectionVisibility()
    End If
  End Sub

  Protected Sub btnCancelSettings_Click(sender As Object, e As EventArgs)
    SaveSelectedFields()
    SetSectionVisibility()
  End Sub

  Protected Sub btnAddNewField_Click(sender As Object, e As EventArgs)
    Dim list As List(Of Field) = Me.RegenerateFields(Me.listSelectedFields)
    Dim NextFieldId As Integer = 0
    Dim NextPosition As Integer = 0
    If list.Count > 0 Then
      NextFieldId = list.Max(Function(t) t.FieldId) + 1
      NextPosition = list.Count + 1
    End If

    If OrderOfFields.Text.Length = 0 Then
      OrderOfFields.Text = "[" + NextFieldId.ToString + "];"
    Else
      OrderOfFields.Text = OrderOfFields.Text + "[" + NextFieldId.ToString + "];"
    End If
    If ddlAvailableFields.SelectedValue.Length Then
      Dim item As Field = New Field() With {.Type = ddlAvailableFields.SelectedValue, .Position = NextPosition, .FieldId = NextFieldId}
      list.Add(item)
    End If

    Me.SelectedFieldsDataSource = list
    Me.listSelectedFields.DataSource = Me.SelectedFieldsDataSource
    Me.listSelectedFields.DataBind()
  End Sub

  Protected Sub validateReCaptchaV2SecretKey_ServerValidate(source As Object, args As ServerValidateEventArgs)
    If checkIncludeReCaptcha.Checked AndAlso inputReCaptchaV2SecretKey.Text = "" Then
      args.IsValid = False
    Else
      args.IsValid = True
    End If
  End Sub

  Protected Sub validateReCaptchaV2SiteKey_ServerValidate(source As Object, args As ServerValidateEventArgs)
    If checkIncludeReCaptcha.Checked AndAlso inputReCaptchaV2SiteKey.Text = "" Then
      args.IsValid = False
    Else
      args.IsValid = True
    End If
  End Sub

  Private Property EmailRecipientsDataSource() As System.Collections.Generic.List(Of EmailRecipient)
    Get
      Return Me.MyContent.EmailRecipients
    End Get
    Set(value As System.Collections.Generic.List(Of EmailRecipient))
      Me.MyContent.EmailRecipients = value
    End Set
  End Property

  Protected Sub AddRecipientButton_Click(sender As Object, e As EventArgs)
    Dim list As System.Collections.Generic.List(Of EmailRecipient) = Me.regenerateDataSource(Me.EmailRecipientDataGrid)
    Dim item As EmailRecipient = Nothing
    list.Add(item)
    Me.EmailRecipientsDataSource = list
    Me.EmailRecipientDataGrid.DataSource = Me.EmailRecipientsDataSource
    Me.EmailRecipientDataGrid.DataBind()
  End Sub

  Private Function regenerateDataSource(Grid As DataGrid) As System.Collections.Generic.List(Of EmailRecipient)
    Dim list As System.Collections.Generic.List(Of EmailRecipient) = New System.Collections.Generic.List(Of EmailRecipient)()

    For Each item As DataGridItem In EmailRecipientDataGrid.Items
      Dim li As EmailRecipient = New EmailRecipient()
      li.DisplayName = DirectCast(item.FindControl("NameTextBox"), TextBox).Text
      li.EmailAddress = DirectCast(item.FindControl("EmailAddressTextBox"), TextBox).Text
      list.Add(li)
    Next

    Return list
  End Function

  Protected Sub EmailRecipientCustomValidator_ServerValidate(source As Object, args As ServerValidateEventArgs)
    args.IsValid = True
    Dim emailExpression As New Regex("^[ ]*[A-Za-z0-9\-_.'`!#$%&*+/=?^{|}~]+@[A-Za-z0-9][A-Za-z0-9\-]*(\.[A-Za-z0-9][A-Za-z0-9\-]*)*\.[A-Za-z]{2,6}[ ]*$")

    If EmailRecipientDataGrid.Items.Count = 0 Then
      EmailRecipientCustomValidator.ErrorMessage = "At least one acknowledgement recipient is required."
      args.IsValid = False
    Else
      For Each item As DataGridItem In EmailRecipientDataGrid.Items
        Dim name = DirectCast(item.FindControl("NameTextBox"), TextBox).Text
        Dim email = DirectCast(item.FindControl("EmailAddressTextBox"), TextBox).Text
        If name = "" Or email = "" Then
          EmailRecipientCustomValidator.ErrorMessage = "Display name and email address are required for all email recipients."
          args.IsValid = False
          Exit For
        ElseIf Not emailExpression.IsMatch(email) Then
          EmailRecipientCustomValidator.ErrorMessage = "One or more recipient email addresses are invalid."
          args.IsValid = False
          Exit For
        End If
      Next
    End If
  End Sub

  Protected Sub EmailRecipientDataGrid_ItemCommand(source As Object, e As DataGridCommandEventArgs)
    If e.CommandArgument = "Remove" Then
      Dim list As System.Collections.Generic.List(Of EmailRecipient) = Me.regenerateDataSource(Me.EmailRecipientDataGrid)
      list.RemoveAt(e.Item.ItemIndex)

      Me.EmailRecipientsDataSource = list
      Me.EmailRecipientDataGrid.DataSource = Me.EmailRecipientsDataSource
      Me.EmailRecipientDataGrid.DataBind()
    End If
  End Sub

  Private Sub SaveEmailRecipients()
    Dim list As System.Collections.Generic.List(Of EmailRecipient) = Me.regenerateDataSource(Me.EmailRecipientDataGrid)
    MyContent.EmailRecipients = list
  End Sub
  Private Sub LoadEmailRecipients()
    Me.EmailRecipientsDataSource = MyContent.EmailRecipients
    Me.EmailRecipientDataGrid.DataSource = Me.EmailRecipientsDataSource
    Me.EmailRecipientDataGrid.DataBind()
  End Sub

  Private Sub SaveAcknowledgementSettings()
    SaveEmailRecipients()
    Try
      Dim template As EmailTemplate = New EmailTemplate(Integer.Parse(hfEmailTemplateId.Value))
      template.DataSourceID = 303
      template.ContentHTML = htmlAcknowledgementEditor.StorageHTML
      template.Name = inputAcknowledgementName.Text
      template.Subject = inputAcknowledgementSubject.Text
      template.FromAddress = inputAcknowledgementFromAddress.Text
      template.FromDisplayName = inputAcknowledgementFromName.Text
      template.Save()
    Catch ex As Exception
      Throw ex
    End Try
  End Sub
  Private Sub PopulateAcknowledgementSettings()
    LoadEmailRecipients()

    Dim template As EmailTemplate = New EmailTemplate(Integer.Parse(hfEmailTemplateId.Value))

    inputAcknowledgementName.Text = template.Name
    inputAcknowledgementSubject.Text = template.Subject
    inputAcknowledgementFromAddress.Text = template.FromAddress
    inputAcknowledgementFromName.Text = template.FromDisplayName

    If MyContent.PartIsNotNew = False Then
      htmlAcknowledgementEditor.StorageHTML = DefaultAcknowledgementStorageHTML
    Else
      htmlAcknowledgementEditor.StorageHTML = template.ContentHTML
    End If
  End Sub

  Private Sub SaveSuccessSettings()
    MyContent.SuccessMessageStorageHTML = htmlSuccessMessageEditor.StorageHTML
    MyContent.ConfirmationPageId = DirectCast(pageLinkConfirmationPage, Blackbaud.Web.Content.Portal.PageLink).PageID
    MyContent.DisplayAlternativeSuccessPage = ColumnCustomRadioButton.Checked
  End Sub

  Private Sub PopulateSuccessSettings()
    If MyContent.PartIsNotNew = False Then
      htmlSuccessMessageEditor.StorageHTML = DefaultSuccessMessageStorageHTML
    Else
      htmlSuccessMessageEditor.StorageHTML = MyContent.SuccessMessageStorageHTML
    End If

    DirectCast(pageLinkConfirmationPage, Blackbaud.Web.Content.Portal.PageLink).PageID = MyContent.ConfirmationPageId
    ColumnCustomRadioButton.Checked = MyContent.DisplayAlternativeSuccessPage
  End Sub

  Protected Sub EmailField_TextTypes_TextChanged(sender As Object, e As EventArgs)
    If (EmailField_TextTypes.Text.Split(vbLf).ToList.Count = 1 And EmailField_TextTypes.Text.Length > 0) Then
      EmailField_CheckAddTypeToLabel.Enabled = True
    Else
      EmailField_CheckAddTypeToLabel.Enabled = False
      EmailField_CheckAddTypeToLabel.Checked = False
    End If
  End Sub

  Protected Sub AddressField_TextTypes_TextChanged(sender As Object, e As EventArgs)
    If (AddressField_TextTypes.Text.Split(vbLf).ToList.Count = 1 And AddressField_TextTypes.Text.Length > 0) Then
      AddressField_CheckAddTypeToLabel.Enabled = True
    Else
      AddressField_CheckAddTypeToLabel.Enabled = False
      AddressField_CheckAddTypeToLabel.Checked = False
    End If
  End Sub

  Protected Sub PhoneField_TextTypes_TextChanged(sender As Object, e As EventArgs)
    If (PhoneField_TextTypes.Text.Split(vbLf).ToList.Count = 1 And PhoneField_TextTypes.Text.Length > 0) Then
      PhoneField_CheckAddTypeToLabel.Enabled = True
    Else
      PhoneField_CheckAddTypeToLabel.Enabled = False
      PhoneField_CheckAddTypeToLabel.Checked = False
    End If
  End Sub

  Protected Sub AddressField_CheckAllowOnlyUSA_CheckedChanged(sender As Object, e As EventArgs)
    AddressField_CheckDefaultToUSA.Enabled = Not (AddressField_CheckAllowOnlyUSA.Checked)
    AddressField_CheckDefaultToUSA.Checked = True
  End Sub

  Protected Sub DateTimeField_RadioDateType_SelectedIndexChanged(sender As Object, e As EventArgs)
    DateTimeField_CheckSplitMonthDayYear.Enabled = (DateTimeField_RadioDateType.SelectedIndex = 0)
    DateTimeField_CheckSplitMonthDayYear.Checked = (DateTimeField_RadioDateType.SelectedIndex = 0)
  End Sub

  Protected Sub FileUpload_RadioFileType_SelectedIndexChanged(sender As Object, e As EventArgs)
    PopulateRadioFileType()
  End Sub
  Private Sub PopulateRadioFileType()
    If FileUpload_RadioFileType.SelectedIndex = 0 Then
      FileUpload_TextFileTypesPermitted.Text = "jpg, jpeg, png, gif"
      FileUpload_TextHelpText.Text = "jpg, jpeg, png or gif"
    Else
      FileUpload_TextFileTypesPermitted.Text = "pdf, doc, docx, xls, xlsx, csv, txt, rtf"
      FileUpload_TextHelpText.Text = "pdf, doc(x), xls(x), csv, txt or rtf"
    End If
  End Sub

  Protected Sub validateAcknowledgementName_ServerValidate(source As Object, args As ServerValidateEventArgs)
    If Not IsEmailTemplateNameOK() Then
      validateAcknowledgementName.ErrorMessage = "Acknowledgement name of email must be unique. Please specify another name."
      args.IsValid = False
    Else
      args.IsValid = True
    End If
  End Sub

  Public Function IsEmailTemplateNameOK() As Boolean
    Dim result As Boolean = False
    Try
      result = WrapperRoutines.ExecuteScalar(DataObject.AppConnString(), Integer.Parse(hfEmailTemplateId.Value), Me.inputAcknowledgementName.Text, Integer.Parse(hfSiteId.Value))
    Catch ex As System.Exception
      result = False
    End Try
    Return result
  End Function

  Protected Sub buttonHtmlSuccessMessageLoadDefault_Click(sender As Object, e As EventArgs)
    htmlSuccessMessageEditor.StorageHTML = DefaultSuccessMessageStorageHTML
  End Sub
  Public ReadOnly Property DefaultSuccessMessageStorageHTML() As String
    Get
      Dim html As String = ""
      html += "<h3>Submission successful</h3>" + vbCrLf
      html += "<p>Thank you for your form submission.</p>" + vbCrLf
      Return html
    End Get
  End Property

  Protected Sub buttonHtmlAcknowledgementDefault_Click(sender As Object, e As EventArgs)
    htmlAcknowledgementEditor.StorageHTML = DefaultAcknowledgementStorageHTML
  End Sub
  Public ReadOnly Property DefaultAcknowledgementStorageHTML() As String
    Get
      Dim html As String = ""
      html += "<p><img src=""insertField.field?id=21&amp;nmode=0&amp;name=Recipient+display+name&amp;type=199621441"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""21"" attribid=""0"" searchable=""0"" fieldname=""Recipient display name"" fieldtype=""199621441"" htmlencode=""True"" isloop=""False"">,</p>" + vbCrLf
      html += "<p><strong>A new form submission has been received.<br></strong></p>" + vbCrLf
      html += "<table border=""0"" style=""width: 500px;""><tbody>" + vbCrLf
      html += "<tr><td>Constituent lookup ID:</td>" + vbCrLf
      html += "<td><img src=""insertField.field?id=25&amp;nmode=0&amp;name=Constituent+lookup+ID&amp;type=199621441"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""25"" attribid=""0"" searchable=""0"" fieldname=""Constituent lookup ID"" fieldtype=""199621441"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
      html += "<tr><td>Submitted on:</td>" + vbCrLf
      html += "<td><img src=""insertField.field?id=24&amp;nmode=0&amp;name=Date+submitted&amp;type=199621441"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""24"" attribid=""0"" searchable=""0"" fieldname=""Date submitted"" fieldtype=""199621441"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
      html += "<tr><td>Originating form:</td>" + vbCrLf
      html += "<td><img src=""insertField.field?id=26&amp;nmode=0&amp;name=Form+name&amp;type=199621441"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""26"" attribid=""0"" searchable=""0"" fieldname=""Form name"" fieldtype=""199621441"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
      html += "<tr><td>Originating page:</td>" + vbCrLf
      html += "<td><img src=""insertField.field?id=22&amp;nmode=0&amp;name=Page+name&amp;type=199621441"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""22"" attribid=""0"" searchable=""0"" fieldname=""Page name"" fieldtype=""199621441"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
      html += "<tr><td>Originating website:</td>" + vbCrLf
      html += "<td><img src=""insertField.field?id=23&amp;nmode=0&amp;name=Site+name&amp;type=199621441"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""23"" attribid=""0"" searchable=""0"" fieldname=""Site name"" fieldtype=""199621441"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
      html += "</tbody></table>" + vbCrLf
      html += "<p><strong>Submitted fields</strong></p>" + vbCrLf
      html += "<img src=""insertField.field?id=1&amp;nmode=0&amp;name=Submitted+fields&amp;type=199621441"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""1"" attribid=""0"" searchable=""0"" fieldname=""Submitted fields"" fieldtype=""199621441"" htmlencode=""True"" isloop=""False"">"
      Return html
    End Get
  End Property

  Private Sub LoadEmailTemplate()
    Dim eT As EmailTemplate
    If hfEmailTemplateId.Value = "-1" Then
      eT = Me.CreateNewEmailTemplateObject()
      SaveEmailTemplateId(eT.ID)
      hfEmailTemplateId.Value = eT.ID
    End If
  End Sub

  Private Sub SaveEmailTemplateId(emailTemplateId As Integer)
    Dim addForm = New AddForms.FormBuilderEmailProperties.BBISFormBuilderEmailPropertiesAddDataFormData
    Try
      addForm.EMAILTEMPLATEID = emailTemplateId
      addForm.ContextRecordID = Me.Content.ContentID
      addForm.Save(Me.API.AppFxWebServiceProvider)
    Catch ex As Exception
      Throw ex
    End Try
  End Sub

  Private Function CreateNewEmailTemplateObject() As Blackbaud.Web.Content.Core.EmailTemplate

    Try
      Dim emailTemplate As Blackbaud.Web.Content.Core.EmailTemplate = New Blackbaud.Web.Content.Core.EmailTemplate(Blackbaud.Web.Content.Common.Enumerations.EmailType.Acknowledgement)

      Dim name As String = "Form Builder Notification (" + hfPartTitle.Value + ")"
      emailTemplate.ClientSitesID = hfSiteId.Value
      emailTemplate.Name = name
      emailTemplate.Subject = "Form submission received"
      emailTemplate.Save()
      Return emailTemplate

    Catch ex As Exception
      Throw ex
    End Try

    Return Nothing
  End Function

  Private Sub SetContentPartDetails()
    Dim view = New ViewForms.FormBuilderEmailProperties.BBISFormBuilderContentPartDetailsViewDataFormData
    Try
      view = ViewForms.FormBuilderEmailProperties.BBISFormBuilderContentPartDetailsViewDataForm.LoadData(Me.API.AppFxWebServiceProvider, Me.Content.ContentID)
      hfSiteId.Value = view.SITEID
      hfSiteName.Value = view.SITENAME
      hfPartTitle.Value = view.TITLE
      hfEmailTemplateId.Value = view.EMAILTEMPLATEID
    Catch ex As Exception
      Throw ex
    End Try
  End Sub

  Protected Sub EmailField_CheckRequired_CheckedChanged(sender As Object, e As EventArgs)
    If EmailField_CheckRequired.Checked Then
      EmailField_CheckTypeRequired.Enabled = True
    Else
      EmailField_CheckTypeRequired.Enabled = False
      EmailField_CheckTypeRequired.Checked = False
    End If
  End Sub

  Protected Sub AddressField_CheckAddressRequired_CheckedChanged(sender As Object, e As EventArgs)
    If AddressField_CheckAddressRequired.Checked Then
      AddressField_CheckTypeRequired.Enabled = True
    Else
      AddressField_CheckTypeRequired.Enabled = False
      AddressField_CheckTypeRequired.Checked = False
    End If
  End Sub

  Protected Sub PhoneField_CheckRequired_CheckedChanged(sender As Object, e As EventArgs)
    If PhoneField_CheckRequired.Checked Then
      PhoneField_CheckTypeRequired.Enabled = True
    Else
      PhoneField_CheckTypeRequired.Enabled = False
      PhoneField_CheckTypeRequired.Checked = False
    End If
  End Sub

  Protected Sub NumberField_RadioNumberType_SelectedIndexChanged(sender As Object, e As EventArgs)
    If NumberField_RadioNumberType.SelectedValue = 0 Then 'Number
      NumberField_TextAvailableCharacters.Text = "-."
      NumberField_TextBlankValue.Text = "0"
    ElseIf NumberField_RadioNumberType.SelectedValue = 1 Then 'Currency
      NumberField_TextAvailableCharacters.Text = "$-."
      NumberField_TextBlankValue.Text = "$0.00"
    ElseIf NumberField_RadioNumberType.SelectedValue = 2 Then 'Percentage
      NumberField_TextAvailableCharacters.Text = "%."
      NumberField_TextBlankValue.Text = "0%"
    End If
  End Sub
  Private Sub BindDropDownFolderList()
    Me.FileUpload_DropDownFolderList.FolderType = SiteFolder.EFolderType.Files

    Me.FileUpload_DropDownFolderList.IncludeAllFoldersOption = False
    Me.FileUpload_DropDownFolderList.DataBind()
  End Sub

  Protected Sub FileUpload_ButtonFolderDisplayChange_Click(sender As Object, e As EventArgs)
    FileUpload_FolderDisplayPanel.Visible = False
    FileUpload_FolderSelectorPanel.Attributes.Remove("class")
    BindDropDownFolderList()
  End Sub

  Protected Sub FileUpload_ButtonFolderDisplaySave_Click(sender As Object, e As EventArgs)
    Dim folderSelector As Folders.FolderTree = FileUpload_DropDownFolderList.FindControl("treeFolders")
    Dim webTree As UltraWebTree = folderSelector.FindControl("treeFolders")

    Dim folderId = webTree.SelectedNode.Tag
    Dim folderPath = webTree.SelectedNode.DataPath

    FileUpload_FolderDisplayIdHidden.Value = folderId
    FileUpload_TextFolderDisplay.Text = folderPath

    FileUpload_FolderSelectorPanel.Attributes.Add("class", "HiddenRow")
    FileUpload_FolderDisplayPanel.Visible = True
  End Sub

  Protected Sub FileUpload_FolderDisplayCustomValidator_ServerValidate(source As Object, args As ServerValidateEventArgs)
    If FileUpload_FolderDisplayPanel.Visible Then
      args.IsValid = True
    Else
      args.IsValid = False
    End If
  End Sub

  Private Sub PopulateCodeTableField(ByRef list As DropDownList)
    list.Items.Clear()
    For Each Item In CodeTableValues()
      list.Items.Add(Item)
    Next
  End Sub
  Private Function CodeTableValues() As List(Of ListItem)
    Dim list = New List(Of ListItem)

    list.Add(New ListItem("", ""))

    Dim CodeTables() As Blackbaud.AppFx.WebAPI.SimpleDataListsData
    CodeTables = Blackbaud.AppFx.WebAPI.SimpleDataLists.LoadSimpleDataList(Me.API.AppFxWebServiceProvider, Guid.Parse("fb1a8321-2272-444b-b4f5-584181fd1e72"))

    For Each item In CodeTables
      list.Add(New ListItem(item.Label, item.Value))
    Next

    Return list
  End Function

  Protected Sub MultipleChoice_ListCodeTables_SelectedIndexChanged(sender As Object, e As EventArgs)
    Dim list As DropDownList = sender
    Dim enabled As Boolean = (list.SelectedValue = "")

    MultipleChoice_TextValues.Enabled = enabled
    MultipleChoice_TextValues_Required.Enabled = enabled
  End Sub

  Protected Sub DropDownMenu_ListCodeTables_SelectedIndexChanged(sender As Object, e As EventArgs)
    Dim list As DropDownList = sender
    Dim enabled As Boolean = (list.SelectedValue = "")

    DropDownMenu_TextValues.Enabled = enabled
    DropDownMenu_TextValues_Required.Enabled = enabled
  End Sub
End Class