Imports BBNCExtensions
Imports Blackbaud.AppFx.WebAPI.ServiceProxy
Imports Blackbaud.AppFx.WebAPI


Partial Public Class PageIntroDisplay
    Inherits BBNCExtensions.Parts.CustomPartDisplayBase

    Private mContent As PageIntroProperties
    Private appFx As AppFxWebService
    Private _wsp As AppFxWebServiceProvider

    Public ReadOnly Property Wsp As AppFxWebServiceProvider
        Get
            If _wsp Is Nothing Then
                _wsp = BBNCExtensions.API.NetCommunity.Current.AppFxWebServiceProvider
                _wsp.Credentials = BBNCExtensions.API.NetCommunity.Current.AppFxWebServiceProvider.Credentials
            End If
            Return _wsp
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Wsp.CreateClientAppInfoRequestHeader()
        appFx = Wsp.CreateAppFxWebService()

        SetContentPartDetails()

    End Sub

    Private ReadOnly Property MyContent() As PageIntroProperties
        Get
            If mContent Is Nothing Then
                mContent = Me.Content.GetContent(GetType(PageIntroProperties))
                If mContent Is Nothing Then
                    mContent = New PageIntroProperties
                End If
            End If
            Return mContent
        End Get
    End Property

    Private Sub SetContentPartDetails()
        Try
            If Not Me.IsPostBack Then
                Dim dfReq As DataFormLoadRequest = New DataFormLoadRequest
                Dim dfReply As DataFormLoadReply
                dfReq.ClientAppInfo = Wsp.CreateClientAppInfoRequestHeader()
                dfReq.FormID = New Guid("A859D6DD-3415-4795-9FE0-0453F187E57A")
                dfReq.RecordID = Me.Context.Request.Url.AbsoluteUri
                dfReply = appFx.DataFormLoad(dfReq)

                If dfReply.DataFormItem.Values("HTML").Value IsNot Nothing Then
                    txthtml.InnerHtml = dfReply.DataFormItem.Values("HTML").Value.ToString()
                End If


            End If
        Catch ex As Exception
            lblError.Text = ex.StackTrace
            Throw ex
        End Try
    End Sub

End Class