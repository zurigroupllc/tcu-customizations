Imports BBNCExtensions


Partial Public Class OnlineGivingDisplayDisplay
    Inherits BBNCExtensions.Parts.CustomPartDisplayBase

    Private mContent As OnlineGivingDisplayProperties

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetContentPartDetails()
    End Sub

    Private ReadOnly Property MyContent() As OnlineGivingDisplayProperties
        Get
            If mContent Is Nothing Then
                mContent = Me.Content.GetContent(GetType(OnlineGivingDisplayProperties))
                If mContent Is Nothing Then
                    mContent = New OnlineGivingDisplayProperties
                End If
            End If
            Return mContent
        End Get
    End Property

    Private Sub SetContentPartDetails()
        Dim view = New ViewForms.OnlineGivingConfiguration.OnlineGivingConfigurationViewDataFormData
        Try
            Dim loadRequest As Blackbaud.AppFx.WebAPI.ServiceProxy.DataFormLoadRequest

            loadRequest = Blackbaud.AppFx.WebAPI.DataFormServices.CreateDataFormLoadRequest(Me.API.AppFxWebServiceProvider, New Guid("fa9c582a-41be-4688-ab19-bed5725f4ae5"))
            loadRequest.RecordID = ""
            'view = ViewForms.OnlineGivingConfiguration.OnlineGivingConfigurationViewDataForm.LoadData(Me.API.AppFxWebServiceProvider, "6F6B8ED1-0B85-4E60-B879-566DA241E409")

            'lblTitle.Text = view..ToString
            'lblTitleCaption.Text = "Title"

            'hfSiteId.Value = view.SITEID
            'hfSiteName.Value = view.SITENAME
            'hfPartTitle.Value = view.TITLE
            'hfEmailTemplateId.Value = view.EMAILTEMPLATEID
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class