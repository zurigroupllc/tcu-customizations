<%@ Assembly Name="ZuriGroup.TCU.OnlineGiving.BBIS.Web"%>
<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="OnlineGivingDisplayDisplay.ascx.vb" Inherits="ZuriGroup.TCU.OnlineGiving.BBIS.Web.OnlineGivingDisplayDisplay" %>

<%@ import Namespace="ZuriGroup.TCU.OnlineGiving.BBIS.Web"%>
<%@ Register TagPrefix="bc" Namespace="Blackbaud.Web.Content.Core.Controls" Assembly="Blackbaud.Web.Content.Core" %>

<asp:Label ID="lblError" runat="server" Font-Bold="true" ForeColor="red" />

<asp:Panel ID="ContactUsWrapper" runat="server">
    <table id="tableContactUsForm" class="BBFormTable ContactUsFormTable">
        <tbody>
            <tr>
                <td>
                    <asp:Label ID="lblTitleCaption" runat="server" />
                </td>
                <td>
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="true"  />
                </td>
            </tr>
        </tbody>
    </table>
    
</asp:Panel>