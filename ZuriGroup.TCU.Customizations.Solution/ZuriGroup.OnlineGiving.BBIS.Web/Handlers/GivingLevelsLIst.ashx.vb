﻿Imports System.Web
Imports Blackbaud.AppFx.XmlTypes.DataForms

Public Class GivingLevelsList
    Inherits ASHXHandlerBase
    Implements IHttpHandler

    ''' <summary>
    '''  You will need to configure this handler in the Web.config file of your 
    '''  web and register it with IIS before being able to use it. For more information
    '''  see the following link: http://go.microsoft.com/?linkid=8101007
    ''' </summary>
    Overrides Sub ProcessRequest(context As HttpContext) Implements IHttpHandler.ProcessRequest
        Try
            If context.Request.Params.AllKeys(0) = "getamounts" Then
                Dim url As String = context.Request.Params(0)
                Dim givinglevelslist = New CRMConnectedClass(Wsp).GetDatalistReply(New Guid("b23b139e-36a7-4880-ade9-97e96b5732ba"), url.ToString())
                context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(givinglevelslist.Rows, Newtonsoft.Json.Formatting.None))
            End If
        Catch ex As Exception
            context.Response.Write("Handler error: " + ex.Message)
        End Try
    End Sub

End Class
