<%@ assembly Name="ZuriGroup.TCU.OnlineGiving.BBIS.Web"%>
<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SidebarContentEditor.ascx.vb" Inherits="ZuriGroup.TCU.OnlineGiving.BBIS.Web.SidebarContentEditor" %>
<%@ import Namespace="ZuriGroup.TCU.OnlineGiving.BBIS.Web"%>
<asp:Label ID="lblError" runat="server" Font-Bold="true" ForeColor="red" />
<asp:Label ID="lblMsg" runat="server" Text="This part does not have any design time options." />