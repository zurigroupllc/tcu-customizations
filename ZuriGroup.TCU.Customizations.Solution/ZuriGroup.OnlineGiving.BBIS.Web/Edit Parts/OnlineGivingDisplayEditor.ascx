<%@ Assembly Name="ZuriGroup.TCU.OnlineGiving.BBIS.Web"%>
<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="OnlineGivingDisplayEditor.ascx.vb" Inherits="ZuriGroup.TCU.OnlineGiving.BBIS.Web.OnlineGivingDisplayEditor" %>
<%@ import Namespace="ZuriGroup.OnlineGiving.BBIS.Web"%>
<asp:Label ID="lblError" runat="server" Font-Bold="true" ForeColor="red" />
<asp:Label ID="lblMsg" runat="server" Text="This part does not have any design time options." />