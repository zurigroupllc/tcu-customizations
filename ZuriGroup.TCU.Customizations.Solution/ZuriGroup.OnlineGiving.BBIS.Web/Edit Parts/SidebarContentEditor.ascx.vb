Imports BBNCExtensions

Partial Public Class SidebarContentEditor
    Inherits BBNCExtensions.Parts.CustomPartEditorBase

    Private mContent As SidebarContentProperties

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub

    Public Overrides Function OnSaveContent(Optional ByVal bDialogIsClosing As Boolean = True) As Boolean
        With MyContent
            '.Title = txtTitle.Text
        End With
        Me.Content.SaveContent(MyContent)
        Return True
    End Function

    Private ReadOnly Property MyContent() As SidebarContentProperties
        Get
            If mContent Is Nothing Then
                mContent = Me.Content.GetContent(GetType(SidebarContentProperties))
                If mContent Is Nothing Then
                    mContent = New SidebarContentProperties
                End If
            End If
            Return mContent
        End Get
    End Property


End Class