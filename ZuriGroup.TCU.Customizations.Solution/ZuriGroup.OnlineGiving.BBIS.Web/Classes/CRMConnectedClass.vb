﻿Imports Blackbaud.AppFx.WebAPI
Imports Blackbaud.AppFx.WebAPI.ServiceProxy
Imports Blackbaud.AppFx.XmlTypes.DataForms

Public Class CRMConnectedClass
    Protected _wsp As AppFxWebServiceProvider
    Private _appFx As AppFxWebService

    Public Sub New(ForceWsp As AppFxWebServiceProvider)
        _wsp = ForceWsp
    End Sub

    Public Sub New()

    End Sub

    Public ReadOnly Property Wsp As AppFxWebServiceProvider
        Get
            If _wsp Is Nothing Then
                _wsp = BBNCExtensions.API.NetCommunity.Current.AppFxWebServiceProvider
                _wsp.Credentials = BBNCExtensions.API.NetCommunity.Current.AppFxWebServiceProvider.Credentials
            End If
            Return _wsp
        End Get
    End Property

    Public ReadOnly Property AppFx As AppFxWebService
        Get
            If _appFx Is Nothing Then
                _appFx = Wsp.CreateAppFxWebService()
            End If
            Return _appFx
        End Get
    End Property

    Public Function GetDatalistReply(datalistID As Guid, contextRecordID As String, Optional filterParameters As DataFormFieldValueSet = Nothing, Optional MaxRows As Integer = 1000) As DataListLoadReply
        Dim dlReq = New DataListLoadRequest()
        dlReq.ClientAppInfo = Wsp.CreateClientAppInfoRequestHeader()
        dlReq.DataListID = datalistID
        dlReq.MaxRows = MaxRows
        If contextRecordID <> String.Empty Then
            dlReq.ContextRecordID = contextRecordID
        End If
        If filterParameters IsNot Nothing Then
            dlReq.Parameters = New DataFormItem() With {.Values = filterParameters}
        End If
        Return AppFx.DataListLoad(dlReq)
    End Function

    Public Function SaveAddForm(formID As Guid, formFieldValues As DataFormItem, contextRecordID As String) As DataFormSaveReply
        Dim dfSave = New DataFormSaveRequest()
        dfSave.ClientAppInfo = Wsp.CreateClientAppInfoRequestHeader()
        dfSave.FormID = formID
        If contextRecordID <> Guid.Empty.ToString() Then
            dfSave.ContextRecordID = contextRecordID
        End If
        dfSave.DataFormItem = formFieldValues
        Return AppFx.DataFormSave(dfSave)
    End Function

    Public Function LoadForm(formID As Guid, recordID As String, Optional contextRecordID As String = "") As DataFormLoadReply
        Dim dfLoadRequest As New DataFormLoadRequest
        dfLoadRequest.ClientAppInfo = Wsp.CreateClientAppInfoRequestHeader()
        dfLoadRequest.FormID = formID
        dfLoadRequest.RecordID = recordID
        If contextRecordID <> String.Empty Then
            dfLoadRequest.ContextRecordID = contextRecordID
        End If
        Return AppFx.DataFormLoad(dfLoadRequest)
    End Function

    Public Function SaveEditForm(formID As Guid, formFieldValues As DataFormItem, recordID As String, contextRecordID As String) As DataFormSaveReply
        Dim dfSave = New DataFormSaveRequest()
        dfSave.DataFormItem = formFieldValues
        dfSave.ClientAppInfo = Wsp.CreateClientAppInfoRequestHeader()
        dfSave.FormID = formID
        dfSave.ID = recordID
        If contextRecordID <> Guid.Empty.ToString() Then
            dfSave.ContextRecordID = contextRecordID
        End If
        Return AppFx.DataFormSave(dfSave)
    End Function

    Public Function ExecuteRecordOperation(recordOperationCatalogID As Guid, recordID As String) As RecordOperationPerformReply
        Dim roRequest = New RecordOperationPerformRequest()
        roRequest.ClientAppInfo = Wsp.CreateClientAppInfoRequestHeader()
        roRequest.RecordOperationID = recordOperationCatalogID
        If Not String.IsNullOrEmpty(recordID) Then
            roRequest.ID = recordID
        End If
        Return AppFx.RecordOperationPerform(roRequest)
    End Function

    'Public Function GetCodeTableEntries(codeTableName As String, Optional IncludeInactive As Boolean = False) As List(Of CodeTableEntry)
    '    Dim result As New List(Of CodeTableEntry)
    '    Dim ctReq = New CodeTableEntryGetListRequest()
    '    ctReq.ClientAppInfo = Wsp.CreateClientAppInfoRequestHeader()
    '    ctReq.CodeTableName = codeTableName
    '    Dim codeTableValuesFromCRM = AppFx.CodeTableEntryGetList(ctReq).Rows.ToList()

    '    For Each ctv In codeTableValuesFromCRM
    '        If IncludeInactive OrElse ctv.Active Then
    '            result.Add(New CodeTableEntry(Wsp, codeTableName, ctv.Code))
    '        End If
    '    Next

    '    Return result
    'End Function

    Public Function SearchListExecute(SearchListID As Guid, filterValues As DataFormFieldValueSet, Optional MaxRows As Integer = 100) As SearchListLoadReply
        Dim searchRequest As New SearchListLoadRequest()
        searchRequest.SearchListID = SearchListID
        searchRequest.ClientAppInfo = Wsp.CreateClientAppInfoRequestHeader()
        If filterValues IsNot Nothing Then
            searchRequest.Filter = New DataFormItem() With {.Values = filterValues}
        End If
        searchRequest.MaxRecords = MaxRows
        Return AppFx.SearchListLoad(searchRequest)
    End Function

    Public Overridable Sub SavetoDatabase()
        '
    End Sub

    Public Function GetSimpleDatalistValues(simpleDatalistCatalogID As Guid, Optional parameters As DataFormItem = Nothing) As SimpleDataListLoadReply
        Dim sdlReq = New SimpleDataListLoadRequest()
        sdlReq.ClientAppInfo = Wsp.CreateClientAppInfoRequestHeader()
        sdlReq.DataListID = simpleDatalistCatalogID
        sdlReq.Parameters = parameters
        Return AppFx.SimpleDataListLoad(sdlReq)
    End Function
End Class

