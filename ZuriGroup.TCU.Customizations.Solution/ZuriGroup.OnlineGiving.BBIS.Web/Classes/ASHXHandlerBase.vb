﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports Blackbaud.AppFx.WebAPI
Imports Blackbaud.AppFx.WebAPI.ServiceProxy

Public Class ASHXHandlerBase
    Implements IHttpHandler, IRequiresSessionState

    Public Overridable ReadOnly Property IsReusable As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Overridable Sub ProcessRequest(context As HttpContext) Implements IHttpHandler.ProcessRequest
        'left for descendants to override
    End Sub

    Private Function GetCurrentProvider() As AppFxWebServiceProvider
        Dim result As New AppFxWebServiceProvider()
        Try
            Dim connectionString As String = ConfigurationManager.AppSettings("ConnectionString")
            Dim conn As New SqlConnection(connectionString)
            Try
                Dim cmd As New SqlCommand("USR_USP_DATALIST_GETBBISSETTINGS", conn)
                cmd.CommandType = CommandType.StoredProcedure
                conn.Open()
                Dim rdr As SqlDataReader = cmd.ExecuteReader()

                Dim databaseName As String = String.Empty
                Dim url As String = String.Empty
                While rdr.Read()
                    If rdr("NAME") = "BBAppFXURL" Then
                        url = rdr("VALUE")
                    End If
                    If rdr("NAME") = "BBAppFXDatabaseToUse" Then
                        databaseName = rdr("VALUE")
                    End If
                    If Not String.IsNullOrEmpty(url) AndAlso Not String.IsNullOrEmpty(databaseName) Then
                        Exit While
                    End If
                End While

                result.ApplicationName = "BBIS"
                result.Database = databaseName
                'result.Credentials = CredentialCache.DefaultNetworkCredentials
                'If Environment.MachineName.Contains("CHET") Then
                '    result.Credentials = CredentialCache.DefaultNetworkCredentials
                'Else
                result.Credentials = New NetworkCredential("chetrosson", "cwr0X#1112", "tcu")
                'End If
                result.Url = url
            Finally
                conn.Close()
            End Try
        Catch ex As Exception
            Throw
        End Try

        Return result
    End Function

    Public ReadOnly Property Wsp As AppFxWebServiceProvider
        Get
            If _wsp Is Nothing Then
                _wsp = GetCurrentProvider()
            End If
            If _appFx Is Nothing Then
                _appFx = _wsp.CreateAppFxWebService()
            End If
            Return _wsp
        End Get
    End Property

    Private _wsp As AppFxWebServiceProvider
    Protected _appFx As AppFxWebService
    Public Function GetJSONValueForKey(jsonString, keyname) As String
        Dim jsonReader As New Newtonsoft.Json.JsonTextReader(New StringReader(jsonString))
        Dim comingUp = False
        While jsonReader.Read()
            If jsonReader.Value IsNot Nothing Then
                If comingUp Then
                    Return jsonReader.Value
                End If
                If jsonReader.Value.ToString() = keyname Then
                    comingUp = True
                End If
            End If
        End While
        Return String.Empty
    End Function
End Class
