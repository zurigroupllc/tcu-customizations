﻿Option Infer On
Option Strict Off

Imports bbAppFxWebAPI = Blackbaud.AppFx.WebAPI



Namespace ViewForms

    Namespace [OnlineGivingConfiguration]
    
		#Region "Enums"
Namespace [OnlineGivingConfigurationViewDataFormEnums]




End Namespace
#End Region

#Region "Enum Helper Props"
Partial Class [OnlineGivingConfigurationViewDataFormData]

Partial Class [GIVINGLEVELS_DATAITEM]
End Class



Partial Class [DESIGNATIONS_DATAITEM]
End Class



Partial Class [PRECONFIGURATIONS_DATAITEM]
End Class


End Class

#End Region


		    ''' <summary>
    ''' Provides WebApi access to the "Online Giving Configuration View Data Form" catalog feature.  A data form for viewing online giving configuration records
    ''' </summary>
<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
        Public NotInheritable Class [OnlineGivingConfigurationViewDataForm]

            Private Sub New()
            End Sub

            Private Shared ReadOnly _specId As Guid = New Guid("e648c2ed-1924-4a62-a826-da261c42b8a7")

			''' <summary>
			''' The Spec ID value for the "Online Giving Configuration View Data Form" ViewForm
			''' </summary>
            Public Shared ReadOnly Property SpecId() As Guid
                Get
                    Return _specId
                End Get
            End Property
 
            Public Shared Function CreateRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider) As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest
                Return CreateRequest(provider, Nothing)
            End Function
            
            Public Shared Function CreateRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal options As bbAppFxWebAPI.LoadRequestOptions) As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest
                Return Blackbaud.AppFx.WebAPI.DataFormServices.CreateDataFormLoadRequest(provider, _specId, options)
            End Function

            Public Shared Function LoadData(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal recordId As String) As OnlineGivingConfigurationViewDataFormData
				Return LoadDataWithOptions(provider, recordId, Nothing)
            End Function

            Public Shared Function LoadDataWithOptions(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal recordId As String, ByVal options As bbAppFxWebAPI.LoadRequestOptions) As OnlineGivingConfigurationViewDataFormData

				bbAppFxWebAPI.DataFormServices.ValidateRecordId(recordId)

                Dim request = CreateRequest(provider, options)

				
				
				request.RecordID = recordId

                Return LoadData(provider, request)

            End Function

            Public Shared Function LoadData(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal request As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest) As OnlineGivingConfigurationViewDataFormData
                Return bbAppFxWebAPI.DataFormServices.GetFormData(Of OnlineGivingConfigurationViewDataFormData)(provider, request)
            End Function

        End Class

#Region "Data Class"
	
	    ''' <summary>
        ''' Represents the data form field values in the "Online Giving Configuration View Data Form" data form.
        ''' </summary>
		<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
	    Public NotInheritable Class OnlineGivingConfigurationViewDataFormData
			Inherits bbAppFxWebAPI.ViewFormData
		
			Private [_FRIENDLYURL] As String
''' <summary>
''' Friendly URL
''' </summary>
Public Property [FRIENDLYURL] As String
    Get
        Return Me.[_FRIENDLYURL]
    End Get
    Set
        Me.[_FRIENDLYURL] = value 
    End Set
End Property

Public GIVINGLEVELS As GIVINGLEVELS_DATAITEM()
Public DESIGNATIONS As DESIGNATIONS_DATAITEM()
Public PRECONFIGURATIONS As PRECONFIGURATIONS_DATAITEM()

	        
			Public Sub New()
				MyBase.New()
			End Sub

			Public Sub New(ByVal reply as bbAppFxWebAPI.ServiceProxy.DataFormLoadReply)
				If (reply IsNot Nothing) AndAlso (reply.DataFormItem IsNot Nothing) Then
					Me.SetValues(reply.DataFormItem)
				End If
			End Sub
        
			Public Sub New(ByVal dataFormItemXml As String)
				MyBase.New()
				Me.SetValuesFromDataFormItem(dataFormItemXml)
			End Sub
        
			Public Overrides ReadOnly Property DataFormInstanceID() As System.Guid
				Get
					Return ViewForms.[OnlineGivingConfiguration].OnlineGivingConfigurationViewDataForm.SpecId
				End Get
			End Property
	    
			Friend Sub SetValues(ByVal dfi As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
	            
				
Dim value As Object = Nothing
Dim dfiFieldValue As Blackbaud.AppFx.XmlTypes.DataForms.DataFormFieldValue = Nothing
Dim stringFieldValue As String = Nothing

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("FRIENDLYURL", stringFieldValue) Then
Me.[FRIENDLYURL] = stringFieldValue
End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("GIVINGLEVELS", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
value=value.Items
If value IsNot Nothing Then
dim GIVINGLEVELS_list as New System.Collections.Generic.List(of GIVINGLEVELS_DATAITEM)
For each cf_dfi as Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem in value
dim GIVINGLEVELS_item as New GIVINGLEVELS_DATAITEM
GIVINGLEVELS_item.SetValues(cf_dfi)
GIVINGLEVELS_list.Add(GIVINGLEVELS_item)
next cf_dfi
value = GIVINGLEVELS_list.ToArray()
Me.[GIVINGLEVELS] = value
End If
End If

	End If

End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("DESIGNATIONS", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
value=value.Items
If value IsNot Nothing Then
dim DESIGNATIONS_list as New System.Collections.Generic.List(of DESIGNATIONS_DATAITEM)
For each cf_dfi as Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem in value
dim DESIGNATIONS_item as New DESIGNATIONS_DATAITEM
DESIGNATIONS_item.SetValues(cf_dfi)
DESIGNATIONS_list.Add(DESIGNATIONS_item)
next cf_dfi
value = DESIGNATIONS_list.ToArray()
Me.[DESIGNATIONS] = value
End If
End If

	End If

End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("PRECONFIGURATIONS", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
value=value.Items
If value IsNot Nothing Then
dim PRECONFIGURATIONS_list as New System.Collections.Generic.List(of PRECONFIGURATIONS_DATAITEM)
For each cf_dfi as Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem in value
dim PRECONFIGURATIONS_item as New PRECONFIGURATIONS_DATAITEM
PRECONFIGURATIONS_item.SetValues(cf_dfi)
PRECONFIGURATIONS_list.Add(PRECONFIGURATIONS_item)
next cf_dfi
value = PRECONFIGURATIONS_list.ToArray()
Me.[PRECONFIGURATIONS] = value
End If
End If

	End If

End If


	            
			End Sub
				
			Public Overrides Sub SetValuesFromDataFormItem(ByVal dataFormItem As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
				Me.SetValues(dataFormItem)
			End Sub

			Public NotInheritable Class GIVINGLEVELS_DATAITEM
Private [_AMOUNT] As Nullable(of Decimal)
''' <summary>
''' Amount
''' </summary>
Public Property [AMOUNT] As Nullable(of Decimal)
    Get
        Return Me.[_AMOUNT]
    End Get
    Set
        Me.[_AMOUNT] = value 
    End Set
End Property

Private [_LABEL] As String
''' <summary>
''' Label
''' </summary>
Public Property [LABEL] As String
    Get
        Return Me.[_LABEL]
    End Get
    Set
        Me.[_LABEL] = value 
    End Set
End Property

Private [_SEQUENCE] As Nullable(of Integer)
''' <summary>
''' Sequence
''' </summary>
Public Property [SEQUENCE] As Nullable(of Integer)
    Get
        Return Me.[_SEQUENCE]
    End Get
    Set
        Me.[_SEQUENCE] = value 
    End Set
End Property



Friend Sub SetValues(ByVal dfi As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)


Dim value As Object = Nothing
Dim dfiFieldValue As Blackbaud.AppFx.XmlTypes.DataForms.DataFormFieldValue = Nothing
Dim stringFieldValue As String = Nothing

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("AMOUNT", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
If TypeOf value Is String Then 
Me.[AMOUNT] = Blackbaud.AppFx.DataListUtility.DataListStringValueToDec(value)

Else
Me.[AMOUNT] = value
End If
End If

	End If

End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("LABEL", stringFieldValue) Then
Me.[LABEL] = stringFieldValue
End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("SEQUENCE", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
If TypeOf value Is String Then 
Me.[SEQUENCE] = Blackbaud.AppFx.DataListUtility.DataListStringValueToInt(value)

Else
Me.[SEQUENCE] = value
End If
End If

	End If

End If



End Sub

Friend Function BuildDataFormItemForSave() As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem

Dim dfi = New Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem



Dim value As Object = Nothing
value = Me.[AMOUNT]
	dfi.SetValueIfNotNull("AMOUNT",value)
value = Me.[LABEL]
	dfi.SetValueIfNotNull("LABEL",value)
value = Me.[SEQUENCE]
	dfi.SetValueIfNotNull("SEQUENCE",value)


Return dfi

End Function
End Class

Public NotInheritable Class DESIGNATIONS_DATAITEM
Private [_DESIGNATIONID] As Nullable(of System.Guid)
''' <summary>
''' Required field. Designation
''' </summary>
Public Property [DESIGNATIONID] As Nullable(of System.Guid)
    Get
        Return Me.[_DESIGNATIONID]
    End Get
    Set
        Me.[_DESIGNATIONID] = value 
    End Set
End Property

Private [_LABEL] As String
''' <summary>
''' Required field. Label
''' </summary>
Public Property [LABEL] As String
    Get
        Return Me.[_LABEL]
    End Get
    Set
        Me.[_LABEL] = value 
    End Set
End Property

Private [_USEPUBLICNAME] As Nullable(of Boolean)
''' <summary>
''' Use public name
''' </summary>
Public Property [USEPUBLICNAME] As Nullable(of Boolean)
    Get
        Return Me.[_USEPUBLICNAME]
    End Get
    Set
        Me.[_USEPUBLICNAME] = value 
    End Set
End Property

Private [_PUBLICNAME] As String
''' <summary>
''' Public name
''' </summary>
Public Property [PUBLICNAME] As String
    Get
        Return Me.[_PUBLICNAME]
    End Get
    Set
        Me.[_PUBLICNAME] = value 
    End Set
End Property

Private [_SEQUENCE] As Nullable(of Integer)
''' <summary>
''' Sequence
''' </summary>
Public Property [SEQUENCE] As Nullable(of Integer)
    Get
        Return Me.[_SEQUENCE]
    End Get
    Set
        Me.[_SEQUENCE] = value 
    End Set
End Property



Friend Sub SetValues(ByVal dfi As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)


Dim value As Object = Nothing
Dim dfiFieldValue As Blackbaud.AppFx.XmlTypes.DataForms.DataFormFieldValue = Nothing
Dim stringFieldValue As String = Nothing

Dim guidFieldValue As System.Guid
Dim boolFieldValue As Boolean
guidFieldValue = System.Guid.Empty
If dfi.TryGetValueForPropertyAssignment("DESIGNATIONID", guidFieldValue) Then
Me.[DESIGNATIONID] = guidFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("LABEL", stringFieldValue) Then
Me.[LABEL] = stringFieldValue
End If

boolFieldValue = False
If dfi.TryGetValueForPropertyAssignment("USEPUBLICNAME", boolFieldValue) Then
Me.[USEPUBLICNAME] = boolFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("PUBLICNAME", stringFieldValue) Then
Me.[PUBLICNAME] = stringFieldValue
End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("SEQUENCE", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
If TypeOf value Is String Then 
Me.[SEQUENCE] = Blackbaud.AppFx.DataListUtility.DataListStringValueToInt(value)

Else
Me.[SEQUENCE] = value
End If
End If

	End If

End If



End Sub

Friend Function BuildDataFormItemForSave() As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem

Dim dfi = New Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem



Dim value As Object = Nothing
value = Me.[DESIGNATIONID]
	dfi.SetValueIfNotNull("DESIGNATIONID",value)
value = Me.[LABEL]
	dfi.SetValueIfNotNull("LABEL",value)
value = Me.[USEPUBLICNAME]
	dfi.SetValueIfNotNull("USEPUBLICNAME",value)
value = Me.[PUBLICNAME]
	dfi.SetValueIfNotNull("PUBLICNAME",value)
value = Me.[SEQUENCE]
	dfi.SetValueIfNotNull("SEQUENCE",value)


Return dfi

End Function
End Class

Public NotInheritable Class PRECONFIGURATIONS_DATAITEM
Private [_ID] As Nullable(of System.Guid)
''' <summary>
''' ID
''' </summary>
Public Property [ID] As Nullable(of System.Guid)
    Get
        Return Me.[_ID]
    End Get
    Set
        Me.[_ID] = value 
    End Set
End Property

Private [_SOURCECODEID] As String
''' <summary>
''' Source code
''' </summary>
Public Property [SOURCECODEID] As String
    Get
        Return Me.[_SOURCECODEID]
    End Get
    Set
        Me.[_SOURCECODEID] = value 
    End Set
End Property

Private [_DESIGNATIONID] As Nullable(of System.Guid)
''' <summary>
''' Required field. Designation
''' </summary>
Public Property [DESIGNATIONID] As Nullable(of System.Guid)
    Get
        Return Me.[_DESIGNATIONID]
    End Get
    Set
        Me.[_DESIGNATIONID] = value 
    End Set
End Property

Private [_AMOUNT] As Nullable(of Decimal)
''' <summary>
''' Amount
''' </summary>
Public Property [AMOUNT] As Nullable(of Decimal)
    Get
        Return Me.[_AMOUNT]
    End Get
    Set
        Me.[_AMOUNT] = value 
    End Set
End Property

Private [_URLFORMATID] As String
''' <summary>
''' URL
''' </summary>
Public Property [URLFORMATID] As String
    Get
        Return Me.[_URLFORMATID]
    End Get
    Set
        Me.[_URLFORMATID] = value 
    End Set
End Property

Private [_SEQUENCE] As Nullable(of Integer)
''' <summary>
''' Sequence
''' </summary>
Public Property [SEQUENCE] As Nullable(of Integer)
    Get
        Return Me.[_SEQUENCE]
    End Get
    Set
        Me.[_SEQUENCE] = value 
    End Set
End Property



Friend Sub SetValues(ByVal dfi As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)


Dim value As Object = Nothing
Dim dfiFieldValue As Blackbaud.AppFx.XmlTypes.DataForms.DataFormFieldValue = Nothing
Dim stringFieldValue As String = Nothing

Dim guidFieldValue As System.Guid
guidFieldValue = System.Guid.Empty
If dfi.TryGetValueForPropertyAssignment("ID", guidFieldValue) Then
Me.[ID] = guidFieldValue
End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("SOURCECODEID", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
If TypeOf value Is String Then 
Me.[SOURCECODEID] = value

Else
Me.[SOURCECODEID] = value
End If
End If

	End If

End If

guidFieldValue = System.Guid.Empty
If dfi.TryGetValueForPropertyAssignment("DESIGNATIONID", guidFieldValue) Then
Me.[DESIGNATIONID] = guidFieldValue
End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("AMOUNT", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
If TypeOf value Is String Then 
Me.[AMOUNT] = Blackbaud.AppFx.DataListUtility.DataListStringValueToDec(value)

Else
Me.[AMOUNT] = value
End If
End If

	End If

End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("URLFORMATID", stringFieldValue) Then
Me.[URLFORMATID] = stringFieldValue
End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("SEQUENCE", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
If TypeOf value Is String Then 
Me.[SEQUENCE] = Blackbaud.AppFx.DataListUtility.DataListStringValueToInt(value)

Else
Me.[SEQUENCE] = value
End If
End If

	End If

End If



End Sub

Friend Function BuildDataFormItemForSave() As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem

Dim dfi = New Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem



Dim value As Object = Nothing
value = Me.[ID]
	dfi.SetValueIfNotNull("ID",value)
value = Me.[SOURCECODEID]
	dfi.SetValueIfNotNull("SOURCECODEID",value)
value = Me.[DESIGNATIONID]
	dfi.SetValueIfNotNull("DESIGNATIONID",value)
value = Me.[AMOUNT]
	dfi.SetValueIfNotNull("AMOUNT",value)
value = Me.[URLFORMATID]
	dfi.SetValueIfNotNull("URLFORMATID",value)
value = Me.[SEQUENCE]
	dfi.SetValueIfNotNull("SEQUENCE",value)


Return dfi

End Function
End Class


	 
		End Class

#End Region
    
    End Namespace

End Namespace



