Imports Blackbaud.AppFx.Communications.UIModel
Imports Blackbaud.Web.Content.Core

Public Class OnlineGivingPageContentAddFormUIModel

    Private Const NETCOMMUNITYLINKESTABLISHEDVIEWID As String = "b3ade835-e789-4312-905b-cffe60174ba3"
    Private _bbncURL As String

    Private Sub OnlineGivingPageContentAddFormUIModel_Loaded(ByVal sender As Object, ByVal e As Blackbaud.AppFx.UIModeling.Core.LoadedEventArgs) Handles Me.Loaded
        Dim urlRequest As New Server.DataFormLoadRequest With {.FormID = New Guid(NETCOMMUNITYLINKESTABLISHEDVIEWID)}
        Dim urlReply = Server.DataFormLoad(urlRequest, Me.GetRequestContext)
        If urlReply IsNot Nothing AndAlso urlReply.DataFormItem IsNot Nothing Then
            Dim dffv As New XmlTypes.DataForms.DataFormFieldValue
            If urlReply.DataFormItem.TryGetValue("BBNCURL", dffv) Then _bbncURL = FormatSSLURL(dffv.Value.ToString)
        End If

        If Me.Mode = DataFormMode.Edit Then

        End If
    End Sub

    Private Sub _insertmailimage_CustomFormConfirmed(ByVal sender As Object, ByVal e As UIModeling.Core.CustomFormConfirmedEventArgs) Handles _insertmailimage.CustomFormConfirmed
        Dim model = CType(e.Model, ImageLibraryCustomFormUIModel)
        If model IsNot Nothing Then
            If model.SELECTEDIMAGEID.Value > 0 Then
                Me.INSERTMAILIMAGEHTML.Value = EmailMessageHelper.BuildImageHtml(_bbncURL, model.SELECTEDIMAGEID.Value)
            End If
        End If
    End Sub

    Public Shared Function FormatSSLURL(ByVal appPathURL As String) As String
        Dim destinationURI As Uri = Nothing
        Try
            If Uri.IsWellFormedUriString(appPathURL, UriKind.Absolute) Then
                destinationURI = New Uri(appPathURL)
            End If
        Catch ex As Exception
            ' Uri could not be treated, becuse sURL was invalid
        End Try

        If URLBuilder.IsSSL() AndAlso appPathURL.StartsWith("http://") AndAlso (Not destinationURI Is Nothing) Then
            appPathURL = String.Concat("https://", appPathURL.Substring(7))
        End If
        Return appPathURL
    End Function

End Class