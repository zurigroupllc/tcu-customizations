'
'
' This file is used by Code Analysis to maintain SuppressMessage 
' attributes that are applied to this project.
' Project-level suppressions either have no target or are given 
' a specific target and scoped to a namespace, type, member, etc.
'
' To add a suppression to this file, right-click the message in the 
' Code Analysis results, point to "Suppress Message", and click 
' "In Suppression File".
' You do not need to add suppressions to this file manually.


<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Zuri", Scope:="namespace", Target:="ZuriGroup.TCU.OnlineGiving.UIModel")> 
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Zuri")> 
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope:="member", Target:="ZuriGroup.TCU.OnlineGiving.UIModel.AddDonationFormConfigurationAddDataFormDESIGNATIONSUIModel.#GetDesignationPublicName()")> 
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope:="member", Target:="ZuriGroup.TCU.OnlineGiving.UIModel.AddDonationFormConfigurationAddDataFormUIModel.#GetDesignationSelectedItem()")> 
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope:="member", Target:="ZuriGroup.TCU.OnlineGiving.UIModel.AddDonationFormConfigurationAddDataFormUIModel.#GetGivingLevelSelectedItem()")> 
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope:="member", Target:="ZuriGroup.TCU.OnlineGiving.UIModel.AddDonationFormConfigurationAddDataFormUIModel.#UpdateActions()")> 
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Blackbaud.Security", "BB070:ServiceMethodCheckSecurity")> 
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId:="0#", Scope:="member", Target:="ZuriGroup.TCU.OnlineGiving.UIModel.OnlineGivingPageContentAddFormUIModel.#FormatSSLURL(System.String)")> 
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="ZuriGroup.TCU.OnlineGiving.UIModel.OnlineGivingPageContentAddFormUIModel.#FormatSSLURL(System.String)")> 
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId:="System.String.StartsWith(System.String)", Scope:="member", Target:="ZuriGroup.TCU.OnlineGiving.UIModel.OnlineGivingPageContentAddFormUIModel.#FormatSSLURL(System.String)")> 
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="0", Scope:="member", Target:="ZuriGroup.TCU.OnlineGiving.UIModel.OnlineGivingPageContentAddFormUIModel.#FormatSSLURL(System.String)")> 
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope:="member", Target:="ZuriGroup.TCU.OnlineGiving.UIModel.AddDonationFormConfigurationAddDataFormUIModel.#_bbncURL")> 