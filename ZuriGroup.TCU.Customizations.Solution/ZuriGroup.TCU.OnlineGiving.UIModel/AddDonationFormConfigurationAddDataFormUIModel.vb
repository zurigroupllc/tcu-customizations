Imports Blackbaud.AppFx.XmlTypes.DataForms

Public Class AddDonationFormConfigurationAddDataFormUIModel

    Private Const NETCOMMUNITYLINKESTABLISHEDVIEWID As String = "b3ade835-e789-4312-905b-cffe60174ba3"
    Private Const CMSURLVIEWFORMID As String = "aa1adcb0-7fc2-4ed4-a0d4-f596dc06f0c6"
    'Private _bbncURL As String

    Private Enum SortDirection
        Ascending = 0
        Descending = 1
    End Enum

    Private Enum MoveDirection
        Up = -1
        Down = 1
    End Enum

    Private Enum CollectionType
        GivingLevels = 0
        Designation = 1
    End Enum

    Private Sub AddDonationFormConfigurationAddDataFormUIModel_BeginValidate(sender As Object, e As BeginValidateEventArgs) Handles Me.BeginValidate
        're-sequence items based on current order
        Dim index = 1
        For Each m In _givinglevels.Value
            m.SEQUENCE.Value = index
            index += 1
        Next
    End Sub

    Private Sub AddDonationFormConfigurationAddDataFormUIModel_Loaded(ByVal sender As Object, ByVal e As Blackbaud.AppFx.UIModeling.Core.LoadedEventArgs) Handles Me.Loaded
        If Me.Mode = DataFormMode.Edit Then
            Me.FORMHEADER.Value = Replace(Me.FORMHEADER.Value, "Add", "Edit")
            CheckStatus()
        End If

        SetupEditButtons()

        'Dim urlRequest As New Server.DataFormLoadRequest With {.FormID = New Guid(NETCOMMUNITYLINKESTABLISHEDVIEWID)}
        'Dim urlReply = Server.DataFormLoad(urlRequest, Me.GetRequestContext)
        'If urlReply IsNot Nothing AndAlso urlReply.DataFormItem IsNot Nothing Then
        '    Dim dffv As New XmlTypes.DataForms.DataFormFieldValue
        '    If urlReply.DataFormItem.TryGetValue("BBNCURL", dffv) Then Me.FRIENDLYURLBASE.Value = OnlineGivingPageContentAddFormUIModel.FormatSSLURL(dffv.Value.ToString)
        '    If Me.Mode = DataFormMode.Add Then
        '        Me.FRIENDLYURL.Value = Me.FRIENDLYURLBASE.Value
        '    End If
        'End If

        Me.FRIENDLYURL.Enabled = False
        Me.STATUSCODE.Enabled = False

        UpdatePageContentActions()

        For Each row In Me.PRECONFIGURATIONS.Value
            row.URLFORMATID.DBReadOnly = True
        Next
    End Sub

    Private Sub _moveup_InvokeAction(ByVal sender As Object, ByVal e As UIModeling.Core.InvokeActionEventArgs) Handles _moveup.InvokeAction
        MoveItems(-1, CollectionType.GivingLevels)
    End Sub

    Private Sub _movedown_InvokeAction(ByVal sender As Object, ByVal e As UIModeling.Core.InvokeActionEventArgs) Handles _movedown.InvokeAction
        MoveItems(1, CollectionType.GivingLevels)
    End Sub

    Private Sub _movedesignationup_InvokeAction(sender As Object, e As InvokeActionEventArgs) Handles _movedesignationup.InvokeAction
        MoveItems(-1, CollectionType.Designation)
    End Sub

    Private Sub _movedesignationdown_InvokeAction(sender As Object, e As InvokeActionEventArgs) Handles _movedesignationdown.InvokeAction
        MoveItems(1, CollectionType.Designation)
    End Sub

    Private Sub _ascend_InvokeAction(ByVal sender As Object, ByVal e As UIModeling.Core.InvokeActionEventArgs) Handles _ascend.InvokeAction
        SortGivingLevels(SortDirection.Ascending)
    End Sub

    Private Sub _descend_InvokeAction(ByVal sender As Object, ByVal e As UIModeling.Core.InvokeActionEventArgs) Handles _descend.InvokeAction
        SortGivingLevels(SortDirection.Descending)
    End Sub

    Private Sub _designationascend_InvokeAction(sender As Object, e As InvokeActionEventArgs) Handles _designationascend.InvokeAction
        SortDesignations(SortDirection.Ascending)
    End Sub

    Private Sub _designationdescend_InvokeAction(sender As Object, e As InvokeActionEventArgs) Handles _designationdescend.InvokeAction
        SortDesignations(SortDirection.Descending)
    End Sub

    Private Sub MoveItems(ByVal offset As Integer, collection As CollectionType)
        Dim minIndex = Integer.MaxValue
        If collection = CollectionType.GivingLevels Then

            For Each model In _givinglevels.SelectedItems
                If _givinglevels.Value.IndexOf(model) < minIndex Then minIndex = _givinglevels.Value.IndexOf(model)
            Next

            MoveSelectedItems(minIndex + offset, collection)

        Else
            For Each model In _designations.SelectedItems
                If _designations.Value.IndexOf(model) < minIndex Then minIndex = _designations.Value.IndexOf(model)
            Next

            MoveSelectedItems(minIndex + offset, collection)
        End If

        SetSelectionButtonState(collection)

    End Sub

    Private Sub SortGivingLevels(direction As SortDirection)
        'Dim direction As SortDirection = CType(e.Parameters("SORTDIRECTION"), SortDirection)
        Dim orderedList As List(Of AddDonationFormConfigurationAddDataFormGIVINGLEVELSUIModel) = Nothing
        Select Case direction
            Case SortDirection.Ascending
                orderedList = Me._givinglevels.Value.OrderBy(SortFunction).ToList
            Case SortDirection.Descending
                orderedList = Me._givinglevels.Value.OrderByDescending(SortFunction).ToList
        End Select

        _givinglevels.Value.Clear()

        For Each level In orderedList
            _givinglevels.Value.Add(level)
        Next
    End Sub

    Private Sub SortDesignations(direction As SortDirection)
        Dim orderedList As List(Of AddDonationFormConfigurationAddDataFormDESIGNATIONSUIModel) = Nothing
        Select Case direction
            Case SortDirection.Ascending
                orderedList = Me._designations.Value.OrderBy(SortDesignationFunction).ToList
            Case SortDirection.Descending
                orderedList = Me._designations.Value.OrderByDescending(SortDesignationFunction).ToList
        End Select

        _designations.Value.Clear()

        For Each designation In orderedList
            _designations.Value.Add(designation)
        Next
    End Sub

    Private Shared Function SortFunction() As System.Func(Of AddDonationFormConfigurationAddDataFormGIVINGLEVELSUIModel, Decimal)
        Return Function(level)
                   Return level.AMOUNT.Value
               End Function
    End Function

    Private Shared Function SortDesignationFunction() As System.Func(Of AddDonationFormConfigurationAddDataFormDESIGNATIONSUIModel, String)
        Return Function(designation)
                   Return designation.PUBLICNAME.Value
               End Function
    End Function

    Private Sub MoveSelectedItems(ByVal startIndex As Integer, collection As CollectionType)

        If collection = CollectionType.GivingLevels Then
            Dim items As New Dictionary(Of Integer, AddDonationFormConfigurationAddDataFormGIVINGLEVELSUIModel)

            For Each model In _givinglevels.SelectedItems
                Dim index = _givinglevels.Value.IndexOf(model)
                items.Add(index, model)
            Next

            Dim offset = startIndex
            ' sort the list by index
            Dim itemsToMove = (From i In items
                               Order By i.Key
                               Select i.Value).ToArray

            _givinglevels.Selection.BeginSelectionChange()
            _givinglevels.Selection.Clear()

            'Remove the items
            For Each model In itemsToMove
                _givinglevels.Value.Remove(model)
            Next model

            'Dragging within the grid                                      
            For Each model In itemsToMove
                _givinglevels.Value.Insert(offset, model)
                _givinglevels.Selection.Add(model)
                offset += 1
            Next model

            _givinglevels.Selection.EndSelectionChange()
        Else
            Dim items As New Dictionary(Of Integer, AddDonationFormConfigurationAddDataFormDESIGNATIONSUIModel)

            For Each model In _designations.SelectedItems
                Dim index = _designations.Value.IndexOf(model)
                items.Add(index, model)
            Next

            Dim offset = startIndex
            ' sort the list by index
            Dim itemsToMove = (From i In items
                               Order By i.Key
                               Select i.Value).ToArray

            _designations.Selection.BeginSelectionChange()
            _designations.Selection.Clear()

            'Remove the items
            For Each model In itemsToMove
                _designations.Value.Remove(model)
            Next model

            'Dragging within the grid                                      
            For Each model In itemsToMove
                _designations.Value.Insert(offset, model)
                _designations.Selection.Add(model)
                offset += 1
            Next model

            _designations.Selection.EndSelectionChange()
        End If

    End Sub

    Private Sub SetupEditButtons()
        If Me.PAGEINTROID.Value = Guid.Empty Then
            Me.EDITPAGEINTRO.Enabled = True
        Else
            Me.EDITPAGEINTRO.Enabled = False
        End If

        If Me.SIDEBARID.Value = Guid.Empty Then
            Me.EDITSIDEBAR.Enabled = True
        Else
            Me.EDITSIDEBAR.Enabled = False
        End If

    End Sub

    Private Sub UpdatePageContentActions()
        Dim givingLevelSelectedItem = GetGivingLevelSelectedItem()
        Dim designationSelectedItem = GetDesignationSelectedItem()

        _deletegivinglevel.Enabled = (givingLevelSelectedItem IsNot Nothing)
        _deletedesignation.Enabled = (designationSelectedItem IsNot Nothing)

    End Sub

    Private Sub SetSelectionButtonState(collection As CollectionType)

        If collection = CollectionType.GivingLevels Then
            _moveup.Enabled = True
            _movedown.Enabled = True

            For Each item In _givinglevels.SelectedItems

                If MOVEUP.Enabled AndAlso _givinglevels.Value.IndexOf(item) = 0 Then
                    MOVEUP.Enabled = False
                End If

                If MOVEDOWN.Enabled AndAlso _givinglevels.Value.IndexOf(item) = _givinglevels.Value.Count - 1 Then
                    MOVEDOWN.Enabled = False
                End If

            Next item
        Else
            _movedesignationup.Enabled = True
            _movedesignationdown.Enabled = True

            For Each item In _designations.SelectedItems

                If MOVEDESIGNATIONUP.Enabled AndAlso _designations.Value.IndexOf(item) = 0 Then
                    MOVEDESIGNATIONUP.Enabled = False
                End If

                If MOVEDESIGNATIONDOWN.Enabled AndAlso _designations.Value.IndexOf(item) = _designations.Value.Count - 1 Then
                    MOVEDESIGNATIONDOWN.Enabled = False
                End If

            Next item
        End If
        UpdatePageContentActions()

    End Sub

    Private Function GetGivingLevelSelectedItem() As AddDonationFormConfigurationAddDataFormGIVINGLEVELSUIModel
        If _givinglevels.SelectedItems.Count > 0 Then
            Return _givinglevels.SelectedItems(0)
        Else
            Return Nothing
        End If
    End Function

    Private Function GetDesignationSelectedItem() As AddDonationFormConfigurationAddDataFormDESIGNATIONSUIModel
        If _designations.SelectedItems.Count > 0 Then
            Return _designations.SelectedItems(0)
        Else
            Return Nothing
        End If
    End Function

    Private Function GetPreConfigurationSelectedItem() As AddDonationFormConfigurationAddDataFormPRECONFIGURATIONSUIModel
        If _preconfigurations.SelectedItems.Count > 0 Then
            Return _preconfigurations.SelectedItems(0)
        Else
            Return Nothing
        End If
    End Function

    'Private Sub OnCreated()
    '    Dim cf = Me.DESIGNATIONS
    '    AddHandler cf.Value.ListChanged, AddressOf Me.ListChangedHandler
    'End Sub

    'Private Sub ListChangedHandler(ByVal sender As Object, ByVal e As ListChangedEventArgs)

    '    If e.ListChangedType = ListChangedType.ItemAdded Then

    '    End If

    'End Sub

    Private Sub _givinglevels_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles _givinglevels.SelectionChanged
        SetSelectionButtonState(CollectionType.GivingLevels)
    End Sub

    Private Sub _designation_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles _designations.SelectionChanged
        SetSelectionButtonState(CollectionType.Designation)
    End Sub

    Private Sub _friendlyurlsuffix_ValueChanged(sender As Object, e As ValueChangedEventArgs) Handles _friendlyurlsuffix.ValueChanged
        Me.FRIENDLYURL.Value = Me.FRIENDLYURLBASE.Value + Me.FRIENDLYURLSUFFIX.Value
    End Sub

    Private Sub _deletegivinglevel_InvokeAction(sender As Object, e As InvokeActionEventArgs) Handles _deletegivinglevel.InvokeAction
        Dim selectedItem = GetGivingLevelSelectedItem()

        If selectedItem IsNot Nothing Then
            Prompts.Add(New UIPrompt() With {.ButtonStyle = UIPromptButtonStyle.YesNo, .Text = My.Resources.Content.DeleteGivingLevel, .Callback = AddressOf DeleteSelectedGivingLevel})
        End If
    End Sub

    Private Sub DeleteSelectedGivingLevel(ByVal sender As Object, ByVal e As UIPromptResponseEventArgs)
        If (e IsNot Nothing AndAlso e.Response IsNot Nothing AndAlso TypeOf e.Response Is Boolean AndAlso DirectCast(e.Response, Boolean)) Then
            Dim itemToRemove = GetGivingLevelSelectedItem()
            If itemToRemove IsNot Nothing Then
                'remove the designation to be removed
                GIVINGLEVELS.Value.Remove(itemToRemove)
                UpdatePageContentActions()
            End If
        End If
    End Sub

    Private Sub _deletedesignation_InvokeAction(sender As Object, e As InvokeActionEventArgs) Handles _deletedesignation.InvokeAction
        Dim selectedItem = GetDesignationSelectedItem()

        If selectedItem IsNot Nothing Then
            Prompts.Add(New UIPrompt() With {.ButtonStyle = UIPromptButtonStyle.YesNo, .Text = My.Resources.Content.DeleteDesignation, .Callback = AddressOf DeleteSelectedDesignation})
        End If
    End Sub

    Private Sub DeleteSelectedDesignation(ByVal sender As Object, ByVal e As UIPromptResponseEventArgs)
        If (e IsNot Nothing AndAlso e.Response IsNot Nothing AndAlso TypeOf e.Response Is Boolean AndAlso DirectCast(e.Response, Boolean)) Then
            Dim itemToRemove = GetDesignationSelectedItem()
            If itemToRemove IsNot Nothing Then
                'remove the designation to be removed
                DESIGNATIONS.Value.Remove(itemToRemove)
                UpdatePageContentActions()
            End If
        End If
    End Sub

    Private Sub _deletepreconfigurations_InvokeAction(sender As Object, e As InvokeActionEventArgs) Handles _deletepreconfigurations.InvokeAction
        Dim selectedItem = GetPreConfigurationSelectedItem()

        If selectedItem IsNot Nothing Then
            Prompts.Add(New UIPrompt() With {.ButtonStyle = UIPromptButtonStyle.YesNo, .Text = My.Resources.Content.DeletePreConfiguration, .Callback = AddressOf DeleteSelectedPreConfigurations})
        End If
    End Sub

    Private Sub DeleteSelectedPreConfigurations(ByVal sender As Object, ByVal e As UIPromptResponseEventArgs)
        If (e IsNot Nothing AndAlso e.Response IsNot Nothing AndAlso TypeOf e.Response Is Boolean AndAlso DirectCast(e.Response, Boolean)) Then
            Dim itemToRemove = GetPreConfigurationSelectedItem()
            If itemToRemove IsNot Nothing Then
                'remove the designation to be removed
                PRECONFIGURATIONS.Value.Remove(itemToRemove)
                UpdatePageContentActions()
            End If
        End If
    End Sub

    Private Sub _addpageintro_AddFormConfirmed(sender As Object, e As AddFormConfirmedEventArgs) Handles _addpageintro.AddFormConfirmed
        Me.PAGEINTROID.ResetDataSource()
    End Sub

    Private Sub _addsidebar_AddFormConfirmed(sender As Object, e As AddFormConfirmedEventArgs) Handles _addsidebar.AddFormConfirmed
        Me.SIDEBARID.ResetDataSource()
    End Sub

    Private Sub _startdate_ValueChanged(sender As Object, e As ValueChangedEventArgs) Handles _startdate.ValueChanged
        CheckStatus()
    End Sub

    Private Sub _enddate_ValueChanged(sender As Object, e As ValueChangedEventArgs) Handles _enddate.ValueChanged
        CheckStatus()
    End Sub

    Private Sub CheckStatus()
        Dim cd As DateTime = DateTime.Now()

        If Me.FRIENDLYURLSUFFIX.Value Is Nothing Then
            Me.STATUSCODE.Value = STATUSCODES.Inaccessible
            Exit Sub
        End If

        If cd > Me.STARTDATE.Value And cd < Me.ENDDATE.Value Then
            Me.STATUSCODE.Value = STATUSCODES.Active
        Else
            Me.STATUSCODE.Value = STATUSCODES.Inactive
        End If
    End Sub

    Public Shared Sub BuildAmountsList(rootUIModel As RootUIModel)
        DirectCast(rootUIModel, AddDonationFormConfigurationAddDataFormUIModel).RebuildAmountsList()
    End Sub

    Private Sub RebuildAmountsList()
        Dim lst As New List(Of SimpleDataListItem)
        Dim item As SimpleDataListItem
        For Each row In Me.GIVINGLEVELS.Value
            item = New SimpleDataListItem()
            item.Value = row.AMOUNT.Value
            item.Label = row.LABEL.Value
            lst.Add(item)
        Next

        Me.PRECONFIGURATIONS.DefaultItem.AMOUNT.ReloadDataSource(lst)

        For Each row In Me.PRECONFIGURATIONS.Value
            row.AMOUNT.ReloadDataSource(lst)
        Next

    End Sub

    Public Shared Sub ResetDefaultDesignations(rootUIModel As RootUIModel, ID As Guid)
        DirectCast(rootUIModel, AddDonationFormConfigurationAddDataFormUIModel).ResetDesignationsDefaults(ID)
    End Sub

    Private Sub ResetDesignationsDefaults(ByVal ID As Guid)
        For Each item In Me.DESIGNATIONS.Value
            If Not item.ID.Value = ID Then
                item.ISDEFAULT.Value = False
            End If
        Next
    End Sub

    Public Shared Sub ResetDefaultAmounts(rootUIModel As RootUIModel, ID As Guid)
        DirectCast(rootUIModel, AddDonationFormConfigurationAddDataFormUIModel).ResetAmountDefaults(ID)
    End Sub

    Private Sub ResetAmountDefaults(ByVal ID As Guid)
        For Each item In Me.GIVINGLEVELS.Value
            If Not item.ID.Value = ID Then
                item.ISDEFAULT.Value = False
            End If
        Next
    End Sub

    Private Sub BuildDesignations()
        Dim lst As New List(Of SimpleDataListItem)
        Dim item As SimpleDataListItem
        For Each row In Me.DESIGNATIONS.Value
            item = New SimpleDataListItem()
            item.Value = row.DESIGNATIONID.Value
            item.Label = IIf(row.USEPUBLICNAME.Value, row.PUBLICNAME.Value, row.LABEL.Value)
            item.Description = IIf(row.USEPUBLICNAME.Value, row.PUBLICNAME.Value, row.LABEL.Value)
            lst.Add(item)
        Next

        Me.PRECONFIGURATIONS.DefaultItem.DESIGNATIONID.ReloadDataSource(lst)
        Me.PRECONFIGURATIONS.DefaultItem.DESIGNATIONID.UpdateDisplayText()

        For Each row In Me.PRECONFIGURATIONS.Value
            row.DESIGNATIONID.ReloadDataSource(lst)
        Next

        For Each row In Me.PRECONFIGURATIONS.Value
            row.DESIGNATIONID.UpdateDisplayText()
        Next

    End Sub

    'Private Sub _preconfigurations_ActiveFieldChanged(sender As Object, e As ActiveFieldChangedEventArgs) Handles _preconfigurations.ActiveFieldChanged
    '    RebuildAmountsList()
    'End Sub

    Private Sub _preconfigurations_ActiveRecordChanged(sender As Object, e As ActiveRecordChangedEventArgs) Handles _preconfigurations.ActiveRecordChanged
        RebuildAmountsList()
        BuildDesignations()
    End Sub

    Private Sub _donationformid_ValueChanged(sender As Object, e As ValueChangedEventArgs) Handles _donationformid.ValueChanged
        Dim urlRequest As New Server.DataFormLoadRequest With {.FormID = New Guid(CMSURLVIEWFORMID), .RecordID = _donationformid.Value}
        Dim urlReply = Server.DataFormLoad(urlRequest, Me.GetRequestContext)
        If urlReply IsNot Nothing AndAlso urlReply.DataFormItem IsNot Nothing Then
            Dim dffv As New XmlTypes.DataForms.DataFormFieldValue
            If urlReply.DataFormItem.TryGetValue("PRIMARYSITEURL", dffv) AndAlso dffv.Value IsNot Nothing Then Me.FRIENDLYURLBASE.Value = OnlineGivingPageContentAddFormUIModel.FormatSSLURL(dffv.Value.ToString)
            If dffv.Value IsNot Nothing Then
                Me.FRIENDLYURL.Value = Me.FRIENDLYURLBASE.Value + IIf(Me.FRIENDLYURLSUFFIX.Value <> String.Empty, Me.FRIENDLYURLSUFFIX.Value, String.Empty)
            Else
                Me.FRIENDLYURL.Value = String.Empty
            End If
            'If Me.Mode = DataFormMode.Add Then
            '    Me.FRIENDLYURL.Value = Me.FRIENDLYURLBASE.Value
            'End If

            'If dffv.Value IsNot Nothing Then
            '    Me.FRIENDLYURL.Value = String.Empty
            'End If
        End If
    End Sub
End Class

Partial Public Class [AddDonationFormConfigurationAddDataFormDESIGNATIONSUIModel]

    Private Sub _designationid_ValueChanged(sender As Object, e As ValueChangedEventArgs) Handles _designationid.ValueChanged
        If Not Me.RootUIModel.Loading Then
            Me.LABEL.Value = String.Empty
            Me.PUBLICNAME.Value = Me.DESIGNATIONID.SearchDisplayText.ToString

            If Me.USEPUBLICNAME.Value Then
                Me.LABEL.Required = False
                Me.LABEL.Enabled = False
                Me.LABEL.Value = String.Empty
            Else
                Me.LABEL.Required = True
                Me.LABEL.Enabled = True
            End If
        End If
    End Sub

    Private Sub _isdefault_ValueChanged(sender As Object, e As ValueChangedEventArgs) Handles _isdefault.ValueChanged
        If Not Me.RootUIModel.Loading AndAlso Me.ISDEFAULT.Value Then
            AddDonationFormConfigurationAddDataFormUIModel.ResetDefaultDesignations(RootUIModel(), Me.ID.Value)
        End If
    End Sub

    Private Sub OnCreated()
        Me.PUBLICNAME.Enabled = False
    End Sub

    Private Sub _usepublicname_ValueChanged(sender As Object, e As ValueChangedEventArgs) Handles _usepublicname.ValueChanged
        If Me.DESIGNATIONID.Value.ToString <> "00000000-0000-0000-0000-000000000000" Then
            If Me.USEPUBLICNAME.Value Then
                Me.PUBLICNAME.Value = GetDesignationPublicName()
            Else
                Me.PUBLICNAME.Value = String.Empty
            End If
        End If
    End Sub

    Public Function GetDesignationPublicName() As String
        Dim vanityName As String = String.Empty

        Dim request As New Blackbaud.AppFx.Server.DataFormLoadRequest
        With request
            .RecordID = Me.DESIGNATIONID.Value.ToString
            .FormID = New Guid("4eadc264-0a44-4df5-8c8c-d89a1c48746c")
            .IncludeMetaData = True
        End With

        Dim reply As Blackbaud.AppFx.Server.DataFormLoadReply = Server.ServiceMethods.DataFormLoad(request, Me.RootUIModel.GetRequestContext)

        If reply IsNot Nothing Then
            Dim dfi As DataFormItem = reply.DataFormItem
            dfi.TryGetValue("VANITYNAME", vanityName)
        End If

        Return vanityName
    End Function

End Class

Partial Public Class [AddDonationFormConfigurationAddDataFormGIVINGLEVELSUIModel]

    Private Sub _amount_ValueChanged(sender As Object, e As ValueChangedEventArgs) Handles _amount.ValueChanged
        AddDonationFormConfigurationAddDataFormUIModel.BuildAmountsList(RootUIModel())
    End Sub

    Private Sub _isdefault_ValueChanged(sender As Object, e As ValueChangedEventArgs) Handles _isdefault.ValueChanged
        If Not Me.RootUIModel.Loading AndAlso Me.ISDEFAULT.Value Then
            AddDonationFormConfigurationAddDataFormUIModel.ResetDefaultAmounts(RootUIModel(), Me.ID.Value)
        End If
    End Sub

End Class

Partial Public Class [AddDonationFormConfigurationAddDataFormPRECONFIGURATIONSUIModel]

    Private Sub _designationid_ValueChanged(sender As Object, e As ValueChangedEventArgs) Handles _designationid.ValueChanged
        Dim newId As Guid = Guid.NewGuid()
        Me.URLFORMATID.Value = Me.RootUIModel.Fields("FRIENDLYURL").ValueObject + "?source=" + newId.ToString()
        Me.ID.Value = newId
    End Sub
End Class