Public Class OnlineGivingConfigurationViewDataFormUIModel

    Private Sub OnlineGivingConfigurationViewDataFormUIModel_Loaded(ByVal sender As Object, ByVal e As Blackbaud.AppFx.UIModeling.Core.LoadedEventArgs) Handles Me.Loaded
        Me.DESIGNATIONS.DBReadOnly = True
        Me.PRECONFIGURATIONS.DBReadOnly = True
        Me.GIVINGLEVELS.DBReadOnly = True

        Me.DESIGNATIONS.AllowAdd = False
        Me.PRECONFIGURATIONS.AllowAdd = False
        Me.GIVINGLEVELS.AllowAdd = False


    End Sub

End Class