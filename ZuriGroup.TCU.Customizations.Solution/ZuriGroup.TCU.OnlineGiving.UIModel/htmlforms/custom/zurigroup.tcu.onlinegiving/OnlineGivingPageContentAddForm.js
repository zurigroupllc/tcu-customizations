﻿/// <reference path="../../../../../../Blackbaud.AppFx.Server/Deploy/browser/jQuery/jquery-vsdoc.js" />
/// <reference path="../../../../../../Blackbaud.AppFx.Server/Deploy/browser/ExtJS/ext-3.2.0/adapter/jquery/ext-jquery-adapter.js" />
/// <reference path="../../../../../../Blackbaud.AppFx.Server/Deploy/browser/ExtJS/ext-3.2.0/ext-all-debug.js" />
/// <reference path="../../../../../../Blackbaud.AppFx.Server/Deploy/browser/json/json.js" />
/// <reference path="../../../../../../Blackbaud.AppFx.Server/Deploy/browser/bbui/bbui/bbui.js" />
/// <reference path="../../../../../../Blackbaud.AppFx.Server/Deploy/browser/bbui/forms/utility.js" />
/// <reference path="../../../../../../Blackbaud.AppFx.Server/Deploy/browser/bbui/crud/service.js" />
/// <reference path="../../../../../../Blackbaud.AppFx.Server/Deploy/browser/bbui/uimodeling/servicecontracts.js" />
/// <reference path="../../../../../../Blackbaud.AppFx.Server/Deploy/browser/bbui/uimodeling/service.js" />
/// <reference path="../../../../../../Blackbaud.AppFx.Server/Deploy/browser/bbui/forms/actionmapping.js" />
/// <reference path="../../../../../../Blackbaud.AppFx.Server/Deploy/browser/bbui/forms/fieldmapping.js" />
/// <reference path="../../../../../../Blackbaud.AppFx.Server/Deploy/browser/bbui/forms/fieldgroupcontainer.js" />
/// <reference path="../../../../../../Blackbaud.AppFx.Server/Deploy/browser/bbui/forms/formcontainer.js" />

/*jslint bitwise: true, browser: true, eqeqeq: true, undef: true, white: true, onevar: true */
/*global BBUI, Ext, $ */
/*JSLint documentation: http://www.jslint.com/lint.html */

// Main application entry point
(function (container, modelInstanceId) {
    // Form vars
    var Util = BBUI.forms.Utility;

    container.on("formupdated", function (args) {
        var insertEmailImageHtml = Util.findFieldInFormUpdateResult(args.formUpdateResult, "INSERTMAILIMAGEHTML", modelInstanceId),
            fieldValue,
            htmlSelection;

        // Insert image into content        
        if (insertEmailImageHtml) {
            fieldValue = insertEmailImageHtml.value;

            if (BBUI.is(fieldValue) && (fieldValue !== "")) {
                container.insertHtml(modelInstanceId, "HTML", fieldValue);
                container.updateField(modelInstanceId, "INSERTMAILIMAGEHTML", "");
            }
        }
    });
})();