Imports Blackbaud.AppFx.Server
Imports System.Data.SqlClient
Imports Blackbaud.Web.Content.Core

Public NotInheritable Class FormBuilderUploadedFilesDataList
  Inherits AppCatalog.AppDataList

  Public Overrides Function GetListResults() As Blackbaud.AppFx.Server.AppCatalog.AppDataListResult
    Dim resultList As New Generic.List(Of DataListResultRow)
    Dim conn As SqlConnection = Me.RequestContext.OpenAppDBConnection
    Using comm As SqlCommand = conn.CreateCommand()
      comm.CommandTimeout = 45
      Try
        comm.CommandText = String.Format("select duf.ID, duf.Name [FILENAME], duf.UploadDate [UPLOADDATE], " & _
          "case when c.ID is not null then c.NAME when fbs.CLIENTUSERSID is not null then cu.FirstName + ' ' + cu.LastName else 'Anonymous' end [UPLOADEDBY], " & _
          "CAST(CONVERT(decimal, duf.Size) / 1024 AS decimal(10,2)) [SIZE], " & _
          "fbs.ID [FORMBUILDERSUBMISSIONID], fbs.CONSTITUENTID, fbs.CLIENTUSERSID, ISNULL(duf.FOLDERID, 0) [FOLDERID], cs.ID [SITEID], cs.Name [SITE] " & _
          "from USR_FORMBUILDERSUBMISSION fbs " & _
          "cross apply dbo.USR_UFN_GETFORMBUILDERSUBMISSIONFILES(fbs.ID) sf " & _
          "inner join dbo.DocUploadFiles duf on duf.ID = sf.ID " & _
          "left join ClientSites cs on cs.ID = duf.CLIENTSITESID " & _
          "left join CONSTITUENT c on c.ID = fbs.CONSTITUENTID " & _
          "left join ClientUsers cu on cu.ID = fbs.CLIENTUSERSID " & _
          "where fbs.SITECONTENTID = @FORMID and duf.INTRASHBIN = 0")

        comm.Parameters.Add(New SqlParameter("@FORMID", SqlDbType.Int))
        comm.Parameters("@FORMID").Value = ProcessContext.ContextRecordID

        Using reader As SqlDataReader = comm.ExecuteReader()
          While reader.Read()
            Dim result As New DataListResultRow
            Dim valueList As New Generic.List(Of String)

            Dim url = URLBuilder.BuildDocumentLink(reader("ID"), True, "", Nothing)
            Dim folder = SiteFolder.BuildFullFolderName(SiteFolder.EFolderType.Files, reader("FOLDERID"))

            valueList.Add(reader("ID").ToString)
            valueList.Add(reader("FILENAME").ToString)
            valueList.Add(reader("UPLOADDATE").ToString)
            valueList.Add(reader("UPLOADEDBY").ToString)
            valueList.Add(reader("SIZE").ToString)
            valueList.Add(reader("FORMBUILDERSUBMISSIONID").ToString)
            valueList.Add(reader("CONSTITUENTID").ToString)
            valueList.Add(reader("CLIENTUSERSID").ToString)
            valueList.Add(reader("FOLDERID").ToString)
            valueList.Add(folder)
            valueList.Add(reader("SITEID").ToString)
            valueList.Add(reader("SITE").ToString)
            valueList.Add(url)
            result.Values = valueList.ToArray
            resultList.Add(result)
          End While
        End Using

      Catch ex As Exception
        Throw New ArgumentException("Bad syntax, unable to process data list " + ex.Message)
      End Try
    End Using
    Return New AppCatalog.AppDataListResult(resultList)
  End Function
End Class


