<%@ assembly Name="ZuriGroup.TCU.ContactUs.BBIS.Web"%>
<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ContactUsEditor.ascx.vb" Inherits="ZuriGroup.TCU.ContactUs.BBIS.Web.ContactUsEditor" %>
<%@ import Namespace="ZuriGroup.TCU.ContactUs.BBIS.Web"%>
<%@ Register TagPrefix="uc" TagName="HTMLEditor" Src="~/admin/HtmlEditorControl2.ascx" %>
<%@ Register TagPrefix="uc2" TagName="pagelink" Src="~/admin/PageLink.ascx" %>
<%@ Register TagPrefix="bbnc" Namespace="BBNCExtensions.ServerControls" Assembly="BBNCExtensions" %>
<asp:Label ID="lblError" runat="server" Font-Bold="true" ForeColor="red" />
<style>
    .eventContent, .editorContent {
        display: none;
        padding: 20px;
        background: #ffffff;
        border: 1px solid #cccccc;
        border-top: 0;
        -moz-border-radius: 0 0 5px 5px;
        -webkit-border-radius: 0 0 5px 5px;
        border-radius: 0 0 5px 5px;
        height: auto;
    }

    .StepGrouping {
        padding-left: 10px;
    }

    .validationerror li {
        padding-bottom: 2px;
    }

    .RadioContainer .ribbon_tab_btn {
        background: #f7fbff !important;
    }
</style>
<asp:HiddenField ID="hfPartTitle" runat="server" />
<asp:HiddenField ID="hfSiteId" runat="server" />
<asp:HiddenField ID="hfSiteName" runat="server" />
<asp:HiddenField ID="hfEmailTemplateId" runat="server" />
<asp:ValidationSummary ID="ValidationSummary" runat="server" CssClass="YellowBox validationerror" Style="padding: 4px 0px 2px 4px; margin-bottom: 10px;" />
<div id="tabs_Main" style="display: none">
    <ul>
        <li><a href="#tab_FormOptions">Contact Us Form</a></li>
        <li><a href="#tab_SubmissionOptions">Acknowledgement Email</a></li>
    </ul>
    <div id="tab_FormOptions" class="editorContent">
        <div class="StepGrouping">
            <h1 class="StepGroupingHeading">Subject options</h1>
            <div class="StepGroupingBody">
                <div class="HelpText">
                    Select the subject options to display on the contact us form. At least one option must be selected. 
                </div>
                <asp:ListView ID="listSubjectList" runat="server" ItemPlaceholderID="SubjectItemPlaceholder">
                    <ItemTemplate>
                        <div class="VerticalOption">
                            <table class="CheckboxOption">
                                <tbody>
                                    <tr>
                                        <td class="CheckboxOptionField">
                                            <asp:CheckBox ID="checkSubjectCheckbox" runat="server" Text='<%# Eval("Label")%>' Checked='<%# Eval("Checked")%>' />
                                            <asp:HiddenField ID="hfSubjectId" runat="server" Value='<%# Eval("Value")%>' />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
                <asp:CustomValidator ID="validateSubjects" runat="server" OnServerValidate="validateSubjects_ServerValidate"
                    ErrorMessage="At least one subject is required." Display="None" ValidateEmptyText="true"></asp:CustomValidator>
            </div>
        </div>
        <div class="StepGrouping">
            <h1 class="StepGroupingHeading">Additional fields</h1>
            <div class="StepGroupingBody">
                <div class="HelpText">
                    Select the additional fields to display on the contact us form.
                </div>
                <asp:ListView ID="listAdditionalFields" runat="server" ItemPlaceholderID="AdditionalFieldItemPlaceholder">
                    <LayoutTemplate>
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="GridContainer">
                                            <table class="DataGrid" style="width: 650px; border-collapse: collapse;">
                                                <tbody>
                                                    <tr class="DataGridHeader">
                                                        <td>Field</td>
                                                        <td style="text-align: center;">Include</td>
                                                        <td style="text-align: center;">Required</td>
                                                    </tr>
                                                    <tr runat="server" id="AdditionalFieldItemPlaceholder"></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class="DataGridItem">
                            <td class="DataGridItemCell DataGridItemCellLeft">
                                <asp:Label ID="labelFieldName" runat="server" Text='<%# Eval("Label")%>'></asp:Label><asp:HiddenField ID="hfFieldName" runat="server" Value='<%# Eval("Field")%>' />
                            </td>
                            <td class="DataGridItemCell" style="width: 150px; text-align: center;">
                                <asp:CheckBox ID="checkIncludedCheckbox" runat="server" Checked='<%# Eval("Included")%>' />
                            </td>
                            <td class="DataGridItemCell" style="width: 150px; text-align: center;">
                                <asp:CheckBox ID="checkRequiredCheckbox" runat="server" Checked='<%# Eval("Required")%>' Enabled='<%# Eval("RequiredEnabled")%>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <tr class="DataGridItemAlternating">
                            <td class="DataGridItemCell DataGridItemCellLeft">
                                <asp:Label ID="labelFieldName" runat="server" Text='<%# Eval("Label")%>'></asp:Label><asp:HiddenField ID="hfFieldName" runat="server" Value='<%# Eval("Field")%>' />
                            </td>
                            <td class="DataGridItemCell" style="width: 150px; text-align: center;">
                                <asp:CheckBox ID="checkIncludedCheckbox" runat="server" Checked='<%# Eval("Included")%>' />
                            </td>
                            <td class="DataGridItemCell" style="width: 150px; text-align: center;">
                                <asp:CheckBox ID="checkRequiredCheckbox" runat="server" Checked='<%# Eval("Required")%>' />
                            </td>
                        </tr>
                    </AlternatingItemTemplate>
                </asp:ListView>
            </div>
        </div>
        <div class="StepGrouping">
            <h1 class="StepGroupingHeading">Google reCAPTCHA</h1>
            <div class="StepGroupingBody">
                <div class="HelpText">
                    Choose whether the contact us form should display Google reCAPTCHA functionality.
                </div>
                <div class="VerticalOption" id="divReCaptchaSettings" runat="server">
                    <table class="CheckboxOption">
                        <tbody>
                            <tr>
                                <td class="CheckboxOptionField">
                                    <asp:CheckBox ID="checkIncludeReCaptcha" runat="server" Text="Include Google reCAPTCHA V2" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="SubOptionContainer" id="trReCaptchaOptions">
                        <div class="StepGrouping">
                            <div class="StepGroupingBody">
                                <div class="HelpText">
                                    Enter your Google reCAPTCHA V2 site key and secret key. You can find more information about Google 
                            reCAPTCHA V2 and the required keys by visting <a href="https://www.google.com/recaptcha" target="_blank" title="Google reCAPTCHA">https://www.google.com/recaptcha</a>.
                                </div>
                                <div class="VerticalOption">
                                    <div class="SelectOption">
                                        <div class="SelectOptionField" style="width: 60%">
                                            <div class="FieldHeading">
                                                <asp:Label ID="labelReCaptchaV2SiteKey" runat="server" AssociatedControlID="inputReCaptchaV2SiteKey" Text="Site key: " CssClass="FieldLabel"></asp:Label>
                                                <span class="FieldRequired">*</span>
                                                <span class="clear"></span>
                                            </div>
                                            <div class="FieldContent">
                                                <asp:TextBox ID="inputReCaptchaV2SiteKey" runat="server" CssClass="FieldSelect FieldInput"></asp:TextBox>
                                                <asp:CustomValidator ID="validateReCaptchaV2SiteKey" runat="server" ControlToValidate="inputReCaptchaV2SiteKey"
                                                    ErrorMessage="Please enter your reCAPTCHA V2 site key." Display="None" ValidateEmptyText="true"
                                                    OnServerValidate="validateReCaptchaV2SiteKey_ServerValidate"></asp:CustomValidator>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="VerticalOption">
                                    <div class="SelectOption">
                                        <div class="SelectOptionField" style="width: 60%">
                                            <div class="FieldHeading">
                                                <asp:Label ID="labelReCaptchaV2SecretKey" runat="server" AssociatedControlID="inputReCaptchaV2SecretKey" Text="Secret key: " CssClass="FieldLabel"></asp:Label>
                                                <span class="FieldRequired">*</span>
                                                <span class="clear"></span>
                                            </div>
                                            <div class="FieldContent">
                                                <asp:TextBox ID="inputReCaptchaV2SecretKey" runat="server" CssClass="FieldSelect FieldInput"></asp:TextBox>
                                                <asp:CustomValidator ID="validateReCaptchaV2SecretKey" runat="server" ControlToValidate="inputReCaptchaV2SecretKey"
                                                    ErrorMessage="Please enter your reCAPTCHA V2 secret key." Display="None" ValidateEmptyText="true"
                                                    OnServerValidate="validateReCaptchaV2SecretKey_ServerValidate"></asp:CustomValidator>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="VerticalOption" style="padding-top: 5px;">
                                    <table class="CheckboxOption">
                                        <tbody>
                                            <tr>
                                                <td class="CheckboxOptionField">
                                                    <asp:CheckBox ID="checkRequireRegisteredReCaptcha" runat="server" Text="Require linked users to complete reCAPTCHA test" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="StepGrouping">
            <h1 class="StepGroupingHeading">Form header</h1>
            <div class="StepGroupingBody">
                <div class="HelpText">
                    Define text that will appear above the contact us form. This text will not be visible after the form is submitted.
                </div>
                <div class="VerticalOption">
                    <bbnc:htmleditor id="htmlFormHeaderEditor" runat="server" />
                </div>
            </div>
        </div>
        <div class="StepGrouping">
            <h1 class="StepGroupingHeading">Form submission</h1>
            <div class="StepGroupingBody">
                <div class="HelpText">
                    Choose whether the contact us form will display a customizable message or navigate to an alternative page after the form has been submitted.
                </div>
                <div class="RadioContainer">
                    <div class="RadioGroupingNarrow">
                        <p id="ColumnDefaultItem" class="RadioSelected">
                            <asp:RadioButton runat="server" ID="ColumnDefaulRadioButton" GroupName="ColumnGroup"
                                Text="Success message" Checked="true"></asp:RadioButton>
                        </p>
                        <p id="ColumnCustomItem">
                            <asp:RadioButton runat="server" ID="ColumnCustomRadioButton" GroupName="ColumnGroup"
                                Text="Alternative Page"></asp:RadioButton>
                        </p>
                    </div>
                    <div class="SelectedAreaNarrow">
                        <div class="SelectedAreaInner">
                            <div id="ColumnDefaultPanel">
                                <div class="HelpText">
                                    Define what the success message will look like on the contact us page after the form has been submitted.
                                </div>
                                <div>
                                    <p style="text-align: right">
                                        <asp:LinkButton ID="buttonHtmlSuccessMessageLoadDefault" runat="server" Text="Load default success message" OnClick="buttonHtmlSuccessMessageLoadDefault_Click" CausesValidation="false"></asp:LinkButton>
                                    </p>
                                    <bbnc:htmleditor id="htmlSuccessMessageEditor" runat="server" />
                                </div>
                            </div>
                            <div id="ColumnCustomPanel" style="display: none;">
                                <div class="HelpText">
                                    Define the alternative page that will be displayed after the form has been submitted.
                                </div>
                                <uc2:pagelink ID="pageLinkConfirmationPage" CanDelete="true" runat="server" Tag="Confirmation page" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="tab_SubmissionOptions" class="editorContent">
        <div class="StepGrouping">
            <h1 class="StepGroupingHeading">Acknowledgement recipients</h1>
            <div class="StepGroupingBody">
                <div class="HelpText">
                    Define who will receive the contact us acknowledgement email when the form has been submitted. At least one recipient must be added.
                </div>
                <div class="VerticalOption">
                    <asp:UpdatePanel ID="EmailRecipientUpdatePanel" runat="server" ChildrenAsTriggers="true" RenderMode="Block" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:CustomValidator ID="EmailRecipientCustomValidator" runat="server" Display="None" OnServerValidate="EmailRecipientCustomValidator_ServerValidate" />
                            <asp:DataGrid ID="EmailRecipientDataGrid" runat="server" AutoGenerateColumns="false" CssClass="DataGrid" OnItemCommand="EmailRecipientDataGrid_ItemCommand"
                                CellPadding="0" CellSpacing="0" GridLines="None">
                                <ItemStyle CssClass="DataGridItem" />
                                <AlternatingItemStyle CssClass="DataGridItemAlternating" />
                                <HeaderStyle CssClass="DataGridHeader" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Display name">
                                        <HeaderStyle Width="40%" />
                                        <ItemStyle Wrap="false" CssClass="DataGridItemCell DataGridItemCellLeft" />
                                        <ItemTemplate>
                                            <asp:TextBox ID="NameTextBox" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.DisplayName") %>' Width="90%" runat="server" />
                                            <asp:Label ID="NameRequiredLabel" CssClass="RequiredFieldMarker" Text="*" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Email address">
                                        <HeaderStyle Width="40%" />
                                        <ItemStyle Wrap="false" CssClass="DataGridItemCell" />
                                        <ItemTemplate>
                                            <asp:TextBox ID="EmailAddressTextBox" MaxLength="80" Text='<%# DataBinder.Eval(Container, "DataItem.EmailAddress")%>' Width="90%" runat="server" />
                                            <asp:Label ID="EmailAddressRequiredLabel" CssClass="RequiredFieldMarker" Text="*" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemStyle HorizontalAlign="Center" CssClass="DataGridItemCell DataGridItemCellRight" />
                                        <ItemTemplate>
                                            <asp:Button ID="RemoveRecipientButton" CausesValidation="false" CssClass="CommandButton" Text='Remove'
                                                ToolTip="Remove" runat="server" CommandName="Recipient" CommandArgument="Remove" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="ListAdditionButton">
                                <asp:Button ID="AddRecipientButton" CausesValidation="false" CssClass="CommandButton"
                                    Text="Add Recipient" Width="125px" runat="server" OnClick="AddRecipientButton_Click" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div class="StepGrouping">
            <h1 class="StepGroupingHeading">Acknowledgement email</h1>
            <div class="StepGroupingBody">
                <div class="HelpText"></div>
                <div class="VerticalOption">
                    <table>
                        <tr>
                            <td class="captionCell">
                                <asp:Label ID="labelAcknowledgementName" runat="server" Text="Name of email:"></asp:Label>
                                <asp:RequiredFieldValidator ID="requiredAcknowledgementName" runat="server" ErrorMessage="Acknowledgement name of email is required" Display="None" ControlToValidate="inputAcknowledgementName"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="validateAcknowledgementName" runat="server" ErrorMessage="Error" Display="None" ControlToValidate="inputAcknowledgementName" OnServerValidate="validateAcknowledgementName_ServerValidate"></asp:CustomValidator>
                            </td>
                            <td class="controlCell" style="width: 297px;">
                                <asp:TextBox ID="inputAcknowledgementName" runat="server" Style="width: 275px;"></asp:TextBox><span class="RequiredFieldMarker">*</span>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="captionCell">
                                <asp:Label ID="labelAcknowledgementSubject" runat="server" Text="Subject:"></asp:Label>
                                <asp:RequiredFieldValidator ID="requiredAcknowledgementSubject" runat="server" ErrorMessage="Acknowledgement subject is required" Display="None" ControlToValidate="inputAcknowledgementSubject"></asp:RequiredFieldValidator>
                            </td>
                            <td class="controlCell" style="width: 297px;">
                                <asp:TextBox ID="inputAcknowledgementSubject" runat="server" Style="width: 275px;"></asp:TextBox><span class="RequiredFieldMarker">*</span>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="3">"[Subject]" is dynamic and will be replaced by the user's selected subject value.
                            </td>
                        </tr>
                        <tr>
                            <td class="captionCell">
                                <asp:Label ID="labelAcknowledgementFromAddress" runat="server" Text="From address:"></asp:Label>
                                <asp:RequiredFieldValidator ID="requiredAcknowledgementFromAddress" runat="server" ErrorMessage="Acknowledgement email from address is required" Display="None" ControlToValidate="inputAcknowledgementFromAddress"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="validAcknowledgementFromAddress" runat="server" ControlToValidate="inputAcknowledgementFromAddress"
                                    ErrorMessage="Acknowledgement email from address is invalid" Display="None"
                                    ValidationExpression="^[ ]*[A-Za-z0-9\-_.'`!#$%&*+/=?^{|}~]+@[A-Za-z0-9][A-Za-z0-9\-]*(\.[A-Za-z0-9][A-Za-z0-9\-]*)*\.[A-Za-z]{2,6}[ ]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td class="controlCell" style="width: 297px;">
                                <asp:TextBox ID="inputAcknowledgementFromAddress" runat="server" Style="width: 275px;"></asp:TextBox><span class="RequiredFieldMarker">*</span>
                            </td>
                            <td class="captionCell">
                                <asp:Label ID="labelAcknowledgementFromName" runat="server" Text="From name:"></asp:Label>
                                <asp:RequiredFieldValidator ID="requiredAcknowledgementFromName" runat="server" ErrorMessage="Acknowledgement email from name is required" Display="None" ControlToValidate="inputAcknowledgementFromName"></asp:RequiredFieldValidator>
                            </td>
                            <td class="controlCell">
                                <asp:TextBox ID="inputAcknowledgementFromName" runat="server"></asp:TextBox><span class="RequiredFieldMarker">*</span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="VerticalOption">
                    <p style="text-align: right">
                        <asp:LinkButton ID="buttonHtmlAcknowledgementDefault" runat="server" Text="Load default acknowledgement" OnClick="buttonHtmlAcknowledgementDefault_Click" CausesValidation="false"></asp:LinkButton>
                    </p>
                    <uc:HTMLEditor id="htmlAcknowledgementEditor" runat="server"></uc:HTMLEditor>
                </div>

            </div>
        </div>
    </div>
</div>

<%--used to keep the user on the correct tab after a postback--%>
<asp:HiddenField runat="server" ID="hidLastTab" Value="0" />
<script>
    var stepCurrentSelection = new Array("PlaceholderForZeroIndex", "ColumnDefault");

    function ChangePanel(stepNumber, newSelection)
    {		
        currentSelection = stepCurrentSelection[stepNumber];
        ToggleSelection(currentSelection, newSelection);
        stepCurrentSelection[stepNumber]=newSelection;
    }

    function ToggleSelection(oldSelection, newSelection)
    {
        //change radio button background
        currentRadioItem = $get(oldSelection+'Item');
        newRadioItem = $get(newSelection+'Item');
		
        currentRadioItem.className="";
        newRadioItem.className="RadioSelected";
				
        //change panel
        currentPanel = $get(oldSelection+'Panel');
        newPanel = $get(newSelection+'Panel');
		
        currentPanel.style.display = 'none';
        newPanel.style.display = '';		
    }

    $(document).ready(function () {
        //start tabs
        $("#tabs_Main").tabs({ 
            show: function() { 
                var sel = $("#tabs_Main").tabs("option", "selected"); 
                $("#<%= hidLastTab.ClientID %>").val(sel); 
            }, selected: <%= hidLastTab.Value %>             
            });  
        //end tabs
        $("#tabs_Main").show();
    });
</script>
