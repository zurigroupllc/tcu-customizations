Imports BBNCExtensions
Imports Blackbaud.AppFx.WebAPI
Imports Blackbaud.Web.Content.Core
Imports Blackbaud.Web.Content.SPWrap.fnEmailTemplateNameIsOK

Partial Public Class ContactUsEditor
    Inherits BBNCExtensions.Parts.CustomPartEditorBase

    Private mContent As ContactUsProperties

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)
        RenderPanels()
    End Sub
    Private Sub RenderPanels()
        Dim stringBuilder As StringBuilder = New StringBuilder()
        If MyContent.DisplayAlternativeSuccessPage Then
            stringBuilder.Append("ChangePanel(1, 'ColumnCustom');")
        End If
        ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "LoadPanels", stringBuilder.ToString(), True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            SetContentPartDetails()
            LoadEmailTemplate()
            PopulateSubjects()
            PopulateAdditionalFields()
            PopulateReCAPTCHASettings()
            PopulateSuccessSettings()
            PopulateAcknowledgementSettings()
            PopulateFormHeader()
        End If
        ColumnCustomRadioButton.Attributes("onClick") = "ChangePanel(1,'ColumnCustom');"
        ColumnDefaulRadioButton.Attributes("onClick") = "ChangePanel(1, 'ColumnDefault');"
    End Sub

    Private Sub htmlSuccessMessageEditorr_Init(ByVal sender As Object, ByVal e As EventArgs) Handles htmlSuccessMessageEditor.Init
        htmlSuccessMessageEditor.MergeFields = BBNCExtensions.Interfaces.EMergeFields.None

        htmlSuccessMessageEditor.CustomMergeFields = New ContactRequestFieldProvider
        htmlSuccessMessageEditor.LayoutMode = False
    End Sub
    Private Sub htmlAcknowledgementEditor_Init(ByVal sender As Object, ByVal e As EventArgs) Handles htmlAcknowledgementEditor.Init
        htmlAcknowledgementEditor.MergeFields = BBNCExtensions.Interfaces.EMergeFields.None

        htmlAcknowledgementEditor.EmailMode = True
        htmlAcknowledgementEditor.LayoutMode = False

        htmlAcknowledgementEditor.FieldListProvider = New Blackbaud.Web.Content.Core.Data.IFieldListProvider() {New AdminMergeFieldsProvider}
    End Sub

    Private Sub LoadEmailTemplate()
        Dim eT As EmailTemplate
        If hfEmailTemplateId.Value = "-1" Then
            eT = Me.CreateNewEmailTemplateObject()
            SaveEmailTemplateId(eT.ID)
            hfEmailTemplateId.Value = eT.ID
        End If
    End Sub

    Private Sub SaveEmailTemplateId(emailTemplateId As Integer)
        Dim addForm = New AddForms.ContactRequestEmailProperties.BBISContactRequestEmailPropertiesAddDataFormData
        Try
            addForm.EMAILTEMPLATEID = emailTemplateId
            addForm.ContextRecordID = Me.Content.ContentID
            addForm.Save(Me.API.AppFxWebServiceProvider)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Overrides Function OnSaveContent(Optional ByVal bDialogIsClosing As Boolean = True) As Boolean
        Page.Validate()
        If Page.IsValid Then
            With MyContent
                SaveSelectedSubjects()
                SaveAdditionalFields()
                SaveReCAPTCHASettings()
                SaveSuccessSettings()
                SaveFormHeader()
                SaveAcknowledgementSettings()

                MyContent.PartIsNotNew = True
            End With
            Me.Content.SaveContent(MyContent)
            Return True
        Else
            Return False
        End If
    End Function

    Private ReadOnly Property MyContent() As ContactUsProperties
        Get
            If mContent Is Nothing Then
                mContent = Me.Content.GetContent(GetType(ContactUsProperties))
                If mContent Is Nothing Then
                    mContent = New ContactUsProperties
                End If
            End If
            Return mContent
        End Get
    End Property

    Private Sub PopulateSubjects()
        Dim subjects As List(Of Subject) = New List(Of Subject)

        For Each item As CodeTableEntryItem In CodeTableServices.GetList(Me.API.AppFxWebServiceProvider, "USR_ONLINECONTACTREQUESTSUBJECTCODE", CodeTableEntryIncludeOption.IncludeActiveOnly)
            Dim subject = New Subject()
            subject.Value = item.EntryID
            subject.Label = item.Description
            subject.Checked = False

            If Not (MyContent.Subjects Is Nothing) AndAlso MyContent.Subjects.Contains(item.EntryID) Then
                subject.Checked = True
            End If

            subjects.Add(subject)

        Next

        listSubjectList.DataSource = subjects
        listSubjectList.DataBind()

    End Sub
    Private Sub PopulateAdditionalFields()
        Dim fields As List(Of ContactUsField) = New List(Of ContactUsField)
        If MyContent.PartIsNotNew = False Then
            fields.Add(New ContactUsField("inputMiddleInitial", "Middle initial", True))
            fields.Add(New ContactUsField("inputMaidenName", "Maiden name", True))
            fields.Add(New ContactUsField("inputPhoneNumber", "Phone number", True))
            fields.Add(New ContactUsField("inputClassYear", "Class year", True))
            fields.Add(New ContactUsField("inputEmailAddressValidate", "Verify email address", True))
        Else
            fields.Add(New ContactUsField("inputMiddleInitial", "Middle initial"))
            fields.Add(New ContactUsField("inputMaidenName", "Maiden name"))
            fields.Add(New ContactUsField("inputPhoneNumber", "Phone number"))
            fields.Add(New ContactUsField("inputClassYear", "Class year"))
            fields.Add(New ContactUsField("inputEmailAddressValidate", "Verify email address"))

            If Not (MyContent.IncludedFields Is Nothing) Then
                For Each item In fields
                    item.Included = MyContent.IncludedFields.Contains(item.Field)
                Next
            End If

            If Not (MyContent.RequiredFields Is Nothing) Then
                For Each item In fields
                    If item.Field = "inputEmailAddressValidate" Then
                        item.Required = True
                        item.RequiredEnabled = False
                    Else
                        item.Required = MyContent.RequiredFields.Contains(item.Field)
                    End If
                Next
            End If
        End If

        listAdditionalFields.DataSource = fields
        listAdditionalFields.DataBind()

    End Sub
    Private Sub PopulateReCAPTCHASettings()
        checkIncludeReCaptcha.Checked = MyContent.UseReCaptcha
        checkRequireRegisteredReCaptcha.Checked = MyContent.RequireRegisteredReCaptcha
        inputReCaptchaV2SiteKey.Text = MyContent.ReCaptchaV2SiteKey
        inputReCaptchaV2SecretKey.Text = MyContent.ReCaptchaV2SecretKey
    End Sub
    Private Sub PopulateSuccessSettings()
        If MyContent.PartIsNotNew = False Then
            htmlSuccessMessageEditor.StorageHTML = DefaultSuccessMessageStorageHTML
        Else
            htmlSuccessMessageEditor.StorageHTML = MyContent.SuccessMessageStorageHTML
        End If

        DirectCast(pageLinkConfirmationPage, Blackbaud.Web.Content.Portal.PageLink).PageID = MyContent.ConfirmationPageId
        ColumnCustomRadioButton.Checked = MyContent.DisplayAlternativeSuccessPage
    End Sub
    Private Sub PopulateFormHeader()
        If MyContent.PartIsNotNew = False Then
            htmlFormHeaderEditor.StorageHTML = DefaultFormHeader
        Else
            htmlFormHeaderEditor.StorageHTML = MyContent.FormHeaderStorageHTML
        End If
    End Sub
    Private Sub PopulateAcknowledgementSettings()
        LoadEmailRecipients()

        Dim template As EmailTemplate = New EmailTemplate(Integer.Parse(hfEmailTemplateId.Value))

        inputAcknowledgementName.Text = template.Name
        inputAcknowledgementSubject.Text = template.Subject
        inputAcknowledgementFromAddress.Text = template.FromAddress
        inputAcknowledgementFromName.Text = template.FromDisplayName

        If MyContent.PartIsNotNew = False Then
            htmlAcknowledgementEditor.StorageHTML = DefaultAcknowledgementStorageHTML
        Else
            htmlAcknowledgementEditor.StorageHTML = template.ContentHTML
        End If
    End Sub

    Private Sub SaveReCAPTCHASettings()
        MyContent.UseReCaptcha = checkIncludeReCaptcha.Checked
        MyContent.RequireRegisteredReCaptcha = checkRequireRegisteredReCaptcha.Checked
        MyContent.ReCaptchaV2SiteKey = inputReCaptchaV2SiteKey.Text
        MyContent.ReCaptchaV2SecretKey = inputReCaptchaV2SecretKey.Text
    End Sub
    Private Sub SaveSelectedSubjects()
        MyContent.Subjects = New List(Of Guid)

        For Each item As ListViewItem In listSubjectList.Items
            Dim checked = DirectCast(item.FindControl("checkSubjectCheckbox"), CheckBox).Checked
            Dim value = DirectCast(item.FindControl("hfSubjectId"), HiddenField).Value
            If checked AndAlso value <> "" Then
                MyContent.Subjects.Add(Guid.Parse(value))
            End If
        Next
    End Sub
    Private Sub SaveAdditionalFields()
        MyContent.IncludedFields = New List(Of String)
        MyContent.RequiredFields = New List(Of String)

        For Each item As ListViewItem In listAdditionalFields.Items
            Dim included = DirectCast(item.FindControl("checkIncludedCheckbox"), CheckBox).Checked
            Dim required = DirectCast(item.FindControl("checkRequiredCheckbox"), CheckBox).Checked
            Dim field = DirectCast(item.FindControl("hfFieldName"), HiddenField).Value

            If included Then
                MyContent.IncludedFields.Add(field)
            End If
            If required Then
                MyContent.RequiredFields.Add(field)
            End If
        Next

    End Sub
    Private Sub SaveSuccessSettings()
        MyContent.SuccessMessageStorageHTML = htmlSuccessMessageEditor.StorageHTML
        MyContent.ConfirmationPageId = DirectCast(pageLinkConfirmationPage, Blackbaud.Web.Content.Portal.PageLink).PageID
        MyContent.DisplayAlternativeSuccessPage = ColumnCustomRadioButton.Checked
    End Sub

    Private Sub SaveAcknowledgementSettings()
        SaveEmailRecipients()
        Try
            Dim template As EmailTemplate = New EmailTemplate(Integer.Parse(hfEmailTemplateId.Value))
            template.DataSourceID = 303
            template.ContentHTML = htmlAcknowledgementEditor.StorageHTML
            template.Name = inputAcknowledgementName.Text
            template.Subject = inputAcknowledgementSubject.Text
            template.FromAddress = inputAcknowledgementFromAddress.Text
            template.FromDisplayName = inputAcknowledgementFromName.Text
            template.Save()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SaveFormHeader()
        MyContent.FormHeaderStorageHTML = htmlFormHeaderEditor.StorageHTML
    End Sub

    Protected Sub checkIncludedCheckbox_CheckedChanged(sender As Object, e As EventArgs)
        Dim included = DirectCast(sender, CheckBox)
        Dim required = DirectCast(included.Parent.FindControl("checkRequiredCheckbox"), CheckBox)
        required.Enabled = included.Checked
        If Not (included.Checked) Then
            required.Checked = False
        End If
    End Sub

    Protected Sub validateReCaptchaV2SecretKey_ServerValidate(source As Object, args As ServerValidateEventArgs)
        If checkIncludeReCaptcha.Checked AndAlso inputReCaptchaV2SecretKey.Text = "" Then
            args.IsValid = False
        Else
            args.IsValid = True
        End If
    End Sub

    Protected Sub validateReCaptchaV2SiteKey_ServerValidate(source As Object, args As ServerValidateEventArgs)
        If checkIncludeReCaptcha.Checked AndAlso inputReCaptchaV2SiteKey.Text = "" Then
            args.IsValid = False
        Else
            args.IsValid = True
        End If
    End Sub

    Protected Sub validateSubjects_ServerValidate(source As Object, args As ServerValidateEventArgs)
        Dim checked As Integer = 0
        For Each item In listSubjectList.Items
            Dim checkbox = DirectCast(item.FindControl("checkSubjectCheckbox"), CheckBox)
            If checkbox.Checked Then
                checked += 1
            End If
        Next

        args.IsValid = (checked > 0)
    End Sub

    Private Function CreateNewEmailTemplateObject() As Blackbaud.Web.Content.Core.EmailTemplate

        Try
            Dim emailTemplate As Blackbaud.Web.Content.Core.EmailTemplate = New Blackbaud.Web.Content.Core.EmailTemplate(Blackbaud.Web.Content.Common.Enumerations.EmailType.Acknowledgement)

            Dim name As String = "Contact Us Notification (" + hfPartTitle.Value + ")"
            emailTemplate.ClientSitesID = hfSiteId.Value
            emailTemplate.Name = name
            emailTemplate.Subject = "Contact us - [Subject]"
            emailTemplate.Save()
            Return emailTemplate

        Catch ex As Exception
            Throw ex
        End Try

        Return Nothing
    End Function
    Private Sub SetContentPartDetails()
        Dim view = New ViewForms.SiteContent.BBISContentPartDetailsViewDataFormData
        Try
            view = ViewForms.SiteContent.BBISContentPartDetailsViewDataForm.LoadData(Me.API.AppFxWebServiceProvider, Me.Content.ContentID)
            hfSiteId.Value = view.SITEID
            hfSiteName.Value = view.SITENAME
            hfPartTitle.Value = view.TITLE
            hfEmailTemplateId.Value = view.EMAILTEMPLATEID
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub buttonHtmlSuccessMessageLoadDefault_Click(sender As Object, e As EventArgs)
        htmlSuccessMessageEditor.StorageHTML = DefaultSuccessMessageStorageHTML
    End Sub
    Public ReadOnly Property DefaultSuccessMessageStorageHTML() As String
        Get
            Dim html As String = ""
            html += "<h3>Your message has been sent!</h3>" + vbCrLf
            html += "<p>Thank you for contacting us regarding&nbsp;<img src=""insertField.field?id=7&amp;nmode=0&amp;name=Subject&amp;type=15"" style=""cursor: move;"" title=""Subject"" runat=""server"" fieldid=""7"" attribid=""0"" searchable=""0"" fieldname=""Subject"" fieldtype=""15"" htmlencode=""True"" isloop=""False"">. We will reach out to you as soon as possible.</p>" + vbCrLf
            Return html
        End Get
    End Property

    Protected Sub buttonHtmlAcknowledgementDefault_Click(sender As Object, e As EventArgs)
        htmlAcknowledgementEditor.StorageHTML = DefaultAcknowledgementStorageHTML
    End Sub
    Public ReadOnly Property DefaultAcknowledgementStorageHTML() As String
        Get
            Dim html As String = ""
            html += "<p><img src=""insertField.field?id=21&amp;nmode=0&amp;name=Recipient+display+name&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""21"" attribid=""0"" searchable=""0"" fieldname=""Recipient display name"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False"">,</p>" + vbCrLf
            html += "<p><strong>A new contact us form submission has been received.<br></strong></p>" + vbCrLf
            html += "<table border=""0"" style=""width: 500px;""><tbody>" + vbCrLf
            html += "<tr><td>Submitted by:</td>" + vbCrLf
            html += "<td><img src=""insertField.field?id=1&amp;nmode=0&amp;name=First+name&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""1"" attribid=""0"" searchable=""0"" fieldname=""First name"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False"">&nbsp;<img src=""insertField.field?id=2&amp;nmode=0&amp;name=Last+name&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""2"" attribid=""0"" searchable=""0"" fieldname=""Last name"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
            html += "<tr><td>Constituent lookup ID:</td>" + vbCrLf
            html += "<td><img src=""insertField.field?id=25&amp;nmode=0&amp;name=Constituent+lookup+ID&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""25"" attribid=""0"" searchable=""0"" fieldname=""Constituent lookup ID"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
            html += "<tr><td>Submitted on:</td>" + vbCrLf
            html += "<td><img src=""insertField.field?id=24&amp;nmode=0&amp;name=Date+submitted&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""24"" attribid=""0"" searchable=""0"" fieldname=""Date submitted"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
            html += "<tr><td>Originating page:</td>" + vbCrLf
            html += "<td><img src=""insertField.field?id=22&amp;nmode=0&amp;name=Page+name&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""22"" attribid=""0"" searchable=""0"" fieldname=""Page name"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
            html += "<tr><td>Originating website:</td>" + vbCrLf
            html += "<td><img src=""insertField.field?id=23&amp;nmode=0&amp;name=Site+name&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""23"" attribid=""0"" searchable=""0"" fieldname=""Site name"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
            html += "</tbody></table>" + vbCrLf
            html += "<p><strong>Submitted fields</strong></p>" + vbCrLf
            html += "<table border=""0"" style=""width: 500px;""><tbody>" + vbCrLf
            html += "<tr><td>First name:</td>" + vbCrLf
            html += "<td><img src=""insertField.field?id=1&amp;nmode=0&amp;name=First+name&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""1"" attribid=""0"" searchable=""0"" fieldname=""First name"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
            html += "<tr><td>Middle initial:</td>" + vbCrLf
            html += "<td><img src=""insertField.field?id=9&amp;nmode=0&amp;name=Middle+initial&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""9"" attribid=""0"" searchable=""0"" fieldname=""Middle initial"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
            html += "<tr><td>Last name:</td>" + vbCrLf
            html += "<td><img src=""insertField.field?id=2&amp;nmode=0&amp;name=Last+name&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""2"" attribid=""0"" searchable=""0"" fieldname=""Last name"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
            html += "<tr><td>Maiden name:</td>" + vbCrLf
            html += "<td><img src=""insertField.field?id=3&amp;nmode=0&amp;name=Maiden+name&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""3"" attribid=""0"" searchable=""0"" fieldname=""Maiden name"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
            html += "<tr><td>Class year:</td>" + vbCrLf
            html += "<td><img src=""insertField.field?id=4&amp;nmode=0&amp;name=Class+year&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""4"" attribid=""0"" searchable=""0"" fieldname=""Class year"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
            html += "<tr><td>Phone number:</td>" + vbCrLf
            html += "<td><img src=""insertField.field?id=5&amp;nmode=0&amp;name=Phone+number&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""5"" attribid=""0"" searchable=""0"" fieldname=""Phone number"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
            html += "<tr><td>Email address:</td>" + vbCrLf
            html += "<td><img src=""insertField.field?id=6&amp;nmode=0&amp;name=Email+address&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""6"" attribid=""0"" searchable=""0"" fieldname=""Email address"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
            html += "<tr><td>Subject:</td>" + vbCrLf
            html += "<td><img src=""insertField.field?id=7&amp;nmode=0&amp;name=Subject&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""7"" attribid=""0"" searchable=""0"" fieldname=""Subject"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
            html += "<tr><td colspan=""2"">Comments:</td></tr>" + vbCrLf
            html += "<tr><td colspan=""2""><img src=""insertField.field?id=8&amp;nmode=0&amp;name=Comments&amp;type=199123421"" style=""cursor: move;"" title="""" runat=""server"" fieldid=""8"" attribid=""0"" searchable=""0"" fieldname=""Comments"" fieldtype=""199123421"" htmlencode=""True"" isloop=""False""></td></tr>" + vbCrLf
            html += "</tbody></table>" + vbCrLf
            Return html
        End Get
    End Property

    Public ReadOnly Property DefaultFormHeader() As String
        Get
            Dim html As String = ""
            html += "<p>Please complete the form below and we will reach out to you shortly.</p>"
            Return html
        End Get
    End Property


    Protected Sub validateAcknowledgementName_ServerValidate(source As Object, args As ServerValidateEventArgs)
        If Not IsEmailTemplateNameOK() Then
            validateAcknowledgementName.ErrorMessage = "Acknowledgement name of email must be unique. Please specify another name."
            args.IsValid = False
        Else
            args.IsValid = True
        End If
    End Sub

    Public Function IsEmailTemplateNameOK() As Boolean
        Dim result As Boolean = False
        Try
            result = WrapperRoutines.ExecuteScalar(DataObject.AppConnString(), Integer.Parse(hfEmailTemplateId.Value), Me.inputAcknowledgementName.Text, Integer.Parse(hfSiteId.Value))
        Catch ex As System.Exception
            result = False
        End Try
        Return result
    End Function

    Private Property EmailRecipientsDataSource() As System.Collections.Generic.List(Of EmailRecipient)
        Get
            Return Me.MyContent.EmailRecipients
        End Get
        Set(value As System.Collections.Generic.List(Of EmailRecipient))
            Me.MyContent.EmailRecipients = value
        End Set
    End Property

    Protected Sub AddRecipientButton_Click(sender As Object, e As EventArgs)
        Dim list As System.Collections.Generic.List(Of EmailRecipient) = Me.regenerateDataSource(Me.EmailRecipientDataGrid)
        Dim item As EmailRecipient = Nothing
        list.Add(item)
        Me.EmailRecipientsDataSource = list
        Me.EmailRecipientDataGrid.DataSource = Me.EmailRecipientsDataSource
        Me.EmailRecipientDataGrid.DataBind()
    End Sub

    Private Function regenerateDataSource(Grid As DataGrid) As System.Collections.Generic.List(Of EmailRecipient)
        Dim list As System.Collections.Generic.List(Of EmailRecipient) = New System.Collections.Generic.List(Of EmailRecipient)()

        For Each item As DataGridItem In EmailRecipientDataGrid.Items
            Dim li As EmailRecipient = New EmailRecipient()
            li.DisplayName = DirectCast(item.FindControl("NameTextBox"), TextBox).Text
            li.EmailAddress = DirectCast(item.FindControl("EmailAddressTextBox"), TextBox).Text
            list.Add(li)
        Next

        Return list
    End Function

    Private Sub SaveEmailRecipients()
        Dim list As System.Collections.Generic.List(Of EmailRecipient) = Me.regenerateDataSource(Me.EmailRecipientDataGrid)
        MyContent.EmailRecipients = list
    End Sub
    Private Sub LoadEmailRecipients()
        Me.EmailRecipientsDataSource = MyContent.EmailRecipients
        Me.EmailRecipientDataGrid.DataSource = Me.EmailRecipientsDataSource
        Me.EmailRecipientDataGrid.DataBind()
    End Sub

    Protected Sub EmailRecipientCustomValidator_ServerValidate(source As Object, args As ServerValidateEventArgs)
        args.IsValid = True
        Dim emailExpression As New Regex("^[ ]*[A-Za-z0-9\-_.'`!#$%&*+/=?^{|}~]+@[A-Za-z0-9][A-Za-z0-9\-]*(\.[A-Za-z0-9][A-Za-z0-9\-]*)*\.[A-Za-z]{2,6}[ ]*$")

        If EmailRecipientDataGrid.Items.Count = 0 Then
            EmailRecipientCustomValidator.ErrorMessage = "At least one acknowledgement recipient is required."
            args.IsValid = False
        Else
            For Each item As DataGridItem In EmailRecipientDataGrid.Items
                Dim name = DirectCast(item.FindControl("NameTextBox"), TextBox).Text
                Dim email = DirectCast(item.FindControl("EmailAddressTextBox"), TextBox).Text
                If name = "" Or email = "" Then
                    EmailRecipientCustomValidator.ErrorMessage = "Display name and email address are required for all email recipients."
                    args.IsValid = False
                    Exit For
                ElseIf Not emailExpression.IsMatch(email) Then
                    EmailRecipientCustomValidator.ErrorMessage = "One or more recipient email addresses are invalid."
                    args.IsValid = False
                    Exit For
                End If
            Next
        End If
    End Sub

    Protected Sub EmailRecipientDataGrid_ItemCommand(source As Object, e As DataGridCommandEventArgs)
        If e.CommandArgument = "Remove" Then
            Dim list As System.Collections.Generic.List(Of EmailRecipient) = Me.regenerateDataSource(Me.EmailRecipientDataGrid)
            list.RemoveAt(e.Item.ItemIndex)

            Me.EmailRecipientsDataSource = list
            Me.EmailRecipientDataGrid.DataSource = Me.EmailRecipientsDataSource
            Me.EmailRecipientDataGrid.DataBind()
        End If
    End Sub
End Class