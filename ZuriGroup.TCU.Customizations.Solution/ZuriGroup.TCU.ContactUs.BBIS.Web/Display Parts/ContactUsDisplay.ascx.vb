Imports BBNCExtensions
Imports Blackbaud.AppFx.WebAPI
Imports System.Net
Imports System.IO
Imports System.Web.Script.Serialization
Imports Blackbaud.Web.Content.Core
Imports Blackbaud.Web.Content.Common

Partial Public Class ContactUsDisplay
    Inherits BBNCExtensions.Parts.CustomPartDisplayBase

    Private mContent As ContactUsProperties

    Private FirstNameValidationMessage As String
    Private MiddleInitialValidationMessage As String
    Private LastNameValidationMessage As String
    Private MaidenNameValidationMessage As String
    Private PhoneNumberValidationMessage As String
    Private PhoneNumberInvalidMessage As String
    Private EmailAddressValidationMessage As String
    Private EmailAddressInvalidMessage As String
    Private EmailAddressValidateValidationMessage As String
    Private EmailAddressesDoNotMatchMessage As String
    Private ClassYearValidationMessage As String
    Private ClassYearInvalidMessage As String
    Private SubjectValidationMessage As String
    Private CommentsValidationMessage As String
    Private ReCaptchaErrorMessage As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ContactUsMultiView.ActiveViewIndex = 0

            SetContentPartDetails()
            CheckForFatalErrors()

            SetValidationErrorMessages()
            SetRequiredFields()
            SetAdditionalFieldProperties()
            SetReCaptchaProperties()
            PopulateSubjects()
            PopulateUserDetails()
            RenderFormHeader()
        End If
        Page.Header.Controls.Add(New LiteralControl("<script src='https://www.google.com/recaptcha/api.js'></script>"))
    End Sub

    Private Sub CheckForFatalErrors()
        Dim fatalError As Boolean = False

        If MyContent IsNot Nothing Then
            If MyContent.EmailRecipients IsNot Nothing AndAlso MyContent.Subjects IsNot Nothing Then
                If MyContent.EmailRecipients.Count = 0 Or MyContent.Subjects.Count = 0 Then
                    fatalError = True
                End If
            End If
        End If

        If MyContent.UseReCaptcha AndAlso MyContent.ReCaptchaV2SiteKey = "" Then
            fatalError = True
        End If
        If MyContent.DisplayAlternativeSuccessPage AndAlso MyContent.ConfirmationPageId = Nothing Then
            fatalError = True
        End If
        Dim emailTemplate = New EmailTemplate(Integer.Parse(hfEmailTemplateId.Value))
        If emailTemplate Is Nothing Then
            fatalError = True
        Else
            If emailTemplate.FromAddress = String.Empty Or emailTemplate.FromDisplayName = String.Empty Then
                fatalError = True
            End If
        End If
        If fatalError Then
            ContactUsMultiView.ActiveViewIndex = 2
            labelFatalError.Text += "This form is not configured correctly. Please check that all required setup fields are filled in."
        End If
    End Sub


    Private ReadOnly Property MyContent() As ContactUsProperties
        Get
            If mContent Is Nothing Then
                mContent = Me.Content.GetContent(GetType(ContactUsProperties))
                If mContent Is Nothing Then
                    mContent = New ContactUsProperties
                End If
            End If
            Return mContent
        End Get
    End Property

    Public Overrides Sub RegisterLanguageFields()

        Me.RegisterLanguageField("48BDA43E-5BA0-41FE-A129-810DC94E01D1", labelFirstName, "First name", "First name:", "Field labels", False)
        Me.RegisterLanguageField("B33DD496-E09A-44AD-992D-8C4A1E5A5A54", labelMiddleInitial, "Middle initial", "Middle initial:", "Field labels", False)
        Me.RegisterLanguageField("B720AF9D-5B3C-4ADF-8F45-9797FAD54FE8", labelLastName, "Last name", "Last name:", "Field labels", False)
        Me.RegisterLanguageField("E7C50967-F309-4FA6-A3FF-96E6759C9F28", labelMaidenName, "Maiden name", "Birth last name:", "Field labels", False)
        Me.RegisterLanguageField("4E19A30A-4D31-42A8-9619-975E1494A8DF", labelPhoneNumber, "Phone number", "Phone number:", "Field labels", False)
        Me.RegisterLanguageField("8ED84050-83E9-4634-B247-08E33BAE4DCB", labelEmailAddress, "Email address", "Email address:", "Field labels", False)
        Me.RegisterLanguageField("714BD947-D9ED-4823-8071-0B12715BEB68", labelEmailAddressValidate, "Verify email address", "Verify email address:", "Field labels", False)
        Me.RegisterLanguageField("6DFDBE89-4EC8-4EBC-948A-7AE528540384", labelClassYear, "Class year", "Class year:", "Field labels", False)
        Me.RegisterLanguageField("6137C17C-27ED-4C04-98B3-5C46CB5EB0B5", labelSubjectDropdown, "Subject", "Subject:", "Field labels", False)
        Me.RegisterLanguageField("13DCE9D6-CE10-4AE5-A60B-4D7CBB32822C", labelComments, "Comments/Questions", "Enter comments or a description here:", "Field labels", False)
        Me.RegisterLanguageField("3F4ED170-E334-4C33-9AD6-82A9159297D6", SubmitButton, "Submit button", "Submit", "Field labels", False)
        Me.RegisterLanguageField("F1C2AA2C-8833-4285-8136-4CAED8F9186F", labelPhoneNumberHint, "Phone format hint", "(e.g., 555-123-4567)", "Field labels", False)

        Me.RegisterLanguageField("7596FC9C-0C30-46AE-B38C-EF05C3E608BF", Nothing, "Validation summary", "The following issues must be corrected before you can continue", "Validation messages", False)
        Me.RegisterLanguageField("47DC3F03-1DB2-49DC-84B9-70CBD42B3D23", Nothing, "First name required (dynamic)", "[First name] is required", "Validation messages", False)
        Me.RegisterLanguageField("DF375E45-B972-4412-82FE-4794DCE0FF08", Nothing, "Middle initial required (dynamic)", "[Middle initial] is required", "Validation messages", False)
        Me.RegisterLanguageField("A6BC6818-C7AF-4171-BB4A-278948C845D3", Nothing, "Last name required (dynamic)", "[Last name] is required", "Validation messages", False)
        Me.RegisterLanguageField("D97CCE98-7EC1-4D1C-86A5-0908C17671A6", Nothing, "Maiden name required (dynamic)", "[Maiden name] is required", "Validation messages", False)
        Me.RegisterLanguageField("22945EB0-20FA-4F70-BFA5-8878A8270045", Nothing, "Phone number required (dynamic)", "[Phone number] is required", "Validation messages", False)
        Me.RegisterLanguageField("5D86AE5D-7ECF-4441-965D-059DFC679F2D", Nothing, "Email address required (dynamic)", "[Email address] is required", "Validation messages", False)
        Me.RegisterLanguageField("5D8F630A-4DF2-4F2E-BF1D-0619D2B5C7F0", Nothing, "Verify email address required (dynamic)", "[Verify email address] is required", "Validation messages", False)
        Me.RegisterLanguageField("EEE712DA-3953-48F2-B61F-DCA5E5CFE2CA", Nothing, "Class year required (dynamic)", "[Class year] is required", "Validation messages", False)
        Me.RegisterLanguageField("52369CF0-38E3-4ABB-BECC-F3EC5AAE48C7", Nothing, "Subject required (dynamic)", "[Subject] is required", "Validation messages", False)
        Me.RegisterLanguageField("68977B2E-2017-4C23-9078-FC9C7316B063", Nothing, "Comment required (dynamic)", "[Comment] is required", "Validation messages", False)

        Me.RegisterLanguageField("5BC6A893-2A41-4DA3-8454-7CA3C018D9AA", Nothing, "Phone number invalid (dynamic)", "[Phone number] is invalid. Please use only numbers and hyphens.", "Validation messages", False)
        Me.RegisterLanguageField("05256D2C-CE98-4460-8102-D710EB534263", Nothing, "Email address invalid (dynamic)", "[Email address] is invalid. Please enter a valid email address.", "Validation messages", False)
        Me.RegisterLanguageField("80B8E9AA-D1A3-4A2F-9BCC-CFDA997CA05A", Nothing, "Email addresses do not match (dynamic)", "[Email address] and [Verify email address] do not match", "Validation messages", False)
        Me.RegisterLanguageField("CAC110CD-7141-49F8-B792-61887C2D984D", Nothing, "Class year is invalid (dynamic)", "[Class year] is invalid. Please enter a 4 digit year.", "Validation messages", False)
        Me.RegisterLanguageField("8E54BA13-DA86-4B32-B65D-3C50E9E4588A", Nothing, "reCAPTCHA required", "The reCAPTCHA challenge must be completed", "Validation messages", False)

        MyBase.RegisterLanguageFields()
    End Sub
    Private Sub SetValidationErrorMessages()
        SetLanguageVariables()
        requiredFirstName.ErrorMessage = FirstNameValidationMessage
        requiredMiddleInitial.ErrorMessage = MiddleInitialValidationMessage
        requiredLastName.ErrorMessage = LastNameValidationMessage
        requiredMaidenName.ErrorMessage = MaidenNameValidationMessage
        requiredPhoneNumber.ErrorMessage = PhoneNumberValidationMessage
        requiredEmailAddress.ErrorMessage = EmailAddressValidationMessage
        requiredClassYear.ErrorMessage = ClassYearValidationMessage
        requiredSubject.ErrorMessage = SubjectValidationMessage
        requiredComments.ErrorMessage = CommentsValidationMessage

        validPhoneNumber.ErrorMessage = PhoneNumberInvalidMessage
        validEmailAddress.ErrorMessage = EmailAddressInvalidMessage
        validClassYear.ErrorMessage = ClassYearInvalidMessage
        validatorReCaptcha.ErrorMessage = ReCaptchaErrorMessage
    End Sub
    Private Sub SetLanguageVariables()
        FirstNameValidationMessage = Replace(Me.GetLanguageString("{47DC3F03-1DB2-49DC-84B9-70CBD42B3D23}"), "[First name]", Replace(Me.GetLanguageString("{48BDA43E-5BA0-41FE-A129-810DC94E01D1}"), ":", ""))
        MiddleInitialValidationMessage = Replace(Me.GetLanguageString("{DF375E45-B972-4412-82FE-4794DCE0FF08}"), "[Middle initial]", Replace(Me.GetLanguageString("{B33DD496-E09A-44AD-992D-8C4A1E5A5A54}"), ":", ""))
        LastNameValidationMessage = Replace(Me.GetLanguageString("{A6BC6818-C7AF-4171-BB4A-278948C845D3}"), "[Last name]", Replace(Me.GetLanguageString("{B720AF9D-5B3C-4ADF-8F45-9797FAD54FE8}"), ":", ""))
        MaidenNameValidationMessage = Replace(Me.GetLanguageString("{D97CCE98-7EC1-4D1C-86A5-0908C17671A6}"), "[Maiden name]", Replace(Me.GetLanguageString("{E7C50967-F309-4FA6-A3FF-96E6759C9F28}"), ":", ""))
        PhoneNumberValidationMessage = Replace(Me.GetLanguageString("{22945EB0-20FA-4F70-BFA5-8878A8270045}"), "[Phone number]", Replace(Me.GetLanguageString("{4E19A30A-4D31-42A8-9619-975E1494A8DF}"), ":", ""))
        EmailAddressValidationMessage = Replace(Me.GetLanguageString("{5D86AE5D-7ECF-4441-965D-059DFC679F2D}"), "[Email address]", Replace(Me.GetLanguageString("{8ED84050-83E9-4634-B247-08E33BAE4DCB}"), ":", ""))
        EmailAddressValidateValidationMessage = Replace(Me.GetLanguageString("{5D8F630A-4DF2-4F2E-BF1D-0619D2B5C7F0}"), "[Verify email address]", Replace(Me.GetLanguageString("{714BD947-D9ED-4823-8071-0B12715BEB68}"), ":", ""))
        ClassYearValidationMessage = Replace(Me.GetLanguageString("{EEE712DA-3953-48F2-B61F-DCA5E5CFE2CA}"), "[Class year]", Replace(Me.GetLanguageString("{6DFDBE89-4EC8-4EBC-948A-7AE528540384}"), ":", ""))
        SubjectValidationMessage = Replace(Me.GetLanguageString("{52369CF0-38E3-4ABB-BECC-F3EC5AAE48C7}"), "[Subject]", Replace(Me.GetLanguageString("{6137C17C-27ED-4C04-98B3-5C46CB5EB0B5}"), ":", ""))
        CommentsValidationMessage = Replace(Me.GetLanguageString("{68977B2E-2017-4C23-9078-FC9C7316B063}"), "[Comment]", Replace(Me.GetLanguageString("{13DCE9D6-CE10-4AE5-A60B-4D7CBB32822C}"), ":", ""))

        PhoneNumberInvalidMessage = Replace(Me.GetLanguageString("{5BC6A893-2A41-4DA3-8454-7CA3C018D9AA}"), "[Phone number]", Replace(Me.GetLanguageString("{4E19A30A-4D31-42A8-9619-975E1494A8DF}"), ":", ""))
        EmailAddressInvalidMessage = Replace(Me.GetLanguageString("{05256D2C-CE98-4460-8102-D710EB534263}"), "[Email address]", Replace(Me.GetLanguageString("{8ED84050-83E9-4634-B247-08E33BAE4DCB}"), ":", ""))
        EmailAddressesDoNotMatchMessage = Replace(Me.GetLanguageString("{80B8E9AA-D1A3-4A2F-9BCC-CFDA997CA05A}"), "[Verify email address]", Replace(Me.GetLanguageString("{714BD947-D9ED-4823-8071-0B12715BEB68}"), ":", ""))
        EmailAddressesDoNotMatchMessage = Replace(Me.GetLanguageString("{80B8E9AA-D1A3-4A2F-9BCC-CFDA997CA05A}"), "[Email address]", Replace(Me.GetLanguageString("{8ED84050-83E9-4634-B247-08E33BAE4DCB}"), ":", ""))
        ClassYearInvalidMessage = Replace(Me.GetLanguageString("{CAC110CD-7141-49F8-B792-61887C2D984D}"), "[Class year]", Replace(Me.GetLanguageString("{6DFDBE89-4EC8-4EBC-948A-7AE528540384}"), ":", ""))
        ReCaptchaErrorMessage = Me.GetLanguageString("8E54BA13-DA86-4B32-B65D-3C50E9E4588A")
    End Sub

    Private Sub PopulateSubjects()
        If Not (MyContent.Subjects Is Nothing) AndAlso MyContent.Subjects.Count > 0 Then
            Dim li As ListItem

            inputSubjectDropdown.Items.Add("")

            For Each item As CodeTableEntryItem In CodeTableServices.GetList(Me.API.AppFxWebServiceProvider, "USR_ONLINECONTACTREQUESTSUBJECTCODE", CodeTableEntryIncludeOption.IncludeActiveOnly)
                li = New ListItem
                li.Value = item.EntryID.ToString()
                li.Text = item.Description

                If MyContent.Subjects.Contains(item.EntryID) Then
                    inputSubjectDropdown.Items.Add(li)
                End If
            Next
        Else
            inputSubjectDropdown.Enabled = False
            requiredSubject.Enabled = False
        End If
    End Sub


    Private Sub SetContentPartDetails()
        Dim view = New ViewForms.SiteContent.BBISContentPartDetailsViewDataFormData
        Try
            view = ViewForms.SiteContent.BBISContentPartDetailsViewDataForm.LoadData(Me.API.AppFxWebServiceProvider, Me.Content.ContentID)
            hfSiteId.Value = view.SITEID
            hfSiteName.Value = view.SITENAME
            hfPartTitle.Value = view.TITLE
            hfEmailTemplateId.Value = view.EMAILTEMPLATEID
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub SetRequiredFields()
        If Not (MyContent.RequiredFields Is Nothing) Then
            For Each field In MyContent.RequiredFields
                If field = requiredMiddleInitial.ControlToValidate Then
                    labelMiddleInitialRequired.Visible = True
                    requiredMiddleInitial.Enabled = True
                End If
                If field = requiredMaidenName.ControlToValidate Then
                    labelMaidenNameRequired.Visible = True
                    requiredMaidenName.Enabled = True
                End If
                If field = requiredPhoneNumber.ControlToValidate Then
                    labelPhoneNumberRequired.Visible = True
                    requiredPhoneNumber.Enabled = True
                End If
                If field = requiredEmailAddress.ControlToValidate Then
                    labelEmailAddressRequired.Visible = True
                    requiredEmailAddress.Enabled = True
                End If
                If field = requiredClassYear.ControlToValidate Then
                    labelClassYearRequired.Visible = True
                    requiredClassYear.Enabled = True
                End If
            Next
        End If
    End Sub
    Private Sub SetAdditionalFieldProperties()
        If Not (MyContent.IncludedFields Is Nothing) Then
            For Each field In MyContent.IncludedFields
                If field = inputMiddleInitial.ID Then
                    trMiddleInitial.Visible = True
                End If
                If field = inputMaidenName.ID Then
                    trMaidenName.Visible = True
                End If
                If field = inputPhoneNumber.ID Then
                    trPhoneNumber.Visible = True
                    trPhoneNumberHint.Visible = True
                End If
                If field = inputClassYear.ID Then
                    trClassYear.Visible = True
                End If
                If field = inputEmailAddressValidate.ID Then
                    trEmailAddressValidate.Visible = True
                    requiredEmailAddressValidate.Enabled = True
                    compareEmailAddressValidate.Enabled = True
                End If
            Next
        End If
    End Sub

    Private Sub SetReCaptchaProperties()
        If MyContent.UseReCaptcha Then
            Dim user = BBNCExtensions.API.NetCommunity.Current.Users.CurrentUser
            If MyContent.RequireRegisteredReCaptcha Or user.BackOfficeGuid = Guid.Empty Then
                trReCaptcha.Visible = True
                panelReCaptchaV2.Visible = True
                panelReCaptchaV2.Enabled = True
                validatorReCaptcha.Enabled = True

                If MyContent.ReCaptchaV2SiteKey <> "" Then
                    panelReCaptchaV2.Attributes.Add("data-sitekey", MyContent.ReCaptchaV2SiteKey)
                Else
                    lblError.Text += vbCrLf + "reCAPTCHA has been enabled but no site key has been provided. Please update the form settings."
                End If
            End If
        End If
    End Sub

    Protected Sub validatorReCaptcha_ServerValidate(source As Object, args As ServerValidateEventArgs)
        args.IsValid = reCaptchaValidate()
    End Sub
    Public Function reCaptchaValidate() As Boolean
        Dim response As String = Request("g-recaptcha-response")
        Dim valid As Boolean = False
        Dim req As HttpWebRequest = DirectCast(WebRequest.Create(Convert.ToString("https://www.google.com/recaptcha/api/siteverify?secret=" + MyContent.ReCaptchaV2SecretKey + "&response=") & response), HttpWebRequest)

        Try
            Using wResponse As WebResponse = req.GetResponse()

                Using readStream As New StreamReader(wResponse.GetResponseStream())

                    Dim jsonResponse As String = readStream.ReadToEnd()
                    Dim js As New JavaScriptSerializer()
                    Dim data As ReCaptchaSuccess = js.Deserialize(Of ReCaptchaSuccess)(jsonResponse)

                    valid = Convert.ToBoolean(data.success)
                    Return valid
                End Using
            End Using
        Catch ex As Exception
            Return False
        End Try
    End Function

    Protected Sub SubmitButton_Click(sender As Object, e As EventArgs)
        If SaveOnlineContactRequest() AndAlso SendEmailNotification(FillMergeData()) Then


            If MyContent.DisplayAlternativeSuccessPage Then
                Response.Redirect(URLBuilder.BuildSitePageLink(MyContent.ConfirmationPageId))
            Else
                RenderSuccessMessage()
                ContactUsMultiView.ActiveViewIndex = 1
            End If
        Else
            lblError.Text = "An error occured while trying to submit your contact request. Please try again."
        End If
    End Sub
    Private Function SaveOnlineContactRequest() As Boolean
        Dim addForm = New AddForms.OnlineContactRequest.BBISOnlineContactRequestAddDataFormData
        Try
            Dim user = BBNCExtensions.API.NetCommunity.Current.Users.CurrentUser
            If user.BackOfficeGuid <> Guid.Empty Then
                addForm.CONSTITUENTID = user.BackOfficeGuid
            End If

            If user.UserID <> 0 Then
                addForm.CLIENTUSERSID = user.UserID
            End If

            addForm.FIRSTNAME = inputFirstName.Text
            addForm.MIDDLEINITIAL = inputMiddleInitial.Text
            addForm.LASTNAME = inputLastName.Text
            addForm.BIRTHLASTNAME = inputMaidenName.Text
            addForm.PHONENUMBER = inputPhoneNumber.Text
            addForm.EMAILADDRESS = inputEmailAddress.Text
            addForm.CLASSYEAR = inputClassYear.Text
            addForm.ONLINECONTACTREQUESTSUBJECTCODEID = Guid.Parse(inputSubjectDropdown.SelectedValue)
            addForm.MESSAGE = inputComments.Text
            addForm.BBISPAGEID = BBNCExtensions.API.NetCommunity.Current.Pages.CurrentPage.Id

            addForm.Save(Me.API.AppFxWebServiceProvider)

            Return True

        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Sub RenderFormHeader()
        Dim htmlControl As New BBNCExtensions.ServerControls.HtmlDisplay
        htmlControl.StorageHTML = MyContent.FormHeaderStorageHTML

        If htmlControl.StorageHTML IsNot Nothing AndAlso htmlControl.StorageHTML.Length > 0 Then
            Me.phFormHeader.Visible = True
            Me.phFormHeader.Controls.Add(htmlControl)
        End If
    End Sub
    Private Function FillMergeData() As List(Of AdminMergeData)
        Dim list As List(Of AdminMergeData) = New List(Of AdminMergeData)

        Dim data As AdminMergeData = New AdminMergeData()

        data.FirstName = inputFirstName.Text
        data.MiddleInitial = inputMiddleInitial.Text
        data.LastName = inputLastName.Text
        data.MaidenName = inputMaidenName.Text
        data.ClassYear = inputClassYear.Text
        data.PhoneNumber = inputPhoneNumber.Text
        data.EmailAddress = inputEmailAddress.Text
        data.Subject = inputSubjectDropdown.SelectedItem.Text
        data.Comments = inputComments.Text

        data.PageName = API.Pages.CurrentPage.Name
        data.SiteName = hfSiteName.Value
        data.DateSubmitted = Date.Now()
        data.ConstituentLookupId = API.Users.CurrentUser.LookupID

        list.Add(data)

        Return list
    End Function
    Private Sub RenderSuccessMessage()
        Try
            Dim htmlControl As New BBNCExtensions.ServerControls.HtmlDisplay
            Dim mergeData As ContactRequestDataProvider

            htmlControl.StorageHTML = MyContent.SuccessMessageStorageHTML
            If htmlControl.StorageHTML IsNot Nothing Then
                mergeData = New ContactRequestDataProvider(inputFirstName.Text, _
                                                           inputLastName.Text, _
                                                           inputSubjectDropdown.SelectedItem.Text, _
                                                           inputComments.Text, _
                                                           inputMiddleInitial.Text, _
                                                           inputMaidenName.Text, _
                                                           inputClassYear.Text, _
                                                           inputPhoneNumber.Text, _
                                                           inputEmailAddress.Text)
                Dim mergeDatas() As BBNCExtensions.API.Merge.MergeData = {DirectCast(mergeData, BBNCExtensions.API.Merge.MergeData)}

                htmlControl.MergeData = mergeDatas
                htmlControl.DataBind()

                Me.phSuccessMessage.Controls.Add(htmlControl)

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Function SendEmailNotification(mergeData As List(Of AdminMergeData)) As Boolean
        Dim result As Boolean = True
        Dim template As New EmailTemplate(Integer.Parse(hfEmailTemplateId.Value))
        For Each item In MyContent.EmailRecipients
            Dim email As New EMail(template)
            email.Save()

            Dim name = String.Empty
            Dim emailAddress = String.Empty
            Dim dataToSend = mergeData(0)

            If dataToSend IsNot Nothing Then
                name = item.DisplayName
                dataToSend.RecipientDisplayName = name
                emailAddress = item.EmailAddress
            End If

            If Not String.IsNullOrWhiteSpace(emailAddress) Then

                Dim provider() As AdminMergeFieldsData = {New AdminMergeFieldsData(mergeData.ToArray())}

                email.Subject = Replace(email.Subject, "[Subject]", inputSubjectDropdown.SelectedItem.Text)
                email.ReplyAddress = mergeData(0).EmailAddress

                Try
                    email.Send(emailAddress, name, Nothing, Nothing, provider, Me.Page)
                Catch ex As Exception
                    result = False
                End Try
            End If
        Next
        Return result
    End Function

    Private Sub PopulateUserDetails()
        Dim user = BBNCExtensions.API.NetCommunity.Current.Users.CurrentUser

        If user.BackOfficeGuid <> Guid.Empty Then
            PopulateLinkedUserDetails(user.BackOfficeGuid)
        ElseIf user.UserID <> 0 Then
            inputFirstName.Text = user.FirstName
            inputLastName.Text = user.LastName
        End If
    End Sub
    Private Sub PopulateLinkedUserDetails(UserId As Guid)
        Dim view = New ViewForms.Constituent.BBISLinkedUserDetailsViewDataFormData
        Try
            view = ViewForms.Constituent.BBISLinkedUserDetailsViewDataForm.LoadData(Me.API.AppFxWebServiceProvider, UserId.ToString())
            inputFirstName.Text = view.FIRSTNAME
            inputMiddleInitial.Text = view.MIDDLEINITIAL
            inputLastName.Text = view.LASTNAME
            inputMaidenName.Text = view.MAIDENNAME

            If view.PHONENUMBER.Length > 0 Then
                Dim initialPhoneNumber As String = view.PHONENUMBER
                Dim not_num_dash = New Regex("[^0-9-]")
                Dim not_num = New Regex("[^0-9]")
                Dim finalPhoneNumber As String = ""
                Dim numbersOnlyPhoneNumber = not_num.Replace(initialPhoneNumber, String.Empty)
                If numbersOnlyPhoneNumber.Length = 10 Then
                    finalPhoneNumber = numbersOnlyPhoneNumber.Substring(0, 3) + "-" + numbersOnlyPhoneNumber.Substring(2, 3) + "-" + numbersOnlyPhoneNumber.Substring(6, 4)
                Else
                    finalPhoneNumber = not_num_dash.Replace(initialPhoneNumber, String.Empty)
                End If

                inputPhoneNumber.Text = finalPhoneNumber
            End If

            inputEmailAddress.Text = view.EMAILADDRESS
            inputEmailAddressValidate.Text = view.EMAILADDRESS
            inputClassYear.Text = view.CLASSYEAR

        Catch ex As Exception
        End Try
    End Sub
End Class