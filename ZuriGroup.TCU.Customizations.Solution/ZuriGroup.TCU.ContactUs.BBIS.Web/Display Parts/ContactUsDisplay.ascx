<%@ Assembly Name="ZuriGroup.TCU.ContactUs.BBIS.Web" %>
<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ContactUsDisplay.ascx.vb" Inherits="ZuriGroup.TCU.ContactUs.BBIS.Web.ContactUsDisplay" %>
<%@ Import Namespace="ZuriGroup.TCU.ContactUs.BBIS.Web" %>
<%@ Register TagPrefix="bc" Namespace="Blackbaud.Web.Content.Core.Controls" Assembly="Blackbaud.Web.Content.Core" %>
<style>
    .ContactUsFormTable {
        width: 100%;
    }

    .ContactUsFieldTable {
        margin: 0 auto;
        width: 100%;
    }

    .ContactUsFieldCaption {
        padding: 0 .5rem 0 0;
        width: auto;
    }

    .ContactUsFieldControlCell {
        width: auto;
    }

    .ContactUsButtonCell {
        padding: 1rem 0 2rem 0 !important;
        text-align: right;
    }

    .ContactUsFieldMessage {
        display: block;
        padding-bottom: 5px;
        font-size: .8em;
    }

    .ContactUsReCAPTCHAControlCell {
        float: right;
    }

    .ContactUsValidatorSummary {
        color: red;
        font-weight: bold;
    }

        .ContactUsValidatorSummary ul {
            font-weight: normal;
        }
</style>
<asp:HiddenField ID="hfPartTitle" runat="server" />
<asp:HiddenField ID="hfSiteId" runat="server" />
<asp:HiddenField ID="hfSiteName" runat="server" />
<asp:HiddenField ID="hfEmailTemplateId" runat="server" />
<asp:Panel ID="ContactUsWrapper" runat="server">
    <asp:Label ID="lblError" runat="server" Font-Bold="true" ForeColor="red" />
    <asp:MultiView ID="ContactUsMultiView" runat="server" ActiveViewIndex="0">
        <asp:View ID="ContactUsFormView" runat="server">
            <asp:Panel ID="ContactUsFormPanel" runat="server">
                <asp:PlaceHolder ID="phFormHeader" runat="server" Visible="false"></asp:PlaceHolder>
                <table id="tableContactUsForm" class="BBFormTable ContactUsFormTable">
                    <tbody>

                        <tr id="trContactUsValidationSummary">
                            <td>
                                <asp:ValidationSummary ID="validationSummary" runat="server" ValidationGroup="ContactUs" HeaderText="The following issues must be corrected before you can continue" CssClass="BBFormValidatorSummary ContactUsValidatorSummary" />
                            </td>
                        </tr>
                        <tr id="trContactUsFormControls">
                            <td>
                                <table id="tableContactUsFormControls" class="ContactUsFieldTable">
                                    <tbody>
                                        <tr id="trFirstName" runat="server">
                                            <td class="BBFieldCaption ContactUsFieldCaption">
                                                <asp:Label ID="labelFirstName" runat="server" Text="First name" AssociatedControlID="inputFirstName"></asp:Label>
                                                <asp:Label ID="lablFirstNameRequired" runat="server" Text="*" CssClass="BBFormRequiredFieldMarker"></asp:Label>
                                                <asp:RequiredFieldValidator ID="requiredFirstName" runat="server" ControlToValidate="inputFirstName"
                                                    ErrorMessage="First name is required" ValidationGroup="ContactUs" Display="None"></asp:RequiredFieldValidator>
                                            </td>
                                            <td class="BBFieldControlCell ContactUsFieldControlCell">
                                                <asp:TextBox ID="inputFirstName" runat="server" AutoCompleteType="FirstName" CssClass="BBFormTextbox ContactUsTextBox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trMiddleInitial" runat="server" visible="false">
                                            <td class="BBFieldCaption ContactUsFieldCaption">
                                                <asp:Label ID="labelMiddleInitial" runat="server" Text="Middle initial" AssociatedControlID="inputMiddleInitial"></asp:Label>
                                                <asp:Label ID="labelMiddleInitialRequired" runat="server" Text="*" CssClass="BBFormRequiredFieldMarker" Visible="false"></asp:Label>
                                                <asp:RequiredFieldValidator ID="requiredMiddleInitial" runat="server" ControlToValidate="inputMiddleInitial"
                                                    ErrorMessage="Middle initial is required" ValidationGroup="ContactUs" Display="None" Enabled="false"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="validMiddleInitial" runat="server" ControlToValidate="inputMiddleInitial"
                                                    ErrorMessage="Middle initial is too long" ValidationGroup="ContactUs" Display="None"
                                                    ValidationExpression="^[a-zA-Z]{0,1}$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td class="BBFieldControlCell ContactUsFieldControlCell">
                                                <asp:TextBox ID="inputMiddleInitial" runat="server" AutoCompleteType="MiddleName" CssClass="BBFormTextbox ContactUsTextBox ContactUsSmall"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trLastName" runat="server">
                                            <td class="BBFieldCaption ContactUsFieldCaption">
                                                <asp:Label ID="labelLastName" runat="server" Text="Last name" AssociatedControlID="inputLastName"></asp:Label>
                                                <asp:Label ID="labelLastNameRequired" runat="server" Text="*" CssClass="BBFormRequiredFieldMarker"></asp:Label>
                                                <asp:RequiredFieldValidator ID="requiredLastName" runat="server" ControlToValidate="inputLastName"
                                                    ErrorMessage="Last name is required" ValidationGroup="ContactUs" Display="None"></asp:RequiredFieldValidator>
                                            </td>
                                            <td class="BBFieldControlCell ContactUsFieldControlCell">
                                                <asp:TextBox ID="inputLastName" runat="server" AutoCompleteType="LastName" CssClass="BBFormTextbox ContactUsTextBox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trMaidenName" runat="server" visible="false">
                                            <td class="BBFieldCaption ContactUsFieldCaption">
                                                <asp:Label ID="labelMaidenName" runat="server" Text="Maiden name" AssociatedControlID="inputMaidenName"></asp:Label>
                                                <asp:Label ID="labelMaidenNameRequired" runat="server" Text="*" CssClass="BBFormRequiredFieldMarker" Visible="false"></asp:Label>
                                                <asp:RequiredFieldValidator ID="requiredMaidenName" runat="server" ControlToValidate="inputMaidenName"
                                                    ErrorMessage="Maiden name is required" ValidationGroup="ContactUs" Display="None" Enabled="false"></asp:RequiredFieldValidator>
                                            </td>
                                            <td class="BBFieldControlCell ContactUsFieldControlCell">
                                                <asp:TextBox ID="inputMaidenName" runat="server" CssClass="BBFormTextbox ContactUsTextBox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trPhoneNumber" runat="server" visible="false">
                                            <td class="BBFieldCaption ContactUsFieldCaption">
                                                <asp:Label ID="labelPhoneNumber" runat="server" Text="Phone number" AssociatedControlID="inputPhoneNumber"></asp:Label>
                                                <asp:Label ID="labelPhoneNumberRequired" runat="server" Text="*" CssClass="BBFormRequiredFieldMarker" Visible="false"></asp:Label>
                                                <asp:RequiredFieldValidator ID="requiredPhoneNumber" runat="server" ControlToValidate="inputPhoneNumber"
                                                    ErrorMessage="Phone number is required" ValidationGroup="ContactUs" Display="None" Enabled="false"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="validPhoneNumber" runat="server" ControlToValidate="inputPhoneNumber"
                                                    ErrorMessage="Phone number is invalid" ValidationGroup="ContactUs" Display="None"
                                                    ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td class="BBFieldControlCell ContactUsFieldControlCell">
                                                <asp:TextBox ID="inputPhoneNumber" runat="server" AutoCompleteType="HomePhone" CssClass="BBFormTextbox ContactUsTextBox"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr id="trPhoneNumberHint" runat="server" visible="false">
                                            <td></td>
                                            <td class="ContactUsFieldHintCell">
                                                <asp:Label ID="labelPhoneNumberHint" runat="server" Text="(e.g., 555-123-4567)" CssClass="ContactUsFieldMessage"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trEmailAddress" runat="server">
                                            <td class="BBFieldCaption ContactUsFieldCaption">
                                                <asp:Label ID="labelEmailAddress" runat="server" Text="Email address" AssociatedControlID="inputEmailAddress"></asp:Label>
                                                <asp:Label ID="labelEmailAddressRequired" runat="server" Text="*" CssClass="BBFormRequiredFieldMarker"></asp:Label>
                                                <asp:RequiredFieldValidator ID="requiredEmailAddress" runat="server" ControlToValidate="inputEmailAddress"
                                                    ErrorMessage="Email address is required" ValidationGroup="ContactUs" Display="None"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="validEmailAddress" runat="server" ControlToValidate="inputEmailAddress"
                                                    ErrorMessage="Email address is invalid" ValidationGroup="ContactUs" Display="None"
                                                    ValidationExpression="^[ ]*[A-Za-z0-9\-_.'`!#$%&*+/=?^{|}~]+@[A-Za-z0-9][A-Za-z0-9\-]*(\.[A-Za-z0-9][A-Za-z0-9\-]*)*\.[A-Za-z]{2,6}[ ]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td class="BBFieldControlCell ContactUsFieldControlCell">
                                                <asp:TextBox ID="inputEmailAddress" runat="server" AutoCompleteType="Email" CssClass="BBFormTextbox ContactUsTextBox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trEmailAddressValidate" runat="server" visible="false">
                                            <td class="BBFieldCaption ContactUsFieldCaption">
                                                <asp:Label ID="labelEmailAddressValidate" runat="server" Text="Verify email address" AssociatedControlID="inputEmailAddressValidate"></asp:Label>
                                                <asp:Label ID="labelEmailAddressValidateRequired" runat="server" Text="*" CssClass="BBFormRequiredFieldMarker"></asp:Label>
                                                <asp:RequiredFieldValidator ID="requiredEmailAddressValidate" runat="server" ControlToValidate="inputEmailAddressValidate"
                                                    ErrorMessage="Verify email address is required" ValidationGroup="ContactUs" Display="None" Enabled="false"></asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="compareEmailAddressValidate" runat="server" ControlToValidate="inputEmailAddressValidate" ControlToCompare="inputEmailAddress"
                                                    ErrorMessage="Email address and Verify email address do not match" ValidationGroup="ContactUs" Display="None" Enabled="false"></asp:CompareValidator>
                                            </td>
                                            <td class="BBFieldControlCell ContactUsFieldControlCell">
                                                <asp:TextBox ID="inputEmailAddressValidate" runat="server" AutoCompleteType="Email" CssClass="BBFormTextbox ContactUsTextBox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trClassYear" runat="server" visible="false">
                                            <td class="BBFieldCaption ContactUsFieldCaption">
                                                <asp:Label ID="labelClassYear" runat="server" Text="Class year" AssociatedControlID="inputClassYear"></asp:Label>
                                                <asp:Label ID="labelClassYearRequired" runat="server" Text="*" CssClass="BBFormRequiredFieldMarker" Visible="false"></asp:Label>
                                                <asp:RequiredFieldValidator ID="requiredClassYear" runat="server" ControlToValidate="inputClassYear"
                                                    ErrorMessage="Class year is required" ValidationGroup="ContactUs" Display="None" Enabled="false"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="validClassYear" runat="server" ControlToValidate="inputClassYear"
                                                    ErrorMessage="Class year is invalid" ValidationGroup="ContactUs" Display="None"
                                                    ValidationExpression="^\d{4}$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td class="BBFieldControlCell ContactUsFieldControlCell">
                                                <asp:TextBox ID="inputClassYear" runat="server" CssClass="BBFormTextbox ContactUsTextBox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trSubject" runat="server">
                                            <td class="BBFieldCaption ContactUsFieldCaption">
                                                <asp:Label ID="labelSubjectDropdown" runat="server" Text="Subject" AssociatedControlID="inputSubjectDropdown"></asp:Label>
                                                <asp:Label ID="labelSubjectDropdownRequired" runat="server" Text="*" CssClass="BBFormRequiredFieldMarker"></asp:Label>
                                                <asp:RequiredFieldValidator ID="requiredSubject" runat="server" ControlToValidate="inputSubjectDropdown" InitialValue=""
                                                    ErrorMessage="Subject is required" ValidationGroup="ContactUs" Display="None"></asp:RequiredFieldValidator>
                                            </td>
                                            <td class="BBFieldControlCell ContactUsFieldControlCell">
                                                <asp:DropDownList ID="inputSubjectDropdown" runat="server" CssClass="BBFormSelectList ContactUsSelectList"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="trCommentsLabel" runat="server">
                                            <td class="BBFieldCaption ContactUsFieldCaption" colspan="2">
                                                <asp:Label ID="labelComments" runat="server" Text="Enter comments or a description here" AssociatedControlID="inputComments"></asp:Label>
                                                <asp:Label ID="labelCommentsRequired" runat="server" Text="*" CssClass="BBFormRequiredFieldMarker"></asp:Label>
                                                <asp:RequiredFieldValidator ID="requiredComments" runat="server" ControlToValidate="inputComments"
                                                    ErrorMessage="Comment or description is required" ValidationGroup="ContactUs" Display="None"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr id="trComments" runat="server">
                                            <td class="BBFieldControlCell ContactUsFieldControlCell" colspan="2">
                                                <asp:TextBox ID="inputComments" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trReCaptcha" runat="server" visible="false">
                                            <td></td>
                                            <td class="BBFieldControlCell ContactUsReCAPTCHAControlCell">
                                                <asp:Panel ID="panelReCaptchaV2" runat="server" Enabled="false" Visible="false" CssClass="g-recaptcha"></asp:Panel>
                                                <div style="clear: both;"></div>
                                                <asp:CustomValidator ID="validatorReCaptcha" runat="server" ErrorMessage="You must complete the reCAPTCHA challenge"
                                                    ClientValidationFunction="getReCaptchaResponse" OnServerValidate="validatorReCaptcha_ServerValidate"
                                                    ValidationGroup="ContactUs" Display="None" Enabled="false"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr id="trSubmitButton" runat="server">
                                            <td class="BBFormButtonCell ContactUsButtonCell" colspan="2">
                                                <asp:Button ID="SubmitButton" runat="server"
                                                    OnClick="SubmitButton_Click" Text="Submit" ValidationGroup="ContactUs"></asp:Button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </asp:View>
        <asp:View ID="ContactUsSuccessView" runat="server">
            <asp:Panel ID="ContactUsSuccessPanel" runat="server">
                <asp:PlaceHolder ID="phSuccessMessage" runat="server"></asp:PlaceHolder>
            </asp:Panel>
        </asp:View>
        <asp:View ID="ContactUsFatalErrorView" runat="server">
            <asp:Panel ID="ContactUsFatalErrorPanel" runat="server">
                <asp:Label ID="labelFatalError" runat="server"></asp:Label>
            </asp:Panel>
        </asp:View>
    </asp:MultiView>
</asp:Panel>

<script>
    function getReCaptchaResponse(source, arguments) {

        var response = grecaptcha.getResponse();

        if (response.length == 0)
            arguments.IsValid = false
        else
            arguments.IsValid = true
    };
</script>
