﻿Imports BBNCExtensions.API.Transactions

Public Class ContactRequestDataProvider
    Inherits BBNCExtensions.API.Merge.MergeData

    Public FirstName As String
    Public MiddleInitial As String
    Public LastName As String
    Public MaidenName As String
    Public ClassYear As String
    Public PhoneNumber As String
    Public EmailAddress As String
    Public Subject As String
    Public Comments As String

    Public Overloads Overrides Function GetDataForField(fieldId As Integer) As String

        Select Case DirectCast(fieldId, ContactRequestFieldProvider.eField)
            Case ContactRequestFieldProvider.eField.FirstName
                Return FirstName
            Case ContactRequestFieldProvider.eField.MiddleInitial
                Return MiddleInitial
            Case ContactRequestFieldProvider.eField.LastName
                Return LastName
            Case ContactRequestFieldProvider.eField.MaidenName
                Return MaidenName
            Case ContactRequestFieldProvider.eField.ClassYear
                Return ClassYear
            Case ContactRequestFieldProvider.eField.PhoneNumber
                Return PhoneNumber
            Case ContactRequestFieldProvider.eField.EmailAddress
                Return EmailAddress
            Case ContactRequestFieldProvider.eField.Subject
                Return Subject
            Case ContactRequestFieldProvider.eField.Comments
                Return Comments
            Case Else
                Return ""
        End Select
    End Function

    Public Sub New(firstName As String, lastName As String, subject As String, comments As String)
        Me.FirstName = firstName
        Me.LastName = lastName
        Me.Subject = subject
        Me.Comments = comments
    End Sub
    Public Sub New(firstName As String, lastName As String, subject As String, comments As String, middleInitial As String, maidenName As String, classYear As String, phoneNumber As String, emailAddress As String)
        Me.FirstName = firstName
        Me.MiddleInitial = middleInitial
        Me.LastName = lastName
        Me.Subject = subject
        Me.Comments = comments
        Me.MaidenName = maidenName
        Me.ClassYear = classYear
        Me.PhoneNumber = phoneNumber
        Me.EmailAddress = emailAddress
    End Sub
End Class
