﻿Partial Public Class AdminMergeFieldsProvider
    Inherits Blackbaud.Web.Content.Core.Data.BaseFieldProvider

    Public Const FIELD_TYPE = 199123421

    Protected Overrides Function GetFieldData() As DataSet

        If m_fieldData Is Nothing Then
            m_fieldData = New DataSet
            CreateFieldsTable(m_fieldData)

            Dim table As System.Data.DataTable = m_fieldData.Tables.Item("Fields")

            AddField(table, FIELD_TYPE, eAdminField.Category_UserFields)
            AddField(table, FIELD_TYPE, eAdminField.Category_AdminDetails)

            AddField(table, FIELD_TYPE, eAdminField.FirstName, eAdminField.Category_UserFields)
            AddField(table, FIELD_TYPE, eAdminField.MiddleInitial, eAdminField.Category_UserFields)
            AddField(table, FIELD_TYPE, eAdminField.LastName, eAdminField.Category_UserFields)
            AddField(table, FIELD_TYPE, eAdminField.MaidenName, eAdminField.Category_UserFields)
            AddField(table, FIELD_TYPE, eAdminField.ClassYear, eAdminField.Category_UserFields)
            AddField(table, FIELD_TYPE, eAdminField.PhoneNumber, eAdminField.Category_UserFields)
            AddField(table, FIELD_TYPE, eAdminField.EmailAddress, eAdminField.Category_UserFields)
            AddField(table, FIELD_TYPE, eAdminField.Subject, eAdminField.Category_UserFields)
            AddField(table, FIELD_TYPE, eAdminField.Comments, eAdminField.Category_UserFields)

            AddField(table, FIELD_TYPE, eAdminField.RecipientDisplayName, eAdminField.Category_AdminDetails)
            AddField(table, FIELD_TYPE, eAdminField.PageName, eAdminField.Category_AdminDetails)
            AddField(table, FIELD_TYPE, eAdminField.SiteName, eAdminField.Category_AdminDetails)
            AddField(table, FIELD_TYPE, eAdminField.DateSubmitted, eAdminField.Category_AdminDetails)
            AddField(table, FIELD_TYPE, eAdminField.ConstituentLookupId, eAdminField.Category_AdminDetails)

            m_fieldData.Relations.Add("FieldsTree", m_fieldData.Tables("Fields").Columns("FieldId"), m_fieldData.Tables("Fields").Columns("ParentFieldId"))
        End If

        Return m_fieldData
    End Function

    Public Overrides Function GetFieldName(fieldId As Integer, attributeId As Integer, Optional fullyQualified As Boolean = False) As String
        Select Case CType(fieldId, eAdminField)
            Case eAdminField.Category_UserFields
                Return "User fields"
            Case eAdminField.Category_AdminDetails
                Return "Admin details"
            Case eAdminField.FirstName
                Return "First name"
            Case eAdminField.MiddleInitial
                Return "Middle initial"
            Case eAdminField.LastName
                Return "Last name"
            Case eAdminField.MaidenName
                Return "Maiden name"
            Case eAdminField.PhoneNumber
                Return "Phone number"
            Case eAdminField.EmailAddress
                Return "Email address"
            Case eAdminField.ClassYear
                Return "Class year"
            Case eAdminField.Subject
                Return "Subject"
            Case eAdminField.Comments
                Return "Comments"
            Case eAdminField.RecipientDisplayName
                Return "Recipient display name"
            Case eAdminField.PageName
                Return "Page name"
            Case eAdminField.SiteName
                Return "Site name"
            Case eAdminField.DateSubmitted
                Return "Date submitted"
            Case eAdminField.ConstituentLookupId
                Return "Constituent lookup ID"
            Case Else
                Return "???"
        End Select
    End Function
End Class
