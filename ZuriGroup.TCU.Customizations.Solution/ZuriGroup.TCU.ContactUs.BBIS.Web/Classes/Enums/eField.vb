﻿Partial Public Class ContactRequestFieldProvider
    Public Enum eField
        FirstName = 1
        MiddleInitial = 9
        LastName = 2
        MaidenName = 3
        ClassYear = 4
        PhoneNumber = 5
        EmailAddress = 6
        Subject = 7
        Comments = 8

        Category_General = 101
    End Enum
End Class