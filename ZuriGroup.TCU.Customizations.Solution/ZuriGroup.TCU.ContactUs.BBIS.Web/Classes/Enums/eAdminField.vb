﻿Partial Public Class AdminMergeFieldsProvider
    Public Enum eAdminField
        FirstName = 1
        MiddleInitial = 9
        LastName = 2
        MaidenName = 3
        ClassYear = 4
        PhoneNumber = 5
        EmailAddress = 6
        Subject = 7
        Comments = 8

        RecipientDisplayName = 21
        PageName = 22
        SiteName = 23
        DateSubmitted = 24
        ConstituentLookupId = 25

        Category_UserFields = 101
        Category_AdminDetails = 102
    End Enum
End Class