﻿Public Class ContactUsField
    Private _field As String
    Public Property Field() As String
        Get
            Return _field
        End Get
        Set(ByVal value As String)
            _field = value
        End Set
    End Property

    Private _required As Boolean
    Public Property Required() As Boolean
        Get
            Return _required
        End Get
        Set(ByVal value As Boolean)
            _required = value
        End Set
    End Property

    Private _label As String
    Public Property Label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            _label = value
        End Set
    End Property

    Private _included As Boolean
    Public Property Included() As Boolean
        Get
            Return _included
        End Get
        Set(ByVal value As Boolean)
            _included = value
        End Set
    End Property
    Public Sub New()

    End Sub
    Private _requiredEnabled As Boolean
    Public Property RequiredEnabled() As Boolean
        Get
            Return _requiredEnabled
        End Get
        Set(ByVal value As Boolean)
            _requiredEnabled = value
        End Set
    End Property

    Public Sub New(field As String, label As String)
        Me.Field = field
        Me.Label = label
        Me.RequiredEnabled = True
    End Sub

    Public Sub New(field As String, label As String, included As Boolean)
        Me.New(field, label)
        Me.Included = included
    End Sub

    Public Sub New(field As String, label As String, included As Boolean, required As Boolean)
        Me.New(field, label)
        Me.Included = included
        Me.Required = required
    End Sub
End Class
