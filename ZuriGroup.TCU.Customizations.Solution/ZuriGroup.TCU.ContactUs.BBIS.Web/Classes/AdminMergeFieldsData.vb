﻿Public Class AdminMergeFieldsData
    Implements Blackbaud.Web.Content.Core.Data.IDataProvider2

    Private Property Data As AdminMergeData()

    Public Property GetEFieldType As Integer Implements Blackbaud.Web.Content.Core.Data.IDataProvider.GetEFieldType
        Get
            Return AdminMergeFieldsProvider.FIELD_TYPE
        End Get
        Set(value As Integer)
        End Set
    End Property

    Public Sub New(ByVal data As AdminMergeData())
        Me.Data = data
    End Sub

    Public Function GetFieldById(fieldId As Integer, attributeId As Integer) As String Implements Blackbaud.Web.Content.Core.Data.IDataProvider.GetFieldById
        Return GetFieldById(fieldId, attributeId, 0)
    End Function

    Public Function GetFieldById(fieldId As Integer, attributeId As Integer, rowNumber As Integer) As String Implements Blackbaud.Web.Content.Core.Data.IDataProvider.GetFieldById
        Return GetFieldById(fieldId, attributeId, CStr(rowNumber))
    End Function
    Public Function GetFieldById(fieldId As Integer, attributeId As Integer, rowNumber As String) As String Implements Blackbaud.Web.Content.Core.Data.IDataProvider2.GetFieldById

        If 0 = rowNumber.Trim.Length Then
            rowNumber = "0"
        End If

        Dim rowNum() As Integer = Array.ConvertAll(rowNumber.Split(CChar("_")), New Converter(Of String, Integer)(AddressOf Convert.ToInt32))

        Try
            Dim max As Integer = rowNum.Length - 1

            Select Case CType(fieldId, AdminMergeFieldsProvider.eAdminField)
                Case AdminMergeFieldsProvider.eAdminField.FirstName
                    Return Me.Data(rowNum(0)).FirstName
                Case AdminMergeFieldsProvider.eAdminField.MiddleInitial
                    Return Me.Data(rowNum(0)).MiddleInitial
                Case AdminMergeFieldsProvider.eAdminField.LastName
                    Return Me.Data(rowNum(0)).LastName
                Case AdminMergeFieldsProvider.eAdminField.MaidenName
                    Return Me.Data(rowNum(0)).MaidenName
                Case AdminMergeFieldsProvider.eAdminField.PhoneNumber
                    Return Me.Data(rowNum(0)).PhoneNumber
                Case AdminMergeFieldsProvider.eAdminField.EmailAddress
                    Return Me.Data(rowNum(0)).EmailAddress
                Case AdminMergeFieldsProvider.eAdminField.ClassYear
                    Return Me.Data(rowNum(0)).ClassYear
                Case AdminMergeFieldsProvider.eAdminField.Subject
                    Return Me.Data(rowNum(0)).Subject
                Case AdminMergeFieldsProvider.eAdminField.Comments
                    Return Me.Data(rowNum(0)).Comments
                Case AdminMergeFieldsProvider.eAdminField.RecipientDisplayName
                    Return Me.Data(rowNum(0)).RecipientDisplayName
                Case AdminMergeFieldsProvider.eAdminField.PageName
                    Return Me.Data(rowNum(0)).PageName
                Case AdminMergeFieldsProvider.eAdminField.SiteName
                    Return Me.Data(rowNum(0)).SiteName
                Case AdminMergeFieldsProvider.eAdminField.DateSubmitted
                    Return Me.Data(rowNum(0)).DateSubmitted
                Case AdminMergeFieldsProvider.eAdminField.ConstituentLookupId
                    Return Me.Data(rowNum(0)).ConstituentLookupId
            End Select

        Catch ex As Exception
            Throw ex
        End Try
        Return String.Empty
    End Function

#Region "Ignore"
    Public Function GetChildRecords(fieldCategory As Integer, fieldIds As String) As DataTable Implements Blackbaud.Web.Content.Core.Data.IDataProvider.GetChildRecords
        Return Nothing
    End Function


    Public Function GetFieldNameById(fieldId As Integer, attributeId As Integer, Optional fullyQualified As Boolean = False) As String Implements Blackbaud.Web.Content.Core.Data.IDataProvider.GetFieldNameById
        Return Nothing
    End Function

    Public Function GetMultiRowRecordCount(fieldCategory As Integer) As Integer Implements Blackbaud.Web.Content.Core.Data.IDataProvider.GetMultiRowRecordCount

    End Function

    Public Function IsFieldPrivate(fieldId As Integer, attributeId As Integer) As Boolean Implements Blackbaud.Web.Content.Core.Data.IDataProvider.IsFieldPrivate

    End Function

    Public Function NewRecord(fieldCategory As Integer) As Integer Implements Blackbaud.Web.Content.Core.Data.IDataProvider.NewRecord

    End Function

    Public Sub SetFieldById(fieldId As Integer, attributeId As Integer, rowNumber As Integer, value As String) Implements Blackbaud.Web.Content.Core.Data.IDataProvider.SetFieldById

    End Sub

    Public Sub SetFieldById(fieldId As Integer, attributeId As Integer, value As String) Implements Blackbaud.Web.Content.Core.Data.IDataProvider.SetFieldById

    End Sub

    Public Sub SetFieldById(fieldId As Integer, attributeId As Integer, value() As String) Implements Blackbaud.Web.Content.Core.Data.IDataProvider.SetFieldById

    End Sub
#End Region

End Class
