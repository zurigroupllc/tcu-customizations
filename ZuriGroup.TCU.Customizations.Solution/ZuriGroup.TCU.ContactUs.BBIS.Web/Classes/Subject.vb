﻿Public Class Subject
    Private _value As Guid
    Public Property Value() As Guid
        Get
            Return _value
        End Get
        Set(ByVal value As Guid)
            _value = value
        End Set
    End Property
    Private _label As String
    Public Property Label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            _label = value
        End Set
    End Property
    Private _checked As String
    Public Property Checked() As String
        Get
            Return _checked
        End Get
        Set(ByVal value As String)
            _checked = value
        End Set
    End Property
End Class
