﻿Imports BBNCExtensions.API.Transactions

Public Class ContactRequestAdminDataProvider
    Inherits BBNCExtensions.API.Merge.MergeData

    Public FirstName As String
    Public LastName As String
    Public MaidenName As String
    Public ClassYear As String
    Public PhoneNumber As String
    Public EmailAddress As String
    Public Subject As String
    Public Comments As String

    Public PageName As String
    Public SiteName As String
    Public DateSubmitted As String
    Public ConstituentLookupId As String
    Public RecipientDisplayName As String

    Public Overloads Overrides Function GetDataForField(fieldId As Integer) As String

        Select Case DirectCast(fieldId, ContactRequestAdminFieldProvider.CustomMergeFields)
            Case ContactRequestAdminFieldProvider.CustomMergeFields.FirstName
                Return FirstName
            Case ContactRequestAdminFieldProvider.CustomMergeFields.LastName
                Return LastName
            Case ContactRequestAdminFieldProvider.CustomMergeFields.MaidenName
                Return MaidenName
            Case ContactRequestAdminFieldProvider.CustomMergeFields.ClassYear
                Return ClassYear
            Case ContactRequestAdminFieldProvider.CustomMergeFields.PhoneNumber
                Return PhoneNumber
            Case ContactRequestAdminFieldProvider.CustomMergeFields.EmailAddress
                Return EmailAddress
            Case ContactRequestAdminFieldProvider.CustomMergeFields.Subject
                Return Subject
            Case ContactRequestAdminFieldProvider.CustomMergeFields.Comments
                Return Comments
            Case ContactRequestAdminFieldProvider.CustomMergeFields.PageName
                Return PageName
            Case ContactRequestAdminFieldProvider.CustomMergeFields.SiteName
                Return SiteName
            Case ContactRequestAdminFieldProvider.CustomMergeFields.DateSubmitted
                Return DateSubmitted
            Case ContactRequestAdminFieldProvider.CustomMergeFields.ConstituentLookupId
                Return ConstituentLookupId
            Case ContactRequestAdminFieldProvider.CustomMergeFields.RecipientDisplayName
                Return RecipientDisplayName
            Case Else
                Return ""
        End Select
    End Function
    Public Sub New()

    End Sub
    Public Sub New(firstName As String, lastName As String, subject As String, comments As String)
        Me.FirstName = firstName
        Me.LastName = lastName
        Me.Subject = subject
        Me.Comments = comments
    End Sub
    Public Sub New(firstName As String, lastName As String, subject As String, comments As String, maidenName As String, classYear As String, phoneNumber As String, emailAddress As String)
        Me.FirstName = firstName
        Me.LastName = lastName
        Me.Subject = subject
        Me.Comments = comments
        Me.MaidenName = maidenName
        Me.ClassYear = classYear
        Me.PhoneNumber = phoneNumber
        Me.EmailAddress = emailAddress
    End Sub
    Public Sub New(firstName As String, lastName As String, subject As String, comments As String, maidenName As String, classYear As String, phoneNumber As String, emailAddress As String, pageName As String, siteName As String, dateSubmitted As String, constituentLookupId As String, recipientDisplayName As String)
        Me.FirstName = firstName
        Me.LastName = lastName
        Me.Subject = subject
        Me.Comments = comments
        Me.MaidenName = maidenName
        Me.ClassYear = classYear
        Me.PhoneNumber = phoneNumber
        Me.EmailAddress = emailAddress

        Me.PageName = pageName
        Me.SiteName = siteName
        Me.DateSubmitted = dateSubmitted
        Me.ConstituentLookupId = constituentLookupId
        Me.RecipientDisplayName = recipientDisplayName
    End Sub
End Class
