﻿Imports BBNCExtensions.API.Merge

Partial Public Class ContactRequestFieldProvider
    Inherits MergeFields

    Public Sub New()
        With Me.Fields
            .Add(eField.Category_General, New MergeField(eField.Category_General, "General"))

            .Add(eField.FirstName, New MergeField(eField.FirstName, "First name", eField.Category_General, ToolTip:="First name"))
            .Add(eField.MiddleInitial, New MergeField(eField.MiddleInitial, "Middle initial", eField.Category_General, ToolTip:="Middle initial"))
            .Add(eField.LastName, New MergeField(eField.LastName, "Last name", eField.Category_General, ToolTip:="Last name"))
            .Add(eField.MaidenName, New MergeField(eField.MaidenName, "Maiden name", eField.Category_General, ToolTip:="Maiden name"))
            .Add(eField.ClassYear, New MergeField(eField.ClassYear, "Class year", eField.Category_General, ToolTip:="Class year"))
            .Add(eField.PhoneNumber, New MergeField(eField.PhoneNumber, "Phone number", eField.Category_General, ToolTip:="Phone number"))
            .Add(eField.EmailAddress, New MergeField(eField.EmailAddress, "Email address", eField.Category_General, ToolTip:="Email address"))
            .Add(eField.Subject, New MergeField(eField.Subject, "Subject", eField.Category_General, ToolTip:="Subject"))
            .Add(eField.Comments, New MergeField(eField.Comments, "Comments", eField.Category_General, ToolTip:="Comments"))

        End With
    End Sub

End Class
