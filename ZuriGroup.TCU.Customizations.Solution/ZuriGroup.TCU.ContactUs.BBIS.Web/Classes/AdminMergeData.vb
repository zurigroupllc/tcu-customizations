﻿<Serializable()>
Public Class AdminMergeData
    Public Property FirstName As String
    Public Property MiddleInitial As String
    Public Property LastName As String
    Public Property MaidenName As String
    Public Property ClassYear As String
    Public Property PhoneNumber As String
    Public Property EmailAddress As String
    Public Property Subject As String
    Public Property Comments As String
    Public Property PageName As String
    Public Property SiteName As String
    Public Property DateSubmitted As String
    Public Property ConstituentLookupId As String
    Public Property RecipientDisplayName As String
End Class
