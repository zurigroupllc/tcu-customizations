﻿Imports BBNCExtensions.API.Merge

Public Class ContactRequestAdminFieldProvider
    Inherits MergeFields

    Public Enum CustomMergeFields
        FirstName
        LastName
        MaidenName
        ClassYear
        PhoneNumber
        EmailAddress
        Subject
        Comments

        PageName
        SiteName
        DateSubmitted
        ConstituentLookupId

        Category_UserFields
        Category_AdminDetails

        RecipientDisplayName
    End Enum

    Public Sub New()
        With Me.Fields
            .Add(CustomMergeFields.Category_UserFields, New MergeField(CustomMergeFields.Category_UserFields, "User fields"))
            .Add(CustomMergeFields.Category_AdminDetails, New MergeField(CustomMergeFields.Category_AdminDetails, "Admin details"))

            .Add(CustomMergeFields.FirstName, New MergeField(CustomMergeFields.FirstName, "First name", CustomMergeFields.Category_UserFields, ToolTip:="First name"))
            .Add(CustomMergeFields.LastName, New MergeField(CustomMergeFields.LastName, "Last name", CustomMergeFields.Category_UserFields, ToolTip:="Last name"))
            .Add(CustomMergeFields.MaidenName, New MergeField(CustomMergeFields.MaidenName, "Maiden name", CustomMergeFields.Category_UserFields, ToolTip:="Maiden name"))
            .Add(CustomMergeFields.ClassYear, New MergeField(CustomMergeFields.ClassYear, "Class year", CustomMergeFields.Category_UserFields, ToolTip:="Class year"))
            .Add(CustomMergeFields.PhoneNumber, New MergeField(CustomMergeFields.PhoneNumber, "Phone number", CustomMergeFields.Category_UserFields, ToolTip:="Phone number"))
            .Add(CustomMergeFields.EmailAddress, New MergeField(CustomMergeFields.EmailAddress, "Email address", CustomMergeFields.Category_UserFields, ToolTip:="Email address"))
            .Add(CustomMergeFields.Subject, New MergeField(CustomMergeFields.Subject, "Subject", CustomMergeFields.Category_UserFields, ToolTip:="Subject"))
            .Add(CustomMergeFields.Comments, New MergeField(CustomMergeFields.Comments, "Comments", CustomMergeFields.Category_UserFields, ToolTip:="Comments"))

            .Add(CustomMergeFields.PageName, New MergeField(CustomMergeFields.PageName, "Page name", CustomMergeFields.Category_AdminDetails, ToolTip:="Page name"))
            .Add(CustomMergeFields.SiteName, New MergeField(CustomMergeFields.SiteName, "Site name", CustomMergeFields.Category_AdminDetails, ToolTip:="Site name"))
            .Add(CustomMergeFields.DateSubmitted, New MergeField(CustomMergeFields.DateSubmitted, "Date submitted", CustomMergeFields.Category_AdminDetails, ToolTip:="Date submitted"))
            .Add(CustomMergeFields.ConstituentLookupId, New MergeField(CustomMergeFields.ConstituentLookupId, "Constituent lookup ID", CustomMergeFields.Category_AdminDetails, ToolTip:="Constituent lookup ID"))
            .Add(CustomMergeFields.RecipientDisplayName, New MergeField(CustomMergeFields.RecipientDisplayName, "Recipient display name", CustomMergeFields.Category_AdminDetails, ToolTip:="Recipient display name"))

        End With
    End Sub

End Class
