REM THIS FILE SHOULD BE KEPT FREE OF LOCAL FILE PATHS.  COPY, RENAME AND EDIT POSTBUILD.USER.TXT TO POSTBUILD.USER.BAT TO DO LOCAL XCOPY POST BUILD COMMANDS 

echo copy the html and js files to the deploy folder in the customizations folder for the project...
xcopy "%~dp0htmlforms\custom\*.*"  "%~dp0..\Deploy\bbappfx\vroot\browser\htmlforms\custom\"  /e /y /r

echo copy the display parts to the deploy folder in the customizations folder for the project...
xcopy "%~dp0display parts\*.ascx"  "%~dp0..\Deploy\netcommunity\custom\display parts\"  /e /y /r

echo copy the edit parts to the deploy folder in the customizations folder for the project...
xcopy "%~dp0edit parts\*.ascx"  "%~dp0..\Deploy\netcommunity\custom\edit parts\"  /e /y /r

echo copy the js parts to the deploy folder in the customizations folder for the project...
xcopy "%~dp0client\scripts\custom\*.js"  "%~dp0..\Deploy\netcommunity\client\scripts\custom\"  /e /y /r

echo copy the css parts to the deploy folder in the customizations folder for the project...
xcopy "%~dp0client\styles\custom\*.css"   "%~dp0..\Deploy\netcommunity\client\styles\custom\"  /e /y /r


if not exist "%~dp0postbuild.user.bat" goto end
echo running postbuild.user.bat
"%~dp0postbuild.user.bat"

:end
echo postbuild.user.bat not found, skipped.
echo copy, rename and edit postbuild.user.txt to postbuild.user.bat to do local xcopy post build commands