<%@ Control Language="vb" AutoEventWireup="false" Codebehind="HtmlEditorControl2.ascx.vb"
    Inherits="Blackbaud.Web.Content.Portal.HtmlEditorControl2" TargetSchema="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" %>
<%@ Register TagPrefix="ce" Namespace="CuteEditor" Assembly="CuteEditor" %>
<%@ Register TagPrefix="cc" Namespace="Blackbaud.Web.Content.Core.Controls" Assembly="Blackbaud.Web.Content.Core" %>
<%@ Register TagPrefix="uc" TagName="ModalPhotoPicker" Src="~/admin/Common/ModalPhotoPicker.ascx" %>
<%@ Register TagPrefix="uc2" TagName="UserLinkPicker" Src="Common/UserLinkPicker.ascx" %>
<%@ Register TagPrefix="ajx" Namespace="AjaxControlToolKit" Assembly="AjaxControlToolKit" %>
<%@ Register TagPrefix="uc3" TagName="ConditionalContentViewAs" Src="~/admin/Email/ConditionalContent/ConditionalContentViewAs.ascx" %>

<style type="text/css">
    .mceStatusbar div
    {
        max-width: 700px;
        overflow: hidden;
    }
</style>

<div id="divTinyMCEWrapper">
	<appfx:HtmlEditor ID="tinyMce" ScriptPath="~/Client/Scripts/tiny_mce"   runat="server">
		<Settings>
			<appfx:HtmlEditorSetting Name="width" Value="'100%'" />
			<appfx:HtmlEditorSetting Name="height" Value="'350px'" />
			<appfx:HtmlEditorSetting Name="plugins" Value="'powerpaste,advlist,lists,image_tools,media,xhtmlxtras,directionality,layer,preview,insertdatetime,table,fullscreen,print,inlinepopups,activitytrigger,advhr,searchreplace,pagebreak,style,visualchars,bblinks,bbimages,advimage,bbstylesheet,spellchecker,bbinlinepopups,bblanguage'" />
			<appfx:HtmlEditorSetting Name="extended_valid_elements" Value="'iframe[align<bottom?left?middle?right?top|class|frameborder|height|id|longdesc|marginheight|marginwidth|name|scrolling<auto?no?yes|src|style|title|width],style[dir&lt;ltr?rtl|lang|media|title|type],#conditionblock[data-conditionblockid|conditionblockid],img[align&lt;bottom?left?middle?right?top|alt|border|class|dir&lt;ltr?rtl|height|hspace|id|ismap&lt;ismap|lang|longdesc|name|onclick|ondblclick|onkeydown|onkeypress|onkeyup|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|src|style|title|usemap|vspace|width|data-runat|runat|data-template_pane|data-existingpane|template_pane|data-paneid|paneid|data-panedescription|data-panetype|panetype|data-hasMultiples|hasMultiples|data-fieldcat|fieldcat|data-fieldid|fieldid|data-attribid|attribid|data-fieldName|fieldName|data-fieldType|fieldType|data-htmlEncode|htmlEncode|data-searchable|searchable|data-isLoop|isLoop|data-stripspantags|stripspantags|data-rowNumber|rowNumber|data-counterFID|counterFID],table[align<center?left?right|bgcolor|border|cellpadding|cellspacing|class|dir<ltr?rtl|frame|height|id|lang|onclick|ondblclick|onkeydown|onkeypress|onkeyup|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|rules|style|summary|title|width|background],section[*],article[*],aside[*],hgroup[*],header[*],footer[*],nav[*],figure[*],figcaption[*],video[*],audio[*],track[*],embed[*],mark[*],progress[*],meter[*],time[*],data[*],dialog[*],ruby[*],rt[*],rp[*],bdi[*],wbr[*],canvas[*],command[*],details[*],datalist[*],keygen[*],output[*],marquee[*],main[*]'" />
			<appfx:HtmlEditorSetting Name="custom_elements" Value="'conditionblock'" />
			<appfx:HtmlEditorSetting Name="valid_children" Value="'+body[style]'" />
			<appfx:HtmlEditorSetting Name="file_browser_callback" Value="'BBImagePropertiesBrowser'" />
			<appfx:HtmlEditorSetting Name="accessibility_warnings" Value="false" />
			<appfx:HtmlEditorSetting Name="skin" Value="'default'" />
			<appfx:HtmlEditorSetting Name="noneditable_leave_contenteditable" Value="true" />
			<appfx:HtmlEditorSetting Name="plugin_preview_width" Value="600" />
			<appfx:HtmlEditorSetting Name="cleanup_on_startup" Value="true" />
			<appfx:HtmlEditorSetting Name="font_size_style_values" Value="'8pt,10pt,12pt,14pt,18pt,24pt,36pt,xx-small,x-small,small,medium,large,x-large,xx-large'" />
			<appfx:HtmlEditorSetting Name="spellchecker_languages" Value="'+English=US,English (UK)=UK'" />
			<appfx:HtmlEditorSetting Name="removeformat_selector" Value="'span,b,strong,em,i,font,u,strike'" />
			<appfx:HtmlEditorSetting Name="tinymce" Value="tinymce" />
			<appfx:HtmlEditorSetting Name="plugin_preview_inline" Value="0" />
			<appfx:HtmlEditorSetting Name="relative_urls" Value="false" />
			<appfx:HtmlEditorSetting Name="onSubmitEvents" Value="{}" />
			<appfx:HtmlEditorSetting Name="convert_urls" Value="false" />
			<appfx:HtmlEditorSetting Name="theme_advanced_path" Value="true" />
			<appfx:HtmlEditorSetting Name="theme_advanced_statusbar_location" Value="'bottom'" />
			<appfx:HtmlEditorSetting Name="theme_advanced_font_sizes" Value="'1,2,3,4,5,6,7,XX-Small,X-Small,Small,Medium,Large,X-Large,XX-Large'" />
			<appfx:HtmlEditorSetting Name="theme_advanced_resizing" Value="true" />
			<appfx:HtmlEditorSetting Name="theme_advanced_resize_horizontal" Value="false" />	
			<appfx:HtmlEditorSetting Name="theme_advanced_source_editor_height" Value="480" />
			<appfx:HtmlEditorSetting Name="theme_ribbon_path" Value="true" />
			<appfx:HtmlEditorSetting Name="theme_ribbon_statusbar_location" Value="'bottom'" />
			<appfx:HtmlEditorSetting Name="theme_ribbon_font_sizes" Value="'1,2,3,4,5,6,7,XX-Small,X-Small,Small,Medium,Large,X-Large,XX-Large'" />
			<appfx:HtmlEditorSetting Name="theme_ribbon_resizing" Value="true" />
			<appfx:HtmlEditorSetting Name="theme_ribbon_resize_horizontal" Value="false" />	
			<appfx:HtmlEditorSetting Name="theme_ribbon_source_editor_height" Value="480" />			
			<appfx:HtmlEditorSetting Name="activitytrigger_callback" Value="function(ed){BLACKBAUD.netcommunity.CallWebServiceMethod(ed.settings.webmethods_url, 'Ping', null, null);}" />
			<appfx:HtmlEditorSetting Name="activitytrigger_maxwaitduration" Value="600000" />
			<appfx:HtmlEditorSetting Name="activitytrigger_minwaitduration" Value="60000" />
            <appfx:HtmlEditorSetting Name="powerpaste_word_import" Value="'inline_styles'" />
            <appfx:HtmlEditorSetting Name="powerpaste_html_import" Value="'inline_styles'" />
            <appfx:HtmlEditorSetting Name="paste_retain_style_properties" Value="'all'" />
            <appfx:HtmlEditorSetting Name="paste_strip_class_attributes" Value="'none'" />
		</Settings>
	</appfx:HtmlEditor>
</div>

<asp:HiddenField runat="server" ID="hdMergeFields" />
<asp:HiddenField runat="server" ID="hdStylesheetOptions" Value="" />
<asp:Button runat="server" ID="btnUpdateStyleSheetOptions" style="display:none;" CausesValidation="false" /> 
