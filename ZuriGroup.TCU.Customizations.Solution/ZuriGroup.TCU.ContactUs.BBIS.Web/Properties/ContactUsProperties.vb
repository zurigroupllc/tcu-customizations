Public Class ContactUsProperties
    Public Subjects As List(Of Guid)
    Public RequiredFields As List(Of String)
    Public IncludedFields As List(Of String)

    Public UseReCaptcha As Boolean
    Public RequireRegisteredReCaptcha As Boolean
    Public ReCaptchaV2SiteKey As String
    Public ReCaptchaV2SecretKey As String

    Public DisplayAlternativeSuccessPage As Boolean

    Public ConfirmationPageId As Integer

    Public SuccessMessageStorageHTML As String
    Public FormHeaderStorageHTML As String

    Public EmailRecipients As System.Collections.Generic.List(Of EmailRecipient)

    Public PartIsNotNew As Boolean
End Class