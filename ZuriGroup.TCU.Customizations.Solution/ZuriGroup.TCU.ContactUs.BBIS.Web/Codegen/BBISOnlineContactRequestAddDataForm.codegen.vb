﻿Option Infer On
Option Strict Off

Imports bbAppFxWebAPI = Blackbaud.AppFx.WebAPI



Namespace AddForms

    Namespace [OnlineContactRequest]
    
		

		

		    ''' <summary>
    ''' Provides WebApi access to the "BBIS Online Contact Request Add Data Form" catalog feature.  A data form for adding BBIS online contact requests
    ''' </summary>
<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
        Public NotInheritable Class [BBISOnlineContactRequestAddDataForm]

            Private Sub New()
            End Sub

            Private Shared ReadOnly _specId As Guid = New Guid("a3f13156-d0d5-4e0a-af00-d4dde42d81eb")

			''' <summary>
			''' The Spec ID value for the "BBIS Online Contact Request Add Data Form" AddForm
			''' </summary>
            Public Shared ReadOnly Property SpecId() As Guid
                Get
                    Return _specId
                End Get
            End Property
 
            Public Shared Function CreateLoadRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider) As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest
                Return CreateLoadRequest(provider, Nothing)
            End Function

            Public Shared Function CreateLoadRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal options as bbAppFxWebAPI.LoadRequestOptions) As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest
                Return Blackbaud.AppFx.WebAPI.DataFormServices.CreateDataFormLoadRequest(provider, _specId, options)
            End Function

            ''' <summary>
            ''' Returns an instance of BBISOnlineContactRequestAddDataFormData with default data form fields populated.
            ''' </summary>
            ''' <returns>An instance of BBISOnlineContactRequestAddDataFormData with the form field properties that have defaults populated with those default values.</returns>
            ''' <remarks>This function will make a web request to the AppFx WebService DataFormLoad method to obtain the default data form field values.</remarks>
            Public Shared Function LoadData(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider) As BBISOnlineContactRequestAddDataFormData
                Return LoadDataWithOptions(provider, Nothing)
            End Function

            ''' <summary>
            ''' Returns an instance of BBISOnlineContactRequestAddDataFormData with default data form fields populated.
            ''' </summary>
            ''' <returns>An instance of BBISOnlineContactRequestAddDataFormData with the form field properties that have defaults populated with those default values.</returns>
            ''' <remarks>This function will make a web request to the AppFx WebService DataFormLoad method to obtain the default data form field values.</remarks>
            Public Shared Function LoadDataWithOptions(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal options As bbAppFxWebAPI.LoadRequestOptions) As BBISOnlineContactRequestAddDataFormData

				

                Dim request = CreateLoadRequest(provider, options)

				
				
				

                Return LoadData(provider, request)

            End Function

            ''' <summary>
            ''' Returns an instance of BBISOnlineContactRequestAddDataFormData with default data form fields populated.
            ''' </summary>
            ''' <returns>An instance of BBISOnlineContactRequestAddDataFormData with the form field properties that have defaults populated with those default values.</returns>
            ''' <remarks>This function will make a web request to the AppFx WebService DataFormLoad method to obtain the default data form field values.</remarks>
            Public Shared Function LoadData(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal request As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest) As BBISOnlineContactRequestAddDataFormData
                Return bbAppFxWebAPI.DataFormServices.GetFormData(Of BBISOnlineContactRequestAddDataFormData)(provider, request)
            End Function
   
        End Class

#Region "Data Class"

        ''' <summary>
        ''' Represents the data form field values in the "BBIS Online Contact Request Add Data Form" data form.
        ''' </summary>
		<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
	    Public NotInheritable Class BBISOnlineContactRequestAddDataFormData
			Inherits bbAppFxWebAPI.AddFormData
        
#Region "Constructors"
        
			Public Sub New()
				Mybase.New()
			End Sub

			Friend Sub New(ByVal reply as bbAppFxWebAPI.ServiceProxy.DataFormLoadReply,request as bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest)
				Mybase.New()					
				If (reply IsNot Nothing) AndAlso (reply.DataFormItem IsNot Nothing) Then
					Me.SetValues(reply.DataFormItem)
				End If
			End Sub
        
        	Public Sub New(ByVal dfi as Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
				Mybase.New()					
				Me.SetValues(dfi)
			End Sub
			
			Public Sub New(ByVal dataFormItemXml As String)
                MyBase.New()
                Me.SetValuesFromDataFormItem(dataFormItemXml)
            End Sub

#End Region
        
#Region "Form Field Properties"

Private [_CONSTITUENTID] As Nullable(of System.Guid)
''' <summary>
''' Constituent ID
''' </summary>
Public Property [CONSTITUENTID] As Nullable(of System.Guid)
    Get
        Return Me.[_CONSTITUENTID]
    End Get
    Set
        Me.[_CONSTITUENTID] = value 
    End Set
End Property

Private [_CLIENTUSERSID] As Nullable(of Integer)
''' <summary>
''' Client users ID
''' </summary>
Public Property [CLIENTUSERSID] As Nullable(of Integer)
    Get
        Return Me.[_CLIENTUSERSID]
    End Get
    Set
        Me.[_CLIENTUSERSID] = value 
    End Set
End Property

Private [_FIRSTNAME] As String
''' <summary>
''' First name
''' </summary>
Public Property [FIRSTNAME] As String
    Get
        Return Me.[_FIRSTNAME]
    End Get
    Set
        Me.[_FIRSTNAME] = value 
    End Set
End Property

Private [_MIDDLEINITIAL] As String
''' <summary>
''' Middle initial
''' </summary>
Public Property [MIDDLEINITIAL] As String
    Get
        Return Me.[_MIDDLEINITIAL]
    End Get
    Set
        Me.[_MIDDLEINITIAL] = value 
    End Set
End Property

Private [_LASTNAME] As String
''' <summary>
''' Last name
''' </summary>
Public Property [LASTNAME] As String
    Get
        Return Me.[_LASTNAME]
    End Get
    Set
        Me.[_LASTNAME] = value 
    End Set
End Property

Private [_BIRTHLASTNAME] As String
''' <summary>
''' Birth last name
''' </summary>
Public Property [BIRTHLASTNAME] As String
    Get
        Return Me.[_BIRTHLASTNAME]
    End Get
    Set
        Me.[_BIRTHLASTNAME] = value 
    End Set
End Property

Private [_PHONENUMBER] As String
''' <summary>
''' Phone number
''' </summary>
Public Property [PHONENUMBER] As String
    Get
        Return Me.[_PHONENUMBER]
    End Get
    Set
        Me.[_PHONENUMBER] = value 
    End Set
End Property

Private [_EMAILADDRESS] As String
''' <summary>
''' Email address
''' </summary>
Public Property [EMAILADDRESS] As String
    Get
        Return Me.[_EMAILADDRESS]
    End Get
    Set
        Me.[_EMAILADDRESS] = value 
    End Set
End Property

Private [_CLASSYEAR] As String
''' <summary>
''' Class year
''' </summary>
Public Property [CLASSYEAR] As String
    Get
        Return Me.[_CLASSYEAR]
    End Get
    Set
        Me.[_CLASSYEAR] = value 
    End Set
End Property

Private [_ONLINECONTACTREQUESTSUBJECTCODEID] As Nullable(of System.Guid)
''' <summary>
''' Subject ID
''' </summary>
Public Property [ONLINECONTACTREQUESTSUBJECTCODEID] As Nullable(of System.Guid)
    Get
        Return Me.[_ONLINECONTACTREQUESTSUBJECTCODEID]
    End Get
    Set
        Me.[_ONLINECONTACTREQUESTSUBJECTCODEID] = value 
    End Set
End Property

Private [_MESSAGE] As String
''' <summary>
''' Message
''' </summary>
Public Property [MESSAGE] As String
    Get
        Return Me.[_MESSAGE]
    End Get
    Set
        Me.[_MESSAGE] = value 
    End Set
End Property

Private [_BBISPAGEID] As Nullable(of Integer)
''' <summary>
''' BBIS Page ID
''' </summary>
Public Property [BBISPAGEID] As Nullable(of Integer)
    Get
        Return Me.[_BBISPAGEID]
    End Get
    Set
        Me.[_BBISPAGEID] = value 
    End Set
End Property

        

#End Region

            Public Overrides ReadOnly Property DataFormInstanceID() As System.Guid
                Get
                    Return AddForms.[OnlineContactRequest].BBISOnlineContactRequestAddDataForm.SpecId
                End Get
            End Property

            Public Overrides Function ToDataFormItem() As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem
                Return Me.BuildDataFormItemForSave()
            End Function
    
			Friend Sub SetValues(ByVal dfi As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
				
				If dfi Is Nothing Then Exit Sub
	            
				
Dim value As Object = Nothing
Dim dfiFieldValue As Blackbaud.AppFx.XmlTypes.DataForms.DataFormFieldValue = Nothing
Dim stringFieldValue As String = Nothing

Dim guidFieldValue As System.Guid
guidFieldValue = System.Guid.Empty
If dfi.TryGetValueForPropertyAssignment("CONSTITUENTID", guidFieldValue) Then
Me.[CONSTITUENTID] = guidFieldValue
End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("CLIENTUSERSID", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
If TypeOf value Is String Then 
Me.[CLIENTUSERSID] = Blackbaud.AppFx.DataListUtility.DataListStringValueToInt(value)

Else
Me.[CLIENTUSERSID] = value
End If
End If

	End If

End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("FIRSTNAME", stringFieldValue) Then
Me.[FIRSTNAME] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("MIDDLEINITIAL", stringFieldValue) Then
Me.[MIDDLEINITIAL] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("LASTNAME", stringFieldValue) Then
Me.[LASTNAME] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("BIRTHLASTNAME", stringFieldValue) Then
Me.[BIRTHLASTNAME] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("PHONENUMBER", stringFieldValue) Then
Me.[PHONENUMBER] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("EMAILADDRESS", stringFieldValue) Then
Me.[EMAILADDRESS] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("CLASSYEAR", stringFieldValue) Then
Me.[CLASSYEAR] = stringFieldValue
End If

guidFieldValue = System.Guid.Empty
If dfi.TryGetValueForPropertyAssignment("ONLINECONTACTREQUESTSUBJECTCODEID", guidFieldValue) Then
Me.[ONLINECONTACTREQUESTSUBJECTCODEID] = guidFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("MESSAGE", stringFieldValue) Then
Me.[MESSAGE] = stringFieldValue
End If

value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("BBISPAGEID", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
If TypeOf value Is String Then 
Me.[BBISPAGEID] = Blackbaud.AppFx.DataListUtility.DataListStringValueToInt(value)

Else
Me.[BBISPAGEID] = value
End If
End If

	End If

End If


	            
			End Sub

			Private Function BuildDataFormItemForSave() As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem
        
				Dim dfi As New Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem	 
	                    
				Dim value As Object = Nothing
value = Me.[CONSTITUENTID]
	dfi.SetValueIfNotNull("CONSTITUENTID",value)
value = Me.[CLIENTUSERSID]
	dfi.SetValueIfNotNull("CLIENTUSERSID",value)
value = Me.[FIRSTNAME]
	dfi.SetValueIfNotNull("FIRSTNAME",value)
value = Me.[MIDDLEINITIAL]
	dfi.SetValueIfNotNull("MIDDLEINITIAL",value)
value = Me.[LASTNAME]
	dfi.SetValueIfNotNull("LASTNAME",value)
value = Me.[BIRTHLASTNAME]
	dfi.SetValueIfNotNull("BIRTHLASTNAME",value)
value = Me.[PHONENUMBER]
	dfi.SetValueIfNotNull("PHONENUMBER",value)
value = Me.[EMAILADDRESS]
	dfi.SetValueIfNotNull("EMAILADDRESS",value)
value = Me.[CLASSYEAR]
	dfi.SetValueIfNotNull("CLASSYEAR",value)
value = Me.[ONLINECONTACTREQUESTSUBJECTCODEID]
	dfi.SetValueIfNotNull("ONLINECONTACTREQUESTSUBJECTCODEID",value)
value = Me.[MESSAGE]
	dfi.SetValueIfNotNull("MESSAGE",value)
value = Me.[BBISPAGEID]
	dfi.SetValueIfNotNull("BBISPAGEID",value)

	    
				Return dfi	    
	        
			End Function
			
			Public Overrides Sub SetValuesFromDataFormItem(ByVal dataFormItem As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
                Me.SetValues(dataFormItem)
            End Sub
            
			
	 
		End Class
    
#End Region
    
    End Namespace

End Namespace

