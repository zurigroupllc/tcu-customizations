﻿Option Infer On
Option Strict Off

Imports bbAppFxWebAPI = Blackbaud.AppFx.WebAPI



Namespace ViewForms

    Namespace [Constituent]
    
		

		    ''' <summary>
    ''' Provides WebApi access to the "BBIS Linked User Details View Data Form" catalog feature.  A data form for viewing bbis linked user details
    ''' </summary>
<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
        Public NotInheritable Class [BBISLinkedUserDetailsViewDataForm]

            Private Sub New()
            End Sub

            Private Shared ReadOnly _specId As Guid = New Guid("450d5320-42e7-4ba9-b02c-07da21f1decb")

			''' <summary>
			''' The Spec ID value for the "BBIS Linked User Details View Data Form" ViewForm
			''' </summary>
            Public Shared ReadOnly Property SpecId() As Guid
                Get
                    Return _specId
                End Get
            End Property
 
            Public Shared Function CreateRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider) As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest
                Return CreateRequest(provider, Nothing)
            End Function
            
            Public Shared Function CreateRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal options As bbAppFxWebAPI.LoadRequestOptions) As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest
                Return Blackbaud.AppFx.WebAPI.DataFormServices.CreateDataFormLoadRequest(provider, _specId, options)
            End Function

            Public Shared Function LoadData(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal recordId As String) As BBISLinkedUserDetailsViewDataFormData
				Return LoadDataWithOptions(provider, recordId, Nothing)
            End Function

            Public Shared Function LoadDataWithOptions(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal recordId As String, ByVal options As bbAppFxWebAPI.LoadRequestOptions) As BBISLinkedUserDetailsViewDataFormData

				bbAppFxWebAPI.DataFormServices.ValidateRecordId(recordId)

                Dim request = CreateRequest(provider, options)

				
				
				request.RecordID = recordId

                Return LoadData(provider, request)

            End Function

            Public Shared Function LoadData(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal request As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest) As BBISLinkedUserDetailsViewDataFormData
                Return bbAppFxWebAPI.DataFormServices.GetFormData(Of BBISLinkedUserDetailsViewDataFormData)(provider, request)
            End Function

        End Class

#Region "Data Class"
	
	    ''' <summary>
        ''' Represents the data form field values in the "BBIS Linked User Details View Data Form" data form.
        ''' </summary>
		<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
	    Public NotInheritable Class BBISLinkedUserDetailsViewDataFormData
			Inherits bbAppFxWebAPI.ViewFormData
		
			Private [_FIRSTNAME] As String
''' <summary>
''' First name
''' </summary>
Public Property [FIRSTNAME] As String
    Get
        Return Me.[_FIRSTNAME]
    End Get
    Set
        Me.[_FIRSTNAME] = value 
    End Set
End Property

Private [_MIDDLEINITIAL] As String
''' <summary>
''' Middle initial
''' </summary>
Public Property [MIDDLEINITIAL] As String
    Get
        Return Me.[_MIDDLEINITIAL]
    End Get
    Set
        Me.[_MIDDLEINITIAL] = value 
    End Set
End Property

Private [_LASTNAME] As String
''' <summary>
''' Last name
''' </summary>
Public Property [LASTNAME] As String
    Get
        Return Me.[_LASTNAME]
    End Get
    Set
        Me.[_LASTNAME] = value 
    End Set
End Property

Private [_MAIDENNAME] As String
''' <summary>
''' Maiden name
''' </summary>
Public Property [MAIDENNAME] As String
    Get
        Return Me.[_MAIDENNAME]
    End Get
    Set
        Me.[_MAIDENNAME] = value 
    End Set
End Property

Private [_PHONENUMBER] As String
''' <summary>
''' Phone number
''' </summary>
Public Property [PHONENUMBER] As String
    Get
        Return Me.[_PHONENUMBER]
    End Get
    Set
        Me.[_PHONENUMBER] = value 
    End Set
End Property

Private [_EMAILADDRESS] As String
''' <summary>
''' Email address
''' </summary>
Public Property [EMAILADDRESS] As String
    Get
        Return Me.[_EMAILADDRESS]
    End Get
    Set
        Me.[_EMAILADDRESS] = value 
    End Set
End Property

Private [_CLASSYEAR] As String
''' <summary>
''' Class year
''' </summary>
Public Property [CLASSYEAR] As String
    Get
        Return Me.[_CLASSYEAR]
    End Get
    Set
        Me.[_CLASSYEAR] = value 
    End Set
End Property


	        
			Public Sub New()
				MyBase.New()
			End Sub

			Public Sub New(ByVal reply as bbAppFxWebAPI.ServiceProxy.DataFormLoadReply)
				If (reply IsNot Nothing) AndAlso (reply.DataFormItem IsNot Nothing) Then
					Me.SetValues(reply.DataFormItem)
				End If
			End Sub
        
			Public Sub New(ByVal dataFormItemXml As String)
				MyBase.New()
				Me.SetValuesFromDataFormItem(dataFormItemXml)
			End Sub
        
			Public Overrides ReadOnly Property DataFormInstanceID() As System.Guid
				Get
					Return ViewForms.[Constituent].BBISLinkedUserDetailsViewDataForm.SpecId
				End Get
			End Property
	    
			Friend Sub SetValues(ByVal dfi As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
	            
				
Dim value As Object = Nothing
Dim dfiFieldValue As Blackbaud.AppFx.XmlTypes.DataForms.DataFormFieldValue = Nothing
Dim stringFieldValue As String = Nothing

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("FIRSTNAME", stringFieldValue) Then
Me.[FIRSTNAME] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("MIDDLEINITIAL", stringFieldValue) Then
Me.[MIDDLEINITIAL] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("LASTNAME", stringFieldValue) Then
Me.[LASTNAME] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("MAIDENNAME", stringFieldValue) Then
Me.[MAIDENNAME] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("PHONENUMBER", stringFieldValue) Then
Me.[PHONENUMBER] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("EMAILADDRESS", stringFieldValue) Then
Me.[EMAILADDRESS] = stringFieldValue
End If

stringFieldValue = Nothing
If dfi.TryGetValueForPropertyAssignment("CLASSYEAR", stringFieldValue) Then
Me.[CLASSYEAR] = stringFieldValue
End If


	            
			End Sub
				
			Public Overrides Sub SetValuesFromDataFormItem(ByVal dataFormItem As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
				Me.SetValues(dataFormItem)
			End Sub

			
	 
		End Class

#End Region
    
    End Namespace

End Namespace



