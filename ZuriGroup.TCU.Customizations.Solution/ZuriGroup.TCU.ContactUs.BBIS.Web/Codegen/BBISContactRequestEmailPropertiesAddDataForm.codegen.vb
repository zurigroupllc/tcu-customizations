﻿Option Infer On
Option Strict Off

Imports bbAppFxWebAPI = Blackbaud.AppFx.WebAPI



Namespace AddForms

    Namespace [ContactRequestEmailProperties]
    
		

		

		    ''' <summary>
    ''' Provides WebApi access to the "BBIS Contact Request Email Properties Add Data Form" catalog feature.  A data form for adding bbis contact request email properties records
    ''' </summary>
<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
        Public NotInheritable Class [BBISContactRequestEmailPropertiesAddDataForm]

            Private Sub New()
            End Sub

            Private Shared ReadOnly _specId As Guid = New Guid("8b021cd1-7b8f-4675-8e35-16080722478c")

			''' <summary>
			''' The Spec ID value for the "BBIS Contact Request Email Properties Add Data Form" AddForm
			''' </summary>
            Public Shared ReadOnly Property SpecId() As Guid
                Get
                    Return _specId
                End Get
            End Property
 
            Public Shared Function CreateLoadRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider) As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest
                Return CreateLoadRequest(provider, Nothing)
            End Function

            Public Shared Function CreateLoadRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal options as bbAppFxWebAPI.LoadRequestOptions) As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest
                Return Blackbaud.AppFx.WebAPI.DataFormServices.CreateDataFormLoadRequest(provider, _specId, options)
            End Function

            ''' <summary>
            ''' Returns an instance of BBISContactRequestEmailPropertiesAddDataFormData with default data form fields populated.
            ''' </summary>
            ''' <returns>An instance of BBISContactRequestEmailPropertiesAddDataFormData with the form field properties that have defaults populated with those default values.</returns>
            ''' <remarks>This function will make a web request to the AppFx WebService DataFormLoad method to obtain the default data form field values.</remarks>
            Public Shared Function LoadData(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal contextRecordId As String) As BBISContactRequestEmailPropertiesAddDataFormData
                Return LoadDataWithOptions(provider, contextRecordId, Nothing)
            End Function

            ''' <summary>
            ''' Returns an instance of BBISContactRequestEmailPropertiesAddDataFormData with default data form fields populated.
            ''' </summary>
            ''' <returns>An instance of BBISContactRequestEmailPropertiesAddDataFormData with the form field properties that have defaults populated with those default values.</returns>
            ''' <remarks>This function will make a web request to the AppFx WebService DataFormLoad method to obtain the default data form field values.</remarks>
            Public Shared Function LoadDataWithOptions(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal contextRecordId As String, ByVal options As bbAppFxWebAPI.LoadRequestOptions) As BBISContactRequestEmailPropertiesAddDataFormData

				

                Dim request = CreateLoadRequest(provider, options)

				
				
				 request.ContextRecordId = contextRecordId

                Return LoadData(provider, request)

            End Function

            ''' <summary>
            ''' Returns an instance of BBISContactRequestEmailPropertiesAddDataFormData with default data form fields populated.
            ''' </summary>
            ''' <returns>An instance of BBISContactRequestEmailPropertiesAddDataFormData with the form field properties that have defaults populated with those default values.</returns>
            ''' <remarks>This function will make a web request to the AppFx WebService DataFormLoad method to obtain the default data form field values.</remarks>
            Public Shared Function LoadData(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal request As bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest) As BBISContactRequestEmailPropertiesAddDataFormData
                Return bbAppFxWebAPI.DataFormServices.GetFormData(Of BBISContactRequestEmailPropertiesAddDataFormData)(provider, request)
            End Function
   
        End Class

#Region "Data Class"

        ''' <summary>
        ''' Represents the data form field values in the "BBIS Contact Request Email Properties Add Data Form" data form.
        ''' </summary>
		<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
	    Public NotInheritable Class BBISContactRequestEmailPropertiesAddDataFormData
			Inherits bbAppFxWebAPI.AddFormData
        
#Region "Constructors"
        
			Public Sub New()
				Mybase.New()
			End Sub

			Friend Sub New(ByVal reply as bbAppFxWebAPI.ServiceProxy.DataFormLoadReply,request as bbAppFxWebAPI.ServiceProxy.DataFormLoadRequest)
				Mybase.New()					
				If (reply IsNot Nothing) AndAlso (reply.DataFormItem IsNot Nothing) Then
					Me.SetValues(reply.DataFormItem)
				End If
			End Sub
        
        	Public Sub New(ByVal dfi as Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
				Mybase.New()					
				Me.SetValues(dfi)
			End Sub
			
			Public Sub New(ByVal dataFormItemXml As String)
                MyBase.New()
                Me.SetValuesFromDataFormItem(dataFormItemXml)
            End Sub

#End Region
        
#Region "Form Field Properties"

Private [_EMAILTEMPLATEID] As Nullable(of Integer)
''' <summary>
''' Email template ID
''' </summary>
Public Property [EMAILTEMPLATEID] As Nullable(of Integer)
    Get
        Return Me.[_EMAILTEMPLATEID]
    End Get
    Set
        Me.[_EMAILTEMPLATEID] = value 
    End Set
End Property

        

#End Region

            Public Overrides ReadOnly Property DataFormInstanceID() As System.Guid
                Get
                    Return AddForms.[ContactRequestEmailProperties].BBISContactRequestEmailPropertiesAddDataForm.SpecId
                End Get
            End Property

            Public Overrides Function ToDataFormItem() As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem
                Return Me.BuildDataFormItemForSave()
            End Function
    
			Friend Sub SetValues(ByVal dfi As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
				
				If dfi Is Nothing Then Exit Sub
	            
				
Dim value As Object = Nothing
Dim dfiFieldValue As Blackbaud.AppFx.XmlTypes.DataForms.DataFormFieldValue = Nothing
value = Nothing
dfiFieldValue = Nothing
If dfi.TryGetValue("EMAILTEMPLATEID", dfiFieldValue) Then
	If dfiFieldValue IsNot Nothing Then
	value = dfiFieldValue.Value
If (value IsNot Nothing) AndAlso (value IsNot System.DBNull.Value) Then
If TypeOf value Is String Then 
Me.[EMAILTEMPLATEID] = Blackbaud.AppFx.DataListUtility.DataListStringValueToInt(value)

Else
Me.[EMAILTEMPLATEID] = value
End If
End If

	End If

End If


	            
			End Sub

			Private Function BuildDataFormItemForSave() As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem
        
				Dim dfi As New Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem	 
	                    
				Dim value As Object = Nothing
value = Me.[EMAILTEMPLATEID]
	dfi.SetValueIfNotNull("EMAILTEMPLATEID",value)

	    
				Return dfi	    
	        
			End Function
			
			Public Overrides Sub SetValuesFromDataFormItem(ByVal dataFormItem As Blackbaud.AppFx.XmlTypes.DataForms.DataFormItem)
                Me.SetValues(dataFormItem)
            End Sub
            
			
	 
		End Class
    
#End Region
    
    End Namespace

End Namespace

