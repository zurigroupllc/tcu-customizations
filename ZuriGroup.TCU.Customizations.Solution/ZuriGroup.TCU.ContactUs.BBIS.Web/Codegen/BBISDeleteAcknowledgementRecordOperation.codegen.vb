﻿Option Infer On
Option Strict Off

Imports bbAppFxWebAPI = Blackbaud.AppFx.WebAPI



Namespace RecordOperations

	Namespace [EmailTemplate]
			
			'<@ENUMS@>

    ''' <summary>
    ''' Provides WebApi access to the "BBIS Delete Acknowledgement Record Operation" catalog feature.  Delete acknowledgement upon part deletion
    ''' </summary>
<System.CodeDom.Compiler.GeneratedCodeAttribute("BBMetalWeb", "2011.8.2.0")> _
		Public NotInheritable Class [BBISDeleteAcknowledgementRecordOperation]

			Private Sub New()
			End Sub

			Private Shared _specId As Guid = New Guid("0fd4753e-ecc9-427c-b1fe-26490f73fba5")

			''' <summary>
			''' The Spec ID value for the "BBIS Delete Acknowledgement Record Operation" record operation
			''' </summary>
			Public Shared ReadOnly Property SpecId() As Guid
				Get
					Return _specId
				End Get
			End Property
			
			Public Shared Function CreateRequest(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider) As bbAppFxWebAPI.ServiceProxy.RecordOperationPerformRequest
                Return bbAppFxWebAPI.RecordOpServices.CreateRecordOperationPerformRequest(provider, [BBISDeleteAcknowledgementRecordOperation].SpecId)
            End Function			

			Public Shared Sub ExecuteOperation(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider , ByVal recordID As String)
			
				Dim request = CreateRequest(provider)
				
				request.ID = recordID
				
				ExecuteOperation(provider, request)
			End Sub
			
 #If 0 Then    			
			Public Shared Sub ExecuteOperation(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider , ByVal recordID As String )
				Dim request = CreateRequest(provider)
				
				request.ID = recordID
			
				 
			
				ExecuteOperation(provider, request)
			End Sub
 #End If			
			
			Public Shared Sub ExecuteOperation(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider, ByVal request As bbAppFxWebAPI.ServiceProxy.RecordOperationPerformRequest)
				bbAppFxWebAPI.RecordOpServices.ExecuteOperation(provider, request)
			End Sub
			
			Public Shared Function GetPrompt(ByVal provider As bbAppFxWebAPI.AppFxWebServiceProvider , ByVal recordID As String) As String
			
				Dim request = bbAppFxWebAPI.RecordOpServices.CreateGetPromptRequest(provider, [BBISDeleteAcknowledgementRecordOperation].SpecId)
				
				request.ID = recordID
			
				Return bbAppFxWebAPI.RecordOpServices.GetPrompt(provider, request)
			End Function
			
		End Class
		


	End Namespace
	
End Namespace


