Public Class ProfileUpdateHistoryDataListUIModel

    Private Sub ProfileUpdateHistoryDataListUIModel_Loaded(ByVal sender As Object, ByVal e As Blackbaud.AppFx.UIModeling.Core.LoadedEventArgs) Handles Me.Loaded
        UpdateDateRange()
    End Sub

    Private Sub _dateUpdated_ValueChanged(sender As Object, e As ValueChangedEventArgs) Handles _dateupdated.ValueChanged
        UpdateDateRange()
    End Sub

    Private Sub ProfileUpdateHistoryDataListUIModel_DefaultValuesLoaded(ByVal sender As Object, ByVal e As Blackbaud.AppFx.UIModeling.Core.DefaultValuesLoadedEventArgs) Handles Me.DefaultValuesLoaded
        Me.CONSTITUENTID.Value = Guid.Parse(ContextRecordId)
        Me.SITEID.ResetDataSource()
        Me.PAGEID.ResetDataSource()
    End Sub

    Private Sub UpdateDateRange()
        If Me.DATEUPDATED.Value = DATEUPDATEDS.SpecificDate Then
            STARTDATE.Required = True
            ENDDATE.Required = True
            STARTDATE.Value = Nothing
            ENDDATE.Value = Nothing
        ElseIf DATEUPDATED.Value = DATEUPDATEDS.AllDates Then
            STARTDATE.Required = False
            ENDDATE.Required = False
            STARTDATE.Value = Nothing
            ENDDATE.Value = Nothing
        ElseIf DATEUPDATED.Value = DATEUPDATEDS.Last30Days Then
            STARTDATE.Value = DateTime.Today.AddDays(-30)
            ENDDATE.Value = DateTime.Today
        ElseIf DATEUPDATED.Value = DATEUPDATEDS.Last60Days Then
            STARTDATE.Value = DateTime.Today.AddDays(-60)
            ENDDATE.Value = DateTime.Today
        ElseIf DATEUPDATED.Value = DATEUPDATEDS.Last90Days Then
            STARTDATE.Value = DateTime.Today.AddDays(-90)
            ENDDATE.Value = DateTime.Today
        ElseIf DATEUPDATED.Value = DATEUPDATEDS.Last6Months Then
            STARTDATE.Value = DateSerial(Today.Year, Today.Month - 6, 1)
            ENDDATE.Value = DateTime.Today
        ElseIf DATEUPDATED.Value = DATEUPDATEDS.Last12Months Then
            STARTDATE.Value = DateSerial(Today.Year - 1, Today.Month, Today.Day)
            ENDDATE.Value = DateTime.Today
        ElseIf DATEUPDATED.Value = DATEUPDATEDS.Last5Years Then
            STARTDATE.Value = DateSerial(Today.Year - 5, Today.Month, Today.Day)
            ENDDATE.Value = DateTime.Today
        End If
        If DATEUPDATED.Value <> DATEUPDATEDS.SpecificDate Then
            STARTDATE.Enabled = False
            ENDDATE.Enabled = False
        Else
            STARTDATE.Enabled = True
            ENDDATE.Enabled = True
        End If
    End Sub

End Class