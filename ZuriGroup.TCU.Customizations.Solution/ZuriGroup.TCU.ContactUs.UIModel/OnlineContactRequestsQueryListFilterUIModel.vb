Public Class OnlineContactRequestsQueryListFilterUIModel

    Private Sub OnlineContactRequestsQueryListFilterUIModel_Loaded(ByVal sender As Object, ByVal e As Blackbaud.AppFx.UIModeling.Core.LoadedEventArgs) Handles Me.Loaded
        UpdateDateRange()
    End Sub

    Private Sub _submittedon_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _submittedon.ValueChanged
        UpdateDateRange()
    End Sub

    Private Sub UpdateDateRange()

        STARTDATE.Enabled = False
        ENDDATE.Enabled = False

        Select Case Me.SUBMITTEDON.Value
            Case SUBMITTEDONS.SpecificDate
                STARTDATE.Enabled = True
                ENDDATE.Enabled = True
                STARTDATE.Required = True
                ENDDATE.Required = True
                STARTDATE.Value = Nothing
                ENDDATE.Value = Nothing
            Case SUBMITTEDONS.AllDates
                STARTDATE.Required = False
                ENDDATE.Required = False
                STARTDATE.Value = Nothing
                ENDDATE.Value = Nothing
            Case SUBMITTEDONS.Last30Days
                STARTDATE.Value = DateTime.Today.AddDays(-30)
                ENDDATE.Value = DateTime.Today
            Case SUBMITTEDONS.Last60Days
                STARTDATE.Value = DateTime.Today.AddDays(-60)
                ENDDATE.Value = DateTime.Today
            Case SUBMITTEDONS.Last90Days
                STARTDATE.Value = DateTime.Today.AddDays(-90)
                ENDDATE.Value = DateTime.Today
            Case SUBMITTEDONS.Last6Months
                STARTDATE.Value = DateSerial(Today.Year, Today.Month - 6, 1)
                ENDDATE.Value = DateTime.Today
            Case SUBMITTEDONS.Last12Months
                STARTDATE.Value = DateSerial(Today.Year - 1, Today.Month, Today.Day)
                ENDDATE.Value = DateTime.Today
            Case SUBMITTEDONS.Last5Years
                STARTDATE.Value = DateSerial(Today.Year - 5, Today.Month, Today.Day)
                ENDDATE.Value = DateTime.Today
        End Select

    End Sub

End Class