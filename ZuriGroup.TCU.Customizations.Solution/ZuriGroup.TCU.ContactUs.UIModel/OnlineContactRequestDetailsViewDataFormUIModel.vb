Public Class OnlineContactRequestDetailsViewDataFormUIModel

    Private Sub OnlineContactRequestDetailsViewDataFormUIModel_Loaded(ByVal sender As Object, ByVal e As Blackbaud.AppFx.UIModeling.Core.LoadedEventArgs) Handles Me.Loaded
        UpdateProcessedImage()
        UpdateLinkedImage()
        UpdateRegisteredImage()
    End Sub

    Private Sub UpdateProcessedImage()
        If PROCESSED.Value Then
            Me.PROCESSEDIMAGE.ValueDisplayStyle = ValueDisplayStyle.GoodImageOnly
        End If
    End Sub

    Private Sub UpdateLinkedImage()
        If LINKED.Value Then
            Me.LINKEDIMAGE.ValueDisplayStyle = ValueDisplayStyle.GoodImageOnly
        End If
    End Sub

    Private Sub UpdateRegisteredImage()
        If REGISTERED.Value Then
            Me.REGISTEREDIMAGE.ValueDisplayStyle = ValueDisplayStyle.GoodImageOnly
        End If
    End Sub
End Class