﻿Option Strict On
Option Explicit On
Option Infer On

'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by BBUIModelLibrary
'     Version:  4.0.158.0
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------
''' <summary>
''' Represents the UI model for the 'Constituent Group Add Form Greek Involvement Extension' data form
''' </summary>
<Global.Blackbaud.AppFx.UIModeling.Core.DataFormUIModelMetadata(Global.Blackbaud.AppFx.UIModeling.Core.DataFormMode.Add, "9d2bd34a-aa1f-4ab6-b071-35bda2b6fe0b", "30800ebc-f90a-41b3-9884-19696b0b2f74", "Constituent", "Constituent")>
Partial Public Class [ConstituentGroupAddFormGreekInvolvementExtensionUIModel]
	Inherits Global.Blackbaud.AppFx.UIModeling.Core.DataFormUIModel

#Region "Extensibility methods"

    Partial Private Sub OnCreated()
    End Sub

#End Region

    Private WithEvents _field1 As Global.Blackbaud.AppFx.UIModeling.Core.StringField

	<System.CodeDom.Compiler.GeneratedCodeAttribute("BBUIModelLibrary", "4.0.158.0")> _
    Public Sub New()
        MyBase.New()

        _field1 = New Global.Blackbaud.AppFx.UIModeling.Core.StringField

        MyBase.Mode = Global.Blackbaud.AppFx.UIModeling.Core.DataFormMode.Add
        MyBase.DataFormTemplateId = New System.Guid("9d2bd34a-aa1f-4ab6-b071-35bda2b6fe0b")
        MyBase.DataFormInstanceId = New System.Guid("30800ebc-f90a-41b3-9884-19696b0b2f74")
        MyBase.RecordType = "Constituent"
        MyBase.ContextRecordType = "Constituent"
        MyBase.ExtensionTabCaption = "Extension"

        '
        '_field1
        '
        _field1.Name = "FIELD1"
        _field1.Caption = "Field 1"
        _field1.Visible = False
        _field1.MaxLength = 10
        Me.Fields.Add(_field1)

		OnCreated()

    End Sub
    
    ''' <summary>
    ''' Field 1
    ''' </summary>
    <System.ComponentModel.Description("Field 1")> _
    <System.CodeDom.Compiler.GeneratedCodeAttribute("BBUIModelLibrary", "4.0.158.0")> _
    Public ReadOnly Property [FIELD1]() As Global.Blackbaud.AppFx.UIModeling.Core.StringField
        Get
            Return _field1
        End Get
    End Property
    
End Class
