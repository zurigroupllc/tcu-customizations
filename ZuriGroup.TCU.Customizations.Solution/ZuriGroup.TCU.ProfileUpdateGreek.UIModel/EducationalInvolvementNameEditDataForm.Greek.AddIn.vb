Partial Public NotInheritable Class EducationalInvolvementNameEditDataFormAddIn

    Private Sub OnInit()
        If EDUCATIONALINVOLVEMENTTYPE.Value = "Greek Involvement" Or EDUCATIONALINVOLVEMENTTYPE.Value = "Student Government" Then
            For Each item In EDUCATIONALINVOLVEMENTNAMES.Value
                If Not IsEditable(item.ID.Value) Then
                    item.NAME.Enabled = False
                End If
            Next
        End If

    End Sub

    Private Sub DELETENAMEACTION_InvokingAction(sender As Object, e As InvokingActionEventArgs) Handles DELETENAMEACTION.InvokingAction
        If Me.EDUCATIONALINVOLVEMENTNAMES.SelectedItems.Count = 1 AndAlso Me.IsEditable(Me.EDUCATIONALINVOLVEMENTNAMES.SelectedItems.Item(0).ID.Value) = False Then
            Throw New System.Data.ConstraintException("Unable to delete entry because is linked to a constituent.")
        End If
    End Sub

    Private Function IsEditable(id As Guid) As Boolean
        Dim editable As Boolean = True
        Try
            Using connection As SqlClient.SqlConnection = Me.HostModel.GetRequestContext.OpenAppDBConnection()
                Using command As SqlClient.SqlCommand = connection.CreateCommand()
                    command.CommandText = "select dbo.USR_UFN_ISEDUCATIONALINVOLVEMENTNAMEEDITABLE(@ID)"
                    command.Parameters.AddWithValue("@ID", id)
                    editable = command.ExecuteScalar()
                End Using
            End Using
        Catch ex As Exception
            Throw New ArgumentException(ex.Message)
        End Try
        Return editable
    End Function

End Class

